@extends('front.layout.main')

@section('content')
@include("front.partner._dashboard_banner")
<!-- ============================================-->
<!-- <section> begin ============================-->
<section class="pt-6 bg-gray">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-12 py-6">
                @include("front.partner._dashboard_navigation")

                <div class="w-100 floatleft">
                    <div class="container rounded bg-white mt-5 mb-5 box-shadow-1">
                        <div class="row">

                            <div class="col-md-12 border-right">
                                <div class="p-3 py-5">

                                    @foreach($bookings as $booking)
                                        <div class="py-4">
                                            <div class="row">

                                                <div class="col-12 col-md-8">
                                                    <div class="box-shadow-1 p-3 h-100">

                                                        <div class="row">
                                                            <div class="col-md-3 border-right">
                                                                <div class="d-flex flex-column align-items-center text-center">
                                                                    <img class="rounded-circle" width="150px" height="150px" src="{{ $booking->patient->avatar_url }}" alt="{{ $booking->patient->name }}">
                                                                    <span></span>
                                                                </div>
                                                            </div>

                                                            <div class="col-12 col-md-9 border-right">
                                                                <div class="p-3">

                                                                    <div class="mb-4">
                                                                        <div class="row mb-3">
                                                                            <div class="col-12 col-md-4 label-detail">
                                                                                <label class="fw-bold">Nama Pasien</label>
                                                                            </div>
                                                                            <div class="col-12 col-md-8">
                                                                                <span>{{ $booking->patient->name }}</span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row mb-3">
                                                                            <div class="col-12 col-md-4 label-detail">
                                                                                <label class="fw-bold">Nama Layanan</label>
                                                                            </div>
                                                                            <div class="col-12 col-md-8">
                                                                                <span>{{ $booking->medical_service->name }}</span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row mb-3">
                                                                            <div class="col-12 col-md-4 label-detail">
                                                                                <label class="fw-bold">Tanggal Layanan</label>
                                                                            </div>
                                                                            <div class="col-12 col-md-8">
                                                                                <span>{{ $booking->created_at->format('d F Y') }}</span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row mb-3">
                                                                            <div class="col-12 col-md-4 label-detail">
                                                                                <label class="fw-bold">Alamat</label>
                                                                            </div>
                                                                            <div class="col-12 col-md-8">
                                                                                <span>{{ $booking->patient->address }}</span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row mb-3">
                                                                            <div class="col-12 col-md-4">
                                                                                <label class="fw-bold"></label>
                                                                            </div>
                                                                            <div class="col-12 col-md-8">
                                                                                <a
                                                                                    href="javascript:void(0);"
                                                                                    data-bs-toggle="modal"
                                                                                    data-bs-target="#ModalMap"
                                                                                    data-id="{{ $booking->id }}"
                                                                                    data-url="{{ route('front.partner.orders.map', $booking->id) }}"
                                                                                    class="btn btn-orange btn-small my-1"
                                                                                >Lihat Alamat di Peta</a>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                                <div class="col-12 col-md-4">
                                                    <div class="box-shadow-1 p-3 h-100">

                                                        <div class="w-100 text-center">
                                                            <h4 class="fw-bold">No. Layanan : {{ $booking->booking_number }}</h4>
                                                            <div class="w-100 floatleft my-4">
                                                                @if($booking->status->is(\App\Enums\BookingStatus::COMPLETED()))
                                                                    <i class="far fa-check-circle fa-3x text-primary"></i>
                                                                    <h5 class="mt-2 fw-bold text-primary">Selesai</h5>
                                                                @else
                                                                    <i class="far fa-times-circle fa-3x text-danger"></i>
                                                                    <h5 class="mt-2 fw-bold text-danger">Dibatalkan</h5>
                                                                @endif
                                                            </div>
                                                            <!-- <a
                                                                href="javascript:void(0);"
                                                                data-bs-toggle="modal"
                                                                data-bs-target="#bookingDetail"
                                                                data-url="{{ route('front.partner.orders.show', $booking->id) }}"
                                                                class="btn btn-green-old btn-medium my-1">
                                                                Lihat Detail
                                                            </a> -->
                                                        </div>

                                                    </div>
                                                </div>


                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>

                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
<!-- <section> close ============================-->
<!-- ============================================-->



<!-- Modal -->
<div class="modal fade" id="ModalMap" tabindex="-1" aria-labelledby="ModalMapLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="ModalMapLabel">Lokasi - No. Layanan : <span class="booking-number"></span></h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">Loading...</div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="bookingDetail" tabindex="-1" aria-labelledby="bookingDetailLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"></h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">Loading...</div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    <script type="text/javascript">
        window.partner_order = true;
    </script>
@endsection
