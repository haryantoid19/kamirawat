<div class="row">
    <div class="col-12 col-md-3 border-right">
        <div class="d-flex flex-column align-items-center text-center p-3">
            <img class="rounded-circle" height="150px" width="150px" src="{{ $booking->patient->avatar_url }}" alt="{{ $booking->patient->name }}">
            <span> </span>
        </div>
    </div>

    <div class="col-12 col-md-9 border-right">
        <div class="p-3">

            <div class="mb-4">
                <div class="row mb-3">
                    <div class="col-12 col-md-4 label-detail">
                        <label class="fw-bold">Nama Pasien</label>
                    </div>
                    <div class="col-12 col-md-8">
                        <span>{{ $booking->patient->name }}</span>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-12 col-md-4 label-detail">
                        <label class="fw-bold">Nama Layanan</label>
                    </div>
                    <div class="col-12 col-md-8">
                        <span>{{ $booking->medical_service->name }}</span>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-12 col-md-4 label-detail">
                        <label class="fw-bold">Tanggal Layanan</label>
                    </div>
                    <div class="col-12 col-md-8">
                        <span>{{ $booking->created_at->format('d F Y') }}</span>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-12 col-md-4 label-detail">
                        <label class="fw-bold">Alamat</label>
                    </div>
                    <div class="col-12 col-md-8">
                        <span>{!! nl2br($booking->patient->address) !!}</span>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>


<div class="w-100">

    <div class="callout large invoice-container">
        <table class="invoice">
            <tr class="details">
                <td colspan="2">
                    <table>
                        <thead>
                        <tr>
                            <th class="desc">Layanan</th>
                            <th class="amt">Subtotal</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($additional_services as $additional_service)
                            <tr class="item">
                                <td class="desc">{{ $additional_service->name }}</td>
                                <td class="amt">@rupiah($additional_service->price)</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr class="totals">
                <td></td>
                <td>
                    <table>
                        <tr class="subtotal">
                            <td class="num">Subtotal</td>
                            <td class="num">@rupiah($total)</td>
                        </tr>
                        <tr class="total">
                            <td>Total</td>
                            <td>@rupiah($total)</td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
</div>
