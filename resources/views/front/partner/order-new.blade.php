@extends('front.layout.main')

@section('content')
@include("front.partner._dashboard_banner")
    <!-- ============================================-->
    <!-- <section> begin ============================-->
    <section class="pt-6 bg-gray">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-12 py-6">
                    @include("front.partner._dashboard_navigation")

                    <div class="w-100 floatleft">
                        <div class="container rounded bg-white mt-5 mb-5 box-shadow-1">
                            <div class="row">

                                <div class="col-md-12 border-right">
                                    <div class="p-3 py-5">
                                        @if($message = request()->session()->get('message'))
                                            <div class="alert alert-success" role="alert">
                                                {{ $message }}
                                            </div>
                                        @endif

                                        @if($errors->any())
                                            <div class="alert alert-danger" role="alert">
                                                <ul>
                                                    {!! implode('', $errors->all('<li>:message</li>')) !!}
                                                </ul>
                                            </div>
                                        @endif

                                        @if($bookings->count() === 0)
                                            <div class="alert alert-secondary" role="alert">
                                                Tidak ada pesanan baru
                                            </div>
                                        @endif

                                        @foreach($bookings as $booking)
                                            <div class="py-4">
                                                <div class="row">

                                                    <div class="col-12 col-md-8">
                                                        <div class="box-shadow-1 p-3 h-100">

                                                            <div class="row">
                                                                <div class="col-12 col-md-3 border-right">
                                                                    <div class="d-flex flex-column align-items-center text-center">
                                                                        <img class="rounded-circle" width="150px" height="150px" alt="{{ $booking->patient->name }}" src="{{ $booking->patient->user->avatar_url }}">
                                                                        <span> </span>
                                                                    </div>
                                                                </div>

                                                                <div class="col-12 col-md-9 border-right">
                                                                    <div class="p-3">

                                                                        <div class="mb-4">
                                                                            <div class="row mb-3">
                                                                                <div class="col-12 col-md-4 label-detail">
                                                                                    <label class="fw-bold">Nama Pasien</label>
                                                                                </div>
                                                                                <div class="col-12 col-md-8">
                                                                                    <span>{{ $booking->patient->name }}</span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row mb-3">
                                                                                <div class="col-12 col-md-4 label-detail">
                                                                                    <label class="fw-bold">Nama Layanan</label>
                                                                                </div>
                                                                                <div class="col-12 col-md-8">
                                                                                    <span>{{ $booking->medical_service->name }}</span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row mb-3">
                                                                                <div class="col-12 col-md-4 label-detail">
                                                                                    <label class="fw-bold">Tanggal Layanan</label>
                                                                                </div>
                                                                                <div class="col-12 col-md-8">
                                                                                    <span>{{ $booking->created_at->format('d F Y') }}</span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row mb-3">
                                                                                <div class="col-12 col-md-4 label-detail">
                                                                                    <label class="fw-bold">Alamat</label>
                                                                                </div>
                                                                                <div class="col-12 col-md-8">
                                                                                    <span>{{ $booking->patient->address }}</span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row mb-3">
                                                                                <div class="col-12 col-md-4">
                                                                                    <label class="fw-bold"></label>
                                                                                </div>
                                                                                <div class="col-12 col-md-8">
                                                                                    <a href="javascript:void(0);" data-bs-toggle="modal" data-id="{{ $booking->id }}" data-url="{{ route('front.partner.orders.map', $booking->id) }}" data-bs-target="#ModalMap" class="btn btn-orange btn-small my-1">Lihat Alamat di Peta</a>
                                                                                </div>
                                                                            </div>

                                                                            <hr>

                                                                            <div class="row mb-3">
                                                                                <div class="col-12 col-md-4">
                                                                                    <label class="fw-bold">Keluhan</label>
                                                                                </div>
                                                                                <div class="col-12 col-md-8">{{ $booking->main_symptom }}</div>
                                                                            </div>
                                                                            <div class="row mb-3">
                                                                                <div class="col-12 col-md-4">
                                                                                    <label class="fw-bold">Riwayat penyakit & alergi</label>
                                                                                </div>
                                                                                <div class="col-12 col-md-8">{{ $booking->history_of_disease_allergies }}</div>
                                                                            </div>
                                                                            <div class="row mb-3">
                                                                                <div class="col-12 col-md-4">
                                                                                    <label class="fw-bold">Permintaan Khusus</label>
                                                                                </div>
                                                                                <div class="col-12 col-md-8">{{ $booking->special_request }}</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-md-4">
                                                        <div class="box-shadow-1 p-3 h-100">

                                                            <div class="w-100 text-center">
                                                                <h4 class="fw-bold">No. Layanan : {{ $booking->booking_number }}</h4>
                                                                <!-- <a href="javascript:void(0);" data-bs-toggle="modal" data-bs-target="#showDetailModal" data-id="{{ $booking->id }}" class="btn btn-green-old btn-medium my-1">Lihat Detail Resume Sebelumnya</a> -->

                                                                <hr>

                                                                <a href="javascript:void(0);" data-bs-toggle="modal" data-bs-target="#ConfirmationAcceptOrderModal" data-id="{{ $booking->id }}" class="btn btn-primary btn-medium my-1">Terima Pesanan</a>

                                                                <a href="javascript:void(0);" data-bs-toggle="modal" data-bs-target="#ConfirmationRejectOrderModal" data-id="{{ $booking->id }}" class="btn btn-grey btn-medium my-1">Tolak Pesanan</a>

                                                            </div>

                                                        </div>
                                                    </div>


                                                </div>
                                            </div>
                                        @endforeach

                                    </div>
                                </div>



                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <!-- <section> close ============================-->
    <!-- ============================================-->


    <!-- Modal -->
    <div class="modal fade" id="ModalMap" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"></h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">Loading...</div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="showDetailModal" tabindex="-1" aria-labelledby="showDetailModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"></h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">Loading...</div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Confirmation Modal -->
    <div id="ConfirmationAcceptOrderModal" class="modal fade">
        <div class="modal-dialog modal-confirm">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Terima Pesanan</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <p>Apakah anda yakin menerima pesanan ini?</p>
                </div>
                <form method="POST" id="approve-form" action="{{ route('front.partner.orders.approval', '__id') }}">
                    @method("PUT")
                    @csrf
                    <input type="hidden" name="approval" value="approve" />
                </form>
                <div class="modal-footer justify-content-center">
                    <button type="button" class="btn btn-secondary" id="approve-button">Terima Pesanan</button>
                    <button type="button" class="btn btn-grey" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal passcode -->
    <div class="modal fade" id="showDetailModalPasscode" tabindex="-1" aria-labelledby="showDetailModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Masukan Passcode</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-info">Mintalah passcode pada pasien anda untuk membuka riwayat pemeriksaan pasien di kamirawat.com <br/> Pada Halaman riwayat pesanan patient anda.</div>
                    <div class="mb-3 mt-1">
                        <label for="passcode" class="form-label">Passcode</label>
                        <input type="text" class="form-control" id="passcode" placeholder="passcode">
                        <div class="text-danger small mt-1 error-message" style="display:none;"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="submit-passcode">Submit</button>
                    <button type="button" class="btn btn-grey" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Confirmation Modal -->
    <div id="ConfirmationRejectOrderModal" class="modal fade">
        <div class="modal-dialog modal-confirm">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Tolak Pesanan</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <p>Apakah anda yakin menolak pesanan ini?</p>
                    <form method="POST" id="reject-form" action="{{ route('front.partner.orders.approval', '__id') }}">
                        @method("PUT")
                        @csrf
                        <input type="hidden" name="approval" value="reject" />
                    </form>
                </div>
                <div class="modal-footer justify-content-center">
                    <button type="button" class="btn btn-danger" id="reject-button">Tolak Pesanan</button>
                    <button type="button" class="btn btn-grey" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script type="text/javascript">
        window.partner_order = true;
        window.get_order_url = '{{ route('front.partner.orders.show', '__id') }}';
        window.get_map_url = '{{ route('front.partner.orders.map', '__id') }}';
    </script>
@endsection
