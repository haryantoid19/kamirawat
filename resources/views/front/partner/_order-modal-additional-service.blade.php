<div class="row">
    <div class="col-12 col-md-3 border-right">
        <div class="d-flex flex-column align-items-center text-center p-3">
            <img class="rounded-circle" width="150px" height="150px" src="{{ $booking->patient->avatar_url }}" alt="{{ $booking->patient->name }}">
            <span> </span>
        </div>
    </div>

    <div class="col-12 col-md-9 border-right">
        <div class="p-3">

            <div class="mb-4">
                <div class="row mb-3">
                    <div class="col-12 col-md-4 label-detail">
                        <label class="fw-bold">Nama Pasien</label>
                    </div>
                    <div class="col-12 col-md-8">
                        <span>{{ $booking->patient->name }}</span>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-12 col-md-4 label-detail">
                        <label class="fw-bold">Nama Layanan</label>
                    </div>
                    <div class="col-12 col-md-8">
                        <span>{{ $booking->medical_service->name }}</span>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-12 col-md-4 label-detail">
                        <label class="fw-bold">Tanggal Layanan</label>
                    </div>
                    <div class="col-12 col-md-8">
                        <span>{{ $booking->created_at->format('d F Y') }}</span>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-12 col-md-4 label-detail">
                        <label class="fw-bold">Alamat</label>
                    </div>
                    <div class="col-12 col-md-8">
                        <span>{!! nl2br($booking->patient->address) !!}</span>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>


<div class="w-100">
    <div class="text-center">
        <h4 class="fw-bold">Tambah Layanan</h4>
    </div>
    <div class="pilihan-layanan mb-6">
        <div class="input-group">
            <div class="ms-3">
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="pilih-layanan" id="pilih-layanan-yes" value="pilih-layanan-yes">
                    <label class="form-check-label" for="pilih-layanan-yes">
                        Ada Layanan Tambahan
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="pilih-layanan" id="pilih-layanan-no" value="pilih-layanan-no">
                    <label class="form-check-label" for="pilih-layanan-no">
                        Tidak Ada Layanan Tambahan
                    </label>
                </div>
            </div>
        </div>
    </div>

    <div class="tambah-layanan" style="display:none;">
        <input type="text" class="form-control" placeholder="Search Layanan" id="filter_layanan"/>

        <div class="col-12 m-3">

            <ul id="layanan-list">
                @foreach($additionalMedicalServices as $additionalMedicalService)
                    <li class="layanan-item">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input additional_services" name="additional_services[]" value="{{ $additionalMedicalService->id }}" id="additional-medical-service-{{ $additionalMedicalService->id }}">
                            <label class="custom-control-label layanan-name" for="additional-medical-service-{{ $additionalMedicalService->id }}">{{ $additionalMedicalService->name }} (Rp {{number_format($additionalMedicalService->price, 2)}}) </label>
                        </div>
                    </li>
                @endforeach
            </ul>

        </div>
    </div>
</div>
