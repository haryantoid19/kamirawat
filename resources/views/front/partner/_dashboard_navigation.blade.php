<h4 class="fw-medium text-dark">Selamat Datang, <b>{{ Auth::user()->name }}</b></h4>

<div class="w-100 floatleft my-3">
    <div class="w-100">
        <a href="{{ route('front.partner.dashboard') }}" class="btn btn-primary m-1 {{ request()->routeIs('front.partner.dashboard') ? 'current-page' : '' }}">Profile</a>
        <a href="{{ route('front.partner.orders', 'new') }}" class="btn btn-primary m-1 {{ request()->is('partner/orders/new') ? 'current-page' : '' }}">Pesanan Baru</a>
        <a href="{{ route('front.partner.orders', 'ongoing') }}" class="btn btn-primary m-1 {{ request()->is('partner/orders/ongoing') ? 'current-page' : '' }}">Sedang Berlangsung</a>
        <a href="{{ route('front.partner.orders', 'complete') }}" class="btn btn-primary m-1 {{ request()->is('partner/orders/complete') ? 'current-page' : '' }}">Pesanan Selesai</a>
    </div>
</div>
