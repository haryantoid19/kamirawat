@extends('front.layout.main')

@section('content')
    <!-- ============================================-->
    <!-- <section> begin ============================-->
    <section class="pt-6 bg-primary">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-12 py-6">
                    <div class="text-center">
                        <h3 class="fw-medium text-white">Daftar Baru Tenaga Medis</h3>
                        <div class="fs-md--2 text-white">Masukkan data diri Anda untuk pengguna baru</div>
                    </div>

                    <div class="row mt-4">
                        <div class="col-md-8 offset-md-2">
                            <form id="daftar" class="" action="{{ route('front.partner.register') }}" method="POST" enctype="multipart/form-data">
                                @csrf

                                @if ($errors->any())
                                    <div class="col-12 mt-6 text-start">
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                @endif

                                <div class="w-100 floatleft">
                                    <div class="mb-4">
                                        <div class="input-group">
                                            <label for="phone_number" class="d-none sr-only">Nomor Handphone</label>
                                            <input type="text" name="phone_number" id="phone_number" class="form-control @error("phone_number") is-invalid @enderror" placeholder="Nomor Handphone" value="{{ old('phone_number') }}" required>
                                            @error('title')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="mb-4">
                                        <div class="input-group">
                                            <label for="name" class="d-none sr-only">Nama Lengkap Anda</label>
                                            <input type="text" name="name" id="name" class="form-control @error("name") is-invalid @enderror" placeholder="Nama Lengkap Anda" value="{{ old('name') }}" required>
                                            @error('name')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="mb-4">
                                        <div class="input-group">
                                            <label for="username" class="form-label fw-bold text-white">Email Anda</label>
                                            <input type="email" name="email" id="email" class="form-control @error("email") is-invalid @enderror" placeholder="Email" value="{{ old('email') }}" required>
                                            @error('email')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="mb-4">
                                        <div class="input-group">
                                            <label for="nationality" class="form-label fw-bold text-white">Kewarganegaraan</label>
                                            <select class="form-select @error("nationality") is-invalid @enderror" name="nationality" id="nationality">
                                                <option value="">Pilih Kewarganegaraan</option>
                                                <option value="WNI" {{ old('nationality') === "WNI" ? "selected" : "" }}>WNI</option>
                                                <option value="WNA" {{ old('nationality') === "WNA" ? "selected" : "" }}>WNA</option>
                                            </select>
                                            @error('nationality')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="mb-4">
                                        <div class="input-group">
                                            <label for="id_type" class="form-label fw-bold text-white">Tipe ID</label>
                                            <select class="form-control @error("id_type") is-invalid @enderror" id="id_type" name="id_type" required="required">
                                                <option value="{{ \App\Enums\IdType::KTP()->value }}" {{ old('id_type') === \App\Enums\IdType::KTP()->value ? "selected" : "" }}>{{ \App\Enums\IdType::KTP()->value }}</option>
                                                <option value="{{ \App\Enums\IdType::PASSPORT()->value }}" {{ old('id_type') === \App\Enums\IdType::PASSPORT()->value ? "selected" : "" }}>{{ \App\Enums\IdType::PASSPORT()->value }}</option>
                                            </select>
                                            @error('id_type')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                        <div class="input-group mt-3">
                                            <label for="id_number" class="d-none sr-only">KTP</label>
                                            <input type="text" name="id_number" id="id_number" class="form-control @error("id_number") is-invalid @enderror" placeholder="Nomor KTP" value="{{ old('id_number') }}" required>
                                            @error('id_number')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="mb-4">
                                        <div class="input-group">
                                            <label for="bod" class="form-label fw-bold text-white">Tanggal Lahir Anda</label>
                                            <input type="text" name="bod" id="bod" class="form-control datepicker-dob @error("bod") is-invalid @enderror" placeholder="Tanggal Lahir" value="{{ old('bod') }}" required>
                                            @error('bod')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="mb-4">
                                        <div class="input-group">
                                            <label for="gender" class="form-label fw-bold text-white">Jenis Kelamin</label>
                                            <div class="ms-3">
                                                <div class="form-check">
                                                    <input
                                                        class="form-check-input @error("gender") is-invalid @enderror"
                                                        type="radio"
                                                        name="gender"
                                                        id="{{ \App\Enums\Gender::MAN()->value }}"
                                                        value="{{ \App\Enums\Gender::MAN()->value }}"
                                                        required
                                                        {{ old('gender') === \App\Enums\Gender::MAN()->value ? "checked" : "" }}
                                                    >
                                                    <label class="form-check-label" for="{{ \App\Enums\Gender::MAN()->value }}">
                                                        Pria
                                                    </label>
                                                </div>
                                                <div class="form-check">
                                                    <input
                                                        class="form-check-input @error("gender") is-invalid @enderror"
                                                        type="radio"
                                                        name="gender"
                                                        id="{{ \App\Enums\Gender::WOMAN()->value }}"
                                                        value="{{ \App\Enums\Gender::WOMAN()->value }}"
                                                        required
                                                        {{ old('gender') === \App\Enums\Gender::WOMAN()->value ? "checked" : "" }}
                                                    >
                                                    <label class="form-check-label" for="{{ \App\Enums\Gender::WOMAN()->value }}">
                                                        Wanita
                                                    </label>
                                                </div>
                                            </div>
                                            @error('gender')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>

                                   

                                    <div class="mb-4">
                                        <div class="input-group">
                                            <label for="address" class="form-label fw-bold text-white">Alamat Domisili</label>
                                            <textarea type="text" name="address" id="address" class="form-control @error("address") is-invalid @enderror" placeholder="Alamat Domisili">{{ old('address') }}</textarea>
                                            @error('address')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="mb-4 row">
                                        <label for="province" class="col-sm-3 col-form-label fw-bold text-white">Provinsi</label>
                                        <div class="col-sm-9">
                                            <select class="form-select @error("province") is-invalid @enderror" id="province" name="province">
                                                <option value="">Pilih Provinsi</option>
                                                @foreach($provinces as $province)
                                                    @if(old('province') == $province->province_code)
                                                        <option value="{{ $province->province_code }}" selected>{{ $province->province_name }}</option>
                                                    @else
                                                        <option value="{{ $province->province_code }}">{{ $province->province_name }}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                            @error('province')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="mb-4 row">
                                        <label for="city" class="col-sm-3 col-form-label fw-bold text-white">Kota</label>
                                        <div class="col-sm-9">
                                            <input type="hidden" name="city_id" value="{{ old('city') }}" />
                                            <select class="form-select @error("city") is-invalid @enderror" id="city" name="city">
                                                <option value="">Pilih Kota</option>
                                            </select>
                                            @error('city')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="mb-4 row">
                                        <label for="subdistrict" class="col-sm-3 col-form-label fw-bold text-white">Kecamatan</label>
                                        <div class="col-sm-9">
                                            <input type="hidden" name="subdistrict_id" value="{{ old('subdistrict') }}" />
                                            <select class="form-select @error("subdistrict") is-invalid @enderror" id="subdistrict" name="subdistrict">
                                                <option value="">Pilih Kecamatan</option>
                                            </select>
                                            @error('subdistrict')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="mb-4 row">
                                        <label for="urban" class="col-sm-3 col-form-label fw-bold text-white">Kelurahan</label>
                                        <div class="col-sm-9">
                                            <input type="hidden" name="urban_id" value="{{ old('urban') }}" />
                                            <select class="form-select @error("urban") is-invalid @enderror" id="urban" name="urban">
                                                <option value="">Pilih Kelurahan</option>
                                            </select>
                                            @error('urban')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="mb-4 row">
                                        <label for="rt" class="col-sm-3 col-form-label fw-bold text-white">RT</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="rt" id="rt" class="form-control @error("rt") is-invalid @enderror" placeholder="RT" value="{{ old('rt') }}" required>
                                            @error('rt')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="mb-4 row">
                                        <label for="rw" class="col-sm-3 col-form-label fw-bold text-white">RW</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="rw" id="rw" class="form-control @error("rw") is-invalid @enderror" placeholder="RW" value="{{ old('rw') }}" required>
                                            @error('rw')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="mb-4 row">
                                        <label for="last_education" class="col-sm-3 col-form-label fw-bold text-white">Pendidikan Terakhir</label>
                                        <div class="col-sm-9">
                                            <select class="form-select @error("last_education") is-invalid @enderror" id="last_education" name="last_education">
                                                <option value="">Pilih Pendidikan Terakhir</option>
                                                @foreach(\App\Enums\LastEducation::getInstances() as $last_education)
                                                    @if(old('last_education') == $last_education->value)
                                                    <option value="{{ $last_education->value }}" selected>{{ $last_education->value }}</option>
                                                    @else
                                                    <option value="{{ $last_education->value }}">{{ $last_education->value }}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                            @error('last_education')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="mb-4">
                                        <div class="input-group input-group-addon-w mb-4" id="show_hide_password">
                                            <label class="d-none sr-only" for="password">{{ __('Password') }}</label>
                                            <input id="password" name="password" type="password" class="form-control @error("password") is-invalid @enderror" placeholder="Kata Sandi" autocomplete="new-password">
                                            <span class="pass-notif">Enter a password longer than 8 characters</span>

                                            <div class="input-group-addon">
                                                <a href="javascript:void(0);"><i class="fa fa-eye-slash" aria-hidden="true"></i></a>
                                            </div>

                                            @error('password')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>

                                        <div class="input-group input-group-addon-w" id="show_hide_password-retype">
                                            <label class="d-none sr-only" for="password_confirmation">{{ __('Confirm Password') }}</label>
                                            <input id="password_confirmation" name="password_confirmation" type="password" class="form-control @error("password_confirmation") is-invalid @enderror" placeholder="{{ __('Confirm Password') }}" autocomplete="new-password">
                                            <span class="pass-notif retype">Please confirm your password</span>

                                            <div class="input-group-addon">
                                                <a href="javascript:void(0);"><i class="fa fa-eye-slash" aria-hidden="true"></i></a>
                                            </div>

                                            @error('password_confirmation')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>

                                <div class="w-100 floatleft my-6">
                                    <div class="text-center mb-4">
                                        <h3 class="fw-medium text-white">Profesi Anda</h3>
                                    </div>

                                    <div class="mb-4">
                                        <div class="input-group">
                                            <label for="profession_id" class="d-none sr-only">Profesi Anda</label>
                                            <select class="form-select @error("profession_id") is-invalid @enderror" id="profession_id" name="profession_id" required>
                                                <option value="">Pilih Profesi</option>
                                                @foreach($professions as $profession)
                                                    @if(old('profession_id') == $profession->id)
                                                        <option value="{{ $profession->id }}" selected>{{ $profession->title }}</option>
                                                    @else
                                                        <option value="{{ $profession->id }}">{{ $profession->title }}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                            @error('profession_id')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="mb-3">
                                        <label for="document_ktp" class="form-label fw-bold text-white">Upload Foto KTP Anda  <span style="font-size:12px;color:red">*Maksimal file 2 MB</span></label>
                                        <input class="form-control @error("document_ktp") is-invalid @enderror document-file" type="file" id="document_ktp" name="document_ktp" accept="image/png,image/jpg,image/jpeg,application/pdf">

                                        @error('document_ktp')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>

                                    <div class="mb-3">
                                        <label for="document_npwp" class="form-label fw-bold text-white">Upload Foto NPWP Anda  <span style="font-size:12px;color:red">*Maksimal file 2 MB</span></label>
                                        <input class="form-control @error("document_npwp") is-invalid @enderror document-file" type="file" id="document_npwp" name="document_npwp" accept="image/png,image/jpg,image/jpeg,application/pdf">

                                        @error('document_npwp')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>

                                    <div class="mb-3">
                                        <label for="document_str" class="form-label fw-bold text-white">Upload STR <span style="font-size:12px;color:red">*Maksimal file 2 MB</span></label>
                                        <input class="form-control @error("document_str") is-invalid @enderror document-file" type="file" id="document_str" name="document_str" accept="image/png,image/jpg,image/jpeg,application/pdf">
                                        @error('document_str')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>

                                    <div class="mb-3">
                                        <label for="document_sip" class="form-label fw-bold text-white">Upload SIP (jika Ada) <span style="font-size:12px;color:red">*Maksimal file 2 MB</span></label>
                                        <input class="form-control @error("document_sip") is-invalid @enderror document-file" type="file" id="document_sip" name="document_sip" accept="image/png,image/jpg,image/jpeg,application/pdf">
                                        @error('document_sip')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>

                                    <div class="mb-3">
                                        <label for="document_certification" class="form-label fw-bold text-white">Upload Dokumen Pendukung Lainnya <span style="font-size:12px;color:red">*Maksimal file 2 MB</span></label>
                                        <input class="form-control @error("document_certification") is-invalid @enderror document-file" type="file" name="document_certification" id="document_certification" accept="image/png,image/jpg,image/jpeg,application/pdf">
                                        @error('document_certification')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                        @enderror
                                    </div>
                                </div>

                                <div class="w-100 floatleft my-3">
                                    <div class="form-check">
                                        <input required class="form-check-input @error("check-tnc") is-invalid @enderror" type="checkbox" value="1" id="check-tnc" name="check-tnc">
                                        <label class="form-check-label" for="check-tnc">
                                            Saya menyetujui <a href="{{ route('front.syaratdanketentuan') }}" target="new" style="color:#717171;text-decoration:underline;"><b>syarat dan ketentuan</b></a> yang berlaku
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input required class="form-check-input @error("check-newsletter") is-invalid @enderror" type="checkbox" value="1" id="check-newsletter" name="check-newsletter">
                                        <label class="form-check-label" for="check-newsletter">
                                        Saya bersedia untuk menerima informasi, edukasi, maupun promosi dari Kamirawat
                                        </label>
                                    </div>
                                </div>


                                <div class="w-100 floatleft mt-3">
                                    <button type="submit" class="btn btn-cyan btn-100">{{ __('Register') }}</button>
                                </div>
                            </form>

                            <div class="w-100 floatleft mt-6 text-center">
                                <a href="#" class="text-white">{{ __('Forgot your password?') }}</a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- <section> close ============================-->
    <!-- ============================================-->
@endsection

@section('script')
    <script type="text/javascript">
        window.register_partner = true;
        window.postalCode = {!! json_encode($window_data) !!}
    </script>
@endsection
