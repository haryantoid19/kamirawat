<div class="row">
    <div class="col-md-3 border-right">
        <div class="d-flex flex-column align-items-center text-center p-3">
            <img width="150px" height="150px" class="rounded-circle" width="150px" src="{{ $booking->patient->avatar_url }}">
            <span> </span>
        </div>
    </div>

    <div class="col-md-9 border-right">
        <div class="p-3">

            <div class="mb-4">
                <div class="row mb-3">
                    <div class="col-md-4 label-detail">
                        <label class="fw-bold">Nama Pasien</label>
                    </div>
                    <div class="col-md-8">
                        <span>{{ $booking->patient->name }}</span>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-md-4 label-detail">
                        <label class="fw-bold">Nama Layanan</label>
                    </div>
                    <div class="col-md-8">
                        <span>{{ $booking->medical_service->name }}</span>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-md-4 label-detail">
                        <label class="fw-bold">Tanggal Layanan</label>
                    </div>
                    <div class="col-md-8">
                        <span>{{ $booking->created_at->format('d F Y') }}</span>
                    </div>
                </div>
                <div class="row mb-3">
                    <div class="col-md-4 label-detail">
                        <label class="fw-bold">Alamat</label>
                    </div>
                    <div class="col-md-8">
                        <span>{{ $booking->patient->address }}</span>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

@if($history_bookings->count() > 0)
    <div class="w-100">
        <h3>Resume Pemeriksaan Pasien</h3>
        <hr>
        <div class="accordion" id="accordionMedicalHistory">
            @foreach($history_bookings as $history_booking)
                <div class="accordion-item">
                    <h2 class="accordion-header" id="heading{{$loop->iteration}}">
                        <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapse{{$loop->iteration}}" aria-expanded="true" aria-controls="collapse{{$loop->iteration}}">
                            Resume [{{ $history_booking->medical_service->name }}] [{{ $history_booking->created_at->format('d F Y') }}]
                        </button>
                    </h2>
                    <div id="collapse{{$loop->iteration}}" class="accordion-collapse collapse {{ ($loop->iteration == 1) ? 'show' : ''}}" aria-labelledby="heading{{$loop->iteration}}" data-bs-parent="#accordionMedicalHistory">
                        <div class="accordion-body">
                            {{ nl2br($history_booking->medical_record_summary) }}
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endif
