@extends('front.layout.main')

@section('content')
    @include("front.partner._dashboard_banner")

    <!-- ============================================-->
    <!-- <section> begin ============================-->
    <section class="pt-6 bg-gray">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-12 py-6">
                    @include("front.partner._dashboard_navigation")

                    <div class="w-100 floatleft">
                        <div class="container rounded bg-white mt-5 mb-5 box-shadow-1">
                            <div class="row">

                                <div class="col-md-12 border-right">
                                    <div class="p-3 py-5">

                                        @if($bookings->count() === 0)
                                            <div class="alert alert-secondary" role="alert">
                                                Tidak ada pesanan sedang berlangsung
                                            </div>
                                        @endif

                                        @if($message = session('message'))
                                            <div class="alert alert-success" role="alert">
                                                {{ $message }}
                                            </div>
                                        @endif

                                        @foreach($bookings as $booking)
                                            <div class="py-4">
                                                <div class="row">

                                                    <div class="col-12 col-md-8">
                                                        <div class="box-shadow-1 p-3 h-100">

                                                            <div class="row">
                                                                <div class="col-md-3 border-right">
                                                                    <div class="d-flex flex-column align-items-center text-center">
                                                                        <img class="rounded-circle" width="150px" height="150px" src="{{ $booking->patient->avatar_url }}" alt="{{ $booking->patient->name }}">
                                                                        <span></span>
                                                                    </div>
                                                                </div>

                                                                <div class="col-12 col-md-9 border-right">
                                                                    <div class="p-3">

                                                                        <div class="mb-4">
                                                                            <div class="row mb-3">
                                                                                <div class="col-12 col-md-4 label-detail">
                                                                                    <label class="fw-bold">Nama Pasien</label>
                                                                                </div>
                                                                                <div class="col-12 col-md-8">
                                                                                    <span>{{ $booking->patient->name }}</span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row mb-3">
                                                                                <div class="col-12 col-md-4 label-detail">
                                                                                    <label class="fw-bold">Nama Layanan</label>
                                                                                </div>
                                                                                <div class="col-12 col-md-8">
                                                                                    <span>{{ $booking->medical_service->name }}</span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row mb-3">
                                                                                <div class="col-12 col-md-4 label-detail">
                                                                                    <label class="fw-bold">Tanggal Layanan</label>
                                                                                </div>
                                                                                <div class="col-12 col-md-8">
                                                                                    <span>{{ $booking->created_at->format('d F Y') }}</span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row mb-3">
                                                                                <div class="col-12 col-md-4 label-detail">
                                                                                    <label class="fw-bold">Alamat</label>
                                                                                </div>
                                                                                <div class="col-12 col-md-8">
                                                                                    <span>{{ $booking->patient->address }}</span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row mb-3">
                                                                                <div class="col-12 col-md-4">
                                                                                    <label class="fw-bold"></label>
                                                                                </div>
                                                                                <div class="col-12 col-md-8">
                                                                                    <a
                                                                                        href="javascript:void(0);"
                                                                                        class="btn btn-orange btn-small my-1"
                                                                                        data-url="{{ route('front.partner.orders.map', $booking->id) }}"
                                                                                        data-bs-toggle="modal"
                                                                                        data-bs-target="#ModalMap"
                                                                                    >Lihat Alamat di Peta</a>
                                                                                </div>
                                                                            </div>


                                                                            <hr>

                                                                            <div class="row mb-3">
                                                                                <div class="col-12 col-md-4">
                                                                                    <label class="fw-bold">Keluhan</label>
                                                                                </div>
                                                                                <div class="col-12 col-md-8">{{ $booking->main_symptom }}</div>
                                                                            </div>
                                                                            <div class="row mb-3">
                                                                                <div class="col-12 col-md-4">
                                                                                    <label class="fw-bold">Riwayat penyakit & alergi</label>
                                                                                </div>
                                                                                <div class="col-12 col-md-8">{{ $booking->history_of_disease_allergies }}</div>
                                                                            </div>
                                                                            <div class="row mb-3">
                                                                                <div class="col-12 col-md-4">
                                                                                    <label class="fw-bold">Permintaan Khusus</label>
                                                                                </div>
                                                                                <div class="col-12 col-md-8">{{ $booking->special_request }}</div>
                                                                            </div>

                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                    <div class="col-12 col-md-4">
                                                        <div class="box-shadow-1 p-3 h-100">

                                                            <div class="w-100 text-center">
                                                                <h4 class="fw-bold">No. Layanan : {{ $booking->booking_number }}</h4>
                                                                <a
                                                                    id="viewResume"
                                                                    href="javascript:void(0);"
                                                                    data-urlview="{{ route('front.partner.orders.show', $booking->id) }}"
                                                                    data-urlpasscode="{{ route('front.partner.orders.passcode-check', $booking->id) }}"
                                                                    data-inputpasscode="{{ $booking->input_passcode_at != NULL ? 'YES' : 'YES' }}"
                                                                    class="btn btn-green-old btn-medium my-1"
                                                                >
                                                                    Lihat Detail Resume Sebelumnya
                                                                </a>

                                                                <hr>

                                                                <div class="w-100 mb-3">
                                                                    <div class="row">
                                                                        <div class="col-12 col-md-6 col-sm-6 text-start">
                                                                            <div class="checkbox-in">
                                                                                <i class="far fa-square handling-check-icon"></i>
                                                                                Tindakan
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-12 col-md-6 col-sm-6 text-end">
                                                                            <a href="javascript:void(0);" data-bs-toggle="modal" data-bs-target="#ModalTindakan" data-id="{{ $booking->id }}" data-url="{{ route('front.partner.orders.popup-handling', $booking->id) }}" class="btn btn-primary btn-medium my-1 btn-small">Tindakan Selesai</a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="w-100 mb-3">
                                                                    <div class="row">
                                                                        <div class="col-12 col-md-6 col-sm-6 text-start">
                                                                            <div class="checkbox-in">
                                                                                <i class="far fa-square medical-record-check-icon"></i>
                                                                                Resume Pemeriksaan
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-12 col-md-6 col-sm-6 text-end">
                                                                            <a href="javascript:void(0);" data-bs-toggle="modal" data-bs-target="#ModalRekamMedis" data-id="{{ $booking->id }}" data-url="{{ route('front.partner.orders.popup-medical-record', $booking->id) }}" class="btn btn-primary btn-medium my-1 btn-small">Resume Pemeriksaan</a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="w-100 mb-3">
                                                                    <div class="row">
                                                                        <div class="col-12 col-md-6 col-sm-6 text-start">
                                                                            <div class="checkbox-in">
                                                                                <i class="far fa-square additional-invoice-check-icon"></i>
                                                                                Invoice / Layanan Tambahan
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-12 col-md-6 col-sm-6 text-end">
                                                                            <a href="javascript:void(0);" data-bs-toggle="modal" data-bs-target="#ModalLayanan" data-id="{{ $booking->id }}" data-url="{{ route('front.partner.orders.popup-additional-service', $booking->id) }}" class="btn btn-primary btn-medium my-1 btn-small">Tambah Layanan</a>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>

                                                        </div>
                                                    </div>


                                                </div>
                                            </div>
                                        @endforeach

                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <!-- <section> close ============================-->
    <!-- ============================================-->


    <!-- Modal -->
    <div class="modal fade" id="ModalMap" tabindex="-1" aria-labelledby="ModalMapLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="ModalMapLabel">Lokasi - No. Layanan : <span class="booking-number"></span></h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">Loading...</div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="ModalTindakan" tabindex="-1" aria-labelledby="ModalTindakanlLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Tindakan</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">Loading...</div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="saveMedicalHandling">Save changes</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Rekam Medis -->
    <div class="modal fade" id="ModalRekamMedis" tabindex="-1" aria-labelledby="ModalRekamMedisLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Rekam Medis</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">Loading...</div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="saveMedicalRecord">Save changes</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Layanan -->
    <div class="modal fade" id="ModalLayanan" tabindex="-1" aria-labelledby="ModalLayananLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Layanan</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="spinner-grow blockui-spinner" style="width: 3rem; height: 3rem;display:none" role="status">
                    <span class="visually-hidden">Loading...</span>
                </div>
                <div class="modal-body">Loading...</div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="finish-layanan">Selesaikan Layanan</button>
                </div>
            </div>
        </div>
    </div>


    <!-- Modal Invoice -->
    <div class="modal fade" id="ModalInvoice" tabindex="-1" aria-labelledby="ModalInvoiceLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Layanan</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">Loading...</div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" id="finish-modal-invoice">Selesaikan Layanan</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="showDetailModal" tabindex="-1" aria-labelledby="showDetailModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"></h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">Loading...</div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>


    <!-- Modal passcode -->
    <div class="modal fade" id="showDetailModalPasscode" tabindex="-1" aria-labelledby="showDetailModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Masukan Passcode</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="alert alert-info">Mintalah passcode pada pasien anda untuk membuka riwayat pemeriksaan pasien di kamirawat.com <br/> Pada Halaman riwayat pesanan patient anda.</div>
                    <div class="mb-3 mt-1">
                        <label for="passcode" class="form-label">Passcode</label>
                        <input type="text" class="form-control" id="passcode" placeholder="passcode">
                        <div class="text-danger small mt-1 error-message" style="display:none;"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="submit-passcode">Submit</button>
                    <button type="button" class="btn btn-grey" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Payment URL -->
    <div class="modal fade" id="showPaymentURLModal" tabindex="-1" aria-labelledby="showPaymentURLModalLabel" aria-hidden="true" data-bs-keyboard="false" data-bs-backdrop="static">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Payment</h5>
                </div>
                <div class="modal-body" style="text-align: center">
                    <div class="payment-status" style="font-size:120px;display:none;">
                        <i class="fas fa-check-circle text-primary"></i>
                        <p style="font-size:20px;">Payment Success</p>
                    </div>
                    <div id="qrcode-payment-link" style="margin: 0 auto 20px;display:inline-block;"></div>
                    <div id="payment-check-manual" style="display:none;">
                        <p>Silahkan Scan QRCode untuk menuju ke halaman pembayaran</p>
                        <button type="button" class="btn btn-secondary btn-payment-check">Periksa pembayaran</button>
                        <button class="btn btn-primary btn-loading" type="button" disabled style="color: black;display:none;">
                            <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
                            <span class="">Loading...</span>
                        </button>
                    </div>
                    <div id="payment-bank-transfer" style="display:block;">
                        <div class="floatleft">
                            <div class="container rounded bg-white mb-5">
                                <div class="row align-items-center">
                                    <div class="col-12 text-center pb-6">
                                        <h2 class="fw-medium">Selamat!</h2>

                                        <div class="row mt-4">
                                            <div class="col-md-8 offset-md-2">
                                                <div class="w-100 floatleft mb-4">
                                                    <i class="far fa-check-circle text-sux fa-5x text-success"></i>
                                                </div>

                                                <p class="fw-bold">Berikut Informasi Pembayaran Anda</p>

                                                <p class="fw-bold bank-transfer-detail"></p>
                                            </div>
                                        </div>

                                        <h5 class="fw-medium mt-4">Silahkan konfirmasi pembayaran anda ke nomor berikut <a target="_blank" href="http://wa.me/+6281909201120">+6281909201120</a></h5>
                                    </div>

                                    <button type="button" class="btn btn-secondary btn-finish" data-bs-dismiss="modal">Selesai</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer" style="display:none;">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('script')
    <script type="text/javascript" src="{{ asset('assets/frontend/vendors/qrcodejs/qrcode.min.js') }}"></script>
    <script type="text/javascript">
        window.partner_order = true;
        window.get_order_url = '{{ route('front.partner.orders.show', '__id') }}';
        window.get_confirmation_additional_services_url = '{{ route('front.partner.orders.popup-confirmation-additional-service', '__id') }}';
        window.completion_order_url = '{{ route('front.partner.orders.completion', '__id') }}'
        window.get_map_url = '{{ route('front.partner.orders.map', '__id') }}';
        window.get_order_payment_check_url = '{{ route('front.partner.orders.payment-check', '__id') }}';
    </script>
@endsection
