@extends('front.layout.main')

@section('content')
 

    <section>
      <div class="container">
          <div class="row justify-content-center">
              <div class="col-md-10">
                  <div class="text-content">
                      <div class="row">
                          <div class="col-12 text-center">
                              <h1 class="mb-6 fw-medium title-section">Home Care</h1>
                          </div>
                      </div>

                      <p class="fw-medium">Menurut penelitian, menghabiskan waktu bersama orang tersayang sangat efektif untuk memperpanjang usia harapan hidup, meningkatkan kesehatan fisik dan mental serta dapat mengurangi tingkat stress. Kamirawat dapat membantu menjaga kesehatan orang tersayang di lingkungan paling nyaman yaitu rumah Anda. </p>
                          <br>
                      <p class="fw-medium">Pelayanan Home Care yang dilakukan oleh Tenaga Medis Kamirawat dapat membantu pasien menjalani hidup yang lebih berkualitas. Tenaga Medis kami berpengalaman dalam memenuhi kebutuhan perawatan pasien di rumah dengan durasi yang dapat disesuaikan dengan kebutuhan Anda.</p>
                  </div>
              </div>
          </div>
      </div>
    </section>


    <section>
      <div class="img-wrapper mb-4" style="background:url('{{ asset('assets/frontend/img/kunjungan-dokter.jpeg') }}') center center/cover no-repeat;"></div>
      <div class="container">
          <div class="row">
              <div class="offset-sm-6 col-sm-6">
                  <div class="text-content">
                      <div class="row">
                          <div class="col-12 text-center">
                              <h1 class="mb-6 fw-medium title-section">Layanan Geriatri</h1>
                          </div>
                      </div>

                      <p class="fw-medium">Kamirawat hadir dengan menyediakan Pelayanan Geriatri 24 jam. Dokter kami akan mengkaji masalah kesehatan keluarga Anda, dan dapat merekomendasikan program-program yang tepat dalam menunjang kesehatan pasien.</p>
                          <br>
                      <p class="fw-medium">Program Geriatri antara lain perawatan pasien untuk kebutuhan sehari-hari, pemeriksaan Laboratorium, Fisioterapi, dan Edukasi Kesehatan.</p>
                  </div>
              </div>
          </div>
      </div>
    </section>


    <section>
      <div class="img-wrapper right mb-4" style="background:url('{{ asset('assets/frontend/img/kunjungan-perawat.jpeg') }}') center center/cover no-repeat;"></div>
      <div class="container">
          <div class="row">
              <div class="offset-sm-6 col-sm-6 right">
                  <div class="text-content">
                      <div class="row">
                          <div class="col-12 text-center">
                              <h1 class="mb-6 fw-medium title-section">Perawatan Ibu dan Anak</h1>
                          </div>
                      </div>

                      <p class="fw-medium">Perawatan kesehatan bagi Ibu yang baru melahirkan sangat penting dilakukan untuk memulihkan kondisi kesehatan si Ibu, begitu pula pada bayi baru lahir, atau juga dikenal dengan istilah neonatus. Neonatus, terutama yang lahir secara prematur atau memiliki kondisi medis khusus, lebih rentan terhadap penyakit karena sistem imun tubuhnya yang belum sempurna. Pelayanan ini bertujuan memberikan bimbingan kepada orangtua dan keluarga dalam perawatan sehari-hari neonatus, terutama bagi yang memiliki kondisi khusus atau pasca perawatan intensif, dengan kondisi yang sudah stabil. Tidak perlu lagi khawatir mengenai cara terbaik dalam mengasuh bayi Anda!</p>
                  </div>
              </div>
          </div>
      </div>
    </section>


    <section>
      <div class="container sc-layanan-wrap full-wrap sc-layanan">
          <div class="row justify-content-center">
            <div class="col-md-4 mb-4">
                <div class="sc-layanan-item">
                  <div class="text-content text-center">
                      <h4 class="fw-bold mb-6 title-section">Perawatan Pasca Caesar</h4>
                      <p class="fw-medium">Luka jahitan pasca operasi caesar kerap menimbulkan rasa tidak nyaman dan membutuhkan waktu pemulihan yang tidak sebentar. Namun, dengan perawatan yang tepat, luka jahitan operasi caesar dapat pulih dengan cepat.</p>
                          <br>
                      <p class="fw-medium">Dalam Perawatan Ibu dan Anak oleh Tim Kamirawat, kami juga akan melakukan perawatan untuk luka jahitan pasca operasi agar terjaga tetap bersih dan kering guna mencegah terjadinya infeksi. Bagi Ibu yang menjalani persalinan secara normal, jangan khawatir karena kami juga dapat memberikan perawatan dasar pasca melahirkan untuk area kewanitaan Anda.</p>
                  </div>
                </div>
              </div>
              <div class="col-md-4 mb-4">
                <div class="sc-layanan-item">
                  <div class="text-content text-center">
                      <h4 class="fw-bold mb-6 title-section">Pijat Laktasi</h4>
                      <p class="fw-medium">Pentingnya manfaat ASI untuk mendukung kesehatan dan tumbuh kembang membuat setiap Ibu menginginkan proses menyusui agar berjalan lancar. Namun, acapkali muncul hambatan seperti ASI yang keluar tidak lancar.</p>
                          <br>
                      <p class="fw-medium">ASI yang tidak lancar dapat disebabkan oleh berbagai faktor, salah satunya karena munculnya perasaan cemas dan stres yang berlebih sehingga berdampak pada produksi ASI. Untuk itu, Kamirawat menyediakan layanan pijat laktasi untuk membantu Ibu tetap merasa tenang, nyaman dan tidak stres di masa menyusui. Pijat laktasi memicu produksi hormon oksitosin, hormon yang berperan penting dalam proses pengeluaran ASI.</p>
                  </div>
                </div>
              </div>
              <div class="col-md-4 mb-4">
                <div class="sc-layanan-item">
                  <div class="text-content text-center">
                        <h4 class="fw-bold mb-6 title-section">Paket Perawatan Bayi</h4>
                        <p class="fw-medium">Pengetahuan bagaimana cara merawat bayi baru lahir perlu mendapat perhatian khusus, terutama bagi bayi prematur maupun pasca perawatan intensif. Terlebih jika Anda merupakan pasangan yang baru memiliki anak.</p>
                    </div>
                </div>
              </div>
          </div>
      </div>
    </section>


    <section class="py-5">
      <div class="container text-center">
        <h4 class="fw-bold">Buat janji temu untuk kunjungan Perawat Kamirawat sekarang!</h4>
        <a href="{{ route('front.index') }}/patient/booking/3#nav-head" class="btn btn-primary m-1 fw-bold">Booking Now</a>
      </div>
    </section>


    <!-- ============================================-->
    <!-- <section> begin ============================-->
    <section class="p-0">
        <img src="{{ asset('assets/frontend/img/footer.jpg') }}" class="img-100" alt="">
    </section>
    <!-- <section> close ============================-->
    <!-- ============================================-->


@endsection
