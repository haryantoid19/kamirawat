@extends('front.layout.main')

@section('content')



    <section>
      <div class="img-wrapper mb-4" style="background:url('{{ asset('assets/frontend/img/kunjungan-dokter.jpeg') }}') center center/cover no-repeat;"></div>
      <div class="container">
          <div class="row">
              <div class="offset-sm-6 col-sm-6">
                  <div class="text-content">
                      <div class="row">
                          <div class="col-12 text-center">
                              <h1 class="mb-6 fw-medium title-section">Kunjungan Dokter</h1>
                          </div>
                      </div>

                      <p class="fw-medium">Kamirawat menyediakan layanan kunjungan Dokter. Dokter kami memiliki STR dan SIP aktif, dan berpengalaman melakukan layanan Home Visit.</p>
                          <br>
                      <p class="fw-medium">Dokter kamirawat akan menanyakan keluhan medis, melakukan pemeriksaan fisik, memberikan diagnosa serta melakukan tindakan (jika diperlukan). Dokter kamirawat juga dapat memberikan rujukan pemeriksaan lanjutan ke Laboratorium, Radiologi, atau ke Dokter Spesialis tertentu jika kondisi penyakit pasien dirasa membutuhkan penanganan lebih lanjut.</p>
                      <br>
                      <p class="fw-medium">Kamirawat memiliki Post Care Management yang berfungsi untuk memonitor kondisi kesehatan pasien 2x 24 jam pasca kunjungan dokter atau tenaga medis kamirawat.</p>
                  </div>
              </div>
          </div>
      </div>
    </section>


    <section>
      <div class="img-wrapper right mb-4" style="background:url('{{ asset('assets/frontend/img/kunjungan-perawat.jpeg') }}') center center/cover no-repeat;"></div>
      <div class="container">
          <div class="row">
              <div class="offset-sm-6 col-sm-6 right">
                  <div class="text-content">
                      <div class="row">
                          <div class="col-12 text-center">
                              <h1 class="mb-6 fw-medium title-section">Kunjungan Perawat</h1>
                          </div>
                      </div>

                      <p class="fw-medium">Tim Perawat Kamirawat telah terlatih secara profesional, berpengalaman dan memiliki kompetensi yang sesuai untuk Pelayanan Medis (Home Visit)</p>
                          <br>
                      <p class="fw-medium">Layanan kunjungan perawat, memudahkan Anda mendapatkan perawatan medis langsung ke rumah atau lokasi Anda.</p>
                  </div>
              </div>
          </div>
      </div>
    </section>


    <section class="py-5">
      <div class="container text-center">
        <h4 class="fw-bold">Buat janji temu untuk kunjungan Perawat Kamirawat sekarang!</h4>
        <a href="{{ route('front.index') }}/patient/booking/5#nav-head" class="btn btn-primary m-1 fw-bold">Booking Now</a>
      </div>
    </section>


    <!-- ============================================-->
    <!-- <section> begin ============================-->
    <section class="p-0">
        <img src="{{ asset('assets/frontend/img/footer.jpg') }}" class="img-100" alt="">
    </section>
    <!-- <section> close ============================-->
    <!-- ============================================-->


@endsection
