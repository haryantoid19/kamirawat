@extends('front.layout.main')

@section('content')
 

    <section>
      <div class="container mb-6">
          <div class="row justify-content-center">
              <div class="col-md-10">
                  <div class="text-content text-center">
                    <h5>Selain memberikan pelayanan di rumah, kami juga memiliki Klinik Utama yang berlokasi di Jl. Benda No. 12D. Jakarta Selatan. Pelayanan Klinik Utama Kamirawat adalah:</h5>
                  </div>
              </div>
          </div>
      </div>

      <div class="container">
        <div class="row justify-content-center">
          <div class="col-md-10">
              <div class="text-content text-center">
                  <div class="row">
                      <div class="col-12">
                          <h1 class="mb-6 fw-medium title-section">Klinik Diabetes</h1>
                      </div>
                  </div>
              </div>
          </div>
        </div>
      </div>
    </section>


    <section class="py-5">
      <div class="container text-center">
        <h4 class="fw-bold">Buat janji untuk perawatan organ kewanitaan Anda bersama Kamirawat sekarang!</h4>
        <a href="{{ route('front.patient.login') }}" class="btn btn-primary m-1 fw-bold">Booking Now</a>
      </div>
    </section>


    <!-- ============================================-->
    <!-- <section> begin ============================-->
    <section class="p-0">
        <img src="{{ asset('assets/frontend/img/footer.jpg') }}" class="img-100" alt="">
    </section>
    <!-- <section> close ============================-->
    <!-- ============================================-->


@endsection
