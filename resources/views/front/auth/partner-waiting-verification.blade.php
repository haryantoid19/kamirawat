@extends('front.layout.main')

@section('content')
    <!-- ============================================-->
    <!-- <section> begin ============================-->
    <section class="pt-6 bg-primary">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-12 text-center py-6">
                    <h2 class="fw-medium text-white">Sedang diverifikasi!</h2>

                    <div class="row mt-4">
                        <div class="col-md-8 offset-md-2">
                            <div class="w-100 floatleft mb-4">
                                <i class="far fa-paper-plane fa-5x text-white"></i>
                            </div>

                            <p class="text-white fw-bold">{{ $email }}</p>
                            <br>
                            <p class="mb-0 text-white">Akun Anda sudah berhasil mendaftarkan diri sebagai tenaga medis.</p>
                            <p class="text-white">Tim Kamirawat akan menghubungi Anda untuk keterangan selanjutnya.</p>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- <section> close ============================-->
    <!-- ============================================-->
@endsection
