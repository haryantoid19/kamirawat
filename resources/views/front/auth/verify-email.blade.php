@extends('front.layout.main')

@section('content')
@include("front.partner._dashboard_banner")
<!-- ============================================-->
<!-- <section> begin ============================-->
<section class="pt-6 bg-gray">
    <div class="container">
        <div class="alert alert-warning" role="alert">
            {{ __('Thanks for signing up! Before getting started, could you verify your email address by clicking on the link we just emailed to you? If you didn\'t receive the email, we will gladly send you another.') }}
        </div>

        <!--begin::Session Status-->
        @if (session('status') === 'verification-link-sent')
        <p class="alert alert-success font-medium text-sm text-gray-500 mt-4">
            {{ __('A new verification link has been sent to the email address you provided during registration.') }}
        </p>
        @endif
        <!--end::Session Status-->

        <!--begin::Actions-->
        <div class="d-flex flex-wrap justify-content-start pb-lg-0">

            <form method="POST" action="{{ route('verification.send') }}">
                @csrf
                <button type="submit" class="btn btn-lg btn-primary fw-bolder me-4">{{ __('Resend Verification Email') }}</button>
            </form>

            <form method="POST" action="{{ route('front.patient.logout') }}">
                @csrf
                <button type="submit" class="btn btn-lg btn-secondary fw-bolder me-4">{{ __('Log out') }}</button>
            </form>
        </div>
        <!--end::Actions-->

    </div>
</section>
<!-- <section> close ============================-->
<!-- ============================================-->
@endsection
