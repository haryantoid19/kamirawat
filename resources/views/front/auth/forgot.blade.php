@extends('front.layout.main')

@section('content')
    <!-- ============================================-->
    <!-- <section> begin ============================-->
    <section class="pt-6 bg-primary">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-12 text-center py-6">
                    <h3 class="fw-medium text-white">Masukkan email Anda untuk<br>reset kata sandi!</h3>

                    <div class="row mt-4">

                        <div class="col-md-4 mx-auto">
                            <!--begin::Session Status-->
                            @if (session('status'))
                                <p class="alert alert-primary font-medium text-sm text-gray-500">
                                    {{ session('status') }}
                                </p>
                            @endif
                            <!--end::Session Status-->
                        </div>

                        <div class="col-md-8 offset-md-2">

                            <form class="" action="{{ route('password.email') }}" method="POST">
                                @csrf

                                <div class="mb-4">
                                    <div class="input-group">
                                        <label class="hidden sr-only" for="email">{{ __('Email') }}</label>
                                        <input type="email" name="email" class="form-control" id="email" placeholder="Email Terdaftar" required>
                                    </div>
                                </div>

                                <div class="col-md-6 mx-auto">
                                    @if ($errors->any())
                                        <div class="col-12 text-start">
                                            <div class="alert alert-danger">
                                                {{ $errors->first() }}
                                            </div>
                                        </div>
                                    @endif
                                </div>

                                <div class="w-100 floatleft mt-3">
                                    <button type="submit" class="btn btn-cyan">{{ __('Reset Password') }}</button>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- <section> close ============================-->
    <!-- ============================================-->

@endsection
