@extends('front.layout.main')

@section('content')
    <!-- ============================================-->
    <!-- <section> begin ============================-->
    <section class="pt-6 bg-primary">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-12 text-center py-6">
                    <h3 class="fw-medium text-white">Reset Kata Sandi</h3>

                    <div class="row mt-4">

                        <div class="col-md-4 mx-auto">
                            <!--begin::Session Status-->
                            @if (session('status'))
                                <p class="alert alert-primary font-medium text-sm text-gray-500">
                                    {{ session('status') }}
                                </p>
                            @endif
                            <!--end::Session Status-->
                        </div>

                        <div class="col-md-8 offset-md-2">

                            <form class="" action="{{ route('password.update') }}" method="POST">
                                @csrf

                                <!-- Password Reset Token -->
                                <input type="hidden" name="token" value="{{ $token }}">

                                <div class="mb-4">
                                    <div class="input-group">
                                        <label class="hidden sr-only" for="email">{{ __('Email') }}</label>
                                        <input type="email" name="email" class="form-control" id="email" placeholder="Email Terdaftar" value="{{ old('email', $request->get("email")) }}" required>
                                    </div>
                                </div>

                                <div class="mb-4">
                                    <div class="input-group">
                                        <label class="hidden sr-only" for="password">{{ __('Password') }}</label>
                                        <input type="password" name="password" class="form-control" id="password" placeholder="Password" required>
                                    </div>
                                </div>

                                <div class="mb-4">
                                    <div class="input-group">
                                        <label class="hidden sr-only" for="password_confirmation">{{ __('Password Confirmation') }}</label>
                                        <input type="password" name="password_confirmation" class="form-control" id="password_confirmation" placeholder="Password Confirmation" required>
                                    </div>
                                </div>

                                <div class="col-md-6 mx-auto">
                                    @if ($errors->any())
                                        <div class="col-12 text-start">
                                            <div class="alert alert-danger">
                                                {{ $errors->first() }}
                                            </div>
                                        </div>
                                    @endif
                                </div>

                                <div class="w-100 floatleft mt-3">
                                    <button type="submit" class="btn btn-cyan">{{ __('Submit') }}</button>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- <section> close ============================-->
    <!-- ============================================-->

@endsection
