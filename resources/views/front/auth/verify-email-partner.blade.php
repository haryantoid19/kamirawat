@extends('front.layout.main')

@section('content')
    <!-- ============================================-->
    <!-- <section> begin ============================-->
    <section class="pt-6 bg-primary">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-12 text-center py-6">
                    <h2 class="fw-medium text-white">Verifikasi Email!</h2>

                    <div class="row mt-4">
                        <div class="col-md-8 offset-md-2">
                            <div class="w-100 floatleft mb-4">
                                <i class="far fa-paper-plane fa-5x text-white"></i>
                            </div>

                            <p class="text-white fw-bold">{{ $email }}</p>
                            <br>
                            <p class="mb-0 text-white">Akun Anda sudah berhasil mendaftarkan diri sebagai tenaga medis.</p>
                            <p class="text-white">Namun Anda perlu verifikasi email, silahkan klik link yang sudah Kami kirimkan ke email anda.</p>

                            @if (session('status') === 'verification-link-sent')
                                <div class="d-flex justify-content-center mt-2">
                                    <p class="alert alert-success font-medium text-sm text-gray-500 mt-4">
                                        {{ __('A new verification link has been sent to the email address you provided during registration.') }}
                                    </p>
                                </div>
                            @endif
                            <div class="d-flex justify-content-center pb-lg-0 mt-2">

                                <form method="POST" action="{{ route('verification.send') }}">
                                    @csrf
                                    <button type="submit" class="btn btn-lg btn-secondary me-4">{{ __('Resend Verification Email') }}</button>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- <section> close ============================-->
    <!-- ============================================-->
@endsection
