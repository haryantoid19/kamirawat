@extends('front.layout.main')

@section('content')
  

    <section>
      <div class="container mb-6">
          <div class="row justify-content-center">
              <div class="col-md-10">
                  <div class="text-content text-center">
                    <h5>Selain memberikan pelayanan di rumah, kami juga memiliki Klinik Utama yang berlokasi di Jl. Benda No. 12D. Jakarta Selatan. Pelayanan Klinik Utama Kamirawat adalah:</h5>
                  </div>
              </div>
          </div>
      </div>

      <div class="container">
        <div class="row justify-content-center">
          <div class="col-md-10">
              <div class="text-content text-center">
                  <div class="row">
                      <div class="col-12">
                          <h1 class="mb-6 fw-medium title-section">Calm Clinic - (Counseling & Living Mind Center)</h1>
                      </div>
                  </div>

                  <p class="fw-medium">​​Kesehatan mental adalah komponen yang penting dalam setiap jenjang kehidupan manusia, mulai dari masa kanak-kanak, remaja, hingga dewasa. Bahkan, seringkali disebutkan, kondisi mental pada masa kanak-kanak dapat mempengaruhi perkembangan kejiwaan seseorang hingga dewasa nantinya. Oleh karena itu, kamirawat menyediakan Tenaga Ahli Profesional di bidang Psikologi dan Psikiater.</p>
                  <p class="fw-medium">Untuk ketersedian jadwal silahkan hubungi KamiRawat melalui pesan whatsapp ke nomor +6281909-201120</p>
              </div>
          </div>
        </div>
      </div>
    </section>


    <section class="py-5">
      <div class="container text-center">
        <h4 class="fw-bold">Buat janji untuk perawatan organ kewanitaan Anda bersama Kamirawat sekarang!</h4>
        <a href="{{ route('front.patient.login') }}" class="btn btn-primary m-1 fw-bold">Booking Now</a>
      </div>
    </section>


    <!-- ============================================-->
    <!-- <section> begin ============================-->
    <section class="p-0">
        <img src="{{ asset('assets/frontend/img/footer.jpg') }}" class="img-100" alt="">
    </section>
    <!-- <section> close ============================-->
    <!-- ============================================-->


@endsection
