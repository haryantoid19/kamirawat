@extends('front.layout.main')

@section('content')


    <section>
      <div class="container mb-6">
          <div class="row justify-content-center">
              <div class="col-md-10">
                  <div class="text-content">
                      <div class="row">
                          <div class="col-12 text-center">
                              <h1 class="mb-6 fw-medium title-section">Vanilla Clinic - Vagina Laboratory</h1>
                          </div>
                      </div>

                      <p class="fw-medium">Tidak kelihatan bukan berarti tidak penting untuk diperhatikan. Perawatan organ intim wanita penting dilakukan untuk menjaga organ kewanitaan tetap sehat, indah, bersih, kencang dan terawat. Seiring dengan bertambahnya usia, perubahan berat badan, gaya hidup yang tidak seimbang, proses kehamilan hingga melahirkan dapat mempengaruhi bentuk dan fungsi organ kewanitaan Anda.</p>
                          <br>
                      <p class="fw-medium">Organ intim yang selalu terpelihara dengan baik dapat mencegah munculnya berbagai masalah kesehatan di kemudian hari dan dapat meningkatkan kepercayaan diri serta keharmonisan rumah tangga.</p>
                  </div>
              </div>
          </div>
      </div>

      <div class="container sc-layanan-wrap full-wrap sc-layanan">
          <div class="row justify-content-center">
            <div class="col-md-12 text-content text-center mb-4">
              <h4 class="fw-bold">Layanan Vanilla Clinic:</h4>
            </div>

            <div class="col-md-4 mb-4">
                <div class="sc-layanan-item">
                  <div class="text-content text-center">
                      <h4 class="fw-bold mb-6 title-section">Senam Kegel</h4>
                      <p class="fw-medium">Vanilla Spa (Massage, Vaginal Electrical Stimulation, Ozone Vagina)</p>
                  </div>
                </div>
              </div>
              <div class="col-md-4 mb-4">
                <div class="sc-layanan-item">
                  <div class="text-content text-center">
                      <h4 class="fw-bold mb-6 title-section">Kegel Perineometry</h4>
                      <p class="fw-medium">Vaksin HPV (optional)</p>
                  </div>
                </div>
              </div>
          </div>
      </div>
    </section>


    <section class="py-5">
      <div class="container text-center">
        <h4 class="fw-bold">Buat janji untuk perawatan organ kewanitaan Anda bersama Kamirawat sekarang!</h4>
        <a href="{{ route('front.index') }}/patient/booking/2#nav-head" class="btn btn-primary m-1 fw-bold">Booking Now</a>
      </div>
    </section>


    <!-- ============================================-->
    <!-- <section> begin ============================-->
    <section class="p-0">
        <img src="{{ asset('assets/frontend/img/footer.jpg') }}" class="img-100" alt="">
    </section>
    <!-- <section> close ============================-->
    <!-- ============================================-->


@endsection
