@extends('front.layout.main')

@section('content')
    <!-- ============================================-->
    <!-- <section> begin ============================-->
    <div id="home">
        <div class="container-fluid p-0">
            <div class="hero-slider m-0">
              <div class="slider-item">
                    <img src="{{ asset('assets/frontend/img/slider/slider-1.jpg') }}" alt="">
                </div>
                <div class="slider-item">
                    <img src="{{ asset('assets/frontend/img/slider/slider-2.jpg') }}" alt="">
                </div>
                <div class="slider-item">
                    <img src="{{ asset('assets/frontend/img/slider/slider-3.jpg') }}" alt="">
                </div>
            </div>
        </div>
    </div>
    <!-- <section> close ============================-->
    <!-- ============================================-->

    <section>
      <div class="container">
        <div class="row mb-6">
            <div class="col-12 text-center">
                <h1 class="mb-6 fw-medium title-section">Promo</h1>
            </div>
        </div>


        <div id="promo" class="row">

          <div class="item col-xs-4 col-lg-4">
            <a href="#">
              <div class="item-c">
                  <div class="thumbnail">
                    <img class="group list-group-image" src="{{ asset('assets/frontend/img/kunjungan-dokter.jpeg') }}" alt="" />
                  </div>
                  <div class="caption">
                      <h4 class="group inner list-group-item-heading fw-bold">
                          Promo title</h4>
                      <p class="group inner list-group-item-text">
                          Lorem ipsum dolor sit amet, consectetuer adipiscing elit,
                          sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat....</p>
                  </div>
              </div>
            </a>
          </div>

          <div class="item col-xs-4 col-lg-4">
            <a href="#">
              <div class="item-c">
                  <div class="thumbnail">
                    <img class="group list-group-image" src="{{ asset('assets/frontend/img/kunjungan-dokter.jpeg') }}" alt="" />
                  </div>
                  <div class="caption">
                      <h4 class="group inner list-group-item-heading fw-bold">
                          Promo title</h4>
                      <p class="group inner list-group-item-text">
                          Lorem ipsum dolor sit amet, consectetuer adipiscing elit,
                          sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat....</p>
                  </div>
              </div>
            </a>
          </div>

          <div class="item col-xs-4 col-lg-4">
            <a href="#">
              <div class="item-c">
                  <div class="thumbnail">
                    <img class="group list-group-image" src="{{ asset('assets/frontend/img/kunjungan-dokter.jpeg') }}" alt="" />
                  </div>
                  <div class="caption">
                      <h4 class="group inner list-group-item-heading fw-bold">
                          Promo title</h4>
                      <p class="group inner list-group-item-text">
                          Lorem ipsum dolor sit amet, consectetuer adipiscing elit,
                          sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat....</p>
                  </div>
              </div>
            </a>
          </div>

        </div>
      </div>
    </section>



    <!-- ============================================-->
    <!-- <section> begin ============================-->
    <section class="p-0">
        <img src="{{ asset('assets/frontend/img/footer.jpg') }}" class="img-100" alt="">
    </section>
    <!-- <section> close ============================-->
    <!-- ============================================-->


@endsection
