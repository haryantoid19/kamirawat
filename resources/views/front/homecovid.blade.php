@extends('front.layout.main')

@section('content')


    <section>
      <div class="container mb-6">
          <div class="row justify-content-center">
              <div class="col-md-10">
                  <div class="text-content">
                      <div class="row">
                          <div class="col-12 text-center">
                              <h1 class="mb-6 fw-medium title-section">Home Covid</h1>
                          </div>
                      </div>

                      <p class="fw-medium">Home COVID adalah layanan kesehatan berupa pendampingan bagi pasien positif COVID-19 gejala ringan hingga sedang, yang melakukan isolasi mandiri di rumah. Layanan ini terdiri dari konsultasi dokter, tes swab PCR, pemberian multivitamin, kunjungan dokter dan perawatan oleh perawat medis.</p>
                  </div>
              </div>
          </div>
      </div>

      <div class="container sc-layanan-wrap full-wrap sc-layanan">
          <div class="row justify-content-center">
              <div class="col-md-12 text-content text-center mb-4">
                <h4 class="fw-bold">Layanan yang diberikan selama Home COVID meliputi:</h4>
              </div>

              <div class="col-md-4 mb-4">
                <div class="sc-layanan-item">
                  <div class="text-content text-center">
                      <h4 class="fw-medium">Memeriksa keadaan pasien (kunjungan dokter dan perawat ke rumah)</h4>
                  </div>
                </div>
              </div>
              <div class="col-md-4 mb-4">
                <div class="sc-layanan-item">
                  <div class="text-content text-center">
                        <h4 class="fw-medium">Memberikan arahan mengenai penanganan medis yang diperlukan berdasarkan hasil pemeriksaan</h4>
                  </div>
                </div>
              </div>
              <div class="col-md-4 mb-4">
                <div class="sc-layanan-item">
                  <div class="text-content text-center">
                      <h4 class="fw-medium">Menyuplai kebutuhan obat dan vitamin pasien sesuai instruksi dokter</h4>
                  </div>
                </div>
              </div>
          </div>
      </div>
    </section>

    <section class="py-5">
      <div class="container text-center">
        <h4 class="fw-bold">Buat janji temu untuk kunjungan Perawat Kamirawat sekarang!</h4>
        <a href="{{ route('front.index') }}/patient/booking/4#nav-head" class="btn btn-primary m-1 fw-bold">Booking Now</a>
      </div>
    </section>


    <!-- ============================================-->
    <!-- <section> begin ============================-->
    <section class="p-0">
        <img src="{{ asset('assets/frontend/img/footer.jpg') }}" class="img-100" alt="">
    </section>
    <!-- <section> close ============================-->
    <!-- ============================================-->


@endsection
