@extends('front.layout.main') @section('content')
<!-- ============================================-->
<!-- <section> begin ============================-->

<section>
  <div class="container text-justify">
    <div class="row">
      <div class="col-12 text-center">
        <h2 class="mb-6 fw-medium title-section">
          Tentang Kami
        </h2>
      </div>
    </div>
    <div class="row">
    <p class="fw-medium"><strong>Kamirawat</strong> hadir untuk Anda yang ingin merasakan layanan kesehatan primer langsung ke lokasi Anda. Lupakan antrian panjang di Rumah Sakit, lupakan rumitnya membuat janji temu ke Dokter favorit dan lupakan sulitnya menebus obat di Apotik. <strong>Kamirawat</strong> akan membantu Anda dan Orang Tersayang hanya dengan satu aplikasi.</p>
                            <br>
                        <p class="fw-medium">Melalui filosofi, <strong>“Sentuhan Penuh Perhatian”</strong> kami berupaya menghadirkan layanan perawatan kesehatan dengan kepedulian tinggi dan sepenuh hati. Didukung oleh Dokter dan Tenaga Medis yang profesional, berpengalaman dan tersertifikasi, <strong>Kamirawat</strong> memiliki beberapa layanan andalan, antara lain Home Care, Home Covid, Klinik Geriatri, Klinik Perawatan Luka, Klinik Penyakit Kronik dan Klinik Vanilla (Vagina Laboratory).</p>
                        <br>
                        <p class="fw-medium">Percayakan kesehatan Anda bersama <strong>Kamirawat</strong>, mulai hari ini! Promo kesehatan lainnya, dapat dilihat di sini.</p>
    </div>
  </div>
</section>

@endsection
