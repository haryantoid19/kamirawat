@extends('front.layout.main') @section('content')
<!-- ============================================-->
<!-- <section> begin ============================-->

<section>
  <div class="container">
    <div class="row">
      <div class="col-12 text-center">
        <h2 class="mb-6 fw-medium title-section">
          FAQ kamirawat.com
        </h2>
      </div>
    </div>
    <div class="row">
    <div class="accordion" id="accordionKamirawat">

  <div class="accordion-item">
    <h2 class="accordion-header" id="headingOne">
      <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapse1" aria-expanded="true" aria-controls="collapse1">
      <strong>1. Bagaimana cara mendaftar sebagai pasien di kamirawat?</strong>
      </button>
    </h2>
    <div id="collapse1" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionKamirawat">
      <div class="accordion-body">
        Anda dapat daftar dengan klik tombol “Masuk” dan isi data mulai dari Nama, No Handphone, Kewarganegaraan, No KTP, Tanggal Lahir, Jenis Kelamin, Alamat Email, Alamat Domisili dan tentukan Kata Sandi dengan minimal 8 huruf.
      </div>
    </div>
  </div>

  <div class="accordion-item">
    <h2 class="accordion-header" id="headingTwo">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse2" aria-expanded="false" aria-controls="collapse2">
      <strong> 2. Saya tidak bisa mendaftarkan akun di kamirawat?</strong>
      </button>
    </h2>
    <div id="collapse2" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionKamirawat">
      <div class="accordion-body">
        Anda dapat mengirimkan pesan melalui whatsapp ke nomor +6281909-201120 atau email ke info@kamirawat.com untuk mendapatkan bantuan.
      </div>
    </div>
  </div>
  <div class="accordion-item">
    <h2 class="accordion-header" id="headingThree">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse3" aria-expanded="false" aria-controls="collapse3">
      <strong>3. Bagaimana cara menggunakan layanan di KamiRawat?</strong>
      </button>
    </h2>
    <div id="collapse3" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionKamirawat">
      <div class="accordion-body">
        Buka alamat website www.kamirawat.com , masuk ke akun yang sudah Anda buat, pilih layanan yang diinginkan, kemudian ikuti petunjuk yang ada.
      </div>
    </div>
  </div>

  <div class="accordion-item">
    <h2 class="accordion-header" id="headingThree">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse4" aria-expanded="false" aria-controls="collapse4">
      <strong>4. Bisakah saya melakukan pemesanan layanan home visit tanpa melalui website KamiRawat?</strong>
      </button>
    </h2>
    <div id="collapse4" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionKamirawat">
      <div class="accordion-body">
        Tidak bisa, Anda harus mendaftar terlebih dahulu.
      </div>
    </div>
  </div>

  <div class="accordion-item">
    <h2 class="accordion-header" id="headingThree">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse5" aria-expanded="false" aria-controls="collapse5">
      <strong>5. Apa yang harus saya lakukan setelah melakukan pemesanan pelayangan?</strong>
      </button>
    </h2>
    <div id="collapse5" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionKamirawat">
      <div class="accordion-body">
        Anda cukup menunggu konfirmasi via Whatsapp/email yang dikirim oleh Tim Kamirawat setelah pemesanan dilakukan.
      </div>
    </div>
  </div>

  <div class="accordion-item">
    <h2 class="accordion-header" id="headingThree">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse6" aria-expanded="false" aria-controls="collapse6">
      <strong>6. Apakah saya bisa membuat pesanan untuk orang lain? </strong>
      </button>
    </h2>
    <div id="collapse6" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionKamirawat">
      <div class="accordion-body">
        Bisa, pada saat melakukan pemesanan, pada saat mengisi data pasien  Anda bisa memilih opsi “Saya memesan untuk orang lain”, kemudian silahkan lengkapi data sesuai dengan orang yang ingin Anda pesankan.
      </div>
    </div>
  </div>


  <div class="accordion-item">
    <h2 class="accordion-header" id="headingThree">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse7" aria-expanded="false" aria-controls="collapse7">
      <strong>7. Jika saya ingin mengunjungi klinik kamirawat, apakah saya harus mendaftar melalui website?</strong>
      </button>
    </h2>
    <div id="collapse7" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionKamirawat">
      <div class="accordion-body">
        Anda bisa melakukan pemesanan layanan klinik kamirawat melalui website atau langsung datang ke lokasi di Jl. Benda Raya, no. 12D. Jakarta Selatan.
      </div>
    </div>
  </div>

  <div class="accordion-item">
    <h2 class="accordion-header" id="headingThree">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse8" aria-expanded="false" aria-controls="collapse8">
      <strong>8. Bagaimana cara pembayaran kamirawat?</strong>
      </button>
    </h2>
    <div id="collapse8" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionKamirawat">
      <div class="accordion-body">
        Anda bisa melakukan pembayaran melalui bank transfer ke rekening yang tersedia.
      </div>
    </div>
  </div>

  <div class="accordion-item">
    <h2 class="accordion-header" id="headingThree">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse9" aria-expanded="false" aria-controls="collapse9">
      <strong>9. Bisakah saya menggunakan asuransi medis saya untuk membayar pelayanan di KamiRawat? </strong>
      </button>
    </h2>
    <div id="collapse9" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionKamirawat">
      <div class="accordion-body">
        Tidak bisa, sayangnya untuk saat ini pembayaran menggunakan asuransi belum tersedia di KamiRawat
      </div>
    </div>
  </div>

  <div class="accordion-item">
    <h2 class="accordion-header" id="headingThree">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse10" aria-expanded="false" aria-controls="collapse10">
      <strong>10. Berapa lama dokter atau tenaga medis sampai ke lokasi pasien?</strong>
      </button>
    </h2>
    <div id="collapse10" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionKamirawat">
      <div class="accordion-body">
        Tenaga medis akan sampai ke lokasi paling lambat 2 jam setelah pemesanan
      </div>
    </div>
  </div>

  <div class="accordion-item">
    <h2 class="accordion-header" id="headingThree">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse11" aria-expanded="false" aria-controls="collapse11">
      <strong>11. Jika saya masih sakit apakah ada follow up kembali?</strong>
      </button>
    </h2>
    <div id="collapse11" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionKamirawat">
      <div class="accordion-body">
        KamiRawat memiliki Post Care Management 2x 24 jam yang berfungsi untuk memonitor kondisi kesehatan pasien pasca kunjungan dokter atau tenaga medis kamirawat. 
      </div>
    </div>
  </div>

  <div class="accordion-item">
    <h2 class="accordion-header" id="headingThree">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse12" aria-expanded="false" aria-controls="collapse12">
      <strong>12. Apakah saya bisa mendapat rujukan untuk tindakan atau konsultasi yang lebih komprehensif?</strong>
      </button>
    </h2>
    <div id="collapse12" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionKamirawat">
      <div class="accordion-body">
        Bisa, dokter dan Tenaga Medis kamirawat juga dapat memberikan rujukan pemeriksaan lanjutan ke Laboratorium, Radiologi, atau ke Dokter Spesialis tertentu jika kondisi penyakit pasien dirasa membutuhkan penanganan lebih lanjut.
      </div>
    </div>
  </div>

  <div class="accordion-item">
    <h2 class="accordion-header" id="headingThree">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse13" aria-expanded="false" aria-controls="collapse13">
      <strong>13. Apakah saya bisa melihat rekam medis di KamiRawat?</strong>
      </button>
    </h2>
    <div id="collapse13" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionKamirawat">
      <div class="accordion-body">
        Rekam medis Anda secara otomatis akan tersimpan di dalam akun Profil Anda. Namun, saat ini rekam medis Anda hanya dapat digunakan dalam aplikasi kamirawat.
      </div>
    </div>
  </div>

  <div class="accordion-item">
    <h2 class="accordion-header" id="headingThree">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse14" aria-expanded="false" aria-controls="collapse14">
      <strong>14. Apakah saya bisa memilih perawat untuk bekerja di rumah dalam periode bulanan?</strong>
      </button>
    </h2>
    <div id="collapse14" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionKamirawat">
      <div class="accordion-body">
        Bisa, perawat kami dapat melakukan layanan home care, mulai dari harian, mingguan dan bulanan. Besaran tarif disesuaikan dengan periode yang ditentukan sebelumnya oleh pasien.
      </div>
    </div>
  </div>

  <div class="accordion-item">
    <h2 class="accordion-header" id="headingThree">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse15" aria-expanded="false" aria-controls="collapse15">
      <strong>15. Apa saja layanan yang dapat dilakukan di rumah?</strong>
      </button>
    </h2>
    <div id="collapse15" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionKamirawat">
      <div class="accordion-body">
        Untuk layanan yang bisa dilakukan di rumah adalah: 
        <br>
        Home Visit: Kunjungan dokter, Kunjungan Perawat
        <br>
        Home Care: Layanan Geriatri, Perawatan Ibu dan Bayi, Vanilla Care (Vagina Laboratory)
        <br>
        Home COVID: Rapid Test Antigen & PCR, Paket Isolasi COVID-19
      </div>
    </div>
  </div>

  <div class="accordion-item">
    <h2 class="accordion-header" id="headingThree">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse16" aria-expanded="false" aria-controls="collapse16">
      <strong>16. Apa saja layanan yang dapat dilakukan di Klinik KamiRawat?</strong>
      </button>
    </h2>
    <div id="collapse16" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionKamirawat">
      <div class="accordion-body">
        Klinik KamiRawat menyediakan layanan
        <br>
        Calm Clinic (Counseling & Living Mind Center): Psikologi, Psikiater, Sexolog & Sexual Educator
        <br>
        Dokter Umum
        <br>
        Klinik Diabetes
      </div>
    </div>
  </div>

  <div class="accordion-item">
    <h2 class="accordion-header" id="headingThree">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse17" aria-expanded="false" aria-controls="collapse17">
      <strong>17. Apakah Kami Rawat bisa merawat pasien Covid yang melakukan karantina mandiri di rumah?</strong>
      </button>
    </h2>
    <div id="collapse17" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionKamirawat">
      <div class="accordion-body">
        Bisa. Dokter dan Tenaga Medis kami berpengalaman dalam mengobati pasien yang terinfeksi Virus SARS-CoV-2 (COVID-19). Protokol kesehatan yang ketat diterapkan oleh seluruh tim kamirawat, salah satunya pasien wajib mengisi form screening kondisi pasien sebelum kedatangan Dokter dan Tenaga Medis.
      </div>
    </div>
  </div>

  <div class="accordion-item">
    <h2 class="accordion-header" id="headingThree">
      <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse18" aria-expanded="false" aria-controls="collapse18">
      <strong>18. Kendala lain?</strong>
      </button>
    </h2>
    <div id="collapse18" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionKamirawat">
      <div class="accordion-body">
        Anda dapat mengirimkan pesan melalui whatsapp ke nomor +6281909-201120 atau email ke info@kamirawat.com untuk mendapatkan bantuan.
      </div>
    </div>
  </div>

</div>
    </div>
  </div>
</section>

@endsection
