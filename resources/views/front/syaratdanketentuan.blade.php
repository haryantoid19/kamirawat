@extends('front.layout.main') @section('content')
<!-- ============================================-->
<!-- <section> begin ============================-->

<section>
  <div class="container content-in text-justify">
    <div class="row">
      <div class="col-12 text-center">
        <h2 class="mb-6 fw-medium title-section">
          <b>SYARAT DAN KETENTUAN<br>
          kamirawat.com</b>
        </h2>
      </div>
    </div>
    <div class="row">
      <div class="col-md-8 m-auto">
      <p>MOHON UNTUK MEMBACA, MEMAHAMI DAN MEMERIKSA KETENTUAN PENGGUNAAN DAN KEBIJAKAN PRIVASI KAMI 
        DENGAN SEKSAMA SEBELUM MENDAFTAR DAN MENGGUNAKAN LAYANAN KAMI UNTUK PERTAMA KALI.</p>

      <br>

      <h5><b>PENDAHULUAN</b></h5>
      <p>Selamat datang di website kami. Situs ini (“Situs”) dioperasikan oleh PT Arsatya Jaya Medika 
        (“AJM”, “kami”, atau “milik kami”). Ketentuan ini membuat perjanjian mengikat secara hukum antara 
        Anda dan AJM dan afiliasinya mengenai penggunaan Anda terhadap Situs.</p>

      <br>

      <h5><b>KETENTUAN PENGGUNAAN</b></h5>
      <p>Dengan menggunakan situs Kamirawat.com (“Situs”), Anda setuju bahwa Anda telah membaca, 
        memahami dan menerima Ketentuan Penggunaan ini (“Ketentuan Penggunaan”). Ketentuan Penggunaan 
        ini merupakan suatu perjanjian yang sah antara Anda dengan PT Arsatya Jaya Medika (“Perseroan”) 
        berlaku pada kunjungan dan penggunaan Anda pada situs Kami di Kamirawat.com. Silahkan membatalkan 
        pendaftaran akun Anda, jika Anda tidak setuju atau tidak ingin masuk ke dalam Ketentuan Penggunaan 
        dan Kebijakan Privasi Kami.</p>

        <ol style="list-style-type: upper-roman;">
          <li class="li-bold"><b>HAL-HAL UMUM</b>
            <ol style="list-style-type:upper-alpha;">
              <li><b>PT ARSATYA JAYA MEDIKA</b><br>
              adalah Perseroan Terbatas yang didirikan berdasarkan hukum Negara Republik Indonesia.</li>
              <li>Situs tersebut merupakan sarana untuk kemudahan pelayanan Kesehatan dan mempertemukan 
                antara Pasien dengan Dokter, Perawat, Psikolog dan Klinik. Situs tersebut menawarkan informasi 
                tentang layanan Kesehatan yang ditawarkan oleh Penyedia Layanan. Jenis layanan yang tersedia adalah:

                <ol>
                  <li>Home Visit + Home Care;

                    <ol class="m-0" style="list-style-type:lower-alpha;">
                      <li>Doctor Visit.</li>
                      <li>Nurse Visit.</li>
                      <li>Geriatric Clinic.</li>
                      <li>Outpatient</li>
                    </ol>
                  </li>
                  <li>V-Care;

                    <ol class="m-0" style="list-style-type:lower-alpha;">
                      <li>Brightening.</li>
                      <li>Laser.</li>
                      <li>Inject Miss V.</li>
                      <li>Spa.</li>
                      <li>Tightening.</li>
                      <li>Totok Miss V.</li>
                    </ol>
                  </li>
                  <li>General Practitioners;</li>
                  <li>Doctor Specialist;

                    <ol class="m-0" style="list-style-type:lower-alpha;">
                      <li>Doctor Aesthetic.</li>
                      <li>Doctor Heart.</li>
                      <li>Doctor Internist.</li>
                      <li>Doctor Obgyn.</li>
                      <li>Doctor Psychology.</li>
                      <li>Neurologist.</li>
                    </ol>
                  </li>
                  <li>Consultation, Education and Therapy for Reproductive Problems and Sexual Activity.</li>
                  <li>Pharmacy

                    <ol class="m-0" style="list-style-type:lower-alpha;">
                      <li>Medicine.</li>
                      <li>Supplements.</li>
                      <li>Vaccines.</li>
                      <li>Vitamins.</li>
                    </ol>
                  </li>
                  <li>Layanan lain yang dapat Kami tambahkan dari waktu ke waktu.</li>
                </ol>
              </li>
              <li>Situs tersebut memungkinkan Anda untuk mengirimkan permintaan untuk suatu Layanan kepada Penyedia Layanan. 
                Penyedia Layanan memiliki kebijakan sendiri dan menyeluruh untuk menerima atau menolak setiap permintaan Anda 
                atas Layanan. Penyedia Layanan juga memiliki kebijakannya sendiri dan menyeluruh untuk memilih dan menerima 
                arahan-arahan yang diberikan oleh situs tersebut. Jika Penyedia Layanan menerima permintaan Anda, Penyedia Layanan 
                akan memberitahu Anda.</li>
              <li>Kami akan melakukan semua upaya wajar untuk menghubungkan Anda dengan Penyedia Layanan untuk mendapatkan Layanan, 
                tergantung kepada keberadaan Penyedia Layanan di atau di sekitar lokasi Anda pada saat Anda melakukan pemesanan Layanan.</li>
              <li>E.	Kami adalah perusahaan penyedia layanan Kesehatan dan <b>KAMI HANYA MEMBERIKAN LAYANAN KESEHATAN PADA KLINIK KAMI.</b> 
                Situs tersebut hanya sebagai perantara antara Pasien dengan Penyedia Layanan (“Dokter, Perawat dan Psikolog”). 
                <b>KAMI TIDAK BERTANGGUNG JAWAB ATAS SETIAP TINDAKAN DAN/ATAU KELALAIAN PENYEDIA LAYANAN.</b> Situs tersebut hanya merupakan 
                sarana untuk memudahkan pencarian atas Layanan. Adalah tergantung pada Penyedia Layanan untuk menawarkan Layanan kepada 
                Anda dan tergantung pada Anda apakah Anda akan menerima tawaran Layanan dari Penyedia Layanan.</li>
            </ol>
          </li>
          <li>KETENTUAN UNTUK MENGGUNAKAN SITUS KAMIRAWAT.COM

            <ol style="list-style-type:upper-alpha;">
              <li>Anda menyatakan dan menjamin bahwa Anda adalah individu yang secara hukum berhak untuk mengadakan perjanjian yang mengikat 
                berdasarkan hukum Negara Republik Indonesia, khususnya Ketentuan Penggunaan, untuk menggunakan situs tersebut dan bahwa Anda 
                telah berusia minimal dua puluh satu (21) tahun atau sudah menikah dan tidak berada di bawah perwalian. Jika tidak, Kami atau 
                Penyedia Layanan terkait, berhak berdasarkan hukum untuk membatalkan perjanjian yang dibuat dengan Anda.
                <br>
                Anda selanjutnya menyatakan dan menjamin bahwa Anda memiliki hak, wewenang dan kapasitas untuk menggunakan 
                Layanan dan mematuhi Ketentuan Penggunaan. Jika Anda mendaftarkan atas nama suatu badan hukum, Anda juga menyatakan dan
                menjamin bahwa Anda berwenang untuk mengadakan, dan mengikatkan diri entitas tersebut pada Ketentuan Penggunaan ini dan 
                mendaftarkan untuk Layanan.</li>
              <li>Kami mengumpulkan dan memproses informasi pribadi Anda, seperti nama, nomor identitas, tanggal lahir, jenis kelamin, 
                alamat surat elektronik (“e-mail”), alamat tempat tinggal, nomor Surat Tanda Registrasi, nomor Surat Izin Praktik, dan nomor 
                telepon seluler Anda ketika Anda mendaftar. Anda harus memberikan informasi yang akurat dan lengkap, memperbaharui informasi 
                dan setuju untuk memberikan kepada Kami bukti identitas apapun yang secara wajar dapat Kami mintakan. 
                <br>
                Jika informasi pribadi yang Anda berikan kepada Kami ada yang berubah, misalnya, jika Anda mengubah alamat surat elektronik 
                (“e-mail”), nomor telepon, atau jika Anda ingin membatalkan akun Anda, mohon perbaharui rincian informasi Anda dengan mengirimkan 
                permintaan Anda kepada Kami. Kami akan, sepanjang yang dapat Kami lakukan, memberlakukan perubahan yang diminta tersebut dalam waktu 
                empat belas (14) hari kerja sejak diterimanya pemberitahuan perubahan.</li>
              <li>Anda hanya dapat menggunakan situs tersebut Ketika Anda telah mendaftar pada situs tersebut. Setelah Anda berhasil mendaftarkan 
                diri, situs tersebut akan memberikan Anda suatu akun pribadi yang dapat diakses dengan kata sandi yang Anda pilih.</li>
              <li>Hanya Anda yang dapat menggunakan akun Anda sendiri dan Anda berjanji untuk tidak memberikan wewenang kepada orang lain untuk 
                menggunakan identitas Anda atau menggunakan akun Anda. Anda tidak dapat menyerahkan atau mengalihkan akun Anda kepada pihak lain. 
                Anda harus menjaga keamanan dan kerahasiaan kata sandi akun Anda dan setiap identifikasi yang Kami berikan kepada Anda. Dalam hal 
                terjadi pengungkapan atas kata sandi Anda, dengan cara apapun, yang mengakibatkan setiap penggunaan yang tidak sah atau tanpa kewenangan 
                atas akun atau identitas Anda, pesanan yang diterima dari penggunaan yang tidak sah atau tanpa kewenangan tersebut masih akan dianggap 
                sebagai pesanan yang sah, kecuali Anda memberitahu Kami tentang mengenai hal tersebut sebelum Penyedia Layanan memberikan Layanan yang diminta.</li>
              <li>Anda hanya dapat memiliki satu akun KAMIRAWAT</li>
              <li>Anda berjanji bahwa Anda akan menggunakan situs tersebut hanya untuk tujuan yang dimaksud untuk mendapatkan Layanan Kesehatan. 
                Anda tidak diperbolehkan untuk menyalahgunakan atau menggunakan Situs untuk tujuan penipuan atau menyebabkan ketidaknyamanan kepada 
                orang lain atau melakukan pemesanan palsu.</li>
              <li>Jika Anda juga adalah seorang Penyedia Layanan, Anda tidak dapat menggunakan akun konsumen Anda sendiri (atau akun milik konsumen orang lain) 
                untuk melakukan pemesanan yang akan Anda terima sendiri sebagai seorang Penyedia Layanan.</li>
              <li>Anda tidak diperkenankan untuk membahayakan, mengubah atau memodifikasi situs tersebut dengan cara apapun.</li>  
              <li>Anda akan menjaga kerahasiaan dan tidak akan menyalahgunakan informasi yang Anda terima dari penggunaan situs tersebut.</li>
              <li>ANDA BERJANJI BAHWA ANDA AKAN MEMPERLAKUKAN PENYEDIA LAYANAN DENGAN HORMAT DAN TIDAK AKAN TERLIBAT DALAM PERILAKU ATAU TINDAKAN 
                YANG TIDAK SAH, MENGANCAM ATAU MELECEHKAN KETIKA MENGGUNAKAN LAYANAN MEREKA</li>
              <li>Anda memahami dan setuju bahwa penggunaan situs tersebut oleh Anda akan tunduk pula pada Kebijakan Privasi Kami sebagaimana dapat 
                diubah dari waktu ke waktu. Dengan menggunakan situs tersebut, Anda juga memberikan persetujuan sebagaimana dipersyaratkan berdasarkan Kebijakan Privasi Kami.</li>
              <li>Dengan memberikan informasi kepada Kami, Anda menyatakan bahwa Anda berhak untuk memberikan kepada Kami informasi yang akan Kami gunakan dan berikan kepada Penyedia Layanan.</li>
              <li>Situs tersebut tidak boleh dipergunakan untuk:
                <ol class="m-0">
                  <li>Memberikan surat keterangan sakit dari dokter tanpa indikasi atau kondisi yang sesuai dengan keadaan yang sebenarnya.</li>
                  <li>Memberikan obat penenang dan/atau obat golongan tertentu yang peruntukannya berpotensi untuk disalahgunakan tanpa indikasi atau kondisi yang sesuai dengan keadaan yang sebenarnya.</li>
                  <li>Melakukan layanan kesehatan dengan maksud untuk mengambil keuntungan atas klaim asuransi yang dimiliki peserta dan/atau pengguna situs tersebut secara sengaja, tanpa indikasi atau kondisi yang sesuai dengan keadaan yang sebenarnya.</li>
                  <li>Memberikan layanan dengan maksud lain disamping konsultasi layanan kesehatan yang sesuai dengan indikasi dan keadaan yang sebenarnya.</li>
                  <li>Melakukan tindakan yang melawan perudangan-undangan yang berlaku seperti melakukan Aborsi atau tindakan lain yang bertentangan dengan peraturan yang ada.</li>
                </ol>
              </li>
              <li>Kami dan/atau Penyedia Layanan yang terkait berhak menolak untuk menerima pesanan Anda jika Kami memiliki alasan yang wajar untuk mencurigai bahwa 
                Anda telah, atau dengan menerima pesanan dari Anda, Anda akan melanggar Ketentuan Penggunaan ini atau hukum dan peraturan perundang-undangan yang berlaku.</li>
              <li>Anda mengakui dan setuju untuk memberikan kepada Penyedia Layanan untuk melakukan tindakan pelayanan Kesehatan sesuai dengan indikasi dan keluhan yang Anda sampaikan.</li>
              <li>Anda mengakui dan memahami bahwa tarif atas layanan Kesehatan yang disediakan pada situs tersebut merupakan perkiraan dan dapat berubah dari waktu ke waktu.</li>
              <li>Anda setuju dan mengakui bahwa Anda akan membayar sesuai dengan tagihan yang Anda terima pada e-mail dan/atau situs tersebut yang diterbitkan.</li>
              <li>Mohon menginformasikan kepada Kami jika Anda tidak lagi memiliki kontrol atas akun Anda, sebagai contoh akun Anda dengan cara bagaimanapun diretas (hack) atau 
                telepon Anda hilang atau dicuri, sehingga Kami dapat membatalkan akun Anda dengan sebagaimana mestinya. </li>
              <li>MOHON DIPERHATIKAN BAHWA ANDA BERTANGGUNG JAWAB ATAS PENGGUNAAN AKUN ANDA DAN ANDA MUNGKIN DAPAT DIMINTAKAN TANGGUNG JAWABNYA MERSKIPUN JIKA AKUN ANDA TERSEBUT DISALAHGUNAKAN OLEH ORANG LAIN.</li>
              <li>Jaminan Kami tidak memberikan pernyataan, jaminan atau garansi untuk dapat diandalkannya, ketepatan waktu, kualitas, kesesuaian, ketersediaan, akurasi atau kelengkapan dari Layanan.</li>
              <li>Kami tidak menyatakan atau menjamin bahwa:

                <ol>
                  <li>Penggunaan situs tersebut akan aman, tepat waktu, tanpa gangguan atau terbebas dari kesalahan atau beroperasi dengan kombinasi dengan perangkat keras lain, perangkat lunak, sistem atau data.</li>
                  <li>Layanan akan memenuhi kebutuhan atau harapan Anda.</li>
                  <li>Setiap data yang tersimpan akan akurat atau dapat diandalkan.</li>
                  <li>Kualitas produk, layanan, informasi, atau bahan-bahan lain yang dibeli atau diperoleh oleh Anda melalui situs tersebut akan memenuhi kebutuhan atau harapan Anda.</li>
                  <li>Kesalahan atau kecacatan dalam situs tersebut akan diperbaiki.</li>
                  <li>Situs tersebut terbebas dari virus atau komponen berbahaya lainnya.</li>
                  <li>Semua kondisi, pernyataan dan jaminan, baik tersurat maupun tersirat, yang diwajibkan oleh undang-undang atau sebaliknya, termasuk, 
                    namun tidak terbatas pada, jaminan yang tersirat mengenai layanan kesehatan, kesesuaian untuk tujuan tertentu, atau tidak adanya pelanggaran 
                    hak pihak ketiga, dengan ini dikecualikan dan dikesampingkan dengan batas tertinggi dan maksimum. Anda mengakui dan menyetujui 
                    bahwa seluruh risiko yang timbul dari penggunaan Layanan oleh Anda tetap semata-mata dan sepenuhnya ada pada Anda dan Anda tidak akan memiliki hak untuk meminta ganti rugi apapun dari Kami.</li>
                </ol>
              </li>
            </ol>
          </li>
          <li>PEMBAYARAN

            <ol style="list-style-type:upper-alpha;">
              <li>Penggunaan situs tersebut adalah bebas biaya. Namun demikian, koneksi internet yang dibutuhkan untuk menggunakan situs tersebut 
                adalah tanggung jawab eksklusif Anda dan semata-mata dibebankan kepada Anda.</li>
              <li>Tarif yang berlaku untuk Layanan oleh Penyedia Layanan dapat ditemukan pada situs tersebut. Kami dapat mengubah atau memperbaharui 
                tarif dari waktu ke waktu. Kami akan membantu Penyedia Layanan untuk menghitung biaya berdasarkan pesanan Anda dan memberitahu Anda 
                tentang biaya atas nama Penyedia Layanan.</li>
              <li>Anda setuju bahwa Anda akan membayar Layanan yang diberikan kepada Anda oleh Penyedia Layanan secara penuh dan lunas.</li>
              <li>Pembayaran dapat dilakukan secara tunai atau dengan transfer atau kartu kredit atau dengan menggunakan KAMIRAWAT Credit yang akan 
                dibuat di kemudian hari. Semua pembayaran tunai harus dilakukan dalam Rupiah. KAMIRAWAT Credit dapat diperoleh dengan mentransfer 
                jumlah yang Anda inginkan ke rekening BANK MANDIRI atas nama <b>ARSATYA JAYA MEDIKA</b> dengan nomor rekening <b>1220055565652 Cabang Senayan City Mall</b>.</li>
              <li>KAMIRAWAT bukan merupakan perusahaan milik pemerintah. Kami tidak melayani pembayaran Layanan dalam bentuk klaim dari asuransi 
                apapun dan manapun, kecuali disebutkan oleh provider asuransi tersebut bahwa premi asuransi yang Anda miliki mencakup layanan 
                kesehatan yang dilakukan melalui KAMIRAWAT.</li>
              <li>Dalam hal prosedur reimbursement (baik oleh asuransi maupun perusahaan yang menanggung biaya kesehatan Anda), Anda dapat 
                meminta dokumen-dokumen persyaratan yang diperlukan kepada Penyedia Layanan. Dengan demikian, Anda setuju dan secara sadar memberikan 
                izin pelepasan data rekam medis kepada pihak yang Anda beri wewenang. Dalam hal ini, segala bentuk penyelewengan atau penyalahgunaan 
                data oleh pihak yang Anda beri wewenang bukan tanggung jawab Kami.</li>  
              <li>Biaya untuk Layanan mencakup pajak yang berlaku.</li>  
            </ol>
          </li>
          <li>TANGGUNG JAWAB KAMI

            <ol style="list-style-type:upper-alpha;">
              <li>Kami tidak bertanggung jawab atas setiap cidera, kematian, kerusakan atau kerugian yang disebabkan oleh perilaku dari para 
                Pencari Layanan. Kami juga tidak bertanggung jawab atas kesalahan, termasuk pelanggaran kode etik, atau tindakan kriminal yang 
                dilakukan oleh Penyedia Layanan selama pelaksanaan Layanan. Penyedia Layanan hanya merupakan mitra kerja Kami, bukan pegawai, 
                agen atau perwakilan Kami.</li>
              <li>Kami menggunakan cara-cara teknis dan keamanan yang tepat dan wajar untuk menjaga situs tersebut aman dan terbebas dari 
                virus dan kesalahan. Namun demikian, bagaimanapun etektifnya, tidak ada sistem keamanan yang tidak dapat ditembus. Oleh karena 
                itu Kami tidak dapat menjamin keamanan database Kami.</li>
              <li>Situs tersebut dapat mengalami keterbatasan, penundaan, dan masalah-masalah lain yang terdapat dalam penggunaan internet 
                dan komunikasi elektronik, termasuk perangkat yang digunakan oleh Anda atau Penyedia Layanan rusak tidak terhubung, berada di 
                luar jangkauan, dimatikan atau tidak berfungsi. Kami tidak bertanggung jawab atas keterlambatan, kerusakan atau kerugian yang 
                diakibatkan ole masalah-masalah tersebut.</li>
            </ol>
          </li>
          <li>PEMBATAS TANGGUNG JAWAB
            
            <ol style="list-style-type:upper-alpha;">
              <li>Setiap tuntutan terhadap Kami dalam hal apapun oleh Anda dan/atau Penyedia Layanan, akan dibatasi dengan jumlah total yang 
                sebenarnya dibayar oleh dan/atau terhutang pada Anda ketika menggunakan Layanan selama peristiwa yang menimbulkan klaim tersebut. 
                Dalam hal apapun Kami tidak akan bertanggung jawab kepada Anda atau siapa pun atas biaya, bunga, kerusakan atau kerugian dalam segala 
                jenis atau dalam bentuk apapun (termasuk cidera pribadi, gangguan emosi dan hilangnya data, barang, pendapatan, keuntungan, penggunaan 
                atau keuntungan ekonomi lainnya). Kami tidak akan bertanggung jawab atas kerugian, kerusakan atau cidera yang mungkin ditimbulkan oleh 
                atau disebabkan oleh Anda atau pada setiap orang untuk siapa Anda telah memesan Layanan, termasuk namun tidak terbatas pada kerugian, 
                klaim asuransi yang tidak dibayar, kerusakan atau cidera yang timbul dari, atau dengan cara apapun sehubungan dengan Layanan dan/atau 
                situs  tersebut, termasuk namun tidak terbatas pada penggunaan atau ketidakmampuan untuk menggunakan Layanan dan/atau situs  tersebut.</li>
              <li>Anda secara tegas mengesampingkan dan melepaskan Kami dari setiap dan semua kewajiban, tuntutan atau kerusakan yang timbul dari 
                atau dengan cara apapun sehubungan dengan Penyedia Layanan. Kami tidak akan menjadi pihak dalam sengketa, negosiasi sengketa antara 
                Anda dan Penyedia Layanan. Tanggung jawab untuk keputusan yang Anda buat atas Layanan yang ditemukan melalui situs tersebut merupakan 
                tanggung jawab dan melekat seutuhnya dengan dan pada Anda.</li>
              <li>Anda secara tegas mengesampingkan dan melepaskan Kami dari setiap dan semua kewajiban, tuntutan, penyebab tindakan, atau kerusakan 
                yang timbul dari penggunaan Layanan, situs tersebut, atau dengan cara apapun terkait dengan Penyedia Layanan yang diperkenalkan kepada 
                Anda melalui situs tersebut. Kualitas Layanan yang diperoleh melalui penggunaan situs tersebut sepenuhnya menjadi tanggung jawab Penyedia 
                Layanan yang pada akhirnya memberikan Layanan untuk Anda.</li>
            </ol>
          </li>
          <li>PERIJINAN

            <ol style="list-style-type:upper-alpha;">
              <li>Dengan tergantung pada kepatuhan Anda pada Ketentuan Penggunaan, Kami memberikan Anda ijin yang terbatas. Anda tidak diperkenankan 
                menyalin, memodifikasi, mengadaptasi, menerjemahkan, membuat karya turunan dari, mendistribusikan, memberikan lisensi, menjual, mengalihkan, 
                membuat ulang, mentransmisikan, memindahkan, menyiarkan, menguraikan, atau membongkar bagian manapun dari atau dengan cara lain yang mungkin 
                mengeksploitasi situs  tersebut, merekayasa ulang atau mengakses perangkat lunak Kami untuk membangun produk atau layanan tandingan, membangun 
                produk dengan menggunakan ide, fitur, fungsi atau grafis sejenis situs  tersebut, atau menyalin ide, fitur , fungsi atau grafis situs  tersebut, 
                meluncurkan program otomatis atau script, termasuk, namun tidak terbatas pada, web spiders, web crawlers, web robots, web ants, web indexers, bots, 
                virus atau worm, atau segala program apapun, tanpa memperoleh persetujuan terlebih dahulu dari pemilik hak milik, kecuali sebagaimana diperbolehkan 
                dalam Ketentuan Penggunaan ini. </li>
              <li>Anda tidak akan mengirim spam atau pesan yang bersifat duplikasi atau tidak diminta yang melanggar hukum berlaku, tidak akan mengirim 
                atau menyimpan materi yang melanggar, cabul, mengancam, memfitnah, atau melanggar hukum atau bersifat sadis, termasuk namun tidak terbatas 
                pada bahan berbahaya bagi anak-anak atau yang melanggar hak privasi Kami, tidak akan mengirim bahan yang mengandung virus piranti lunak, worm, 
                trojan horses atau kode computer berbahaya lainnya, dokumen/file, script, agen atau program, tidak mengganggu atau mengacaukan integritas atau 
                kinerja situs  tersebut atau data di dalamnya, tidak mencoba untuk mendapatkan akses yang tidak sah ke situs  tersebut atau sistem atau jaringan 
                terkait, tidak berkedok sebagai orang atau badan lain atau menggambarkan diri Anda sebagai afiliasi seseorang atau suatu badan, dan menahan diri 
                untuk terlibat dalam tindakan yang mungkin bisa merusak reputasi Kami atau dianggap dapat merusak reputasi Kami.</li>
              <li>Kami akan memiliki hak untuk menyelidiki dan menuntut setiap pelanggaran di atas sepanjang yang dimungkinkan oleh hukum. Kami dapat melibatkan 
                dan bekerja sama dengan pihak penegak hukum dalam menuntut pengguna yang melanggar Ketentuan Penggunaan ini. Anda mengakui bahwa Kami tidak memiliki 
                kewajiban untuk memonitor akses Anda ke atau penggunaan situs tersebut, tapi Kami memiliki hak untuk melakukannya untuk tujuan pengoperasian situs 
                tersebut, untuk memastikan kepatuhan dengan Ketentuan Penggunaan ini, atau untuk mematuhi peraturan yang berlaku atau perintah atau persyaratan dari 
                pengadilan, lembaga administratif atau badan pemerintahan lainnya.</li>
            </ol>
          </li>
          <li>HAL-HAL TERKAIT KEKAYAAN INTELEKTUAL

            <ol style="list-style-type:upper-alpha;">
              <li>KAMIRAWAT, termasuk nama dan logo, situs tersebut dan Layanan, dilindungi oleh hak cipta, merek dagang dan hak-hak lain yang disediakan 
                berdasarkan hukum Negara Republik Indonesia. Kami secara eksklusif memiliki semua hak, kepemilikan dan kepentingan dalam dan terhadap situs 
                tersebut, termasuk semua hak kekayaan intelektual terkait.</li>
              <li>Tanpa menyimpang dari hak Kami berdasarkan hukum yang berlaku atau Ketentuan Penggunaan, Anda diberitahukan bahwa setiap berusaha atau 
                pelanggaran nyata atas ketentuan ini akan mengakibatkan penghentian semua hak Anda di bawah Ketentuan Penggunaan. Jika Anda menghindari salah 
                satu cara yang Kami ambil untuk melindungi layanan dari penggunaan secara tidak sah, Anda harus segera menghentikan setiap dan semua penggunaan 
                Layanan, dan Anda setuju untuk melakukannya.</li>
            </ol>
          </li>
          <li>PENGAKHIRAN

            <ol style="list-style-type:upper-alpha;">
              <li>Anda tidak berkewajiban untuk menggunakan situs tersebut dan dapat memilih untuk berhenti menggunakannya setiap saat dengan membatalkan 
                akun Anda dan secara permanen. Anda dapat mengakhiri akun Anda dengan memberitahu Kami maksud Anda untuk membatalkan akun Anda. Kami akan 
                mencoba untuk melakukan yang terbaik untuk menyelesaikan penghentian dalam waktu empat belas (empat belas) hari kerja, sehingga Ketentuan 
                Penggunaan secara otomatis berakhir.</li>
              <li>Kami berhak untuk segera menangguhkan, membatasi atau menghentikan Ketentuan Penggunaan dan penggunaan situs tersebut jika Kami memiliki 
                alasan untuk mencurigai bahwa Anda telah melanggar ketentuan dari Ketentuan Penggunaan ini atau peraturan perundang-undangan yang berlaku.</li>  
            </ol>
          </li>
          <li>LAIN-LAIN

            <ol style="list-style-type:upper-alpha;">
              <li>Pengesampingan atau toleransi atau kegagalan Kami untuk mengklaim suatu pelanggaran ketentuan dari Ketentuan Penggunaan ini atau untuk 
                melaksanakan hak yang dinyatakan oleh Ketentuan Penggunaan ini atau hukum yang berlaku, tidak akan dianggap sebagai pengesampingan sehubungan 
                dengan pelanggaran berikutnya dari setiap ketentuan dari Ketentuan Penggunaan ini.</li>
              <li>Ketentuan Penggunaan ini disusun dalam Bahasa Inggris dan bahasa Indonesia, kedua versi akan mengikat Anda dan Kami. Dalam hal terdapat 
                ketidaksesuaian antara versi bahasa Indonesia dan versi bahasa Inggris, versi bahasa Indonesia yang akan berlaku.</li>  
              <li>Anda tidak dapat memindahkan atau mengalihkan hak dan berdasarkan Ketentuan Penggunaan, tanpa persetujuan tertulis sebelumnya dari Kami. 
                Kami dapat mengalihkan hak Kami berdasarkan Ketentuan Penggunaan kepada pihak semata-mata dan mutlak menurut kebijakan Kami.</li>  
              <li>Jika ada istilah berdasarkan Ketentuan Penggunaan yang dianggap tidak sah, tidak berlaku atau tidak dapat dilaksanakan, baik secara 
                keseluruhan atau sebagian, berdasarkan pengundangan atau ketentuan perundang-undangan, istilah atau bagian dari istilah tersebut akan, sepanjang, 
                dianggap bukan bagian dari Ketentuan Penggunaan ini, namun legalitas, keabsahan atau keberlakuan dari Ketentuan Penggunaan selebihnya tidak akan terpengaruhi.</li>  
              <li>Ketentuan Penggunaan ini diatur oleh dan ditafsirkan berdasarkan hukum Negara Republik Indonesia.</li>
              <li>Ketentuan Penggunaan ini dapat dimodifikasi dan diubah dari waktu ke waktu. Kami akan memberitahu Anda melalui email dan/atau situs tersebut 
                atas modifikasi, dan/atau perubahan atas Ketentuan Penggunaan. Penggunaan situs tersebut secara terus menerus setelah diterimanya pemberitahuan 
                ini merupakan persetujuan dan penerimaan Anda atas modifikasi, dan/atau perubahan.</li>
            </ol>
          </li>
        </ol>

        <br>
        <h5><b>SYARAT DAN KETENTUAN</b></h5>
        <ol>
          <li>Pelanggan wajib memberikan informasi yang benar dan lengkap mengenai jenis dan spesifikasi barang dan/atau layanan yang akan dikirimkan dan/atau diberikan.</li>
          <li>Para Penyedia Layanan adalah tenaga Kesehatan yang telah memiliki Surat Tanda Registrasi dan Surat Ijin Praktek yang resmi dan 
            diakui oleh Negara Republik Indonesia. Penyedia Layanan yang tergabung dalam KAMIRAWAT telah diverifikasi oleh pihak Ikatan Profesi 
            yang Sah.</li>
          <li>KAMIRAWAT tidak bertanggung jawab secara langsung untuk kejadian medis tidak terduga namun tidak terbatas pada syok anafilaksis, 
            Steven Johnson Syndrome, Toxic Epidermal Necrolysis yang melibatkan Penyedia Layanan. Tanggung jawab atas seluruh biaya serta tuntutan 
            yang mungkin timbul atas kejadian tersebut bukan tanggung jawab KAMIRAWAT. Jika ada informasi yang dapat membantu untuk proses investigasi 
            seperti nomor telepon, maka KAMIRAWAT hanya dapat membantu sebagai mediator dalam mempertemukan kedua pihak untuk mencari penyelesaian masalah tersebut.</li>
          <li>Setiap kode voucher hanya bisa digunakan satu kali per akun untuk setiap promosi. *Syarat dan Ketentuan Berlaku.</li>
        </ol>

      </div>
    </div>
  </div>
</section>

@endsection
