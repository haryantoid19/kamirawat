@extends('front.layout.main')

@section('content')
@include("front.partner._dashboard_banner")
<!-- ============================================-->
<!-- <section> begin ============================-->
<section class="pt-6 bg-gray">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-12 py-6">
                @include('front.patient._booking_navigation')

                <div class="w-100 floatleft">
                    <div class="container rounded bg-white mt-5 mb-5 box-shadow-1">
                        <div class="row">

                            <!--begin::Session Status-->
                            @if (session('status') === 'email-verified')
                                <p class="alert alert-success font-medium text-sm text-gray-500 mt-4">
                                    Terima kasih! Akun anda sudah berhasil diverifikasi.
                                </p>
                            @endif
                            <!--end::Session Status-->

                            @if (session('alert') === 'message')
                                <p class="alert alert-{{ session('alert-type', 'success') }} font-medium text-sm text-gray-500 mt-4">
                                    {{ session('message') }}
                                </p>
                            @endif

                            <div class="col-12 col-md-3 border-right container-avatar">
                                <form method="POST" action="{{ route('front.patient.dashboard.update-avatar') }}" enctype="multipart/form-data">
                                    @csrf
                                    @method("PUT")

                                    <div class="avatar-upload">
                                        <div class="avatar-edit">
                                            <input type="file" name="file" id="imageUpload" accept=".png, .jpg, .jpeg" />
                                            <label for="imageUpload"></label>
                                        </div>
                                        <div class="avatar-preview">
                                            <div id="imagePreview" style="background-image: url({{ $patient->avatar_url }});">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="w-100 my-3 text-center">
                                        <button type="submit" class="btn btn-primary">Simpan Avatar</button>
                                    </div>
                                </form>
                            </div>

                            <div class="col-md-9 border-right">
                                <div class="p-3 py-5">

                                    <div class="mb-4">
                                        <div class="row mt-2">
                                            <div class="col-md-12">
                                                <span class="fw-bold">Akun ID : {{ $patient->patient_number }}</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="mb-4">
                                        <div class="row mb-3">
                                            <div class="col-md-3 label-detail">
                                                <label class="fw-bold">Nama Pasien</label>
                                            </div>
                                            <div class="col-md-9">
                                                <span>{{ $patient->name }}</span>
                                            </div>
                                        </div>
                                        <div class="row mb-3">
                                            <div class="col-md-3 label-detail">
                                                <label class="fw-bold text-uppercase">{{ $patient->id_type->value }}</label>
                                            </div>
                                            <div class="col-md-9">
                                                <span>{{ $patient->id_number }}</span>
                                            </div>
                                        </div>
                                        <div class="row mb-3">
                                            <div class="col-md-3 label-detail">
                                                <label class="fw-bold">Alamat</label>
                                            </div>
                                            <div class="col-md-9">
                                                <span>{!! nl2br($patient->address) !!}</span>
                                            </div>
                                        </div>
                                        <div class="row mb-3">
                                            <div class="col-md-3 label-detail">
                                                <label class="fw-bold">Nomor HP</label>
                                            </div>
                                            <div class="col-md-9">
                                                <span>{{ $patient->phone_number }}</span>
                                            </div>
                                        </div>
                                        <div class="row mb-3">
                                            <div class="col-md-3 label-detail">
                                                <label class="fw-bold">Email</label>
                                            </div>
                                            <div class="col-md-9">
                                                <span>{{ $patient->user->email }}</span>
                                                <a href="javascript:void(0);" data-bs-toggle="modal" data-bs-target="#ModalEditEmail">[Ubah Email]</a>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="w-100 mt-3">
                                        <a href="javascript:void(0);" data-bs-toggle="modal" data-bs-target="#ModalEdit" class="btn btn-primary">Ubah Profil</a>
                                    </div>

                                </div>
                            </div>

                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
<!-- <section> close ============================-->
<!-- ============================================-->

<!-- Modal -->
<div class="modal fade" id="ModalEdit" tabindex="-1" aria-labelledby="ModalEditLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <form action="{{ route('front.patient.profile.update') }}" method="POST">
            @csrf
            @method('PUT')

            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="ModalEditLabel">Ubah Profile</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">

                    <div class="mb-4">
                        <div class="row mt-2">
                            <div class="col-md-12">
                                <span class="fw-bold">Akun ID : {{ $patient->patient_number }}</span>
                            </div>
                        </div>
                    </div>


                    <div class="mb-4">
                        <div class="row mt-2">
                            <div class="col-md-12">
                                <!-- <div class="avatar-upload m-0">
                                    <div class="avatar-edit">
                                        <input type='file' id="imageUpload" accept=".png, .jpg, .jpeg" />
                                        <label for="imageUpload"></label>
                                    </div>
                                    <div class="avatar-preview">
                                        <div id="imagePreview" style="background-image: url({{ Auth::user()->avatar_url }});">
                                        </div>
                                    </div>
                                </div> -->
                            </div>
                        </div>
                    </div>

                    <div class="mb-4">
                        <div class="row mb-3 align-items-center">
                            <div class="col-md-3 label-detail">
                                <label class="fw-bold" for="name">Nama Pasien</label>
                            </div>
                            <div class="col-md-9">
                                <div class="input-group">
                                    <input type="text" class="form-control" id="name" name="name" value="{{ $patient->name }}">
                                    @error('name')
                                    <div class="text-danger small mt-1">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3 align-items-center">
                            <div class="col-md-3 label-detail">
                                <label class="fw-bold text-uppercase" for="id_number">{{ $patient->id_type->value }}</label>
                            </div>
                            <div class="col-md-9">
                                <div class="input-group">
                                    <input type="text" class="form-control" id="id_number" name="id_number" value="{{ $patient->id_number }}">
                                    @error('id_number')
                                    <div class="text-danger small mt-1">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3 align-items-top">
                            <div class="col-md-3 label-detail">
                                <label class="fw-bold" for="address">Alamat</label>
                            </div>
                            <div class="col-md-9">
                                <div class="input-group">
                                    <textarea name="address" id="address" rows="3" class="form-control">{{ $patient->address }}</textarea>
                                    @error('address')
                                    <div class="text-danger small mt-1">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row mb-3 align-items-center">
                            <div class="col-md-3 label-detail">
                                <label class="fw-bold" for="phone_number">Nomor HP</label>
                            </div>
                            <div class="col-md-9">
                                <div class="input-group">
                                    <input type="tel" class="form-control" id="phone_number" name="phone_number" value="{{ $patient->phone_number }}">
                                    @error('phone_number')
                                    <div class="text-danger small mt-1">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <button type="button" class="btn btn-grey" data-bs-dismiss="modal">Close</button>
                </div>
            </div>

        </form>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="ModalEditEmail" tabindex="-1" aria-labelledby="ModalEditEmailLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <form action="{{ route('front.change-email') }}" method="POST">
            @csrf

            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="ModalEditLabel">Ubah Email</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">

                    <div class="mb-4">
                        <div class="row mt-2">
                            <div class="col-md-12">
                                <span class="fw-bold">Akun ID : {{ $patient->patient_number }}</span>
                            </div>
                        </div>
                    </div>

                    <div class="mb-4">
                        <div class="row mb-3 align-items-center">
                            <div class="col-md-3 label-detail">
                                <label class="fw-bold" for="email">Email</label>
                            </div>
                            <div class="col-md-9">
                                <div class="input-group">
                                    <input type="text" class="form-control" id="email" name="email" value="{{ old('email') ?: $patient->user->email }}">
                                    @error('email')
                                    <div class="text-danger small mt-1">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <button type="button" class="btn btn-grey" data-bs-dismiss="modal">Close</button>
                </div>
            </div>

        </form>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        var profile_in_validation = '{{ session('profile_in_validation') }}';

        if (profile_in_validation === '1') {
            var modalEdit = new bootstrap.Modal(document.getElementById('ModalEdit'))
            modalEdit.show();
        }

        var email_in_validation = '{{ session('email_in_validation') }}';

        if (email_in_validation === '1') {
            var modalEditEmail = new bootstrap.Modal(document.getElementById('ModalEditEmail'))
            modalEditEmail.show();
        }

    });
</script>

@endsection
