@extends('front.layout.main')

@section('content')
    <section class="pt-6 bg-primary">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-12 text-center py-6">
                    <h3 class="fw-medium text-white">Masuk Pasien</h3>

                    <div class="row mt-4">
                        <div class="col-md-5 m-auto">
                            <form class="" action="{{ route('front.patient.login') }}" method="POST">
                                @csrf
                                <input type="hidden" name="continue" value="{{ route('front.patient.dashboard') }}" />
                                <input type="hidden" name="type" value="{{ \App\Enums\UserRole::PATIENT()->value }}" />
                                <div class="mb-4">
                                    <div class="input-group">
                                        <label class="hidden sr-only" for="email">{{ __('Email') }}</label>
                                        <input type="email" name="email" class="form-control" id="email" placeholder="{{ __("Email") }}" required>
                                    </div>
                                </div>
                                <div class="mb-4">
                                    <div class="input-group input-group-addon-w" id="show_hide_password">
                                        <label class="hidden sr-only" for="password">{{ __('Password') }}</label>
                                        <input type="password" name="password" class="form-control" id="password" placeholder="{{ __('Password') }}" required>
                                        <div class="input-group-addon">
                                            <a href="javascript:void(0);"><i class="fa fa-eye-slash" aria-hidden="true"></i></a>
                                        </div>
                                    </div>
                                </div>

                                <div class="mt-4">
                                    <button type="submit" class="btn btn-cyan">{{ __('Login') }}</button>
                                </div>
                            </form>

                            <div class="col-12 mt-6">
                                <a href="{{ route('front.patient.register') }}" class="text-dark">{{ __('Register') }}</a>
                                <span class="mx-2">|</span>
                                <a href="{{ route('password.request') }}" class="text-white">{{ __('Forgot your password?') }}</a>
                            </div>

                            @if ($errors->any())
                                <div class="col-12 mt-6 text-start">
                                    <div class="alert alert-danger">
                                        <p>
                                            {{ __('Whoops! Something went wrong.') }}
                                        </p>

                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            @endif

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
