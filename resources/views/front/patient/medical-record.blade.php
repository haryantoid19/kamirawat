@extends('front.layout.main')

@section('content')
@include("front.partner._dashboard_banner")
<!-- ============================================-->
<!-- <section> begin ============================-->
<section class="pt-6 bg-gray">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-12 py-6">
                @include('front.patient._booking_navigation')

                <div class="w-100 floatleft">
                    <div class="container rounded bg-white mt-5 mb-5 box-shadow-1">
                        <div class="p-3 py-5">

                            <div class="row">

                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <table id="example" class="table table-striped table-bordered">
                                                <thead>
                                                <tr>
                                                    <th>No Rakam Medis</th>
                                                    <th>Tanggal Layanan</th>
                                                    <th>Layanan</th>
                                                    <th>Nama Dokter/Tenaga Medis</th>
                                                    <th>Diagnosa</th>
                                                    <th>Penanganan</th>
                                                    <th>Data Pendukung</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td>
                                                        <a href="pasien-rekamedis-detail.html">MR2101-00001</a>
                                                    </td>
                                                    <td>08/01/2020</td>
                                                    <td>Doctor Visit</td>
                                                    <td>Lorem ip</td>
                                                    <td>Lorem ip</td>
                                                    <td>Lorem ip</td>
                                                    <td>Lorem ip</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a href="pasien-rekamedis-detail.html">MR2101-00002</a>
                                                    </td>
                                                    <td>08/01/2020</td>
                                                    <td>Home Medicine</td>
                                                    <td>Lorem ip</td>
                                                    <td>Lorem ip</td>
                                                    <td>Lorem ip</td>
                                                    <td>Lorem ip</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a href="pasien-rekamedis-detail.html">MR2101-00003</a>
                                                    </td>
                                                    <td>08/01/2020</td>
                                                    <td>Homecare</td>
                                                    <td>Lorem ip</td>
                                                    <td>Lorem ip</td>
                                                    <td>Lorem ip</td>
                                                    <td>Lorem ip</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a href="pasien-rekamedis-detail.html">MR2101-00004</a>
                                                    </td>
                                                    <td>08/01/2020</td>
                                                    <td>Homecare</td>
                                                    <td>Lorem ip</td>
                                                    <td>Lorem ip</td>
                                                    <td>Lorem ip</td>
                                                    <td>Lorem ip</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a href="pasien-rekamedis-detail.html">MR2101-00005</a>
                                                    </td>
                                                    <td>08/01/2020</td>
                                                    <td>Homecare</td>
                                                    <td>Lorem ip</td>
                                                    <td>Lorem ip</td>
                                                    <td>Lorem ip</td>
                                                    <td>Lorem ip</td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a href="pasien-rekamedis-detail.html">MR2101-00006</a>
                                                    </td>
                                                    <td>08/01/2020</td>
                                                    <td>Homecare</td>
                                                    <td>Lorem ip</td>
                                                    <td>Lorem ip</td>
                                                    <td>Lorem ip</td>
                                                    <td>Lorem ip</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
<!-- <section> close ============================-->
<!-- ============================================-->
@endsection


