@extends('front.layout.main')

@section('content')
@include("front.partner._dashboard_banner")
    <!-- ============================================-->
    <!-- <section> begin ============================-->
    <section class="pt-6 bg-gray">
        <div class="container ">
            <div class="row align-items-center">
                <div class="col-12 py-6">
                    @include('front.patient._booking_navigation')

                    <div class="w-100 floatleft">
                        <div class="mt-3">
                            <h5 class="fw-medium text-dark">Booking {{ $medicalServiceCategory->name }}</h5>
                        </div>

                        <div class="container rounded bg-white mt-4 mb-5 box-shadow-1 p-m-0">
                            <div class="p-3 py-5 section-booking">

                                <div class="row">
                                    <div class="container">
                                        <div class="spinner-grow blockui-spinner" style="width: 3rem; height: 3rem;display:none" role="status">
                                            <span class="visually-hidden">Loading...</span>
                                        </div>
                                        <form id="booking-new" class="wizard-wrap" action="#">
                                            <div>
                                                <h3>Pilih Layanan</h3>
                                                <section>
                                                    <div class="w-100 p-4 p-m-0">

                                                        <div class="text-center w-100 mb-4">
                                                            <h5>Pilih Layanan yang Anda Inginkan!</h5>
                                                        </div>

                                                        <ul class="sc-layanan layanan-radio">
                                                            @foreach ($medical_services as $k => $medical_service)
                                                                <li class="col-6 col-sm-6 col-lg-3 text-center mb-5">
                                                                    <input type="radio" name="selected-service" value="{{ $medical_service->id }}" id="cb{{ $k }}" class="required" />
                                                                    <label for="cb{{ $k }}">
                                                                        <div class="sc-layanan-item">
                                                                            <div class="sc-layanan-item-img">
                                                                                <img src="{{ $medical_service->icon_url }}" alt="...">
                                                                            </div>
                                                                            <h5 class="mt-3 mb-0">{{ $medical_service->name }}</h5>
                                                                            <p class="text-info">{{ $medical_service->description }}</p>
                                                                        </div>
                                                                    </label>
                                                                </li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                </section>
                                                <h3>Data Pasien</h3>
                                                <section>
                                                    <div class="w-100 p-4 p-m-0">
                                                        <div class="mb-4">
                                                            <div class="input-group">
                                                                <div class="form-check w-100">
                                                                    <input class="form-check-input" type="radio" name="for_myself" id="for_myself_yes" value="yes" required="">
                                                                    <label class="form-check-label" for="for_myself_yes">
                                                                        Saya memesan untuk diri saya sendiri.
                                                                    </label>
                                                                </div>

                                                                <div class="form-check w-100">
                                                                    <input class="form-check-input" type="radio" name="for_myself" id="for_myself_no" value="no" required="">
                                                                    <label class="form-check-label" for="for_myself_no">
                                                                        Saya memesan untuk orang lain.
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="mb-4">
                                                            <div class="input-group">
                                                                <label for="phone_number" class="d-none"></label>
                                                                <input type="tel" name="phone_number" class="number form-control" placeholder="Nomor Handphone" id="phone_number" required data-self="{{ $user->patient->phone_number }}">
                                                            </div>
                                                        </div>

                                                        <div class="mb-4">
                                                            <div class="input-group">
                                                                <label for="name" class="d-none"></label>
                                                                <input type="text" name="name" id="name" class="form-control" placeholder="Nama Lengkap" required data-self="{{ $user->patient->name }}">
                                                            </div>
                                                        </div>

                                                        <div class="mb-4">
                                                            <div class="input-group">
                                                                <label for="bod" class="form-label fw-bold">Tanggal Lahir Anda <span class="text-danger">*</span></label>
                                                                <input autocomplete="off" readonly type="text" name="bod" id="bod" class="form-control datepicker-dob" placeholder="Tanggal Lahir" required data-self="{{ $user->patient->bod->format('Y-m-d') }}">
                                                            </div>
                                                        </div>

                                                        <div class="mb-4">
                                                            <div class="input-group">
                                                                <label for="" class="form-label fw-bold">Jenis Kelamin <span class="text-danger">*</span></label>
                                                                <div class="ms-3">
                                                                    <div class="form-check">
                                                                        <input type="hidden" name="self-gender" value="{{ $user->patient->gender->value }}" />
                                                                        <input class="form-check-input" type="radio" name="gender" id="gender-man" value="{{ \App\Enums\Gender::MAN()->value }}" required>
                                                                        <label class="form-check-label" for="gender-man">
                                                                            Pria
                                                                        </label>
                                                                    </div>
                                                                    <div class="form-check">
                                                                        <input class="form-check-input" type="radio" name="gender" id="gender-woman" value="{{ \App\Enums\Gender::WOMAN()->value }}" required>
                                                                        <label class="form-check-label" for="gender-woman">
                                                                            Wanita
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="mb-4">
                                                            <div class="input-group">
                                                                <label for="history_of_disease_allergies" class="form-label fw-bold">Riwayat Penyakit & Alergi</label>
                                                                <textarea type="text" name="history_of_disease_allergies" class="form-control" id="history_of_disease_allergies"></textarea>
                                                            </div>
                                                        </div>

                                                        <div class="mb-4">
                                                            <div class="input-group">
                                                                <label for="main_symptom" class="form-label fw-bold">Keluhan Utama</label>
                                                                <textarea type="text" name="main_symptom" class="form-control" placeholder="" id="main_symptom"></textarea>
                                                            </div>
                                                        </div>

                                                        <div class="mb-4">
                                                            <div class="input-group">
                                                                <label for="special_request" class="form-label fw-bold">Permintaan Khusus</label>
                                                                <textarea type="text" name="special_request" id="special_request" class="form-control"></textarea>
                                                            </div>
                                                        </div>


                                                        <div class="mb-4">
                                                            <div class="input-group">
                                                                <label for="address" class="form-label fw-bold">Alamat Domisili <span class="text-danger">*</span></label>
                                                                <textarea type="text" name="address" id="address" class="form-control" required></textarea>
                                                            </div>
                                                        </div>


                                                        <div class="mb-4">

                                                            <div class="input-group">
                                                                <label for="" class="form-label fw-bold">Pilih PIN poin alamat anda. pada peta dibawah</label>
                                                                <div id="information"></div>
                                                                <div id="map-canvas"></div>
                                                            </div>

                                                        </div>

                                                        <div class="mb-4">
                                                            <div class="input-group">
                                                                <label for="latlong"></label>
                                                                <input type="hidden" name="latlong" id="latlong" class="form-control latlang" required>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </section>
                                                <h3>Pembayaran</h3>
                                                <section>
                                                    <div class="w-100 p-4 p-m-0">
                                                        <div class="w-100">
                                                            <div class="callout large invoice-container">
                                                                <div id="invoice-container"></div>

                                                                <div class="w-100">
                                                                    <h4 class="fw-bold">Pilih Pembayaran</h4>
                                                                    <ul class="sc-layanan payment-radio">
                                                                        @foreach ($payment_methods as $k => $payment_method)
                                                                            <li class="col-6 col-sm-6 col-lg-3 text-center mb-5">
                                                                                <input type="radio" name="payment-method" id="payment-radio-item-{{ $k }}" value="{{ $payment_method->id }}" class="required" />
                                                                                <label for="payment-radio-item-{{ $k }}">
                                                                                    <div class="sc-layanan-item">
                                                                                        <div class="sc-layanan-item-img">
                                                                                            <img src="{{ $payment_method->icon_url  }}" alt="...">
                                                                                        </div>
                                                                                        <h4 class="mt-3 mb-0">{{ $payment_method->label }}</h4>
                                                                                    </div>
                                                                                </label>
                                                                            </li>
                                                                        @endforeach
                                                                    </ul>
                                                                </div>

                                                            </div>
                                                        </div>

                                                        <div class="alert alert-danger" id="booking-alert" role="alert" style="display:none;"></div>
                                                    </div>

                                                </section>
                                            </div>
                                        </form>
                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- <section> close ============================-->
    <!-- ============================================-->

@endsection

@section('script')
    <script>
        window.booking = true;
        window.booking_url = '{{ route('front.patient.booking.store', $medicalServiceCategory->id) }}'
        window.booking_invoice_table_url = '{{ route('front.patient.booking.invoice-table', $medicalServiceCategory->id) }}'
    </script>
@endsection
