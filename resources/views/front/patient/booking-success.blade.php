@extends('front.layout.main')

@section('content')
@include("front.partner._dashboard_banner")
<!-- ============================================-->
<!-- <section> begin ============================-->
<section class="pt-6 bg-gray">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-12 py-6">
                @include('front.patient._booking_navigation')

                <div class="w-100 floatleft">
                    <div class="container rounded bg-white mt-5 mb-5 box-shadow-1">
                        <div class="row align-items-center">
                            <div class="col-12 text-center py-6">
                                <h2 class="fw-medium">Selamat!</h2>

                                <div class="row mt-4">
                                    <div class="col-md-8 offset-md-2">
                                        <div class="w-100 floatleft mb-4">
                                            <i class="far fa-check-circle text-sux fa-5x text-success"></i>
                                        </div>

                                        <p class="fw-bold">Pesanan Anda Kami Terima</p>
                                        <p class="fw-bold">Berikut Informasi Pembayaran Anda</p>

                                        @if($payment_method->gateway->is(\App\Enums\PaymentMethodGateway::XENDIT()))
                                            <p class="fw-bold">Bank  : {{ $payment_method->label }}</p>
                                            <p class="fw-bold">No Va : {{ $va_number }}</p>
                                        @else
                                            <p class="fw-bold">
                                                {{ nl2br($payment_method->description) }}
                                            </p>
                                        @endif

                                    </div>
                                </div>

                                @if(!$payment_method->gateway->is(\App\Enums\PaymentMethodGateway::XENDIT()))
                                <h5 class="fw-medium mt-4">Silahkan konfirmasi pembayaran anda ke nomor berikut <a target="_blank" href="http://wa.me/+6281909201120">+6281909201120</a></h5>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
<!-- <section> close ============================-->
<!-- ============================================-->
@endsection
