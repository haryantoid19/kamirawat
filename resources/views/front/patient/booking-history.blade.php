@extends('front.layout.main')

@section('content')
@include("front.partner._dashboard_banner")

<!-- ============================================-->
<!-- <section> begin ============================-->
<section class="pt-6 bg-gray">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-12 py-6">
                @include('front.patient._booking_navigation')

                <div class="tabs-col-2">
                    <div class="w-100 floatleft">
                        <div class="container rounded bg-white mt-5 mb-5 box-shadow-1">
                            <div class="p-3 py-5">

                                <div class="row">

                                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                                        <li class="nav-item" role="presentation">
                                            <button class="nav-link {{ request()->exists('completed') === false ? 'active' : '' }}" id="home-tab" data-bs-toggle="tab" data-bs-target="#home" type="button" role="tab" aria-controls="home" aria-selected="true">Belum Selesai</button>
                                        </li>
                                        <li class="nav-item" role="presentation">
                                            <button class="nav-link {{ request()->exists('completed') ? 'active' : '' }}" id="profile-tab" data-bs-toggle="tab" data-bs-target="#profile" type="button" role="tab" aria-controls="profile" aria-selected="false">Selesai</button>
                                        </li>
                                    </ul>
                                    <div class="tab-content" id="myTabContent">
                                        <div class="tab-pane fade {{ request()->exists('completed') === false ? 'show active' : '' }}" id="home" role="tabpanel" aria-labelledby="home-tab">

                                            <div class="tab-inner">

                                                <!--begin::Session Status-->
                                                @if (session()->has('message'))
                                                    <p class="alert alert-success font-medium text-sm text-gray-500 mt-4">
                                                        {{ session('message') }}
                                                    </p>
                                                @endif
                                                <!--end::Session Status-->

                                                @foreach($ongoing_bookings as $ongoing_booking)
                                                    @php
                                                        $invoice_active = $ongoing_booking->invoices->where('status', 'ACTIVE')->first();
                                                    @endphp
                                                    <div class="py-4">
                                                        <div class="row box-shadow-1 box-tab-item">
                                                            <div class="col-md-12 mt-2">
                                                                <a href="javascript:void(0);" class="fw-bold" data-bs-toggle="modal" data-bs-target="#exampleModal">No. Layanan : {{ $ongoing_booking->booking_number }}</a>
                                                            </div>
                                                            <hr>

                                                            <div class="col-md-3 border-right">
                                                                <div class="d-flex flex-column align-items-center text-center p-3">
                                                                    <img class="rounded-circle" height="150px" width="150px" src="{{ $ongoing_booking->patient->avatar_url }}">
                                                                    <span> </span>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-9 border-right">
                                                                <div class="p-3">

                                                                    <div class="mb-4">
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <label class="fw-bold">Nama Dokter</label>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                : <span>{{ $ongoing_booking->medical_people ? $ongoing_booking->medical_people->name : '-' }}</span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <label class="fw-bold">Nama Layanan</label>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                : <span>{{ $ongoing_booking->medical_service ? $ongoing_booking->medical_service->name : '-' }}</span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <label class="fw-bold">Status Layanan</label>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                : <span>{{ $ongoing_booking->status->toTextForPatient() }}</span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <label class="fw-bold">Status Pembayaran</label>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                : {!! $invoice_active ? '<span class="text-danger">Belum Lunas</span>' : '<span  class="text-success">Lunas</span>' !!}
                                                                            </div>
                                                                        </div>
                                                                        @if ($invoice_active && $invoice_active->status->isActive() && $invoice_active->xendit_type && $invoice_active->xendit_type->is(\App\Enums\XenditType::VIRTUAL_ACCOUNT()))
                                                                            <div class="row">
                                                                                <div class="col-md-4">
                                                                                    <label class="fw-bold">No. Virtual</label>
                                                                                </div>
                                                                                <div class="col-md-8">
                                                                                    : <span>{{ $invoice_active->xendit_va_number  }}</span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-md-4">
                                                                                    <label class="fw-bold">Bank</label>
                                                                                </div>
                                                                                <div class="col-md-8">
                                                                                    : <span>{{ $invoice_active->payment_method->label  }}</span>
                                                                                </div>
                                                                            </div>
                                                                        @elseif ($invoice_active && $invoice_active->status->isActive() && $invoice_active->payment_method && $invoice_active->payment_method->gateway->is(\App\Enums\PaymentMethodGateway::BANK_TRANSFER()))
                                                                            <div class="row">
                                                                                <div class="col-md-4">
                                                                                    <label class="fw-bold">Bank</label>
                                                                                </div>
                                                                                <div class="col-md-8">
                                                                                    : <span>{{ $invoice_active->payment_method->label  }}</span>
                                                                                </div>
                                                                            </div>
                                                                            <div class="row">
                                                                                <div class="col-md-4">
                                                                                    <label class="fw-bold">Note</label>
                                                                                </div>
                                                                                <div class="col-md-8">
                                                                                    : <span>{!! nl2br($invoice_active->payment_method->description) !!}</span>
                                                                                </div>
                                                                            </div>
                                                                        @endif

                                                                        @if (!$invoice_active)
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <label class="fw-bold">Passcode Riwayat Layanan</label>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                : <span>{{ $ongoing_booking->passcode ?: '-' }}</span>
                                                                            </div>
                                                                        </div>
                                                                        @endif

                                                                    </div>


                                                                    <div class="w-100 mt-3">
                                                                        @if ($invoice_active && $invoice_active->status->isActive() && $invoice_active->xendit_type && $invoice_active->xendit_type->is(\App\Enums\XenditType::INVOICE()))
                                                                            <a href="{{ $invoice_active->xendit_invoice_url }}" target="new" class="btn btn-orange btn-small my-1">Bayar Sekarang</a>
                                                                        @endif
                                                                        <br>
                                                                        @if($ongoing_booking->status->is(\App\Enums\BookingStatus::WAITING_PAYMENT()))
                                                                            <form method="POST" action="{{ route('front.patient.booking.cancel', $ongoing_booking->id) }}">
                                                                                @csrf
                                                                                @method("PUT")

                                                                                <button type="submit" class="btn btn-grey btn-small my-1">Batalkan Pesanan</button>
                                                                            </form>
                                                                        @endif
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach

                                            </div>


                                            <div>
                                                {{ $ongoing_bookings->links() }}
                                            </div>

                                        </div>
                                        <div class="tab-pane fade {{ request()->exists('completed') ? 'show active' : '' }}" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                            <div class="tab-inner">

                                                @foreach($completed_bookings as $completed_booking)
                                                    <div class="py-4">
                                                        <div class="row box-shadow-1 box-tab-item">
                                                            <div class="col-md-12 mt-2">
                                                                <a href="javascript:void(0);" class="fw-bold" data-bs-toggle="modal" data-bs-target="#exampleModal">No. Layanan : {{ $completed_booking->booking_number }}</a>
                                                            </div>

                                                            <div class="col-md-3 border-right">
                                                                <div class="d-flex flex-column align-items-center text-center p-3">
                                                                    <img class="rounded-circle" height="150px" width="150px" src="{{ $completed_booking->patient->avatar_url }}">
                                                                    <span> </span>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-9 border-right">
                                                                <div class="p-3">

                                                                    <div class="mb-4">
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <label class="fw-bold">Nama Dokter</label>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                : <span>{{ $completed_booking->medical_people ? $completed_booking->medical_people->name : '-' }}</span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <label class="fw-bold">Nama Layanan</label>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                : <span>{{ $completed_booking->medical_service ? $completed_booking->medical_service->name : '-' }}</span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <label class="fw-bold">Status Layanan</label>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                : <span>{{ $completed_booking->status->toTextForPatient() }}</span>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-md-4">
                                                                                <label class="fw-bold">Status Pembayaran</label>
                                                                            </div>
                                                                            <div class="col-md-8">
                                                                                : <span>{!! $completed_booking->invoice->status->isPaid() ? '<span  class="text-success">Lunas</span>' : '<span  class="text-danger">Belum bayar</span>' !!}</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    @if ($completed_booking->status->toTextForPatient() != 'Dibatalkan')
                                                                    <div class="w-100 mt-3">
                                                                        @if ($completed_booking->invoice->status->isPaid() === false)
                                                                            <a href="#" class="btn btn-orange btn-small my-1">Bayar Sekarang</a>
                                                                        @endif
                                                                        <br>
                                                                        @if($completed_booking->status->is(\App\Enums\BookingStatus::WAITING_PAYMENT()))
                                                                            <a href="#" class="btn btn-grey btn-small my-1">Batalkan Pesanan</a>
                                                                        @endif
                                                                    </div>
                                                                    @endif

                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                @endforeach

                                            </div>


                                            <div>
                                                {{ $completed_bookings->links() }}
                                            </div>
                                        </div>
                                    </div>


                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
<!-- <section> close ============================-->
<!-- ============================================-->
@endsection


