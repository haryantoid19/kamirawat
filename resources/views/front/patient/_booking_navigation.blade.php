<div id="nav-head" class="w-100">
    <h4 class="fw-medium text-dark">Selamat Datang, <b>{{ auth()->user()->name }}</b></h4>

    <div class="w-100 floatleft my-3">
        <div class="w-100">
            <a href="{{ route('front.patient.dashboard') }}" class="btn btn-primary m-1 {{ request()->routeIs('front.patient.dashboard') ? 'current-page' : '' }}">Profil Pasien</a>
            <a href="{{ route('front.patient.booking.history') }}" class="btn btn-primary m-1 {{ request()->routeIs('front.patient.booking.history') ? 'current-page' : '' }}">Riwayat Layanan</a>
            {{-- <a href="{{ route('front.patient.medical-record.browse') }}" class="btn btn-primary m-1 {{ request()->routeIs('front.patient.medical-record.browse') ? 'current-page' : '' }}">Rekam Medis</a> --}}
            <div class="m-1 d-inline-block">
                <div class="dropdown">
                    <button class="btn btn-primary dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                        Booking
                    </button>
                    <div class="dropdown-menu dropdown-menu-lg-end dropdown-menu-nav mt-2 p-0" aria-labelledby="dropdownMenuButton1">
                        @foreach(App\Models\MedicalServiceCategory::active()->get() as $category)
                            @if($category->type->is(\App\Enums\MedicalServiceCategoryType::ONLINE()))
                                <a href="{{ route('front.patient.booking.create', $category->id) }}#nav-head" class="dropdown-item {{ request()->is('patient/booking/*') ? 'current-page' : '' }}">+ {{ $category->name }}</a>
                            @else
                                <a href="{{ route('front.patient.clinic-service.create', $category->id) }}#nav-head" class="dropdown-item {{ request()->is('patient/clinic-service/*') ? 'current-page' : '' }}">+ {{ $category->name }}</a>
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
