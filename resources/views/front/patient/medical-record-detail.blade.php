@extends('front.layout.main')

@section('content')
@include("front.partner._dashboard_banner")
<!-- ============================================-->
<!-- <section> begin ============================-->
<section class="pt-6 bg-gray">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-12 py-6">
                @include('front.patient._booking_navigation')


                <div class="w-100 floatleft">
                    <div class="container rounded bg-white mt-5 mb-5 box-shadow-1">
                        <div class="p-3 py-5">

                            <div class="row">

                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="mb-4">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <label class="fw-bold">No. Rekam Medis</label>
                                                    </div>
                                                    <div class="col-md-8">
                                                        : <span>MR2101-00001</span>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <label class="fw-bold">Tanggal Layanan</label>
                                                    </div>
                                                    <div class="col-md-8">
                                                        : <span>08/01/2020</span>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <label class="fw-bold">Layanan</label>
                                                    </div>
                                                    <div class="col-md-8">
                                                        : <span>Homecare</span>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <label class="fw-bold">Nama Dokter/Tenaga Medis</label>
                                                    </div>
                                                    <div class="col-md-8">
                                                        : <span>Lorem ip</span>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <label class="fw-bold">Diagnosa</label>
                                                    </div>
                                                    <div class="col-md-8">
                                                        : <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</span>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <label class="fw-bold">Penanganan</label>
                                                    </div>
                                                    <div class="col-md-8">
                                                        : <span>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</span>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <label class="fw-bold">Data Pendukung</label>
                                                    </div>
                                                    <div class="col-md-8">
                                                        :
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
<!-- <section> close ============================-->
<!-- ============================================-->
@endsection


