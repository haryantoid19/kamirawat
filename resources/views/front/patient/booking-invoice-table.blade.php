<table class="invoice">
    <tr class="details">
        <td colspan="2">
            <table>
                <thead>
                <tr>
                    <th class="desc">Nama Layanan</th>
                    <th class="amt">Subtotal</th>
                </tr>
                </thead>
                <tbody>
                <tr class="item">
                    <td class="desc">Jasa Layanan - {{ $medicalService->name }}</td>
                    <td class="amt">@rupiah($medicalService->price)</td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr class="totals">
        <td></td>
        <td>
            <table>
                <tr class="subtotal">
                    <td class="num">Subtotal</td>
                    <td class="num">@rupiah($medicalService->price)</td>
                </tr>
                <tr class="fees">
                    <td class="num">Shipping & Handling</td>
                    <td class="num">@rupiah($shippingFee)</td>
                </tr>
                <tr class="total">
                    <td>Total</td>
                    <td>@rupiah($total)</td>
                </tr>
            </table>
        </td>
    </tr>
</table>
