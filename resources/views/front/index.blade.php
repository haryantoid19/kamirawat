@extends('front.layout.main')

@section('content')
    <!-- ============================================-->
    <!-- <section> begin ============================-->
    <div id="home">
        <div class="container-fluid p-0">
            <div class="hero-slider">
                <div class="slider-item">
                    <img src="{{ asset('assets/frontend/img/slider/slider-1.jpg') }}" alt="">
                </div>
                <div class="slider-item">
                    <img src="{{ asset('assets/frontend/img/slider/slider-2.jpg') }}" alt="">
                </div>
                <div class="slider-item">
                    <img src="{{ asset('assets/frontend/img/slider/slider-3.jpg') }}" alt="">
                </div>
            </div>
        </div>
    </div>
    <!-- <section> close ============================-->
    <!-- ============================================-->


    <!-- ============================================-->
    <!-- <section> begin ============================-->
    <section id="layanan">

        <div class="sc-layanan-wrap sc-layanan mb-5">

            <div class="row">
                <div class="col-12 text-center">
                    <h1 class="mb-6 fw-medium title-section">Layanan Online</h1>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-6 col-lg-6 text-center mb-5">
                    <div class="sc-layanan-item">
                        <div class="sc-layanan-item-img">
                            <img src="{{ asset('assets/frontend/img/services-1.png') }}" height="100" alt="Home Visit + Home Care" />
                        </div>
                        <h4 class="mt-3 mb-0">Home Visit</h4>
                        <!-- <p class="text-info">Doctor Visit, Nurse Visit, Outpatient  & Geriatri Clinic </p> -->
                    </div>
                </div>
                <div class="col-sm-6 col-lg-6 text-center mb-5">
                    <div class="sc-layanan-item">
                        <div class="sc-layanan-item-img">
                            <img src="{{ asset('assets/frontend/img/services-1.png') }}" height="100" alt="Home Visit + Home Care" />
                        </div>
                        <h4 class="mt-3 mb-0">Home Care</h4>
                        <!-- <p class="text-info">Doctor Visit, Nurse Visit, Outpatient  & Geriatri Clinic </p> -->
                    </div>
                </div>
                <div class="col-sm-6 col-lg-6 text-center mb-5">
                    <div class="sc-layanan-item">
                        <div class="sc-layanan-item-img">
                            <img src="{{ asset('assets/frontend/img/services-1.png') }}" height="100" alt="Home Visit + Home Care" />
                        </div>
                        <h4 class="mt-3 mb-0">Home COVID</h4>
                        <!-- <p class="text-info">Doctor Visit, Nurse Visit, Outpatient  & Geriatri Clinic </p> -->
                    </div>
                </div>
                <div class="col-sm-6 col-lg-6 text-center mb-5">
                    <div class="sc-layanan-item">
                        <div class="sc-layanan-item-img">
                            <img src="{{ asset('assets/frontend/img/services-1.png') }}" height="100" alt="Home Visit + Home Care" />
                        </div>
                        <h4 class="mt-3 mb-0">Vanilla Clinic - Vagina Laborator</h4>
                        <!-- <p class="text-info">Doctor Visit, Nurse Visit, Outpatient  & Geriatri Clinic </p> -->
                    </div>
                </div>
            </div>
        </div>



        <div class="sc-layanan-wrap sc-layanan">

            <div class="row">
                <div class="col-12 text-center">
                    <h1 class="mb-6 fw-medium title-section">Klinik Kamirawat</h1>
                </div>
            </div>

            <div class="row">

                <div class="col-sm-4 col-lg-4 text-center mb-5">
                    <div class="sc-layanan-item">
                        <div class="sc-layanan-item-img">
                            <img src="{{ asset('assets/frontend/img/services-4.png') }}" height="100" alt="Dokter Specialist" />
                        </div>
                        <h4 class="mt-3 mb-0">Dokter Umum</h4>
                        <!-- <p class="text-info">Dokter Obgyn, Jantung, Syaraf, Internist, Psikolog dan Aestetic</p> -->
                    </div>
                </div>

                <div class="col-sm-4 col-lg-4 text-center mb-5">
                    <div class="sc-layanan-item">
                        <div class="sc-layanan-item-img">
                            <img src="{{ asset('assets/frontend/img/services-5.png') }}" height="100" alt="Dokter Specialist" />
                        </div>
                        <h4 class="mt-3 mb-0">Klinik Diabetes</h4>
                        <!-- <p class="text-info">Dokter Obgyn, Jantung, Syaraf, Internist, Psikolog dan Aestetic</p> -->
                    </div>
                </div>

                <div class="col-sm-4 col-lg-4 text-center mb-5">
                    <div class="sc-layanan-item">
                        <div class="sc-layanan-item-img">
                            <img src="{{ asset('assets/frontend/img/services-2.png') }}" height="100" alt="Dokter Specialist" />
                        </div>
                        <h4 class="mt-3 mb-0">Klinik Diabetes</h4>
                        <!-- <p class="text-info">Dokter Obgyn, Jantung, Syaraf, Internist, Psikolog dan Aestetic</p> -->
                    </div>
                </div>

                <!-- <div class="col-sm-4 col-lg-4 text-center mb-5">
                    <div class="sc-layanan-item">
                        <div class="sc-layanan-item-img">
                            <img src="{{ asset('assets/frontend/img/services-2.png') }}" height="100" alt="Beauty & V-Care" />
                        </div>
                        <h4 class="mt-3 mb-0">Beauty & V-Care</h4>
                        <p class="text-info">Tightening, Whitening, Laser Totok Miss V, Spa-Inject Miss V </p>
                    </div>
                </div>
                <div class="col-sm-4 col-lg-4 text-center mb-5">
                    <div class="sc-layanan-item">
                        <div class="sc-layanan-item-img">
                            <img src="{{ asset('assets/frontend/img/services-3.png') }}" height="100" alt="Pharmacy" />
                        </div>
                        <h4 class="mt-3 mb-0">Pharmacy</h4>
                        <p class="text-info">Medicine, Vitamins, Supplements, Vaccines </p>
                    </div>
                </div>
                <div class="col-sm-4 col-lg-4 text-center mb-5">
                    <div class="sc-layanan-item">
                        <div class="sc-layanan-item-img">
                            <img src="{{ asset('assets/frontend/img/services-5.png') }}" height="100" alt="Sexologist" />
                        </div>
                        <h4 class="mt-3 mb-0">Sexologist</h4>
                        <p class="text-info">Doctor Consultation, Sexual Health, Menopause Program</p>
                    </div>
                </div>
                <div class="col-sm-4 col-lg-4 text-center mb-5">
                    <div class="sc-layanan-item">
                        <div class="sc-layanan-item-img">
                            <img src="{{ asset('assets/frontend/img/services-6.png') }}" height="100" alt="Sex Therapist" />
                        </div>
                        <h4 class="mt-3 mb-0">Sex Therapist</h4>
                        <p class="text-info">Sex Therapy, Love & Pleasure Private Class</p>
                    </div>
                </div> -->
            </div>
        </div>

    </section>
    <!-- <section> close ============================-->
    <!-- ============================================-->


    <section id="tentang-kami">
        <div class="img-wrapper mb-4" style="background:url('{{ asset('assets/frontend/img/About.jpg') }}') center center/cover no-repeat;"></div>
        <div class="container">
            <div class="row">
                <div class="offset-sm-6 col-sm-6">
                    <div class="text-content">
                        <div class="row">
                            <div class="col-12 text-center">
                                <h1 class="mb-6 fw-medium title-section">Tentang Kami</h1>
                            </div>
                        </div>

                        <p class="fw-medium"><strong>Kamirawat</strong> hadir untuk Anda yang ingin merasakan pelayanan kesehatan primer langsung ke lokasi Anda. Lupakan antrian panjang di Rumah Sakit, lupakan rumitnya membuat janji temu ke Dokter favorit dan lupakan sulitnya menebus obat di Apotik. <strong>Kamirawat</strong> akan membantu Anda dan Orang Tersayang hanya dengan satu aplikasi.</p>
                            <br>
                        <p class="fw-medium">Melalui filosofi, <i>“Sentuhan Penuh Perhatian”</i> kami berupaya menghadirkan layanan perawatan kesehatan dengan kepedulian tinggi dan sepenuh hati. Didukung oleh Dokter dan Tenaga Medis yang profesional, berpengalaman dan tersertifikasi, <strong>Kamirawat</strong> memiliki beberapa layanan andalan, antara lain Home Visit, Home Care, Layanan Geriatri, Perawatan Ibu dan Bayi, Klinik Vanilla (Vagina Laboratory), Calm Clinic (Counseling and Living Mind Center) dan Home COVID.</p>
                        <br>
                        <p class="fw-medium">Percayakan kesehatan Anda bersama <strong>Kamirawat</strong>, mulai hari ini! Promo kesehatan lainnya, dapat dilihat <a href="#">di sini.</a></p>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <!-- ============================================-->
    <!-- <section> begin ============================-->
    <section>
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                    <h1 class="mb-6 fw-medium title-section">Fitur Kamirawat</h1>
                </div>
            </div>
        </div>

        <div class="sc-layanan-wrap sc-layanan">
            <div class="row">
                <div class="col-sm-4 col-lg-4 text-center mb-5">
                    <div class="sc-layanan-item">
                        <!-- <img src="{{ asset('assets/frontend/img/icons/layanan-1.svg') }}" height="100" alt="..." /> -->
                        <!-- <h4 class="mt-3 mb-0">Fitur</h4> -->
                        <h4 class="text-info">Order Secara Online</h4>
                    </div>
                </div>
                <div class="col-sm-4 col-lg-4 text-center mb-5">
                    <div class="sc-layanan-item">
                        <!-- <img src="{{ asset('assets/frontend/img/icons/layanan-1.svg') }}" height="100" alt="..." />
                        <h4 class="mt-3 mb-0">Fitur</h4> -->
                        <h4 class="text-info">Lihat Resume Medis</h4>
                    </div>
                </div>
                <div class="col-sm-4 col-lg-4 text-center mb-5">
                    <div class="sc-layanan-item">
                        <!-- <img src="{{ asset('assets/frontend/img/icons/layanan-1.svg') }}" height="100" alt="..." />
                        <h4 class="mt-3 mb-0">Fitur</h4> -->
                        <h4 class="text-info">Dapatkan Resep Digital</h4>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- <section> close ============================-->
    <!-- ============================================-->



    <!-- ============================================-->
    <!-- <section> begin ============================-->
    <section class="p-0">
        <img src="{{ asset('assets/frontend/img/footer.jpg') }}" class="img-100" alt="">
    </section>
    <!-- <section> close ============================-->
    <!-- ============================================-->


@endsection
