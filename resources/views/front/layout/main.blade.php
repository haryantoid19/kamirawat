<!DOCTYPE html>
<html lang="en-US" dir="ltr">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="noimageindex, noindex, nofollow, nosnippet">


    <!-- ===============================================-->
    <!--    Document Title-->
    <!-- ===============================================-->
    <title>Kamirawat</title>


    <!-- ===============================================-->
    <!--    Favicons-->
    <!-- ===============================================-->
    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('assets/frontend/img/favicons/apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('assets/frontend/img/favicons/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('assets/frontend/img/favicons/favicon-16x16.png') }}">
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('assets/frontend/img/favicons/favicon.png') }}">
    <link rel="manifest" href="{{ asset('assets/frontend/img/favicons/manifest.json') }}">
    <meta name="msapplication-TileImage" content="{{ asset('assets/frontend/img/favicons/mstile-150x150.png') }}">
    <meta name="theme-color" content="#ffffff">
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <!-- ===============================================-->
    <!--    Stylesheets-->
    <!-- ===============================================-->
    <link href="{{ asset('assets/frontend/css/theme.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/frontend/css/custom.css') }}" rel="stylesheet" />
    <script src="{{ asset('assets/frontend/js/jquery.min.js') }}"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.css">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.8/slick-theme.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.5.8/slick.css">

    <link rel="stylesheet" href="{{ asset('assets/frontend/vendors/chat-wa/frontend.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/frontend/vendors/chat-wa/qlwapp-icons.min.css') }}" />

    <style id='font-awesome-official-v4shim-inline-css' type='text/css'>
    @font-face {
    font-family: "FontAwesome";
    font-display: block;
    src: url("https://use.fontawesome.com/releases/v5.13.0/webfonts/fa-brands-400.eot"),
            url("https://use.fontawesome.com/releases/v5.13.0/webfonts/fa-brands-400.eot?#iefix") format("embedded-opentype"),
            url("https://use.fontawesome.com/releases/v5.13.0/webfonts/fa-brands-400.woff2") format("woff2"),
            url("https://use.fontawesome.com/releases/v5.13.0/webfonts/fa-brands-400.woff") format("woff"),
            url("https://use.fontawesome.com/releases/v5.13.0/webfonts/fa-brands-400.ttf") format("truetype"),
            url("https://use.fontawesome.com/releases/v5.13.0/webfonts/fa-brands-400.svg#fontawesome") format("svg");
    }

    @font-face {
    font-family: "FontAwesome";
    font-display: block;
    src: url("https://use.fontawesome.com/releases/v5.13.0/webfonts/fa-solid-900.eot"),
            url("https://use.fontawesome.com/releases/v5.13.0/webfonts/fa-solid-900.eot?#iefix") format("embedded-opentype"),
            url("https://use.fontawesome.com/releases/v5.13.0/webfonts/fa-solid-900.woff2") format("woff2"),
            url("https://use.fontawesome.com/releases/v5.13.0/webfonts/fa-solid-900.woff") format("woff"),
            url("https://use.fontawesome.com/releases/v5.13.0/webfonts/fa-solid-900.ttf") format("truetype"),
            url("https://use.fontawesome.com/releases/v5.13.0/webfonts/fa-solid-900.svg#fontawesome") format("svg");
    }

    @font-face {
    font-family: "FontAwesome";
    font-display: block;
    src: url("https://use.fontawesome.com/releases/v5.13.0/webfonts/fa-regular-400.eot"),
            url("https://use.fontawesome.com/releases/v5.13.0/webfonts/fa-regular-400.eot?#iefix") format("embedded-opentype"),
            url("https://use.fontawesome.com/releases/v5.13.0/webfonts/fa-regular-400.woff2") format("woff2"),
            url("https://use.fontawesome.com/releases/v5.13.0/webfonts/fa-regular-400.woff") format("woff"),
            url("https://use.fontawesome.com/releases/v5.13.0/webfonts/fa-regular-400.ttf") format("truetype"),
            url("https://use.fontawesome.com/releases/v5.13.0/webfonts/fa-regular-400.svg#fontawesome") format("svg");
    unicode-range: U+F004-F005,U+F007,U+F017,U+F022,U+F024,U+F02E,U+F03E,U+F044,U+F057-F059,U+F06E,U+F070,U+F075,U+F07B-F07C,U+F080,U+F086,U+F089,U+F094,U+F09D,U+F0A0,U+F0A4-F0A7,U+F0C5,U+F0C7-F0C8,U+F0E0,U+F0EB,U+F0F3,U+F0F8,U+F0FE,U+F111,U+F118-F11A,U+F11C,U+F133,U+F144,U+F146,U+F14A,U+F14D-F14E,U+F150-F152,U+F15B-F15C,U+F164-F165,U+F185-F186,U+F191-F192,U+F1AD,U+F1C1-F1C9,U+F1CD,U+F1D8,U+F1E3,U+F1EA,U+F1F6,U+F1F9,U+F20A,U+F247-F249,U+F24D,U+F254-F25B,U+F25D,U+F267,U+F271-F274,U+F279,U+F28B,U+F28D,U+F2B5-F2B6,U+F2B9,U+F2BB,U+F2BD,U+F2C1-F2C2,U+F2D0,U+F2D2,U+F2DC,U+F2ED,U+F328,U+F358-F35B,U+F3A5,U+F3D1,U+F410,U+F4AD;
    }
    </style>
</head>


<body>
<!-- ===============================================-->
<!--    Main Content-->
<!-- ===============================================-->
<main class="main" id="top">


    <!-- ============================================-->
    <!-- <section> begin ============================-->
    <section class="bg-primary py-0 d-none d-sm-block">

        <div class="container">
            <div class="row align-items-center">
                <div class="col-auto d-none d-lg-block">
                    <p class="my-2 fs--1"><i class="fas fa-phone-alt me-2"></i><a class="text-900" href="tel:081909201120">+6281909201120</a></p>
                </div>
                <div class="col-auto ms-md-auto order-md-2 d-none d-sm-block">
                    <ul class="list-unstyled list-inline my-2">
                        <li class="list-inline-item"><a target="_blank" class="text-decoration-none" href="https://www.facebook.com/Kamirawat-100445215787535"><i class="fab fa-facebook-f text-900"></i></a></li>
                        <li class="list-inline-item"><a target="_blank" class="text-decoration-none" href="https://www.instagram.com/kamirawat.official/"><i class="fab fa-instagram text-900"> </i></a></li>
                        <li class="list-inline-item"><a target="_blank" class="text-decoration-none" href="https://www.linkedin.com/company/arsatyajayamedika/about/"><i class="fab fa-linkedin text-900"> </i></a></li>
                    </ul>
                </div>
                <div class="col-auto">
                    <p class="my-2 fs--1"><i class="fas fa-envelope me-2"></i><a class="text-900" href="mailto:info@kamirawat.com">info@kamirawat.com</a></p>
                </div>
            </div>
        </div>
        <!-- end of .container-->

    </section>
    <!-- <section> close ============================-->
    <!-- ============================================-->

    <nav class="navbar navbar-expand-lg navbar-light sticky-top py-1 d-block" data-navbar-on-scroll="data-navbar-on-scroll">
        <div class="container">
            <a class="navbar-brand" href="{{ route('front.index') }}">
                <img src="{{ asset('assets/frontend/img/kamirawat-logo.svg') }}" height="70" alt="logo" />
            </a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"> </span></button>
            <div class="collapse navbar-collapse border-top border-lg-0 mt-4 mt-lg-0" id="navbarSupportedContent">
                <ul class="navbar-nav ms-auto pt-2 pt-lg-0">
                    <li class="nav-item px-2"><a class="nav-link active" aria-current="page" href="{{ route('front.index') }}">{{ __('Beranda') }}</a></li>
                    <!-- <li class="nav-item px-2"><a class="nav-link js-section-scroll" aria-current="page" href="{{ route('front.index') }}#layanan">Layanan</a></li> -->
                    <!-- <li class="nav-item px-2"><a class="nav-link js-section-scroll" aria-current="page" href="{{ route('front.index') }}#tentang-kami">Tentang Kami</a></li> -->
                    <li class="nav-item dropdown">
                        <a class="nav-link  dropdown-toggle" href="#" data-bs-toggle="dropdown">Layanan Online</a>
                        <ul class="dropdown-menu dropdown-hover">
                            <li><a class="dropdown-item" href="{{ route('front.home-visit') }}">Home Visit</a></li>
                            <li><a class="dropdown-item" href="{{ route('front.home-care') }}">Home Care</a></li>
                            <li><a class="dropdown-item" href="{{ route('front.home-covid') }}">Home COVID</a></li>
                            <li><a class="dropdown-item" href="{{ route('front.vanilla-clinic') }}">Vanilla Clinic</a></li>
                        </ul>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link  dropdown-toggle" href="#" data-bs-toggle="dropdown">Layanan Offline</a>
                        <ul class="dropdown-menu dropdown-hover">
                            <li><a class="dropdown-item" href="{{ route('front.konsultasi-dokter') }}">Konsultasi Dokter</a></li>
                            <li><a class="dropdown-item" href="{{ route('front.klinik-diabetes') }}">Klinik Diabetes</a></li>
                            <li><a class="dropdown-item" href="{{ route('front.calm-clinic') }}">Calm Clinic</a></li>
                        </ul>
                    </li>
                    <li class="nav-item px-2"><a class="nav-link" aria-current="page" href="{{ route('front.promo') }}">Promo</a></li>
                    <li class="nav-item px-2"><a class="nav-link" aria-current="page" href="{{ route('front.blog') }}">Cerita Kami</a></li>
                    <li class="nav-item px-2"><a class="nav-link" aria-current="page" href="{{ route('front.faq') }}">FAQ</a></li>
                    <li class="nav-item px-2"><a class="nav-link" aria-current="page" href="mailto:info@kamirawat.com">Hubungi Kami</a></li>
                </ul>
                <div class="dropdown {{ config('kamirawat.under_construction_mode') ? 'd-none' : '' }}">
                    @if(Auth::guest())
                        <button type="button" class="btn btn-primary text-white dropdown-toggle ms-2" data-bs-toggle="dropdown" aria-expanded="false">Masuk</button>

                        <div class="dropdown-menu dropdown-menu-lg-end dropdown-menu-nav text-end" aria-labelledby="dropdownMenuButton1" style="top:55px">
                            <a class="nav-link" aria-current="page" href="{{ route('front.patient.login') }}">Sebagai Pasien</a>
                            <a class="nav-link" aria-current="page" href="{{ route('front.partner.login') }}">Sebagai Tenaga Medis</a>
                        </div>
                    @else
                        <button type="button" class="ms-2 btn-icon float-start" data-bs-toggle="dropdown" aria-expanded="false">
                            <div class="img-avatar">
                                @if (Auth::user()->hasRole(\App\Enums\UserRole::PATIENT()->value))
                                    <img class="img-rounded" src="{{ Auth::user()->patient->avatar_url }}">
                                @else
                                    <img class="img-rounded" src="{{ Auth::user()->medical_people->avatar_url }}">
                                @endif
                            </div>
                        </button>

                        @if (Auth::user()->hasRole(\App\Enums\UserRole::PATIENT()->value))
                            <div class="dropdown-menu dropdown-menu-lg-end dropdown-menu-nav" aria-labelledby="dropdownMenuButton1" style="top:55px">
                                <a class="nav-link" aria-current="page" href="{{ route('front.patient.dashboard') }}"><i class="fas fa-user-alt"></i> Profile</a>
                                <a class="nav-link" aria-current="page" href="#" onClick="event.preventDefault();document.getElementById('patient-logout').submit();"><i class="fas fa-sign-out-alt"></i> Logout</a>
                            </div>

                            <form method="POST" action="{{ route('front.patient.logout') }}" id="patient-logout" style="display:none;">
                                @csrf
                                <input type="hidden" name="continue" value="front.index" />
                            </form>
                        @else
                            <div class="dropdown-menu dropdown-menu-lg-end dropdown-menu-nav" aria-labelledby="dropdownMenuButton1" style="top:55px">
                                <a class="nav-link" aria-current="page" href="{{ route('front.partner.dashboard') }}"><i class="fas fa-user-alt"></i> Profile</a>
                                <a class="nav-link" aria-current="page" href="#" onClick="event.preventDefault();document.getElementById('partner-logout').submit();"><i class="fas fa-sign-out-alt"></i> Logout</a>
                            </div>

                            <form method="POST" action="{{ route('front.partner.logout') }}" id="partner-logout" style="display:none;">
                                @csrf
                                <input type="hidden" name="continue" value="front.index" />
                            </form>
                        @endif
                    @endif
                </div>

            </div>
        </div>
    </nav>
    @yield('content', '')

</main>
<!-- ===============================================-->
<!--    End of Main Content-->
<!-- ===============================================-->


<footer>

    <div class="container container-footer">
        <div class="row">
            <div class="col-12 col-sm-3 col-lg-3 mb-3 order-2 order-sm-1">
                <h5 class="fw-bold mb-2">Kamirawat</h5>
                <ul class="list-unstyled mb-md-4 mb-lg-0 footer-nav">
                    <li class="lh-lg"><a href="{{ route('front.tentangkami') }}">Tentang Kami</a></li>
                    <!-- <li class="lh-lg"><a href="{{ route('front.faq') }}">FAQ</a></li> -->
                    <li class="lh-lg"><a href="#!">Karir</a></li>
                    <li class="lh-lg"><a href="#!">Kemitraan</a></li>
                    <li class="lh-lg"><a href="{{ route('front.syaratdanketentuan') }}">Syarat dan Ketentuan</a></li>
                </ul>
            </div>
            <div class="col-12 col-sm-3 col-lg-3 mb-3 order-2 order-sm-1">
                <h5 class="fw-bold mb-2">Hubungi Kami</h5>
                <div class="mb-md-4 mb-lg-0">
                    <p><strong>PT. Arsatya Jaya Medika</strong> <br>
                    Jalan Benda Raya No. 12D <br>
                    Kel. Cilandak Timur Kec. Pasar Minggu <br>
                    Jakarta Selatan 12560<p>
                </div>
            </div>
            <div class="col-12 col-sm-3 col-lg-3 mb-3 order-2 order-sm-1">
                <h5 class="fw-bold mb-2">Customer Service</h5>
                <div class="mb-md-4 mb-lg-0 ps-3">
                    <p>
                        <i class="fas fa-phone-alt me-3"></i> <a href="tel:081909201120">+6281909201120</a>
                    </p>
                    <p>
                        <i class="fas fa-envelope me-3"></i> <a href="mailto:info@kamirawat.com">info@kamirawat.com</a>
                    </p>
                </div>
            </div>
            <div class="col-12 col-sm-3 col-lg-3 mb-3 order-2 order-sm-1">
                <h5 class="fw-bold mb-2">Social Media</h5>
                <ul class="footer-sosmed mb-md-4 mb-lg-0">
                    <li class="list-inline-item"><a class="text-decoration-none" href="https://www.facebook.com/Kamirawat-100445215787535"><i class="fab fa-facebook-f"></i></a></li>
                    <li class="list-inline-item"><a class="text-decoration-none" href="https://www.instagram.com/kamirawat.official/"><i class="fab fa-instagram"> </i></a></li>
                    <li class="list-inline-item"><a class="text-decoration-none" href="https://www.linkedin.com/company/arsatyajayamedika/about/"><i class="fab fa-linkedin"> </i></a></li>
                </ul>
            </div>
        </div>
    </div>
    <!-- end of .container-->


    <div class="copyright">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <p>&copy; Copyright <b>Kamirawat</b>. All Rights Reserved</p>
                </div>
            </div>
        </div>
    </div>

</footer>

<div class="note-wa">
    Klik tombol Whatsapp untuk bantuan
</div>
<div id='fb-pxl-ajax-code'></div><div id="qlwapp" data-autoloadelay="5000" class="qlwapp-premium auto-load qlwapp-bubble qlwapp-bottom-right qlwapp-all qlwapp-rounded ">
  <div class="qlwapp-container">
          <div class="qlwapp-box">
        <div class="qlwapp-header">
          <div class="qlwapp-carousel">

            <div class="qlwapp-slide">
                <i class="qlwapp-close" data-action="close">&times;</i>
                <div class="qlwapp-account">
                    <div class="qlwapp-info">
                        <span class="qlwapp-label">Admin </span>
                        <span class="qlwapp-name">Kamirawat</span>
                    </div>
                    <div class="qlwapp-avatar">
                        <div class="qlwapp-avatar-container">
                            <img alt="Kamirawat" src="{{ asset('assets/frontend/img/264066081_4724610564265953_8137348087327126718_n.jpeg') }}">
                        </div>
                    </div>
                </div>

              </div>

            <!-- <div class="qlwapp-slide">
              <div class="qlwapp-contact">
                <div class="qlwapp-previous" data-action="previous">
                  <i class="qlwf-arrow_left"></i>
                </div>
                <div class="qlwapp-info">
                  <span class="qlwapp-name">%</span>
                  <span class="qlwapp-label">%</span>
                </div>
                <div class="qlwapp-avatar">
                  <div class="qlwapp-avatar-container">
                    <img alt="#" src="#" />
                  </div>
                </div>
              </div>
            </div> -->

          </div>
        </div>
        <div class="qlwapp-body">
          <div class="qlwapp-carousel">
            <!-- <div class="qlwapp-slide">
                <a class="qlwapp-account" data-action="chat" data-timefrom="00:00" data-timeto="00:00" data-timeout="readonly" data-phone="6281909201120" data-timedays="[]" data-timezone="0" data-message="Halo! Ada yang bisa Kamirawat bantu?" href="javascript:void(0);" target="_blank">
                    <div class="qlwapp-avatar">
                        <div class="qlwapp-avatar-container">
                            <img alt="Kamirawat" src="{{ asset('assets/frontend/img/264066081_4724610564265953_8137348087327126718_n.jpeg') }}">
                        </div>
                    </div>
                    <div class="qlwapp-info">
                        <span class="qlwapp-label">Admin </span>
                        <span class="qlwapp-name">Kamirawat</span>
                        <time class="qlwapp-label">Available from 00:00 to 00:00</time>
                    </div>
                </a>
            </div> -->
            <div class="qlwapp-slide">
              <div class="qlwapp-chat">
                <div class="qlwapp-message">
                    <p><span style="font-size: 150%"><strong>Halo!</strong></span></p>
                    <p><span style="font-size: 90%">Ada yang bisa Kamirawat bantu?</span></p>
                </div>
                <!--<div class="qlwapp-user"></div>-->
              </div>
            </div>
          </div>
        </div>
            <!-- <div class="qlwapp-footer" data-contactstimeout="">
            <p><span style="font-size: 100%">Atau kirimkan email anda ke <a href="mailto:info@kamirawat.com">info@kamirawat.com</a></span></p>
          </div> -->
                <div class="qlwapp-response" data-action="response">
          <pre></pre>
          <textarea maxlength="500" name="message" placeholder="Write a response" aria-label="Write a response" tabindex="0"></textarea>
          <div class="qlwapp-buttons">
            <!-- <i class="qlwf-emoji"></i> -->
            <a class="qlwapp-reply" data-action="open" data-message="Halo! Ada yang bisa Kamirawat bantu?" href="javascript:void(0);" target="_blank">
              <i class="qlwf-send"></i>
            </a>
          </div>
        </div>
      </div>
        <a class="qlwapp-toggle" data-action="box" data-phone="6281909201120" data-timefrom="00:00" data-timedays="[]" data-timeto="00:00" data-timeout="readonly" data-phone="6281909201120" data-timezone="0" data-message="Halo! Ada yang bisa Kamirawat bantu?" href="javascript:void(0);" target="_blank">
            <i class="qlwapp-icon qlwf-whatsapp"></i>
            <i class="qlwapp-close" data-action="close">&times;</i>
         </a>

      </div>


<!-- ===============================================-->
<!--    JavaScripts-->
<!-- ===============================================-->
<script src="{{ asset('assets/frontend/vendors/@popperjs/popper.min.js') }}"></script>
<script src="{{ asset('assets/frontend/vendors/bootstrap/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/frontend/vendors/is/is.min.js') }}"></script>
<script src="https://polyfill.io/v3/polyfill.min.js?features=window.scroll"></script>
<script src="{{ asset('assets/frontend/vendors/fontawesome/all.min.js') }}"></script>
<script src="{{ asset('assets/frontend/js/jquery.blockui.js') }}"></script>

<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap.min.js"></script>

<script src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>

<script src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

<script src='https://ajax.aspnetcdn.com/ajax/jquery.validate/1.15.0/jquery.validate.js'></script>
<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery-steps/1.1.0/jquery.steps.js'></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.3/jquery.validate.js"></script>
<script src='https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places&key=AIzaSyCfQuZeOHKKIwEE_HBr0qW0KSkffNbA6vA'></script>

<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.slick/1.5.7/slick.min.js"></script>

@yield('script')

<script src="{{ asset('assets/frontend/js/theme.js') }}"></script>
<script src="{{ mix('js/web.js') }}"></script>

<script src="{{ asset('assets/frontend/vendors/chat-wa/frontend.js') }}"></script>
<script src="{{ asset('assets/frontend/vendors/is/is.min.js') }}"></script>

</body>

</html>
