<!DOCTYPE html>
<html lang="en-US" class="no-js">
<head>
    <meta charset="UTF-8">

    <link href="//www.google-analytics.com" rel="dns-prefetch">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=5.0, minimum-scale=0.86">

    <meta name="description" content="Under Construction - Kamirawat">
      <!-- ===============================================-->
    <!--    Stylesheets-->
    <!-- ===============================================-->
    <link href="{{ asset('assets/frontend/css/theme.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/frontend/css/custom.css') }}" rel="stylesheet" />
    <script src="{{ asset('assets/frontend/js/jquery.min.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('assets/frontend/vendors/chat-wa/frontend.css') }}" />
    <link rel="stylesheet" href="{{ asset('assets/frontend/vendors/chat-wa/qlwapp-icons.min.css') }}" />
    <style>
        * {
            margin: 0;
            padding: 0;
            box-sizing: border-box;
        }
        body {
            font-family: 'Helvetica', 'Arial', sans-serif;
            font-size: 14px;
            font-weight: 400;
            line-height: 1.5;
            color: #717171;
            -webkit-text-size-adjust: 100%;
            -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
        }

        .uppercase {
            text-transform: uppercase;
        }

        a {
            text-decoration: none;
        }

        #logo {
            width: 100%;
            text-align: center;
        }

        #logo img {
            width: 100%;
            max-width: 250px;
        }

        .content-inner {
            text-align: center;
        }

        .content-inner h1 {
            background-color: #1b575e;
            padding: 20px;
            color: #fff;
            border-radius: 20px;
            font-size: 35px;
            text-align: center;
            margin: 20px 0;
        }

        .content-inner h4 {
            color: #1b575e;
            font-size: 24px;
        }

        .btn {
            display: inline-table;
            background-color: #8ed1d3;
            color: #1b575e;
            padding: 10px 50px 10px 30px;
            margin-top: 20px;
            position: relative;
            border: 1px solid #8ed1d3;
        }

        .btn::after {
            background-image: url("data:image/svg+xml,%3Csvg id='Layer_1' data-name='Layer 1' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 20 18'%3E%3Cdefs%3E%3Cstyle%3E.cls-1%7Bfill:%231b575e;%7D%3C/style%3E%3C/defs%3E%3Ctitle%3EArtboard 6%3C/title%3E%3Cpolygon class='cls-1' points='0 0 0 18 20 9 0 0'/%3E%3C/svg%3E");
            background-repeat: no-repeat;
            background-size: cover;
            position: absolute;
            content: '';
            right: 20px;
            top: 0;
            bottom: 0;
            margin: auto;
            width: 15px;
            height: 15px;
        }

        .btn:hover {
            background-color: #fff;
        }

        /* Sosmed */
        .sosmed {
            margin-top: 100px;
        }
        .sosmed ul {
            display: flex;
            gap: 30px;
            align-items: center;
            justify-content: center;
            padding: 0;
        }
        .sosmed ul li {
            display: inline-table;
        }
        .sosmed ul li img {
            width: auto;
            height: 40px;
        }

        .bg {
            position: absolute;
            z-index: -1;
            width: auto;
            height: 100vh;
            right: 0;
        }

        .container {
            align-items: center;
            max-width: 1200px;
            margin: 0 auto;
            height: 100vh;
            display: flex;
            flex-wrap: wrap;
        }

        .col-L {
            width: 100%;
            max-width: 600px;
        }

        .col-L {
            padding: 40px 20px;
            margin: 0 20px;
            width: calc(100% - 40px);
            background: rgba(255, 255, 255, 0.7);
            border-radius: 20px;
        }

        @keyframes downMotion {
            0% {
                transform: translate(0, -15px);
            }

            50% {
                transform: translate(0, 0);
            }

            100% {
                transform: translate(0, -15px);
            }
        }

        @-webkit-keyframes downMotion {
            0% {
                transform: translate(0, -15px);
            }

            50% {
                transform: translate(0, 0);
            }

            100% {
                transform: translate(0, -15px);
            }
        }

        @-moz-keyframes downMotion {
            0% {
                transform: translate(0, -15px);
            }

            50% {
                transform: translate(0, 0);
            }

            100% {
                transform: translate(0, -15px);
            }
        }

        @keyframes upMotion {
            0% {
                transform: translate(0, 15px);
            }

            50% {
                transform: translate(0, 0);
            }

            100% {
                transform: translate(0, 15px);
            }
        }

        @-webkit-keyframes upMotion {
            0% {
                transform: translate(0, 15px);
            }

            50% {
                transform: translate(0, 0);
            }

            100% {
                transform: translate(0, 15px);
            }
        }

        @-moz-keyframes upMotion {
            0% {
                transform: translate(0, 15px);
            }

            50% {
                transform: translate(0, 0);
            }

            100% {
                transform: translate(0, 15px);
            }
        }

        @keyframes rotate {
            100% {
                transform: rotate(360deg);
            }
        }

        @-webkit-keyframes rotate {
            100% {
                -webkit-transform: rotate(360deg);
            }
        }

        @-moz-keyframes rotate {
            100% {
                -webkit-transform: rotate(360deg);
            }
        }

        @-o-keyframes rotate {
            100% {
                -webkit-transform: rotate(360deg);
            }
        }


        @media screen and (max-width: 767px) {

            #logo img {
                max-width: 150px;
            }

            .content-inner h1 {
                font-size: 16px;
            }

            .content-inner h4 {
                font-size: 14px;
            }

            .bg {
                height: 100vw;
            }

            .sosmed ul li img {
                height: 20px;
            }

        }
    </style>

    <link rel="shortcut icon" href="{{ asset('assets/frontend/img/favicons/favicon.png') }}" />
    <title>Under Maintenance - Kamirawat</title>
</head>
<body>
<style media="screen">
      .icon-wa {
        width: 65px;
        height: 65px;  
        position: fixed;
        right: 20px;
        bottom: 80px;
        padding: 15px;
        z-index: 9999;
        background: #40c351;
        border-radius: 100%;
        box-shadow: 2px 2px 3px #999;
      }
      .icon-wa:hover {
        background: #08a317;
      }
      .icon-wa svg {
        width: 35px;
        height: 35px;
      }
    </style>    


<img src="{{ asset('assets/frontend/img/img-bg.svg') }}" class="bg" alt="Kamirawat" />
<div class="container">
    <div class="col-L">
        <div id="logo">
            <img src="{{ asset('assets/frontend/img/kamirawat-logo.svg') }}" alt="Logo Kamirawat" />
        </div>
        <div class="content-inner">
            <h1 class="uppercase">Segera Melayani Anda</h1>

            <h4>Dapatkan info lebih lanjut untuk pendaftaran tenaga medis di bawah ini.</h4>

            <a href="{{ route('front.partner.register') }}" class="uppercase btn">Daftar Tenaga Medis</a>
        </div>


        <div class="sosmed">
            <ul>
                <li>
                    <a target="_blank" href="https://www.instagram.com/kamirawat.official/">
                        <img src="{{ asset('assets/frontend/img/icn-ig.svg') }}" alt="Instagram" />
                    </a>
                </li>
                <li>
                    <a target="_blank" href="https://www.facebook.com/Kamirawat-100445215787535">
                        <img src="{{ asset('assets/frontend/img/icn-fb.svg') }}" alt="Facebook" />
                    </a>
                </li>
                <li>
                    <a target="_blank" href="https://www.linkedin.com/company/arsatyajayamedika/about/">
                        <img src="{{ asset('assets/frontend/img/icn-in.svg') }}" alt="Linkedin" />
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>

<div class="note-wa">
		   Klik tombol Whatsapp untuk bantuan
		</div>
		<div id='fb-pxl-ajax-code'></div>
		<div id="qlwapp" data-autoloadelay="5000" class="qlwapp-premium auto-load qlwapp-bubble qlwapp-bottom-right qlwapp-all qlwapp-rounded ">
			<div class="qlwapp-container">
			   <div class="qlwapp-box">
			      <div class="qlwapp-header">
			         <div class="qlwapp-carousel">
			            <div class="qlwapp-slide">
			               <div class="qlwapp-contact">
			                  <div class="qlwapp-info">
			                     <span class="qlwapp-name">Kamirawat</span>
			                     <span class="qlwapp-label">Admin</span>
			                  </div>
			                  <div class="qlwapp-avatar">
			                     <div class="qlwapp-avatar-container">
			                        <img alt="#" src="{{ asset('assets/frontend/img/264066081_4724610564265953_8137348087327126718_n.jpeg') }}" />
			                     </div>
			                  </div>
			               </div>
			            </div>
			         </div>
			      </div>
			      <div class="qlwapp-body">
			         <div class="qlwapp-carousel">
			            <div class="qlwapp-slide">
			               <div class="qlwapp-chat">
			                  <div class="qlwapp-message">
			                     Halo! Ada yang bisa Kamirawat bantu?
			                  </div>
			               </div>
			            </div>
			         </div>
			      </div>
			      <div class="qlwapp-footer" data-contactstimeout="">
			         <p><span style="font-size: 100%">Atau kirimkan email anda ke <a href="mailto:info@kamirawat.com">info@kamirawat.com</a></span></p>
			      </div>
			      <div class="qlwapp-response" data-action="response">
			         <pre></pre>
			         <textarea maxlength="500" name="message" placeholder="Write a response" aria-label="Write a response" tabindex="0"></textarea>
			         <div class="qlwapp-buttons">
			            <!-- <i class="qlwf-emoji"></i> -->
			            <a class="qlwapp-reply" data-phone="6281909201120" data-action="open" data-message="Halo! Ada yang bisa Kamirawat bantu?" href="javascript:void(0);" target="_blank">
			            <i class="qlwf-send"></i>
			            </a>
			         </div>
			      </div>
			   </div>
			   <a class="qlwapp-toggle" data-action="box" data-phone="6281909201120" data-timeout="readonly" href="javascript:void(0);" target="_blank">
			   <i class="qlwapp-icon qlwf-whatsapp"></i>
			   <i class="qlwapp-close" data-action="close">&times;</i>          
			   </a>
			</div>
		</div>



      <!-- ===============================================-->
        <!--    JavaScripts-->
        <!-- ===============================================-->
      

        <script src="{{ asset('assets/frontend/vendors/chat-wa/frontend.js') }}"></script>
        <script src="{{ asset('assets/frontend/vendors/is/is.min.js') }}"></script>
</body>


</html>
