<!--begin::Toolbar-->
<div class="toolbar" id="kt_toolbar">
    <!--begin::Container-->
    <div id="kt_toolbar_container" class="{{ theme()->printHtmlClasses('toolbar-container', false) }} d-flex flex-stack">
        @if (theme()->getOption('layout', 'page-title/display') && theme()->getOption('layout', 'header/left') !== 'page-title')
            {{ theme()->getView('layout/page-title/_default') }}
        @endif

		<!--begin::Actions-->
        <div class="d-flex align-items-center py-1">
            @if(theme()->hasOption('page', 'create_link'))
                <!--begin::Wrapper-->
                <div data-bs-toggle="tooltip" data-bs-placement="left" data-bs-trigger="hover" title="{{ theme()->getOption('page', 'create_tooltip') }}">
                    <a href="{{ url(theme()->getOption('page', 'create_link')) }}" class="btn btn-sm btn-primary fw-bolder">
                        Create
                    </a>
                </div>
                <!--end::Wrapper-->
            @endif

            @if(theme()->hasOption('page', 'create_modal_target'))
                <!--begin::Wrapper-->
                <div data-bs-toggle="tooltip" data-bs-placement="left" data-bs-trigger="hover" title="{{ theme()->getOption('page', 'create_tooltip') }}">
                    <a href="#" data-bs-toggle="modal" data-bs-target="{{ theme()->getOption('page', 'create_modal_target') }}" class="btn btn-sm btn-primary fw-bolder">
                        Create
                    </a>
                </div>
                <!--end::Wrapper-->
            @endif

        </div>
		<!--end::Actions-->
    </div>
    <!--end::Container-->
</div>
<!--end::Toolbar-->
