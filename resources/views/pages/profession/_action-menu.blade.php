<!--begin::Action--->
<td class="text-end">
    <button data-edit="{{ route('professions.show', $profession->id) }}" class="btn btn-sm btn-light-warning btn-active-warning">
        Edit
    </button>
    <button data-destroy="{{ route('professions.destroy', $profession->id) }}" class="btn btn-sm btn-light-danger btn-active-danger">
        Delete
    </button>
</td>
<!--end::Action--->
