<!--begin::Action--->
<td class="text-end">
    <button data-edit="{{ route('payment-methods.show', $payment_method->id) }}" class="btn btn-sm btn-light-warning btn-active-warning">
        Edit
    </button>
    <button data-destroy="{{ route('payment-methods.destroy', $payment_method->id) }}" class="btn btn-sm btn-light-danger btn-active-danger">
        Delete
    </button>
</td>
<!--end::Action--->
