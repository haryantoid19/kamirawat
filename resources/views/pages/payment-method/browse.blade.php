<x-base-layout>

    <!--begin::Card-->
    <div class="card">
        <!--begin::Card body-->
        <div class="card-body pt-6">
            <!--begin::Table-->
            {{ $dataTable->table() }}
            <!--end::Table-->

            {{-- Inject Scripts --}}
            @section('scripts')
                {{ $dataTable->scripts() }}

                <script type="text/javascript">
                    window.new_payment_method_url = '{{ route('payment-methods.store') }}';

                    $(function() {
                        $.ajaxSetup({headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}});
                        LaravelDataTables["payment-method-table"].on('click', '[data-destroy]', function (e) {
                            e.preventDefault();
                            if (!confirm("Are you sure to delete this record?")) {
                                return;
                            }
                            axios.delete($(this).data('destroy'), {
                                '_method': 'DELETE',
                            })
                                .then(function () {
                                    LaravelDataTables["payment-method-table"].ajax.reload();
                                })
                                .catch(function (error) {
                                    console.log(error);
                                });
                        });
                    });

                </script>
            @endsection
        </div>
        <!--end::Card body-->
    </div>
    <!--end::Card-->

    <!--begin::Modal - New Target-->
    <div class="modal fade" id="kr_new_payment_method_modal" tabindex="-1" aria-hidden="true">
        <!--begin::Modal dialog-->
        <div class="modal-dialog modal-dialog-centered mw-650px">
            <!--begin::Modal content-->
            <div class="modal-content rounded">
                <!--begin::Modal header-->
                <div class="modal-header pb-0 border-0 justify-content-end">
                    <!--begin::Close-->
                    <div class="btn btn-sm btn-icon btn-active-color-primary" data-bs-dismiss="modal">
                        <!--begin::Svg Icon | path: icons/duotune/arrows/arr061.svg-->
                        <span class="svg-icon svg-icon-1">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="black" />
                                <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="black" />
                            </svg>
                        </span>
                        <!--end::Svg Icon-->
                    </div>
                    <!--end::Close-->
                </div>
                <!--begin::Modal header-->
                <!--begin::Modal body-->
                <div class="modal-body scroll-y px-10 px-lg-15 pt-0 pb-15">
                    <!--begin:Form-->
                    <form id="kr_new_payment_method_form" class="form" action="#">
                        <!--begin::Heading-->
                        <div class="mb-13 text-center">
                            <!--begin::Title-->
                            <h1 class="mb-3">Create a New Payment Method</h1>
                            <!--end::Title-->
                            <!--begin::Description-->
                            <div class="text-muted fw-bold fs-5">Silahkan isi form dibawah ini untuk menambah metode pembayaran</div>
                            <!--end::Description-->
                        </div>
                        <!--end::Heading-->
                        <!--begin::Input group-->
                        <div class="d-flex flex-column mb-8 fv-row">
                            <!--begin::Label-->
                            <label class="d-flex align-items-center fs-6 fw-bold mb-2" for="label">
                                <span class="required">Label</span>
                                <i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="Label metode pembayaran akan muncul di halaman booking layanan"></i>
                            </label>
                            <!--end::Label-->
                            <input type="text" class="form-control form-control-solid" id="label" placeholder="Masukkan label" name="label" />
                        </div>
                        <!--end::Input group-->
                        <!--begin::Input group-->
                        <div class="d-flex flex-column mb-8 fv-row">
                            <!--begin::Label-->
                            <label class="d-flex align-items-center fs-6 fw-bold mb-2" for="code">
                                <span class="required">Code</span>
                                <i class="fas fa-exclamation-circle ms-2 fs-7" data-bs-toggle="tooltip" title="Kode metode pembayaran yang akan digunakan untuk integrasi dengan payment services, misal untuk Bank Mandiri, masukkan MANDIRI."></i>
                            </label>
                            <!--end::Label-->
                            <input type="text" class="form-control form-control-solid" id="code" placeholder="Masukkan kode" name="code" />
                        </div>
                        <!--end::Input group-->
                        <!--begin::Input group-->
                        <div class="d-flex flex-column mb-8 fv-row">
                            <!--begin::Label-->
                            <label class="d-flex align-items-center fs-6 fw-bold mb-2" for="gateway">
                                <span class="required">Gateway</span>
                            </label>
                            <!--end::Label-->
                            <select name="gateway" class="form-control form-control-solid" id="gateway">
                                <option value="">Pilih gateway</option>
                                @foreach(\App\Enums\PaymentMethodGateway::getInstances() as $gateway)
                                    <option value="{{ $gateway->value }}">{{ $gateway->label() }}</option>
                                @endforeach
                            </select>
                        </div>
                        <!--end::Input group-->
                        <!--begin::Input group-->
                        <div class="d-flex flex-column mb-8 fv-row">
                            <!--begin::Label-->
                            <label class="d-flex align-items-center fs-6 fw-bold mb-2" for="description">
                                <span>Deskripsi</span>
                            </label>
                            <!--end::Label-->
                            <textarea class="form-control form-control-solid" id="description" placeholder="Masukkan deskripsi" name="description"></textarea>
                        </div>
                        <!--end::Input group-->
                        <!--begin::Input group-->
                        <div class="d-flex flex-column mb-8 fv-row">
                            <!--begin::Label-->
                            <label class="d-flex align-items-center fs-6 fw-bold mb-2" for="status">
                                <span class="required">Status</span>
                            </label>
                            <!--end::Label-->
                            <select name="status" class="form-control form-control-solid" id="status">
                                <option value="">Pilih status</option>
                                @foreach(\App\Enums\PaymentMethodStatus::getInstances() as $status)
                                    <option value="{{ $status->value }}">{{ $status->value }}</option>
                                @endforeach
                            </select>
                        </div>
                        <!--end::Input group-->
                        <!--begin::Input group-->
                        <div class="mb-7">
                            <!--begin::Label-->
                            <label class="fs-6 fw-bold mb-2">
                                <span>Icon</span>
                                <i class="fas fa-exclamation-circle ms-1 fs-7" data-bs-toggle="tooltip" title="Allowed file types: png, jpg, jpeg, svg."></i>
                            </label>
                            <!--end::Label-->
                            <!--begin::Image input wrapper-->
                            <div class="mt-1">
                                <!--begin::Image input-->
                                <div class="image-input image-input-outline bg-contain"
                                     data-kt-image-input="true"
                                     style="background-image: url({{ asset('assets/frontend/img/bank-transfer-icon.jpeg') }});background-position:center;background-size:contain;"
                                >
                                    <!--begin::Preview existing avatar-->
                                    <div
                                        class="image-input-wrapper w-125px h-125px"
                                        style="background-image: url({{ asset('assets/frontend/img/bank-transfer-icon.jpeg') }});background-position:center;background-size:contain;"
                                    ></div>
                                    <!--end::Preview existing avatar-->
                                    <!--begin::Edit-->
                                    <label class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="change" data-bs-toggle="tooltip" title="Change icon">
                                        <i class="bi bi-pencil-fill fs-7"></i>
                                        <!--begin::Inputs-->
                                        <input type="file" name="icon" accept=".png, .jpg, .jpeg" />
                                        <input type="hidden" name="icon_remove" />
                                        <!--end::Inputs-->
                                    </label>
                                    <!--end::Edit-->
                                    <!--begin::Cancel-->
                                    <span class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="cancel" data-bs-toggle="tooltip" title="Cancel icon">
                                        <i class="bi bi-x fs-2"></i>
                                    </span>
                                    <!--end::Cancel-->
                                    <!--begin::Remove-->
                                    <span class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="remove" data-bs-toggle="tooltip" title="Remove icon" style="display:none;">
                                        <i class="bi bi-x fs-2"></i>
                                    </span>
                                    <!--end::Remove-->
                                </div>
                                <!--end::Image input-->
                            </div>
                            <!--end::Image input wrapper-->
                        </div>
                        <!--end::Input group-->
                        <!--begin::Actions-->
                        <div class="text-center">
                            <input type="hidden" name="method" value="new" />
                            <button type="reset" id="kr_modal_new_payment_method_cancel" class="btn btn-light me-3">Cancel</button>
                            <button type="submit" id="kr_modal_new_payment_method_submit" class="btn btn-primary">
                                <span class="indicator-label">Submit</span>
                                <span class="indicator-progress">Please wait... <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span>
                            </button>
                        </div>
                        <!--end::Actions-->
                    </form>
                    <!--end:Form-->
                </div>
                <!--end::Modal body-->
            </div>
            <!--end::Modal content-->
        </div>
        <!--end::Modal dialog-->
    </div>
    <!--end::Modal - New Target-->
</x-base-layout>
