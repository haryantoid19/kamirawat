<!--begin::Action--->
<td class="text-end">
    <button data-edit="{{ route('medical-services.show', $medicalService->id) }}" class="btn btn-sm btn-light-warning btn-active-warning">
        Edit
    </button>
    <button data-destroy="{{ route('medical-services.destroy', $medicalService->id) }}" class="btn btn-sm btn-light-danger btn-active-danger">
        Delete
    </button>
</td>
<!--end::Action--->
