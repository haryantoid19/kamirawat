<!--begin::Card-->
<div class="card pt-4 mb-6 mb-xl-9">
    <!--begin::Card header-->
    <div class="card-header border-0">
        <!--begin::Card title-->
        <div class="card-title">
            <h2>Documents</h2>
        </div>
        <!--end::Card title-->
        <!--begin::Card toolbar-->
        <div class="card-toolbar d-none">
            <!--begin::Filter-->
            <button type="button" class="btn btn-sm btn-flex btn-light-primary">
                <!--begin::Svg Icon | path: icons/duotune/general/gen035.svg-->
                <span class="svg-icon svg-icon-3">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                        <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="5" fill="black" />
                        <rect x="10.8891" y="17.8033" width="12" height="2" rx="1" transform="rotate(-90 10.8891 17.8033)" fill="black" />
                        <rect x="6.01041" y="10.9247" width="12" height="2" rx="1" fill="black" />
                    </svg>
                </span>
                <!--end::Svg Icon-->
                Add documents
            </button>
            <!--end::Filter-->
        </div>
        <!--end::Card toolbar-->
    </div>
    <!--end::Card header-->
    <!--begin::Card body-->
    <div class="card-body pt-0 pb-5">
        <!--begin::Table-->
        <table class="table align-middle table-row-dashed gy-5" id="kt_table_customers_payment">
            <!--begin::Table head-->
            <thead class="border-bottom border-gray-200 fs-7 fw-bolder">
            <!--begin::Table row-->
            <tr class="text-start text-muted text-uppercase gs-0">
                <th class="min-w-100px">Name</th>
                <th>Type</th>
                <th>Size</th>
                <th class="min-w-100px">Date</th>
                <th class="text-end min-w-100px pe-4">Actions</th>
            </tr>
            <!--end::Table row-->
            </thead>
            <!--end::Table head-->
            <!--begin::Table body-->
            <tbody class="fs-6 fw-bold text-gray-600">
            @foreach($documents as $document)
                <!--begin::Table row-->
                <tr>
                    <!--begin::Name=-->
                    <td>
                        {{ $document->getCustomProperty('name', '') }}
                    </td>
                    <!--end::Name=-->
                    <!--begin::Type=-->
                    <td>
                        <span class="badge badge-secondary">{{ strtoupper($document->type) }}</span>
                    </td>
                    <!--end::Type=-->
                    <!--begin::Size=-->
                    <td>{{ $document->human_readable_size }}</td>
                    <!--end::Size=-->
                    <!--begin::Date=-->
                    <td>{{ $document->created_at->format('d M Y H:i:s') }}</td>
                    <!--end::Date=-->
                    <!--begin::Action=-->
                    <td class="pe-0 text-end">
                        <a href="{{ $document->getUrl() }}" target="new" class="btn btn-sm btn-light btn-active-light-primary">View</a>
                    </td>
                    <!--end::Action=-->
                </tr>
                <!--end::Table row-->
            @endforeach
            </tbody>
            <!--end::Table body-->
        </table>
        <!--end::Table-->
    </div>
    <!--end::Card body-->
</div>
<!--end::Card-->
