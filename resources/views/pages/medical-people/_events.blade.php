<!--begin::Card-->
<div class="card pt-4 mb-6 mb-xl-9">
    <!--begin::Card header-->
    <div class="card-header border-0">
        <!--begin::Card title-->
        <div class="card-title">
            <h2>Events</h2>
        </div>
        <!--end::Card title-->
    </div>
    <!--end::Card header-->
    <!--begin::Card body-->
    <div class="card-body py-0">
        <!--begin::Table-->
        <table class="table align-middle table-row-dashed fs-6 text-gray-600 fw-bold gy-5" id="kt_table_customers_events">
            <!--begin::Table body-->
            <tbody>
            <!--begin::Table row-->
            <tr>
                <!--begin::Event=-->
                <td class="min-w-400px">
                    <a href="#" class="text-gray-600 text-hover-primary me-1">Sean Bean</a>has made payment to
                    <a href="#" class="fw-bolder text-gray-900 text-hover-primary">#XRS-45670</a></td>
                <!--end::Event=-->
                <!--begin::Timestamp=-->
                <td class="pe-0 text-gray-600 text-end min-w-200px">10 Mar 2021, 5:20 pm</td>
                <!--end::Timestamp=-->
            </tr>
            <!--end::Table row-->
            <!--begin::Table row-->
            <tr>
                <!--begin::Event=-->
                <td class="min-w-400px">Invoice
                    <a href="#" class="fw-bolder text-gray-900 text-hover-primary me-1">#WER-45670</a>is
                    <span class="badge badge-light-info">In Progress</span></td>
                <!--end::Event=-->
                <!--begin::Timestamp=-->
                <td class="pe-0 text-gray-600 text-end min-w-200px">05 May 2021, 10:10 pm</td>
                <!--end::Timestamp=-->
            </tr>
            <!--end::Table row-->
            <!--begin::Table row-->
            <tr>
                <!--begin::Event=-->
                <td class="min-w-400px">Invoice
                    <a href="#" class="fw-bolder text-gray-900 text-hover-primary me-1">#DER-45645</a>status has changed from
                    <span class="badge badge-light-info me-1">In Progress</span>to
                    <span class="badge badge-light-primary">In Transit</span></td>
                <!--end::Event=-->
                <!--begin::Timestamp=-->
                <td class="pe-0 text-gray-600 text-end min-w-200px">20 Jun 2021, 11:30 am</td>
                <!--end::Timestamp=-->
            </tr>
            <!--end::Table row-->
            <!--begin::Table row-->
            <tr>
                <!--begin::Event=-->
                <td class="min-w-400px">
                    <a href="#" class="text-gray-600 text-hover-primary me-1">Max Smith</a>has made payment to
                    <a href="#" class="fw-bolder text-gray-900 text-hover-primary">#SDK-45670</a></td>
                <!--end::Event=-->
                <!--begin::Timestamp=-->
                <td class="pe-0 text-gray-600 text-end min-w-200px">21 Feb 2021, 11:05 am</td>
                <!--end::Timestamp=-->
            </tr>
            <!--end::Table row-->
            <!--begin::Table row-->
            <tr>
                <!--begin::Event=-->
                <td class="min-w-400px">Invoice
                    <a href="#" class="fw-bolder text-gray-900 text-hover-primary me-1">#WER-45670</a>is
                    <span class="badge badge-light-info">In Progress</span></td>
                <!--end::Event=-->
                <!--begin::Timestamp=-->
                <td class="pe-0 text-gray-600 text-end min-w-200px">19 Aug 2021, 6:05 pm</td>
                <!--end::Timestamp=-->
            </tr>
            <!--end::Table row-->
            <!--begin::Table row-->
            <tr>
                <!--begin::Event=-->
                <td class="min-w-400px">
                    <a href="#" class="text-gray-600 text-hover-primary me-1">Melody Macy</a>has made payment to
                    <a href="#" class="fw-bolder text-gray-900 text-hover-primary">#XRS-45670</a></td>
                <!--end::Event=-->
                <!--begin::Timestamp=-->
                <td class="pe-0 text-gray-600 text-end min-w-200px">20 Dec 2021, 10:30 am</td>
                <!--end::Timestamp=-->
            </tr>
            <!--end::Table row-->
            <!--begin::Table row-->
            <tr>
                <!--begin::Event=-->
                <td class="min-w-400px">
                    <a href="#" class="text-gray-600 text-hover-primary me-1">Melody Macy</a>has made payment to
                    <a href="#" class="fw-bolder text-gray-900 text-hover-primary">#XRS-45670</a></td>
                <!--end::Event=-->
                <!--begin::Timestamp=-->
                <td class="pe-0 text-gray-600 text-end min-w-200px">10 Mar 2021, 8:43 pm</td>
                <!--end::Timestamp=-->
            </tr>
            <!--end::Table row-->
            <!--begin::Table row-->
            <tr>
                <!--begin::Event=-->
                <td class="min-w-400px">Invoice
                    <a href="#" class="fw-bolder text-gray-900 text-hover-primary me-1">#DER-45645</a>status has changed from
                    <span class="badge badge-light-info me-1">In Progress</span>to
                    <span class="badge badge-light-primary">In Transit</span></td>
                <!--end::Event=-->
                <!--begin::Timestamp=-->
                <td class="pe-0 text-gray-600 text-end min-w-200px">05 May 2021, 10:10 pm</td>
                <!--end::Timestamp=-->
            </tr>
            <!--end::Table row-->
            <!--begin::Table row-->
            <tr>
                <!--begin::Event=-->
                <td class="min-w-400px">Invoice
                    <a href="#" class="fw-bolder text-gray-900 text-hover-primary me-1">#KIO-45656</a>status has changed from
                    <span class="badge badge-light-succees me-1">In Transit</span>to
                    <span class="badge badge-light-success">Approved</span></td>
                <!--end::Event=-->
                <!--begin::Timestamp=-->
                <td class="pe-0 text-gray-600 text-end min-w-200px">24 Jun 2021, 6:43 am</td>
                <!--end::Timestamp=-->
            </tr>
            <!--end::Table row-->
            <!--begin::Table row-->
            <tr>
                <!--begin::Event=-->
                <td class="min-w-400px">Invoice
                    <a href="#" class="fw-bolder text-gray-900 text-hover-primary me-1">#LOP-45640</a>has been
                    <span class="badge badge-light-danger">Declined</span></td>
                <!--end::Event=-->
                <!--begin::Timestamp=-->
                <td class="pe-0 text-gray-600 text-end min-w-200px">10 Nov 2021, 2:40 pm</td>
                <!--end::Timestamp=-->
            </tr>
            <!--end::Table row-->
            </tbody>
            <!--end::Table body-->
        </table>
        <!--end::Table-->
    </div>
    <!--end::Card body-->
</div>
<!--end::Card-->
