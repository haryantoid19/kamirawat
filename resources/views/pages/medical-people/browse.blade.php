<x-base-layout>

    <!--begin::Card-->
    <div class="card">
        <!--begin::Card body-->
        <div class="card-body pt-6">
            <!--begin::Table-->
            {{ $dataTable->table() }}
            <!--end::Table-->

            {{-- Inject Scripts --}}
            @section('scripts')
                {{ $dataTable->scripts() }}

                <script type="text/javascript">
                    $(function() {
                        var token = $('meta[name="csrf-token"]').attr('content');
                        $.ajaxSetup({headers: {'X-CSRF-TOKEN': token}});
                        window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token;

                        $(document.body).on('click', '[data-approval]', function() {
                            var status = $(this).data('status');
                            var url = $(this).data('target');

                            Swal.fire({
                                html: "Are you sure to " + status + " this record?",
                                icon: "warning",
                                buttonsStyling: false,
                                showCancelButton: true,
                                confirmButtonText: "Ok, got it!",
                                cancelButtonText: 'Nope, cancel it',
                                customClass: {
                                    confirmButton: "btn btn-primary",
                                    cancelButton: 'btn btn-danger'
                                }
                            }).then(function(result) {
                                if (result.isConfirmed) {
                                    var payload = {
                                        status: status,
                                        _method: "PUT"
                                    };

                                    $.post(url, payload, function() {
                                        LaravelDataTables["medical-people-table"].ajax.reload();
                                        toastr.success("Successfully " + status);
                                    }).fail(function() {
                                        console.log(error);
                                    });
                                }
                            });
                        });
                    });
                </script>
            @endsection
        </div>
        <!--end::Card body-->
    </div>
    <!--end::Card-->

</x-base-layout>
