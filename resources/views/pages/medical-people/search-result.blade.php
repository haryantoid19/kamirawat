<!--begin::Users-->
<div class="mh-300px scroll-y me-n5 pe-5">
    @foreach($medicalPeopleData as $medicalPeople)
        <!--begin::User-->
        <div
            class="d-flex align-items-center p-3 rounded-3 border-hover border border-dashed border-gray-300 cursor-pointer mb-1"
            data-kt-search-element="medical-people"
        >
            <form method="POST" action="{{ route('bookings.assign.medical-people', ['booking' => $booking->id, 'medicalPeople' => $medicalPeople->id]) }}">
                @csrf
            </form>
            <!--begin::Avatar-->
            <div class="symbol symbol-35px symbol-circle me-5">
                <img alt="{{ $medicalPeople->name }}" src="{{ $medicalPeople->avatar_url }}" />
            </div>
            <!--end::Avatar-->
            <!--begin::Info-->
            <div class="fw-bold">
                <span class="fs-6 text-gray-800 me-2">{{ $medicalPeople->name }}</span>
                <span class="badge badge-light">{{ $medicalPeople->profession->title }}</span>
            </div>
            <!--end::Info-->
        </div>
    @endforeach
</div>
<!--end::Users-->
