<!--begin::Action--->
<td class="text-end">
    <a href="{{ route('medical-people.show', $medicalPeople->id) }}" class="btn btn-sm btn-light-primary btn-active-primary">
        View
    </a>
    @if($show_approve_button)
        <button data-approval="1" data-status="{{ \App\Enums\MedicalPeopleStatus::ACCEPTED()->value }}" data-target="{{ route('medical_people.approval', ['medicalPeople' => $medicalPeople->id]) }}" class="btn btn-sm btn-light-success btn-active-success">
            Accept
        </button>
        <button data-approval="1" data-status="{{ \App\Enums\MedicalPeopleStatus::REJECTED()->value }}" data-target="{{ route('medical_people.approval', ['medicalPeople' => $medicalPeople->id]) }}" class="btn btn-sm btn-light-danger btn-active-danger">
            Reject
        </button>
    @endif
</td>
<!--end::Action--->
