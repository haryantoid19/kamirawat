<!--begin::Action--->
<td class="text-end">
    <a href="{{ route('users.show', $user->id) }}" class="btn btn-sm btn-light-primary btn-active-primary">
        View
    </a>
    <button data-destroy="{{ route('users.destroy', $user->id) }}" class="btn btn-sm btn-light-danger btn-active-danger">
        Delete
    </button>
</td>
<!--end::Action--->
