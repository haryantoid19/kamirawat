<!--begin::Card-->
<div class="card pt-4 mb-6 mb-xl-9">
    <!--begin::Card header-->
    <div class="card-header border-0">
        <!--begin::Card title-->
        <div class="card-title">
            <h2>History Service</h2>
        </div>
        <!--end::Card title-->
     
    </div>
    <!--end::Card header-->
    <!--begin::Card body-->
    <div class="card-body pt-0 pb-5">
        <!--begin::Table-->
        <table class="table align-middle table-row-dashed gy-5" id="kt_table_customers_payment">
            <!--begin::Table head-->
            <thead class="border-bottom border-gray-200 fs-7 fw-bolder">
            <!--begin::Table row-->
            <tr class="text-start text-muted text-uppercase gs-0">
                <th class="min-w-100px">Booking No.</th>
                <th class="min-w-100px">Service</th>
                <th>Status</th>
                <th>Amount</th>
                <th class="min-w-100px">Date</th>
                <th class="text-end min-w-100px pe-4">Actions</th>
            </tr>
            <!--end::Table row-->
            </thead>
            <!--end::Table head-->
            <!--begin::Table body-->
            <tbody class="fs-6 fw-bold text-gray-600">
            <!--begin::Table row-->
            <tr>
                <!--begin::Invoice=-->
                <td>
                    <a href="#" class="text-gray-600 text-hover-primary mb-1">8194-6666</a>
                </td>
                <!--end::Invoice=-->
                <!--begin::Status=-->
                <td>Home Visit</td>
                <td>
                    <span class="badge badge-light-success">Successful</span>
                </td>
                <!--end::Status=-->
                <!--begin::Amount=-->
                
                <td>Rp 1,200.00</td>
                <!--end::Amount=-->
                <!--begin::Date=-->
                <td>14 Dec 2020, 8:43 pm</td>
                <!--end::Date=-->
                <!--begin::Action=-->
                <td class="pe-0 text-end">
                    <a href="#" class="btn btn-sm btn-light btn-active-light-primary" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">View
                        <!--begin::Svg Icon | path: icons/duotune/arrows/arr072.svg-->
            
                        <!--end::Svg Icon-->
                    </a>
                    
                </td>
                <!--end::Action=-->
            </tr>
            <!--end::Table row-->
    
            </tbody>
            <!--end::Table body-->
        </table>
        <!--end::Table-->
    </div>
    <!--end::Card body-->
</div>
<!--end::Card-->

<!--begin::Modal - Add Payment-->
<div class="modal fade" id="kt_modal_add_payment" tabindex="-1" aria-hidden="true">
    <!--begin::Modal dialog-->
    <div class="modal-dialog mw-650px">
        <!--begin::Modal content-->
        <div class="modal-content">
            <!--begin::Modal header-->
            <div class="modal-header">
                <!--begin::Modal title-->
                <h2 class="fw-bolder">Add a Payout</h2>
                <!--end::Modal title-->
                <!--begin::Close-->
                <div id="kt_modal_add_payment_close" class="btn btn-icon btn-sm btn-active-icon-primary">
                    <!--begin::Svg Icon | path: icons/duotune/arrows/arr061.svg-->
                    <span class="svg-icon svg-icon-1">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                            <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="black" />
                            <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="black" />
                        </svg>
                    </span>
                    <!--end::Svg Icon-->
                </div>
                <!--end::Close-->
            </div>
            <!--end::Modal header-->
            <!--begin::Modal body-->
            <div class="modal-body scroll-y mx-5 mx-xl-15 my-7">
                <!--begin::Form-->
                <form id="kt_modal_add_payment_form" class="form" action="#">
                    <!--begin::Input group-->
                    <div class="fv-row mb-7">
                        <!--begin::Label-->
                        <label for="invoice" class="fs-6 fw-bold form-label mb-2">
                            <span class="required">Invoice Number</span>
                            <i class="fas fa-exclamation-circle ms-1 fs-7" data-bs-toggle="tooltip" title="The invoice number must be unique."></i>
                        </label>
                        <!--end::Label-->
                        <!--begin::Input-->
                        <input type="text" id="invoice" class="form-control form-control-solid" name="invoice" value="" />
                        <!--end::Input-->
                    </div>
                    <!--end::Input group-->
                    <!--begin::Input group-->
                    <div class="fv-row mb-7">
                        <!--begin::Label-->
                        <label for="status" class="required fs-6 fw-bold form-label mb-2">Status</label>
                        <!--end::Label-->
                        <!--begin::Input-->
                        <select class="form-select form-select-solid fw-bolder" name="status" id="status" data-control="select2" data-placeholder="Select an option" data-hide-search="true">
                            <option></option>
                            <option value="0">Approved</option>
                            <option value="1">Pending</option>
                            <option value="2">Rejected</option>
                            <option value="3">In progress</option>
                            <option value="4">Completed</option>
                        </select>
                        <!--end::Input-->
                    </div>
                    <!--end::Input group-->
                    <!--begin::Input group-->
                    <div class="fv-row mb-7">
                        <!--begin::Label-->
                        <label for="amount" class="required fs-6 fw-bold form-label mb-2">Invoice Amount</label>
                        <!--end::Label-->
                        <!--begin::Input-->
                        <input type="text" id="amount" class="form-control form-control-solid" name="amount" value="" />
                        <!--end::Input-->
                    </div>
                    <!--end::Input group-->
                    <!--begin::Input group-->
                    <div class="fv-row mb-15">
                        <!--begin::Label-->
                        <label for="additional_info" class="fs-6 fw-bold form-label mb-2">
                            <span class="required">Additional Information</span>
                            <i class="fas fa-exclamation-circle ms-1 fs-7" data-bs-toggle="tooltip" title="Information such as description of invoice or product purchased."></i>
                        </label>
                        <!--end::Label-->
                        <!--begin::Input-->
                        <textarea class="form-control form-control-solid rounded-3" id="additional_info" name="additional_info"></textarea>
                        <!--end::Input-->
                    </div>
                    <!--end::Input group-->
                    <!--begin::Actions-->
                    <div class="text-center">
                        <button type="reset" id="kt_modal_add_payment_cancel" class="btn btn-light me-3">Discard</button>
                        <button type="submit" id="kt_modal_add_payment_submit" class="btn btn-primary">
                            <span class="indicator-label">Submit</span>
                            <span class="indicator-progress">Please wait...
                                <span class="spinner-border spinner-border-sm align-middle ms-2"></span>
                            </span>
                        </button>
                    </div>
                    <!--end::Actions-->
                </form>
                <!--end::Form-->
            </div>
            <!--end::Modal body-->
        </div>
        <!--end::Modal content-->
    </div>
    <!--end::Modal dialog-->
</div>
<!--end::Modal - New Card-->
