<!--begin::Action--->
<td class="text-end">
    <a href="{{ route('patients.show', $patient->id) }}" class="btn btn-sm btn-light-primary btn-active-primary">
        View
    </a>
</td>
<!--end::Action--->
