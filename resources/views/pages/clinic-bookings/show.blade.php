<x-base-layout>

    <!--begin::Post-->
    <div class="post d-flex flex-column-fluid" id="kt_post">
        <!--begin::Container-->
        <div id="kt_content_container" class="container-xxl">
            <!--begin::Layout-->
            <div class="d-flex flex-column flex-lg-row">
                <!--begin::Content-->
                <div class="flex-lg-row-fluid me-lg-15 order-2 order-lg-1 mb-10 mb-lg-0">
                    <!--begin::Card-->
                    <div class="card card-flush pt-3 mb-5 mb-xl-10">
                        <!--begin::Card header-->
                        <div class="card-header">
                            <!--begin::Card title-->
                            <div class="card-title">
                                <h2 class="fw-bolder">Booking Details {{$clinicBooking->number}}</h2>
                            </div>
                            <!--begin::Card title-->
                        </div>
                        <!--end::Card header-->
                        <!--begin::Card body-->
                        <div class="card-body pt-3">
                            <!--begin::Section-->
                            <div class="mb-10">
                                <!--begin::Title-->
                                <h5 class="mb-4">Patient detail:</h5>
                                <!--end::Title-->
                                <!--begin::Details-->
                                <div class="d-flex flex-wrap py-5">
                                    <!--begin::Row-->
                                    <div class="flex-equal me-5">
                                        <!--begin::Details-->
                                        <table class="table fs-6 fw-bold gs-0 gy-2 gx-2 m-0">
                                            <!--begin::Row-->
                                            <tr>
                                                <td class="text-gray-400 min-w-175px w-175px">Email:</td>
                                                <td class="text-gray-800 min-w-200px">
                                                    <a href="{{ route('patients.show', $clinicBooking->patient->id) }}" class="text-gray-800 text-hover-primary">{{ $clinicBooking->patient->user->email }}</a>
                                                </td>
                                            </tr>
                                            <!--end::Row-->
                                            <!--begin::Row-->
                                            <tr>
                                                <td class="text-gray-400">Name:</td>
                                                <td class="text-gray-800">{{ $clinicBooking->patient->name }}</td>
                                            </tr>
                                            <!--end::Row-->
                                            <!--begin::Row-->
                                            <tr>
                                                <td class="text-gray-400">Address:</td>
                                                <td class="text-gray-800">{!! nl2br($clinicBooking->patient->address) !!}</td>
                                            </tr>
                                            <!--end::Row-->
                                            <!--begin::Row-->
                                            <tr>
                                                <td class="text-gray-400">Kelurahan:</td>
                                                <td class="text-gray-800">{{ $clinicBooking->patient->postal_code->urban }}</td>
                                            </tr>
                                            <!--end::Row-->
                                            <!--begin::Row-->
                                            <tr>
                                                <td class="text-gray-400">Kecamatan:</td>
                                                <td class="text-gray-800">{{ $clinicBooking->patient->postal_code->sub_district }}</td>
                                            </tr>
                                            <!--end::Row-->
                                            <!--begin::Row-->
                                            <tr>
                                                <td class="text-gray-400">City:</td>
                                                <td class="text-gray-800">{{ $clinicBooking->patient->postal_code->city }}</td>
                                            </tr>

                                            <tr>
                                                <td class="text-gray-400">Services : </td>
                                                <td class="text-gray-800">{{ $clinicBooking->medical_service->name }}</td>
                                            </tr>

                                            <tr>
                                                <td class="text-gray-400">Status : </td>
                                                <td class="text-gray-800">
                                                @if ($clinicBooking->status == 'SUBMITTED')
                                                    <form method="POST" action="{{ route('clinic-bookings.update', $clinicBooking->id) }}">
                                                        @csrf
                                                        @method("PUT")
                                                        <button type="submit" class="btn btn-sm btn-light-warning btn-active-warning">{{ $clinicBooking->status }}</button>
                                                    </form>
                                                @else
                                                    {!! $clinicBooking->status->toBadge() !!}</td>
                                                @endif    
                                                
                                            </tr>
                                            <!--end::Row-->
                                        </table>
                                        <!--end::Details-->
                                    </div>
                                    <!--end::Row-->
                                    <!--begin::Row-->
                                    <div class="flex-equal">
                                        <!--begin::Details-->
                                        <table class="table fs-6 fw-bold gs-0 gy-2 gx-2 m-0">
                                            <!--begin::Row-->
                                            <tr>
                                                <td class="text-gray-400">Phone:</td>
                                                <td class="text-gray-800"><a target="_blank" href="https://wa.me/{{$clinicBooking->patient->phoneNumber()}}">{{ $clinicBooking->patient->phone_number }}</a></td>
                                            </tr>
                                            <!--end::Row-->
                                            <!--begin::Row-->
                                            <tr>
                                                <td class="text-gray-400 min-w-175px w-175px">Birthday:</td>
                                                <td class="text-gray-800 min-w-200px">{{ $clinicBooking->patient->bod->format('d F Y') }}</td>
                                            </tr>
                                            <!--end::Row-->
                                            <!--begin::Row-->
                                            <tr>
                                                <td class="text-gray-400 min-w-175px w-175px">Gender:</td>
                                                <td class="text-gray-800 min-w-200px">{{ $clinicBooking->patient->gender->value }}</td>
                                            </tr>
                                            <!--end::Row-->
                                            <!--begin::Row-->
                                            <tr>
                                                <td class="text-gray-400">Nationality:</td>
                                                <td class="text-gray-800">{{ $clinicBooking->patient->nationality }}</td>
                                            </tr>
                                            <!--end::Row-->
                                            <!--begin::Row-->
                                            <tr>
                                                <td class="text-gray-400">Province:</td>
                                                <td class="text-gray-800">{{ $clinicBooking->patient->postal_code->province->province_name }}</td>
                                            </tr>
                                            <!--end::Row-->
                                            <!--begin::Row-->
                                            <tr>
                                                <td class="text-gray-400">Postal Code:</td>
                                                <td class="text-gray-800">{{ $clinicBooking->patient->postal_code->postal_code }}</td>
                                            </tr>
                                            <!--end::Row-->
                                        </table>
                                        <!--end::Details-->
                                    </div>
                                    <!--end::Row-->
                                </div>
                                <!--end::Row-->
                            </div>
                            <!--end::Section-->
                          
                        </div>
                        <!--end::Card body-->
                    </div>
              
                    <!--end::Card-->
                    <!--begin::Card-->
                    <div class="card card-flush pt-3 mb-5 mb-xl-10">
                        <!--begin::Card header-->
                        <div class="card-header">
                            <!--begin::Card title-->
                            <div class="card-title">
                                <h2>History Clinic Services</h2>
                            </div>
                            <!--end::Card title-->
                        </div>
                        <!--end::Card header-->
                        <!--begin::Card body-->
                        <div class="card-body pt-0">
                            <!--begin::Table wrapper-->
                            <div class="table-responsive">
                                <!--begin::Table-->
                                <table class="table align-middle table-row-dashed fs-6 text-gray-600 fw-bold gy-5" id="kt_table_customers_events">
                                    <!--begin::Table body-->
                                    <tbody>

                                    <!--begin::Table row-->
                                    <tr>
                                        <!--begin::Event=-->
                                        <td class="min-w-400px">
                                            <a href="#" class="fw-bolder text-gray-800 text-hover-primary me-1">CS2202-0003</a> Service Calm Clinic - Psychologist</td>
                                        <!--end::Event=-->
                                        <!--begin::Timestamp=-->
                                        <td class="pe-0 text-gray-600 text-end min-w-200px">15 Apr 2021</td>
                                        <!--end::Timestamp=-->
                                    </tr>
                                    <!--end::Table row-->
                                    
                               
                             
                                    <!--end::Table row-->
                                    </tbody>
                                    <!--end::Table body-->
                                </table>
                                <!--end::Table-->
                            </div>
                            <!--end::Table wrapper-->
                        </div>
                        <!--end::Card body-->
                    </div>
                    <!--end::Card-->
                </div>
                <!--end::Content-->
                <!--begin::Sidebar-->
                
                <!--end::Sidebar-->
            </div>
            <!--end::Layout-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Post-->



    @section('scripts')
        <script type="text/javascript">
            window.booking_id = {{ $clinicBooking->id }}
      
        </script>
    @endsection

</x-base-layout>
