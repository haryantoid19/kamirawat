<!--begin::Action--->
<td class="text-end">
    <a href="{{ route('clinic-bookings.show', $booking->id) }}" class="btn btn-sm btn-light-primary btn-active-primary">
        View
    </a>
</td>
<!--end::Action--->
