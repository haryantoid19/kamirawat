<x-base-layout>

    <!--begin::Post-->
    <div class="post d-flex flex-column-fluid" id="kt_post">
        <!--begin::Container-->
        <div id="kt_content_container" class="container-xxl">
            <!--begin::Layout-->
            <div class="d-flex flex-column flex-lg-row">
                <!--begin::Content-->
                <div class="flex-lg-row-fluid me-lg-15 order-2 order-lg-1 mb-10 mb-lg-0">
                    <!--begin::Card-->
                    <div class="card card-flush pt-3 mb-5 mb-xl-10">
                        <!--begin::Card header-->
                        <div class="card-header">
                            <!--begin::Card title-->
                            <div class="card-title">
                                <h2 class="fw-bolder">Booking Details</h2>
                            </div>
                            <!--begin::Card title-->
                        </div>
                        <!--end::Card header-->
                        <!--begin::Card body-->
                        <div class="card-body pt-3">
                            <!--begin::Section-->
                            <div class="mb-10">
                                <!--begin::Title-->
                                <h5 class="mb-4">Patient detail:</h5>
                                <!--end::Title-->
                                <!--begin::Details-->
                                <div class="d-flex flex-wrap py-5">
                                    <!--begin::Row-->
                                    <div class="flex-equal me-5">
                                        <!--begin::Details-->
                                        <table class="table fs-6 fw-bold gs-0 gy-2 gx-2 m-0">
                                            <!--begin::Row-->
                                            <tr>
                                                <td class="text-gray-400 min-w-175px w-175px">Bill to:</td>
                                                <td class="text-gray-800 min-w-200px">
                                                    <a href="{{ route('patients.show', $booking->patient->id) }}" class="text-gray-800 text-hover-primary">{{ $booking->patient->user->email }}</a>
                                                </td>
                                            </tr>
                                            <!--end::Row-->
                                            <!--begin::Row-->
                                            <tr>
                                                <td class="text-gray-400">Name:</td>
                                                <td class="text-gray-800">{{ $booking->patient->name }}</td>
                                            </tr>
                                            <!--end::Row-->
                                            <!--begin::Row-->
                                            <tr>
                                                <td class="text-gray-400">Address:</td>
                                                <td class="text-gray-800">{!! nl2br($booking->patient->address) !!}</td>
                                            </tr>
                                            <!--end::Row-->
                                            <!--begin::Row-->
                                            <tr>
                                                <td class="text-gray-400">Kelurahan:</td>
                                                <td class="text-gray-800">{{ $booking->patient->postal_code->urban }}</td>
                                            </tr>
                                            <!--end::Row-->
                                            <!--begin::Row-->
                                            <tr>
                                                <td class="text-gray-400">Kecamatan:</td>
                                                <td class="text-gray-800">{{ $booking->patient->postal_code->sub_district }}</td>
                                            </tr>
                                            <!--end::Row-->
                                            <!--begin::Row-->
                                            <tr>
                                                <td class="text-gray-400">City:</td>
                                                <td class="text-gray-800">{{ $booking->patient->postal_code->city }}</td>
                                            </tr>
                                            <!--end::Row-->
                                        </table>
                                        <!--end::Details-->
                                    </div>
                                    <!--end::Row-->
                                    <!--begin::Row-->
                                    <div class="flex-equal">
                                        <!--begin::Details-->
                                        <table class="table fs-6 fw-bold gs-0 gy-2 gx-2 m-0">
                                            <!--begin::Row-->
                                            <tr>
                                                <td class="text-gray-400">Phone:</td>
                                                <td class="text-gray-800"><a target="_blank" href="https://wa.me/{{$booking->patient->phoneNumber()}}">{{ $booking->patient->phone_number }}</td>
                                            </tr>
                                            <!--end::Row-->
                                            <!--begin::Row-->
                                            <tr>
                                                <td class="text-gray-400 min-w-175px w-175px">Birthday:</td>
                                                <td class="text-gray-800 min-w-200px">{{ $booking->patient->bod->format('d F Y') }}</td>
                                            </tr>
                                            <!--end::Row-->
                                            <!--begin::Row-->
                                            <tr>
                                                <td class="text-gray-400 min-w-175px w-175px">Gender:</td>
                                                <td class="text-gray-800 min-w-200px">{{ $booking->patient->gender->value }}</td>
                                            </tr>
                                            <!--end::Row-->
                                            <!--begin::Row-->
                                            <tr>
                                                <td class="text-gray-400">Nationality:</td>
                                                <td class="text-gray-800">{{ $booking->patient->nationality }}</td>
                                            </tr>
                                            <!--end::Row-->
                                            <!--begin::Row-->
                                            <tr>
                                                <td class="text-gray-400">Province:</td>
                                                <td class="text-gray-800">{{ $booking->patient->postal_code->province->province_name }}</td>
                                            </tr>
                                            <!--end::Row-->
                                            <!--begin::Row-->
                                            <tr>
                                                <td class="text-gray-400">Postal Code:</td>
                                                <td class="text-gray-800">{{ $booking->patient->postal_code->postal_code }}</td>
                                            </tr>
                                            <!--end::Row-->
                                        </table>
                                        <!--end::Details-->
                                    </div>
                                    <!--end::Row-->
                                </div>
                                <!--end::Row-->
                            </div>
                            <!--end::Section-->
                            <!--begin::Section-->
                            <div class="mb-0">
                                @can('assign_medical_people', $booking)
                                    <!--begin::Title-->
                                    <h5 class="mb-4">Medical People:</h5>
                                    <!--end::Title-->
                                @endcan
                                @if ($booking->medical_people)
                                    <!--begin::Selected medical people-->
                                    <div class="d-flex align-items-center p-3 mb-2">
                                        <!--begin::Avatar-->
                                        <div class="symbol symbol-60px symbol-circle me-3">
                                            <img alt="{{ $booking->medical_people->name }}" src="{{ $booking->medical_people->avatar_url }}" />
                                        </div>
                                        <!--end::Avatar-->
                                        <!--begin::Info-->
                                        <div class="d-flex flex-column">
                                            <!--begin::Name-->
                                            <a href="{{ route('medical-people.show', $booking->medical_people_id) }}" class="fs-4 fw-bolder text-gray-900 text-hover-primary me-2">{{ $booking->medical_people->name }}</a>
                                            <!--end::Name-->
                                            <!--begin::Email-->
                                            <a href="{{ route('medical-people.show', $booking->medical_people_id) }}" class="fw-bold text-gray-600 text-hover-primary">{{ $booking->medical_people->user->email }}</a>
                                            <!--end::Email-->
                                        </div>
                                        <!--end::Info-->
                                    </div>
                                    <!--end::Selected medical people-->
                                @endif
                                @can('assign_medical_people', $booking)
                                <!--begin::Medical People change button-->
                                    <div class="mb-10">
                                        <a href="#" class="btn btn-light-primary" data-bs-toggle="modal" data-bs-target="#kt_modal_customer_search">Assign Medical People</a>
                                    </div>
                                    <!--end::Medical People change button-->
                                @endcan
                            </div>
                            <!--end::Section-->
                        </div>
                        <!--end::Card body-->
                    </div>
                    <!--end::Card-->
                    <!--begin::Card-->
                    <div class="card card-flush pt-3 mb-5 mb-xl-10">
                        <!--begin::Card header-->
                        <div class="card-header">
                            <!--begin::Card title-->
                            <div class="card-title">
                                <h2>Invoices</h2>
                            </div>
                            <!--end::Card title-->
                        </div>
                        <!--end::Card header-->
                        <!--begin::Card body-->
                        <div class="card-body pt-2">
                            <!--begin::Tab Content-->
                            <div id="kt_referred_users_tab_content" class="tab-content">
                                <!--begin::Tab panel-->
                                <div id="kt_customer_details_invoices_1" class="tab-pane fade show active" role="tabpanel">
                                    <!--begin::Table wrapper-->
                                    <div class="table-responsive">
                                        <!--begin::Table-->
                                        <table id="kt_customer_details_invoices_table_1" class="table align-middle table-row-dashed fs-6 fw-bolder gs-0 gy-4 p-0 m-0">
                                            <!--begin::Thead-->
                                            <thead class="border-bottom border-gray-200 fs-7 text-uppercase fw-bolder">
                                            <tr class="text-start text-gray-400">
                                                <th class="min-w-100px">Order ID</th>
                                                <th class="min-w-100px">Amount</th>
                                                <th class="min-w-100px">Status</th>
                                                <th class="min-w-100px">Date</th>
                                                <th class="min-w-100px">Actions</th>
                                            </tr>
                                            </thead>
                                            <!--end::Thead-->
                                            <!--begin::Tbody-->
                                            <tbody class="fs-6 fw-bold text-gray-600">
                                            @foreach($booking->invoices as $invoice)
                                                <tr>
                                                    <td>
                                                        <a href="#" class="text-gray-600 text-hover-primary">{{ $invoice->invoice_number }}</a>
                                                    </td>
                                                    <td>@rupiah($invoice->total)</td>
                                                    <td>
                                                        {!! $invoice->status->toBadge() !!}
                                                    </td>
                                                    <td>{{ $invoice->created_at->format('d F Y') }}</td>
                                                    <td>
                                                        @if ($invoice->status->isActive() && Auth::user()->can('mark_as_paid', $invoice))
                                                            <form method="POST" action="{{ route('bookings.invoice.mark-as-paid', $invoice->id) }}">
                                                                @csrf
                                                                @method("PUT")
                                                                <button type="submit" class="btn btn-sm btn-light-success btn-active-success">Mark as Paid</button>
                                                            </form>
                                                        @else
                                                            <i>No Action</i>
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                            <!--end::Tbody-->
                                        </table>
                                        <!--end::Table-->
                                    </div>
                                    <!--end::Table wrapper-->
                                </div>
                                <!--end::Tab panel-->
                            </div>
                            <!--end::Tab Content-->
                        </div>
                        <!--end::Card body-->
                    </div>
                    <!--end::Card-->
                    <!--begin::Card-->
                    <div class="card card-flush pt-3 mb-5 mb-xl-10">
                        <!--begin::Card header-->
                        <div class="card-header">
                            <!--begin::Card title-->
                            <div class="card-title">
                                <h2>Medical Records</h2>
                            </div>
                            <!--end::Card title-->
                        </div>
                        <!--end::Card header-->
                        <!--begin::Card body-->
                        <div class="card-body pt-2">
                            <!--begin::Tab Content-->
                            <div id="kt_referred_users_tab_content" class="tab-content">
                                <!--begin::Tab panel-->
                                <div id="kt_customer_details_invoices_1" class="tab-pane fade show active" role="tabpanel">
                                    <!--begin::Table wrapper-->
                                    <div class="table-responsive">
                                        <!--begin::Table-->
                                        <table id="kt_customer_details_invoices_table_1" class="table align-middle table-row-dashed fs-6 fw-bolder gs-0 gy-4 p-0 m-0">
                                            <!--begin::Thead-->
                                            <thead class="border-bottom border-gray-200 fs-7 text-uppercase fw-bolder">
                                                <tr class="text-start text-gray-400">
                                                    <th class="min-w-100px">ID</th>
                                                    <th class="min-w-125px">Date</th>
                                                    <th class="w-100px">Action</th>
                                                </tr>
                                            </thead>
                                            <!--end::Thead-->
                                            <!--begin::Tbody-->
                                            <tbody class="fs-6 fw-bold text-gray-600">
                                                <tr>
                                                    <td>
                                                        <a href="#" class="text-gray-600 text-hover-primary">MR-2110-0001</a>
                                                    </td>
                                                    <td>Nov 01, 2020</td>
                                                    <td class="">
                                                        <button class="btn btn-sm btn-light btn-active-light-primary">View</button>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a href="#" class="text-gray-600 text-hover-primary">MR-2110-0002</a>
                                                    </td>
                                                    <td>Oct 24, 2020</td>
                                                    <td class="">
                                                        <button class="btn btn-sm btn-light btn-active-light-primary">View</button>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <a href="#" class="text-gray-600 text-hover-primary">MR-2110-0003</a>
                                                    </td>
                                                    <td>Oct 08, 2020</td>
                                                    <td class="">
                                                        <button class="btn btn-sm btn-light btn-active-light-primary">View</button>
                                                    </td>
                                                </tr>
                                            </tbody>
                                            <!--end::Tbody-->
                                        </table>
                                        <!--end::Table-->
                                    </div>
                                    <!--end::Table wrapper-->
                                </div>
                                <!--end::Tab panel-->
                            </div>
                            <!--end::Tab Content-->
                        </div>
                        <!--end::Card body-->
                    </div>
                    <!--end::Card-->
                    <!--begin::Card-->

                    <!--end::Card-->
                </div>
                <!--end::Content-->
                <!--begin::Sidebar-->
                <div class="flex-column flex-lg-row-auto w-lg-250px w-xl-300px mb-10 order-1 order-lg-2">
                    <!--begin::Card-->
                    <div class="card card-flush mb-0" data-kt-sticky="true" data-kt-sticky-name="subscription-summary" data-kt-sticky-offset="{default: false, lg: '200px'}" data-kt-sticky-width="{lg: '250px', xl: '300px'}" data-kt-sticky-left="auto" data-kt-sticky-top="150px" data-kt-sticky-animation="false" data-kt-sticky-zindex="95">
                        <!--begin::Card header-->
                        <div class="card-header">
                            <!--begin::Card title-->
                            <div class="card-title">
                                <h2>Summary</h2>
                            </div>
                            <!--end::Card title-->

                            @can('cancel', $booking)
                                <!--begin::Card toolbar-->
                                    <div class="card-toolbar">
                                        <!--begin::More options-->
                                        <a href="#" class="btn btn-sm btn-light btn-icon" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">
                                            <!--begin::Svg Icon | path: icons/duotune/general/gen052.svg-->
                                            <span class="svg-icon svg-icon-3">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                            <rect x="10" y="10" width="4" height="4" rx="2" fill="black" />
                                            <rect x="17" y="10" width="4" height="4" rx="2" fill="black" />
                                            <rect x="3" y="10" width="4" height="4" rx="2" fill="black" />
                                        </svg>
                                    </span>
                                            <!--end::Svg Icon-->
                                        </a>
                                        <!--begin::Menu-->
                                        <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-bold fs-6 w-200px py-4" data-kt-menu="true">
                                            <!--begin::Menu item-->
                                            <div class="menu-item px-3">
                                                <a class="menu-link text-danger px-3" id="btn-cancel-booking">Cancel Booking</a>
                                                <form id="form-cancel-booking" style="display: none;" method="POST" action="{{ route('bookings.cancel', $booking->id) }}">
                                                    @csrf
                                                    @method("PUT")
                                                </form>
                                            </div>
                                            <!--end::Menu item-->
                                        </div>
                                        <!--end::Menu-->
                                        <!--end::More options-->
                                    </div>
                                    <!--end::Card toolbar-->
                            @endcan
                        </div>
                        <!--end::Card header-->
                        <!--begin::Card body-->
                        <div class="card-body pt-0 fs-6">
                            <!--begin::Section-->
                            <div class="mb-7">
                                <!--begin::Details-->
                                <div class="d-flex align-items-center">
                                    <!--begin::Avatar-->
                                    <div class="symbol symbol-60px symbol-circle me-3">
                                        <img alt="{{ $booking->patient->name }}" src="{{ $booking->patient->avatar_url }}" />
                                    </div>
                                    <!--end::Avatar-->
                                    <!--begin::Info-->
                                    <div class="d-flex flex-column">
                                        <!--begin::Name-->
                                        <a href="{{ route('patients.show', $booking->patient_id) }}" class="fs-4 fw-bolder text-gray-900 text-hover-primary me-2">{{ $booking->patient->name }}</a>
                                        <!--end::Name-->
                                        <!--begin::Email-->
                                        <a href="{{ route('patients.show', $booking->patient_id) }}" class="fw-bold text-gray-600 text-hover-primary">{{ $booking->patient->user->email }}</a>
                                        <!--end::Email-->
                                    </div>
                                    <!--end::Info-->
                                </div>
                                <!--end::Details-->
                            </div>
                            <!--end::Section-->
                            <!--begin::Separator-->
                            <div class="separator separator-dashed mb-7"></div>
                            <!--end::Separator-->
                            <!--begin::Section-->
                            <div class="mb-10">
                                <!--begin::Details-->
                                <table class="table fs-6 fw-bold gs-0 gy-2 gx-2">
                                    <!--begin::Row-->
                                    <tr class="">
                                        <td class="text-gray-400">Booking ID:</td>
                                        <td class="text-gray-800">{{ $booking->booking_number }}</td>
                                    </tr>

                                    <tr class="">
                                        <td class="text-gray-400">Services:</td>
                                        <td class="text-gray-800">{{ $booking->medical_service->name }}</td>
                                    </tr>
                                    <!--end::Row-->
                                    <!--begin::Row-->
                                    <tr class="">
                                        <td class="text-gray-400">Status:</td>
                                        <td>
                                            {!! $booking->status->toBadge() !!}
                                        </td>
                                    </tr>
                                    <!--end::Row-->
                                    <!--begin::Row-->
                                    <tr class="">
                                        <td class="text-gray-400">Total</td>
                                        <td class="text-gray-800">@rupiah($booking->total)</td>
                                    </tr>
                                    <!--end::Row-->
                                    <!--begin::Row-->
                                    <tr class="">
                                        <td class="text-gray-400">Created at:</td>
                                        <td class="text-gray-800">{{ $booking->created_at->format('d M Y') }}</td>
                                    </tr>
                                    <!--end::Row-->
                                </table>
                                <!--end::Details-->
                            </div>
                            <!--end::Section-->
                            <!--begin::Actions-->
{{--                            <div class="mb-0">--}}
{{--                                <a href="../../demo1/dist/apps/subscriptions/add.html" class="btn btn-primary" id="kt_subscriptions_create_button">Edit Booking</a>--}}
{{--                            </div>--}}
                            <!--end::Actions-->
                        </div>
                        <!--end::Card body-->
                    </div>
                    <!--end::Card-->
                </div>
                <!--end::Sidebar-->
            </div>
            <!--end::Layout-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Post-->

    <!--begin::Modal - Users Search-->
    <div class="modal fade" id="kt_modal_customer_search" tabindex="-1" aria-hidden="true">
        <!--begin::Modal dialog-->
        <div class="modal-dialog modal-dialog-centered mw-650px">
            <!--begin::Modal content-->
            <div class="modal-content">
                <!--begin::Modal header-->
                <div class="modal-header pb-0 border-0 justify-content-end">
                    <!--begin::Close-->
                    <div class="btn btn-icon btn-sm btn-active-icon-primary" data-bs-dismiss="modal">
                        <!--begin::Svg Icon | path: icons/duotune/arrows/arr061.svg-->
                        <span class="svg-icon svg-icon-1">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="black" />
                                <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="black" />
                            </svg>
                        </span>
                        <!--end::Svg Icon-->
                    </div>
                    <!--end::Close-->
                </div>
                <!--begin::Modal header-->
                <!--begin::Modal body-->
                <div class="modal-body scroll-y mx-5 mx-xl-18 pt-0 pb-15">
                    <!--begin::Content-->
                    <div class="text-center mb-12">
                        <h1 class="fw-bolder mb-3">Search Medical People</h1>
                        <div class="text-gray-400 fw-bold fs-5">Add a medical people to a booking</div>
                    </div>
                    <!--end::Content-->
                    <!--begin::Search-->
                    <div id="kt_modal_customer_search_handler" data-kt-search-keypress="true" data-kt-search-min-length="2" data-kt-search-enter="enter" data-kt-search-layout="inline">
                        <!--begin::Form-->
                        <form data-kt-search-element="form" class="w-100 position-relative mb-5" autocomplete="off">
                            <!--begin::Hidden input(Added to disable form autocomplete)-->
                            <input type="hidden" />
                            <!--end::Hidden input-->
                            <!--begin::Icon-->
                            <!--begin::Svg Icon | path: icons/duotune/general/gen021.svg-->
                            <span class="svg-icon svg-icon-2 svg-icon-lg-1 svg-icon-gray-500 position-absolute top-50 ms-5 translate-middle-y">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                    <rect opacity="0.5" x="17.0365" y="15.1223" width="8.15546" height="2" rx="1" transform="rotate(45 17.0365 15.1223)" fill="black" />
                                    <path d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z" fill="black" />
                                </svg>
                            </span>
                            <!--end::Svg Icon-->
                            <!--end::Icon-->
                            <!--begin::Input-->
                            <label for="search" class="d-none sr-only">Search</label>
                            <input type="text" class="form-control form-control-lg form-control-solid px-15" name="search" id="search" value="" placeholder="Search by username, full name or email..." data-kt-search-element="input" />
                            <!--end::Input-->
                            <!--begin::Spinner-->
                            <span class="position-absolute top-50 end-0 translate-middle-y lh-0 d-none me-5" data-kt-search-element="spinner">
                                <span class="spinner-border h-15px w-15px align-middle text-gray-400"></span>
                            </span>
                            <!--end::Spinner-->
                            <!--begin::Reset-->
                            <span class="btn btn-flush btn-active-color-primary position-absolute top-50 end-0 translate-middle-y lh-0 me-5 d-none" data-kt-search-element="clear">
                                <!--begin::Svg Icon | path: icons/duotune/arrows/arr061.svg-->
                                <span class="svg-icon svg-icon-2 svg-icon-lg-1 me-0">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none">
                                        <rect opacity="0.5" x="6" y="17.3137" width="16" height="2" rx="1" transform="rotate(-45 6 17.3137)" fill="black" />
                                        <rect x="7.41422" y="6" width="16" height="2" rx="1" transform="rotate(45 7.41422 6)" fill="black" />
                                    </svg>
                                </span>
                                <!--end::Svg Icon-->
                            </span>
                            <!--end::Reset-->
                        </form>
                        <!--end::Form-->
                        <!--begin::Wrapper-->
                        <div class="py-5">
                            <!--begin::Suggestions-->
                            <div data-kt-search-element="suggestions">
                                <!--begin::Illustration-->
                                <div class="text-center px-4 pt-10">
                                    <img src="{{ asset('demo1/media/illustrations/sketchy-1/4.png') }}" alt="" class="mw-100 mh-200px" />
                                </div>
                                <!--end::Illustration-->
                            </div>
                            <!--end::Suggestions-->
                            <!--begin::Results-->
                            <div data-kt-search-element="results" class="d-none"></div>
                            <!--end::Results-->
                            <!--begin::Empty-->
                            <div data-kt-search-element="empty" class="text-center d-none">
                                <!--begin::Message-->
                                <div class="fw-bold py-0 mb-10">
                                    <div class="text-gray-600 fs-3 mb-2">No users found</div>
                                    <div class="text-gray-400 fs-6">Try to search by username, full name or email...</div>
                                </div>
                                <!--end::Message-->
                                <!--begin::Illustration-->
                                <div class="text-center px-4">
                                    <img src="{{ asset('demo1/media/illustrations/sketchy-1/9.png') }}" alt="user" class="mw-100 mh-200px" />
                                </div>
                                <!--end::Illustration-->
                            </div>
                            <!--end::Empty-->
                        </div>
                        <!--end::Wrapper-->
                    </div>
                    <!--end::Search-->
                </div>
                <!--end::Modal body-->
            </div>
            <!--end::Modal content-->
        </div>
        <!--end::Modal dialog-->
    </div>
    <!--end::Modal - Users Search-->

    @section('scripts')
        <script type="text/javascript">
            window.booking_id = {{ $booking->id }}
            window.search_medical_people_url = '{{ route('medical_people.search') }}'
        </script>
    @endsection

</x-base-layout>
