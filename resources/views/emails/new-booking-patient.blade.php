@component('mail::message')
# Halo!
Terimakasih sudah melakukan pemesanan untuk:

Nama Layanan: **{{ $service_name }}**<br>
Tanggal Pesanan: **{{ $booking_date }}**<br>
Total Biaya: **{{ $booking_total }}**

@if($bank_info !== null)
Silahkan melakukan pembayaran ke nomor rekening:

{!! nl2br($bank_info) !!}

Silahkan klik tombol dibawah untuk konfirmasi via Whatsapp jika Anda sudah melakukan pembayaran

@component('mail::button', ['url' => 'https://wa.me/6281909201120'])
Konfirmasi Pembayaran via WhatsApp
@endcomponent

@endif

Terima kasih,<br>
{{ config('app.name') }}
@endcomponent
