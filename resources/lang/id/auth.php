<?php

/*
|--------------------------------------------------------------------------
| Authentication Language Lines
|--------------------------------------------------------------------------
|
| The following language lines are used during authentication for various
| messages that we need to display to the user. You are free to modify
| these language lines according to your application's requirements.
|
*/

return [
    'failed'   => 'Mohon maaf email/kata sandi yang Anda masukkan salah.',
    'password' => 'Kata sandi yang dimasukkan salah.',
    'throttle' => 'Terlalu banyak upaya masuk. Silahkan coba lagi dalam :seconds detik.',
    'on_verification' => 'Akun Anda belum dapat digunakan, silahkan hubungi tim Kamirawat melalui tombol Whatsapp untuk informasi lebih lanjut.',
    'not_registered' => 'Mohon maaf email/kata sandi yang Anda masukkan belum terdaftar, mohon untuk melakukan pendaftaran terlebih dahulu.',
    'rejected_status' => 'Mohon maaf untuk saat ini kami belum dapat menyetujui permohonan pendaftaran Anda.',
];
