jQuery(document).ready(function() {

    !function () {
        "use strict"
    },

    function ($) {
        'use strict'

        const RegisterPartner = function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        }

        RegisterPartner.prototype.handleForm = function() {
            const KTP = "KTP";
            const PASSPORT = "PASSPORT";

            $('#phone_number').attr('maxlength','16');
            $("#phone_number").on("input", function(evt) {
                const self = $(this);

                self.val(self.val().replace(/\D/g, ""));
                if ((evt.which < 48 || evt.which > 57))
                {
                    evt.preventDefault();
                }
            });

            $('.document-file').change(function () {
                var fileExtension = ['png','jpg','jpeg','pdf'];
                if ($.inArray($(this).val().split('.').pop().toLowerCase(), fileExtension) == -1) {
                    alert("Upload file hanya bisa .png .jpg .jpeg .pdf");
                    this.value = ''; // Clean field
                    return false;
                }
            });

            $("#name").on("keydown", function(evt) {

                if (evt.ctrlKey || evt.altKey) {
                    evt.preventDefault();
                } else {
                    var key = evt.keyCode;
                    if (!((key == 8) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {
                        evt.preventDefault();
                    }
                }

            });

            $("#id_number").on("input", function(evt) {
                const self = $(this);
                const type = $('#id_type').val();
                if(type === KTP) {
                    self.attr('maxlength','16');
                    self.attr('minlength','16');
                    self.val(self.val().replace(/\D/g, ""));
                    if ((evt.which < 48 || evt.which > 57)) {
                        evt.preventDefault();
                    }
                }else{
                    self.attr('minlength', '9');
                    self.attr('maxlength','9');
                }
            });

            $('#rt').attr('maxlength','3');
            $("#rt").on("input", function(evt) {
                const self = $(this);
                self.val(self.val().replace(/\D/g, ""));
                if ((evt.which < 48 || evt.which > 57)) {
                    evt.preventDefault();
                }
            });

            $('#rw').attr('maxlength','3');
            $("#rw").on("input", function(evt) {
                const self = $(this);
                self.val(self.val().replace(/\D/g, ""));
                if ((evt.which < 48 || evt.which > 57)) {
                    evt.preventDefault();
                }
            });

            $(document.body).on('change', '#id_type', function() {
                const value = $(this).val();
                const label = $('label[for="id_number"]');
                const input = $('#id_number');

                $('#id_number').val('');
                if (value === KTP) {
                    label.html("No. KTP");
                    input.attr('placeholder', 'No. KTP');

                } else {
                    label.html("No. PASSPORT");
                    input.attr('placeholder', 'No. PASSPORT');
                }
            });

            $(document.body).on('change', '#nationality', function() {
                const value = $(this).val();

                if (value === "WNA") {
                    $('#id_type').find('option[value="KTP"]').attr('disabled', 'disabled');
                    $('#id_type').find('option[value="PASSPORT"]').removeAttr('disabled');
                    $('#id_type').val(PASSPORT);
                    $('#id_type').trigger('change');
                } else {
                    $('#id_type').find('option[value="PASSPORT"]').attr('disabled', 'disabled');
                    $('#id_type').find('option[value="KTP"]').removeAttr('disabled');
                    $('#id_type').val(KTP);
                    $('#id_type').trigger('change');
                }
            });

            $("#password_confirmation").on('keyup', function() {
                const password = $("#password").val();
                const confirmPassword = $("#password_confirmation").val();
                if (password !== confirmPassword)
                  $(".retype").html("Password tidak sama").css("color", "red");
                else
                  $(".retype").html("Password sama").css("color", "green");
              });

            // document validation size
            $('input[type="file"]').change(function(){
                var file_size = $(this)[0].files[0].size;
                if(file_size > 2000000) {
                    alert('File tidak boleh lebih dari 2MB');
                    $(this).val('');
                    return false;
                }
                return true;
            });
        }

        RegisterPartner.prototype.init = function() {
            $.RegisterPartner.handleForm();
        }

        $.RegisterPartner = new RegisterPartner
        $.RegisterPartner.Constructor = RegisterPartner

    }(window.jQuery),

    function($) {
        "use strict"

        if (window.register_partner === true) {
            $.RegisterPartner.init();
        }
    }(window.jQuery)
});
