jQuery(document).ready(function() {

    !function () {
        "use strict"
    },

    function ($) {
        'use strict'

        const PartnerOrder = function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            this.ongoing_booking_id = null;

            this.ongoing_order_check = {
                handling: false,
                medical_record: false,
                additional_invoice: false
            }

            this.ongoing_order_value = {
                handling: '',
                medical_record: '',
                additional_services: [],
            }

            if (window.partner_order === true) {
                this.modalPasscode = new bootstrap.Modal(document.getElementById('showDetailModalPasscode'));
                this.modalResume = new bootstrap.Modal(document.getElementById('showDetailModal'));
            }
        }

        PartnerOrder.prototype.handleGetOrderDetail = function() {
            $('#showDetailModal').on('shown.bs.modal', function(e) {
                const getOrderUrl = $(e.relatedTarget).data('urlview');
                const modal = $(this);

                $.get(getOrderUrl, function({html, booking_number}) {
                    modal.find('.modal-title').html(booking_number);
                    modal.find('.modal-body').html(html);
                }).fail(function() {
                    modal.modal('hide');
                    $.PartnerOrder.modalPasscode.show($('#viewResume'));
                });
            });
        }

        PartnerOrder.prototype.handleAcceptOrder = function() {
            $('#ConfirmationAcceptOrderModal').on('show.bs.modal', function(e) {
                const id = $(e.relatedTarget).data('id');
                const modal = $(this);
                const action = modal.find('#approve-form').attr('action');
                const realActionUrl = action.replace('__id', id);
                modal.find('#approve-form').attr('action', realActionUrl);
            });

            $(document.body).on('click', '#approve-button', function() {
                $('#approve-form').submit();
            });
        }

        PartnerOrder.prototype.handleRejectOrder = function() {
            $('#ConfirmationRejectOrderModal').on('show.bs.modal', function(e) {
                const id = $(e.relatedTarget).data('id');
                const modal = $(this);
                const action = modal.find('#reject-form').attr('action');
                const realActionUrl = action.replace('__id', id);
                modal.find('#reject-form').attr('action', realActionUrl);
            });

            $(document.body).on('click', '#reject-button', function() {
                $('#reject-form').submit();
            });
        }

        PartnerOrder.prototype.handleOpenHandlingPopup = function() {
            $('#ModalTindakan')
                .on('shown.bs.modal', function(e) {
                    const button = $(e.relatedTarget);
                    const modal = $(this);
                    const url = button.data('url');

                    $.get(url, function({html}) {
                        modal.find('.modal-body').html(html);
                        $(document.body).find('#ModalTindakan textarea').val($.PartnerOrder.ongoing_order_value.handling);
                    }).fail(function() {
                        alert('Failed get data. Please try again!');
                        modal.modal('hide');
                    });
                })
                .on('hidden.bs.modal', function() {
                    $(this).find('.modal-body').html('Loading...');
                });

            $(document.body).on('click', '#saveMedicalHandling', function(e) {
                e.preventDefault();
                const modal = $(this).closest('.modal');
                const value = modal.find('textarea').val();

                if (value === "") {
                    alert('Tindakan wajib diisi');
                    return false;
                }

                $.PartnerOrder.ongoing_order_check.handling = true;
                $.PartnerOrder.ongoing_order_value.handling = value;

                const icon = $('.handling-check-icon');
                icon.attr('data-icon', 'check-square');

                modal.modal('hide');
            });
        }

        PartnerOrder.prototype.handleOpenMedicalRecordPopup = function() {
            $('#ModalRekamMedis')
                .on('shown.bs.modal', function(e) {
                    const button = $(e.relatedTarget);
                    const modal = $(this);
                    const url = button.data('url');
                    const {handling} = $.PartnerOrder.ongoing_order_check;

                    if (handling === false) {
                        alert('Anda belum mengisi Tindakan');
                        modal.modal('hide');
                        return false;
                    }

                    $.get(url, function({html}) {
                        modal.find('.modal-body').html(html);
                        $(document.body).find('#ModalRekamMedis textarea').val($.PartnerOrder.ongoing_order_value.medical_record);
                    }).fail(function() {
                        alert('Failed get data. Please try again!');
                        modal.modal('hide');
                    });
                })
                .on('hidden.bs.modal', function() {
                    $(this).find('.modal-body').html('Loading...');
                });

            $(document.body).on('click', '#saveMedicalRecord', function(e) {
                e.preventDefault();
                const modal = $(this).closest('.modal');
                const value = modal.find('textarea').val();

                if (value === "") {
                    alert('Rekam medis wajib diisi');
                    return false;
                }

                $.PartnerOrder.ongoing_order_check.medical_record = true;
                $.PartnerOrder.ongoing_order_value.medical_record = value;

                const icon = $('.medical-record-check-icon');
                icon.attr('data-icon', 'check-square');

                modal.modal('hide');
            });
        }

        PartnerOrder.prototype.handleOpenAdditionalServicePopup = function() {
            $('#ModalLayanan')
                .on('shown.bs.modal', function(e) {
                    const button = $(e.relatedTarget);
                    const modal = $(this);
                    const url = button.data('url');
                    const id = button.data('id');
                    const {handling, medical_record} = $.PartnerOrder.ongoing_order_check;

                    if (handling === false) {
                        alert('Anda belum mengisi Tindakan');
                        modal.modal('hide');
                        return false;
                    }

                    if (medical_record === false) {
                        alert('Anda belum mengisi rekam medis');
                        modal.modal('hide');
                        return false;
                    }

                    $.get(url, function({html}) {
                        modal.find('.modal-body').html(html);
                        $.PartnerOrder.ongoing_booking_id = id;
                    }).fail(function() {
                        alert('Failed get data. Please try again!');
                        modal.modal('hide');
                    });
                })
                .on('hidden.bs.modal', function() {
                    $(this).find('.modal-body').html('Loading...');
                });

            $(document.body).on('click', '#finish-layanan', function(e) {
                e.preventDefault();

                const {additional_services} = $.PartnerOrder.ongoing_order_value;

                const icon = $('.additional-invoice-check-icon');
                icon.attr('data-icon', 'check-square');

                if (additional_services.length > 0) {
                    const modal = $(this).closest('.modal');
                    modal.modal('hide');
                    $('#ModalInvoice').modal('show');
                } else {
                    $.PartnerOrder.handleCompletionOrder();
                }
            });

            $(document.body).change('input[name=pilih-layanan]', function(){
                if($("#pilih-layanan-yes").is(':checked')){
                    $(".tambah-layanan").addClass('ly-open').show();
                }else{
                    $(".tambah-layanan").removeClass('ly-open').hide();
                }
            });

            $(document.body).on('change', 'input.additional_services', function() {
                const values = [];
                $('input.additional_services').each(function() {
                    if (this.checked) {
                        values.push($(this).val());
                    }
                });

                $.PartnerOrder.ongoing_order_value.additional_services = values;
            });

            $(document.body).on('keyup', '#filter_layanan', function() {
                var value,name, layanan_item,i;
                value = document.getElementById('filter_layanan').value.toUpperCase();
                layanan_item = document.getElementsByClassName('layanan-item');

                for(i=0;layanan_item.length;i++){
                    name = layanan_item[i].getElementsByClassName('layanan-name');
                    if(name[0].innerHTML.toUpperCase().indexOf(value) > -1){
                        layanan_item[i].style.display ="flex";
                    }else{
                        layanan_item[i].style.display = "none";
                    }
                }
            });
        }

        PartnerOrder.prototype.handleOpenConfirmationAdditionalServicePopup = function() {
            $('#ModalInvoice')
                .on('shown.bs.modal', function(e) {
                    const modal = $(this);
                    const {get_confirmation_additional_services_url} = window;
                    const id = $.PartnerOrder.ongoing_booking_id;
                    const url = get_confirmation_additional_services_url.replace('__id', id);
                    const data = {
                        additional_services: $.PartnerOrder.ongoing_order_value.additional_services,
                    }

                    $.post(url, data, function({html}) {
                        modal.find('.modal-body').html(html);
                    }).fail(function() {
                        alert('Failed get data. Please try again!');
                        modal.modal('hide');
                    });
                })
                .on('hidden.bs.modal', function() {
                    $(this).find('.modal-body').html('Loading...');
                });

            $('#finish-modal-invoice').on('click', function() {
                const modal = $(this).closest('.modal');
                modal.block({message: $('.blockui-spinner')});
                $.PartnerOrder.handleCompletionOrder(function() {
                    modal.unblock();
                });
            });
        }

        PartnerOrder.prototype.handleCompletionOrder = function(callback) {
            if (window.completion_order_url) {
                const url = window.completion_order_url;
                const id = $.PartnerOrder.ongoing_booking_id;
                const completionOrderUrl = url.replace('__id', id);
                const data = {
                    _method: 'PUT',
                    handling: $.PartnerOrder.ongoing_order_value.handling,
                    medical_record: $.PartnerOrder.ongoing_order_value.medical_record,
                    additional_services: JSON.stringify($.PartnerOrder.ongoing_order_value.additional_services),
                }

                $.post(completionOrderUrl, data, function({is_reload, payment_link, invoice_id, payment_method}) {
                    if (is_reload === true) {
                        location.reload();
                    } else {
                        const modal = $('#showPaymentURLModal');
                        $('#ModalLayanan').modal('hide');
                        $('#ModalInvoice').modal('hide');
                        modal.modal('show');
                        modal.find('#payment-check-manual').hide();
                        modal.find('#payment-bank-transfer').hide();

                        if (payment_method.gateway === "XENDIT") {
                            modal.find('#payment-check-manual').show();
                            new QRCode(document.getElementById("qrcode-payment-link"), payment_link);
                            modal.find('.btn-payment-check').attr('data-id', invoice_id);
                        } else {
                            modal.find('#payment-bank-transfer').show();
                            modal.find('.bank-transfer-detail').html(payment_method.description);
                        }
                    }

                    callback();
                }).fail(function() {
                    callback();

                    alert('Failed to process completion order');
                });
            }
        }

        PartnerOrder.prototype.handleCheckPaymentAdditionalService = function() {
            $(document.body).on('click', '#payment-check-manual .btn-payment-check', function() {
                const id = $(this).data('id');
                const container = $(this).closest('#payment-check-manual');
                const url = window.get_order_payment_check_url;
                const paymentCheckUrl = url.replace('__id', id);
                const btn = $(this);
                const loadingEl = container.find('.btn-loading');
                const modal = container.closest('.modal');

                btn.hide();
                loadingEl.show();
                $.get(paymentCheckUrl, function({status}) {
                    if (status === "PAID") {
                        modal.find('#qrcode-payment-link').hide();
                        modal.find('.payment-status').show();
                        modal.find('#payment-check-manual').hide();
                        modal.find('.modal-footer').show();
                    } else {
                        btn.show();
                    }
                }).fail(function() {
                    btn.show();
                    alert('Failed to get payment status. Please try again!');
                }).always(function() {
                    loadingEl.hide();
                });
            });

            $('#showPaymentURLModal').on('hide.bs.modal', function() {
                location.reload();
            });
        }

        PartnerOrder.prototype.handleOpenMap = function() {
            $('#ModalMap')
                .on('shown.bs.modal', function(e) {
                    const btn = $(e.relatedTarget);
                    const url = btn.data('url');
                    const modal = $(this);

                    $.get(url, function({html, booking_number}) {
                        modal.find('.modal-body').html(html);
                        modal.find('.booking-number').html(booking_number);
                    }).fail(function() {
                        alert('Failed to get map data.');
                        modal.modal('hide');
                    });
                })
                .on('hidden.bs.modal', function() {
                    const modal = $(this);
                    modal.find('.modal-body').html('Loading...');
                    modal.find('.booking-number').html('');
                });
        }

        PartnerOrder.prototype.handleBookingDetail = function() {
            $('#bookingDetail')
                .on('shown.bs.modal', function(e) {
                    const btn = $(e.relatedTarget);
                    const url = btn.data('url');
                    const modal = $(this);

                    $.get(url, function({html, booking_number}) {
                        modal.find('.modal-body').html(html);
                        modal.find('.modal-title').html(booking_number);
                    }).fail(function() {
                        alert('Failed tom get order detail.');
                        modal.modal('hide');
                    });
                })
                .on('hidden.bs.modal', function() {
                    const modal = $(this);
                    modal.find('.modal-body').html('Loading...')
                    modal.find('.modal-title').html('');
                });
        }

        PartnerOrder.prototype.handleClickResumeDetail = function() {
            const modalPasscodeEl = document.getElementById('showDetailModalPasscode');
            const modalPasscode = $.PartnerOrder.modalPasscode;
            const modalDetail = $.PartnerOrder.modalResume;
            const btnViewResume = $('#viewResume');
            let inputPasscode = false;

            btnViewResume.on('click', function() {
                if (btnViewResume.data('inputpasscode') === "YES" || inputPasscode === true) {
                    modalDetail.show(btnViewResume);
                } else {
                    modalPasscode.show(btnViewResume);
                }
            });

            modalPasscodeEl.addEventListener('shown.bs.modal', function(e) {
                const btn = $(e.relatedTarget);
                const modal = $(this);

                modal.find('#submit-passcode').attr('data-urlpasscode', btn.data('urlpasscode'));
            });

            $(document.body).on('click', '#submit-passcode', function() {
                const url = $(this).data('urlpasscode');
                const passcode = $(this).closest('.modal').find('input#passcode').val();
                const modal = $(this).closest('.modal');
                const btn = $(this);

                modal.find('.error-message').hide();
                $.post(url, {passcode}, function() {
                    modalPasscode.hide();
                    modalDetail.show(btnViewResume);

                    btn.attr('data-inputpasscode', 'YES');
                    inputPasscode = true;
                }).fail(function(xhr) {
                    const response = JSON.parse(xhr.responseText);
                    if (xhr.status === 422) {
                        modal.find('.error-message').slideDown();
                        modal.find('.error-message').html(response.errors.passcode[0]);
                    }
                });
            });
        }

        PartnerOrder.prototype.init = () => {
            $.PartnerOrder.handleGetOrderDetail();
            $.PartnerOrder.handleAcceptOrder();
            $.PartnerOrder.handleRejectOrder();
            $.PartnerOrder.handleOpenHandlingPopup();
            $.PartnerOrder.handleOpenMedicalRecordPopup();
            $.PartnerOrder.handleOpenAdditionalServicePopup();
            $.PartnerOrder.handleOpenConfirmationAdditionalServicePopup();
            $.PartnerOrder.handleCheckPaymentAdditionalService();
            $.PartnerOrder.handleOpenMap();
            $.PartnerOrder.handleBookingDetail();
            $.PartnerOrder.handleClickResumeDetail();
        }

        $.PartnerOrder = new PartnerOrder
        $.PartnerOrder.Constructor = PartnerOrder
    }(window.jQuery),

    function($) {
        "use strict"

        if (window.partner_order === true) {
            $.PartnerOrder.init();
        }
    }(window.jQuery)

});
