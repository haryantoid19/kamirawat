jQuery(document).ready(function() {
    !function () {
        "use strict"
    },

    function ($) {
        'use strict'

        const RegisterPostalCodeHandling = function () {
            this.urlGetPostalCode = window.postalCode ? window.postalCode.urlGetPostalCode : null;

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        }

        RegisterPostalCodeHandling.prototype.handleProvince = function() {
            $(document.body).on('change', '#province', function() {
                const value = $(this).val();
                const city_id = $('input[name="city_id"]').val();
                const payload = {
                    method: 'city',
                    province_code: value,
                };
                $.post($.RegisterPostalCodeHandling.urlGetPostalCode, payload, function(response) {
                    let option = '<option selected>Pilih Kota</option>';

                    $.each(response.cities, function(i, city) {
                        if (city_id === city) {
                            option += '<option value="' + city + '" selected="selected">' + city + '</option>';
                        } else {
                            option += '<option value="' + city + '">' + city + '</option>';
                        }
                    });

                    $('#city').html(option);
                    $('#subdistrict').html('<option selected>Pilih Kecamatan</option>');
                    $('#urban').html('<option selected>Pilih Kelurahan</option>');

                    if (city_id !== "") {
                        $('#city').trigger('change');
                    }
                }).fail(function() {
                    alert('Failed. Try again!');
                });
            });

            if ($('#province').val() !== "") {
                $('#province').trigger('change');
            }
        }

        RegisterPostalCodeHandling.prototype.handleCity = function() {
            $(document.body).on('change', '#city', function() {
                const value = $(this).val();
                const province_code = $('#province').val();
                const subdistrict_id = $('input[name="subdistrict_id"]').val();
                const payload = {
                    method: 'subdistrict',
                    province_code: province_code,
                    payload: value,
                };
                $.post($.RegisterPostalCodeHandling.urlGetPostalCode, payload, function(response) {
                    let option = '<option selected>Pilih Kecamatan</option>';

                    $.each(response.subdistricts, function(i, subdistrict) {
                        if (subdistrict_id === subdistrict) {
                            option += '<option value="' + subdistrict + '" selected="selected">' + subdistrict + '</option>';
                        } else {
                            option += '<option value="' + subdistrict + '">' + subdistrict + '</option>';
                        }
                    });

                    $('#subdistrict').html(option);
                    $('#urban').html('<option selected>Pilih Kelurahan</option>');

                    if (subdistrict_id !== "") {
                        $('#subdistrict').trigger('change');
                    }
                }).fail(function() {
                    alert('Failed. Try again!');
                });
            });
        }

        RegisterPostalCodeHandling.prototype.handleSubdistrict = function() {
            $(document.body).on('change', '#subdistrict', function() {
                const value = $(this).val();
                const province_code = $('#province').val();
                const urban_id = $('input[name="urban_id"]').val();
                const payload = {
                    method: 'urban',
                    province_code: province_code,
                    payload: value,
                };
                $.post($.RegisterPostalCodeHandling.urlGetPostalCode, payload, function(response) {
                    let option = '<option selected>Pilih Kelurahan</option>';

                    $.each(response.urbans, function(i, urban) {
                        if (urban_id === urban) {
                            option += '<option value="' + urban + '" selected="selected">' + urban + '</option>';
                        } else {
                            option += '<option value="' + urban + '">' + urban + '</option>';
                        }
                    });

                    $('#urban').html(option);
                }).fail(function() {
                    alert('Failed. Try again!');
                });
            });
        }

        RegisterPostalCodeHandling.prototype.init = () => {
            $.RegisterPostalCodeHandling.handleProvince();
            $.RegisterPostalCodeHandling.handleCity();
            $.RegisterPostalCodeHandling.handleSubdistrict();
        }

        $.RegisterPostalCodeHandling = new RegisterPostalCodeHandling
        $.RegisterPostalCodeHandling.Constructor = RegisterPostalCodeHandling
    }(window.jQuery),

    function($) {
        "use strict"

        if (window.postalCode) {
            $.RegisterPostalCodeHandling.init();
        }
    }(window.jQuery)
});
