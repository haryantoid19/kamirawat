jQuery(document).ready(function() {

    !function () {
        "use strict"
    },

    function ($) {
        'use strict'

        const RegisterPatient = function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        }

        RegisterPatient.prototype.handleForm = function() {
            const KTP = "KTP";
            const PASSPORT = "PASSPORT";

            $('#id_number').val('');
            $("#phone_number").on("input", function(evt) {
                const self = $(this);
                self.val(self.val().replace(/\D/g, ""));
                if ((evt.which < 48 || evt.which > 57))
                {
                    evt.preventDefault();
                }
            });

            $("#id_number").on("input", function(evt) {
                const self = $(this);
                const type = $('#id_type').val();
                if(type === KTP) {
                    self.attr('maxlength','16');
                    self.attr('minlength','16');
                    self.val(self.val().replace(/\D/g, ""));
                    if ((evt.which < 48 || evt.which > 57)) {
                        evt.preventDefault();
                    }
                }else{
                    self.attr('maxlength','40');
                    self.removeAttr('minlength');
                }
            });

            $('#rt').attr('maxlength','3');
            $("#rt").on("input", function(evt) {
                const self = $(this);
                self.val(self.val().replace(/\D/g, ""));
                if ((evt.which < 48 || evt.which > 57)) {
                    evt.preventDefault();
                }
            });

            $('#rw').attr('maxlength','3');
            $("#rw").on("input", function(evt) {
                const self = $(this);
                self.val(self.val().replace(/\D/g, ""));
                if ((evt.which < 48 || evt.which > 57)) {
                    evt.preventDefault();
                }
            });

            $(document.body).on('change', '#id_type', function() {
                const value = $(this).val();
                const label = $('label[for="id_number"]');
                const input = $('#id_number');

                if (value === KTP) {
                    label.html("No. KTP");
                    input.attr('placeholder', 'No. KTP');
                } else {
                    label.html("No. PASSPORT");
                    input.attr('placeholder', 'No. PASSPORT');
                }
            });

            $(document.body).on('change', '#nationality', function() {
                const value = $(this).val();

                if (value === "WNA") {
                    $('#id_type').find('option[value="KTP"]').attr('disabled', 'disabled');
                    $('#id_type').val(PASSPORT);
                    $('#id_type').trigger('change');
                } else {
                    $('#id_type').find('option[value="KTP"]').removeAttr('disabled');
                }
            });

            $("#password_confirmation").on('keyup', function() {
                const password = $("#password").val();
                const confirmPassword = $("#password_confirmation").val();
                if (password !== confirmPassword)
                    $(".retype").html("Password tidak sama").css("color", "red");
                else
                    $(".retype").html("Password sama").css("color", "green");
            });

            // document validation size
            $('input[type="file"]').change(function(){
                var file_size = $(this)[0].files[0].size;
                if(file_size > 1000000) {
                    alert('File tidak boleh lebih dari 1MB');
                    $(this).val('');
                    return false;
                }
                return true;
            });
        }

        RegisterPatient.prototype.init = function() {
            $.RegisterPatient.handleForm();
        }

        $.RegisterPatient = new RegisterPatient
        $.RegisterPatient.Constructor = RegisterPatient

    }(window.jQuery),

    function($) {
        "use strict"

        if (window.register_patient === true) {
            $.RegisterPatient.init();
        }
    }(window.jQuery)
});
