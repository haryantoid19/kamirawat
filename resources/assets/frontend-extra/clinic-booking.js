jQuery(document).ready(function() {
    !function () {
        "use strict"
    },

        function ($) {
            'use strict'

            const ClinicBooking = function () {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                this.form = $('#clinic-booking-new');

                this.data = {
                    service: null,
                    self: false,
                    phone_number: null,
                    name: null,
                    bod: null,
                    gender: null,
                    address: null,
                    history_of_disease_allergies: null,
                    main_symptom: null,
                    special_request: null,
                }

                this.element = {
                    radioMySelf: this.form.find('[name="for_myself"]'),
                    textPhoneNumber: this.form.find('[name="phone_number"]'),
                    textName: this.form.find('[name="name"]'),
                    textBod: this.form.find('[name="bod"]'),
                    radioGender: this.form.find('input:radio[name="gender"]'),
                    textAddress: this.form.find('[name="address"]'),
                    textHistoryOfDiseaseAllergies: this.form.find('[name="history_of_disease_allergies"]'),
                    textMainSymptom: this.form.find('[name="main_symptom"]'),
                    textSpecialRequest: this.form.find('[name="special_request"]'),
                }
            }

            ClinicBooking.prototype.handleStep1 = function() {
                $(document.body).on('change', 'input:radio[name="selected-service"]', function() {
                    const value = $(this).val();

                    $.ClinicBooking.data.service = parseInt(value);
                });
            }

            ClinicBooking.prototype.handleStep2 = function() {
                const {textPhoneNumber, textName, textBod} = this.element;
                $(document.body).on('change', '[name="for_myself"]', function() {
                    const value = $(this).val();
                    const selfGender = $.ClinicBooking.form.find('[name="self-gender"]').val();

                    if (value === "yes") {
                        $.ClinicBooking.form.find('[name="phone_number"]').val(textPhoneNumber.data('self')).prop('disabled', true);
                        $.ClinicBooking.form.find('[name="name"]').val(textName.data('self')).prop('disabled', true);
                        $.ClinicBooking.form.find('[name="bod"]').val(textBod.data('self')).prop('disabled', true);
                        $.ClinicBooking.form.find('[name="gender"]').filter(`[value="${selfGender}"]`).prop('checked', true);
                        $.ClinicBooking.form.find('[name="gender"]').prop('disabled', true);
                    } else {
                        $.ClinicBooking.form.find('[name="phone_number"]').val('').prop('disabled', false);
                        $.ClinicBooking.form.find('[name="name"]').val('').prop('disabled', false);
                        $.ClinicBooking.form.find('[name="bod"]').val('').prop('disabled', false);
                        $.ClinicBooking.form.find('[name="gender"]').prop('checked', false).prop('disabled', false);
                    }
                });
            }


            ClinicBooking.prototype.handlenumeric = function(evt) {
                $(document.body).on('input', '.number', function() {
                    const self = $(this);

                    self.val(self.val().replace(/\D/g, ""));
                    
                });
            }

            ClinicBooking.prototype.handleForm = function() {
                const form = $("#clinic-booking-new");
                form.validate({
                    errorPlacement: function errorPlacement(error, element) { element.before(error); },
                    rules: {}
                });
                form.children("div").steps({
                    headerTag: "h3",
                    bodyTag: "section",
                    transitionEffect: "slideLeft",
                    labels: {
                        current: "current step:",
                        pagination: "Pagination",
                        next: "Selanjutnya",
                        previous: "Kembali",
                        finish: "Pesan",
                        loading: "Loading ..."
                    },
                    onInit : function () {
                        $(".datepicker-dob").datepicker({
                            autoclose: true,
                            todayHighlight: true
                        });
                    },
                    onStepChanging: function ()
                    {
                        form.validate().settings.ignore = ":disabled,:hidden";
                        return form.valid();
                    },
                    onFinishing: function ()
                    {
                        form.validate().settings.ignore = ":disabled";
                        return form.valid();
                    },
                    onFinished: function ()
                    {
                        $.ClinicBooking.data.self = $.ClinicBooking.form.find('[name="for_myself"]:checked').val() === "yes";
                        $.ClinicBooking.data.name = $.ClinicBooking.form.find('[name="name"]').val();
                        $.ClinicBooking.data.phone_number = $.ClinicBooking.form.find('[name="phone_number"]').val();
                        $.ClinicBooking.data.gender = $.ClinicBooking.form.find('[name="gender"]').val();
                        $.ClinicBooking.data.address = $.ClinicBooking.form.find('[name="address"]').val();
                        $.ClinicBooking.data.history_of_disease_allergies = $.ClinicBooking.form.find('[name="history_of_disease_allergies"]').val();
                        $.ClinicBooking.data.bod = $.ClinicBooking.form.find('[name="bod"]').val();
                        $.ClinicBooking.data.main_symptom = $.ClinicBooking.form.find('[name="main_symptom"]').val();
                        $.ClinicBooking.data.special_request = $.ClinicBooking.form.find('[name="special_request"]').val();

                        $.ClinicBooking.block();
                        $('#booking-alert').hide();
                        $.post(window.clinic_booking_url, $.ClinicBooking.data, function({redirect_url}) {
                            window.location.href = redirect_url;
                        }).fail(function(xhr) {
                            $.ClinicBooking.unblock();
                            const response = JSON.parse(xhr.responseText);

                            if (xhr.status === 422) {
                                let message = response.message;
                                message += "<br>";
                                message += "<ul>";
                                $.each(response.errors, function (i, error) {
                                    message += "<li>"+error[0]+"</li>";
                                });
                                message += "</ul>";
                                $('#booking-alert').html(message).show();
                            } else {
                                $('#booking-alert').html(response.message).show();
                            }
                        });
                    }
                });
            }

            ClinicBooking.prototype.block = function() {
                $('.section-booking').block({
                    message: $('.blockui-spinner'),
                    css: {
                        top: '50%',
                        left: '35%'
                    }
                });
            }

            ClinicBooking.prototype.unblock = function() {
                $('.section-booking').unblock();
            }

            ClinicBooking.prototype.init = () => {
                $.ClinicBooking.handleForm();
                $.ClinicBooking.handleStep1();
                $.ClinicBooking.handleStep2();
                $.ClinicBooking.handlenumeric();
            }

            $.ClinicBooking = new ClinicBooking
            $.ClinicBooking.Constructor = ClinicBooking
        }(window.jQuery),

        function($) {
            "use strict"

            $.ClinicBooking.init();
        }(window.jQuery)
});
