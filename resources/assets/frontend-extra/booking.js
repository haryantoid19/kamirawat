jQuery(document).ready(function() {
    !function () {
        "use strict"
    },

    function ($) {
        'use strict'

        const Booking = function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            this.form = $('#booking-new');

            this.data = {
                service: null,
                self: false,
                phone_number: null,
                name: null,
                bod: null,
                gender: null,
                address: null,
                history_of_disease_allergies: null,
                main_symptom: null,
                special_request: null,
                latlong: null,
                payment_method: null,
            }

            this.mapData = {
                content: null,
                latitude: 52.525595,
                longitude: 13.393085,
                map: null,
                marker: null
            }

            this.element = {
                radioMySelf: this.form.find('[name="for_myself"]'),
                textPhoneNumber: this.form.find('[name="phone_number"]'),
                textName: this.form.find('[name="name"]'),
                textBod: this.form.find('[name="bod"]'),
                radioGender: this.form.find('input:radio[name="gender"]'),
                textAddress: this.form.find('[name="address"]'),
                textHistoryOfDiseaseAllergies: this.form.find('[name="history_of_disease_allergies"]'),
                textMainSymptom: this.form.find('[name="main_symptom"]'),
                textSpecialRequest: this.form.find('[name="special_request"]'),
                textLatLong: this.form.find('[name="latlong"]'),
                radioPaymentMethod: this.form.find('[name="payment-method"]'),
            }
        }

        Booking.prototype.handleStep1 = function() {
            $(document.body).on('change', 'input:radio[name="selected-service"]', function() {
                const value = $(this).val();

                $.Booking.data.service = parseInt(value);
            });
        }

        Booking.prototype.handleStep2 = function() {
            const {textPhoneNumber, textName, textBod} = this.element;
            $(document.body).on('change', '[name="for_myself"]', function() {
               const value = $(this).val();
               const selfGender = $.Booking.form.find('[name="self-gender"]').val();

               if (value === "yes") {
                   $.Booking.form.find('[name="phone_number"]').val(textPhoneNumber.data('self')).prop('disabled', true);
                   $.Booking.form.find('[name="name"]').val(textName.data('self')).prop('disabled', true);
		   $.Booking.form.find('[name="bod"]').val(textBod.data('self')).prop('disabled', true);
                   $.Booking.form.find('[name="gender"]').filter(`[value="${selfGender}"]`).prop('checked', true);
                   $.Booking.form.find('[name="gender"]').prop('disabled', true);
               } else {
                   $.Booking.form.find('[name="phone_number"]').val('').prop('disabled', false);
                   $.Booking.form.find('[name="name"]').val('').prop('disabled', false);
                   $.Booking.form.find('[name="bod"]').val('').prop('disabled', false);
                   $.Booking.form.find('[name="gender"]').prop('checked', false).prop('disabled', false);
               }
            });
        }


        Booking.prototype.handlenumeric = function(evt) {
            $(document.body).on('input', '.number', function() {
                const self = $(this);

                self.val(self.val().replace(/\D/g, ""));
                
            });
        }

        Booking.prototype.handleStep3 = function() {
            $(document.body).on('change', '[name="payment-method"]', function() {
                $.Booking.data.payment_method = $(this).val();
            });
        }

        Booking.prototype.handleForm = function() {
            const form = $("#booking-new");
            form.validate({
                errorPlacement: function errorPlacement(error, element) { element.before(error); },
                rules: {
                    confirm: {
                        equalTo: "#password"
                    }
                }
            });
            form.children("div").steps({
                headerTag: "h3",
                bodyTag: "section",
                transitionEffect: "slideLeft",
                labels: {
                    current: "current step:",
                    pagination: "Pagination",
                    next: "Selanjutnya",
                    previous: "Kembali",
                    finish: "Bayar",
                    loading: "Loading ..."
                },
                onInit : function () {
                    $(".datepicker-dob").datepicker({
                        autoclose: true,
                        todayHighlight: true
                    });

                },
                onStepChanging: function (event, currentIndex, newIndex)
                {
                    if (newIndex === 2) {
                        $.Booking.data.self = $.Booking.form.find('[name="for_myself"]:checked').val() === "yes";
                        $.Booking.data.name = $.Booking.form.find('[name="name"]').val();
                        $.Booking.data.phone_number = $.Booking.form.find('[name="phone_number"]').val();
                        $.Booking.data.gender = $.Booking.form.find('[name="gender"]').val();
                        $.Booking.data.address = $.Booking.form.find('[name="address"]').val();
                        $.Booking.data.history_of_disease_allergies = $.Booking.form.find('[name="history_of_disease_allergies"]').val();
                        $.Booking.data.bod = $.Booking.form.find('[name="bod"]').val();
                        $.Booking.data.main_symptom = $.Booking.form.find('[name="main_symptom"]').val();
                        $.Booking.data.special_request = $.Booking.form.find('[name="special_request"]').val();
                        $.Booking.data.latlong = $.Booking.form.find('[name="latlong"]').val();

                        $.Booking.block();
                        $.get(window.booking_invoice_table_url + '?service=' + $.Booking.data.service, function(response) {
                            $('#invoice-container').html(response.html);
                            $.Booking.unblock();
                        }).fail(function() {
                            alert('Failed to get invoice summary. Please reload page');
                        });
                    }

                    form.validate().settings.ignore = ":disabled,:hidden";
                    return form.valid();
                },
                onFinishing: function ()
                {
                    form.validate().settings.ignore = ":disabled";
                    return form.valid();
                },
                onFinished: function ()
                {
                    $.Booking.block();
                    $('#booking-alert').hide();
                    $.post(window.booking_url, $.Booking.data, function({redirect_url}) {
                        window.location.href = redirect_url;
                    }).fail(function(xhr) {
                        $.Booking.unblock();
                        const response = JSON.parse(xhr.responseText);

                        if (xhr.status === 422) {
                            let message = response.message;
                            message += "<ul>";
                            $.each(response.errors, function (i, error) {
                                message += "<li>"+error[0]+"</li>";
                            });
                            message += "</ul>";
                            $('#booking-alert').html(message).show();
                        } else {
                            $('#booking-alert').html(response.message).show();
                        }
                    });
                }
            });
        }

        Booking.prototype.block = function() {
            $('.section-booking').block({
                message: $('.blockui-spinner'),
                css: {
                    top: '50%',
                    left: '35%'
                }
            });
        }

        Booking.prototype.unblock = function() {
            $('.section-booking').unblock();
        }

        Booking.prototype.handleMapsMarker = function(location) {
            const {marker, content, map} = $.Booking.mapData;
            marker.setPosition(location);
            content.innerHTML = location.lat() + "," + location.lng();
            google.maps.event.addListener(marker, 'click', function(e) {
                e.preventDefault();
                new google.maps.InfoWindow({
                    content: location.lat() + "," + location.lng()
                }).open(map,marker);
            });

            $('.latlang').val(content.innerHTML);
        }

        Booking.prototype.handleMaps = function() {
           

            navigator.geolocation.getCurrentPosition(function(location) {
                if (location.coords) {
                    $.Booking.mapData.latitude = location.coords.latitude;
                    $.Booking.mapData.longitude = location.coords.longitude;
                }else{
                    console.log(location.coords);
                }

                // Coordinates to center the map
                const myLatLang = new google.maps.LatLng($.Booking.mapData.latitude, $.Booking.mapData.longitude);

                // Other options for the map, pretty much self explanatory
                const mapOptions = {
                    zoom: 16,
                    center: myLatLang,
                    position: location,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };

                // Attach a map to the DOM Element, with the defined settings
                $.Booking.mapData.map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);

                $.Booking.mapData.content = document.getElementById('information');
                google.maps.event.addListener($.Booking.mapData.map, 'click', function(e) {
                    $.Booking.handleMapsMarker(e.latLng);
                });


                const innerHTML = myLatLang.lat() + "," + myLatLang.lng();
                $('.latlang').val(innerHTML);
                $('#information').html(innerHTML);
                $.Booking.mapData.marker = new google.maps.Marker({
                    position: myLatLang,
                    map: $.Booking.mapData.map
                });
            },function(error) {
                if (error.code == error.PERMISSION_DENIED)
                $('#map-canvas').html('<h3>Please allow your location</h3>');
             
              });
        }

        Booking.prototype.init = () => {
            $.Booking.handleForm();
            $.Booking.handleStep1();
            $.Booking.handleStep2();
            $.Booking.handleStep3();
            $.Booking.handleMaps();
            $.Booking.handlenumeric();
        }

        $.Booking = new Booking
        $.Booking.Constructor = Booking
    }(window.jQuery),

    function($) {
        "use strict"

        if (window.booking === true) {
            $.Booking.init();
        }
    }(window.jQuery)
});
