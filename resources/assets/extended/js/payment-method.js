jQuery(document).ready(function() {
    !function () {
        "use strict"
    },

    function ($) {
        'use strict'

        const PaymentMethod = function () {
            // Elements
            const modalEl = document.querySelector('#kr_new_payment_method_modal');

            if (!modalEl) {
                return;
            }

            this.modal = new bootstrap.Modal(modalEl);
            this.form = document.querySelector('#kr_new_payment_method_form');
            this.submitButton = document.getElementById('kr_modal_new_payment_method_submit');
            this.cancelButton = document.getElementById('kr_modal_new_payment_method_cancel');
            this.isEdit = false;
            this.edit_new_payment_method_url = null;

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        }

        PaymentMethod.prototype.init = () => {
            $.PaymentMethod.handleForm();
            $.PaymentMethod.editForm();
        }

        PaymentMethod.prototype.editForm = function() {
            $(document.body).on('click', '[data-edit]', function() {
                const url = $(this).data('edit');
                $.PaymentMethod.edit_new_payment_method_url = url;

                $.PaymentMethod.modal.show();
                $.get(url, function({payment_method: {code, label, gateway, status, description, icon_url}}) {
                    $('[name="code"]').val(code);
                    $('[name="label"]').val(label);
                    $('[name="gateway"]').val(gateway);
                    $('[name="status"]').val(status);
                    $('[name="description"]').val(description);
                    $('#kr_new_payment_method_form .image-input-wrapper').css('background-image', `url(${icon_url})`);
                    $('#kr_new_payment_method_form .image-input').css('background-image', `url(${icon_url})`);
                    $('input[name="method"]').val('edit');
                });
            });
        }

        PaymentMethod.prototype.handleForm = function() {
            document.getElementById('kr_new_payment_method_modal').addEventListener('hidden.bs.modal', function() {
                $('input[name="method"]').val('new');
            });

            const validator = FormValidation.formValidation(
                this.form,
                {
                    fields: {
                        code: {
                            validators: {
                                notEmpty: {
                                    message: 'This field is required'
                                }
                            }
                        },
                        label: {
                            validators: {
                                notEmpty: {
                                    message: 'This field is required'
                                }
                            }
                        },
                        gateway: {
                            validators: {
                                notEmpty: {
                                    message: 'This field is required'
                                }
                            }
                        },
                        status: {
                            validators: {
                                notEmpty: {
                                    message: 'This field is required'
                                }
                            }
                        },
                    },
                    plugins: {
                        trigger: new FormValidation.plugins.Trigger(),
                        bootstrap: new FormValidation.plugins.Bootstrap5({
                            rowSelector: '.fv-row',
                            eleInvalidClass: '',
                            eleValidClass: ''
                        })
                    }
                }
            );

            this.submitButton.addEventListener('click', function (e) {
                e.preventDefault();

                // Validate form before submit
                if (validator) {
                    validator.validate().then(function (status) {
                        if (status !== "Valid") {
                            // Show error message.
                            Swal.fire({
                                text: "Sorry, looks like there are some errors detected, please try again.",
                                icon: "error",
                                buttonsStyling: false,
                                confirmButtonText: "Ok, got it!",
                                customClass: {
                                    confirmButton: "btn btn-primary"
                                }
                            });

                            return false;
                        }

                        $.PaymentMethod.submitButton.setAttribute('data-kt-indicator', 'on');

                        // Disable button to avoid multiple click
                        $.PaymentMethod.submitButton.disabled = true;

                        let url = window.new_payment_method_url;
                        const method = $('input[name="method"]').val();
                        const formData = new FormData();

                        if (method === "edit") {
                            url = $.PaymentMethod.edit_new_payment_method_url;
                            formData.append('_method', 'PUT');
                        }

                        formData.append('code', $('input[name="code"]').val());
                        formData.append('label', $('input[name="label"]').val());
                        formData.append('status', $('select[name="status"]').val());
                        formData.append('gateway', $('select[name="gateway"]').val());
                        formData.append('description', $('textarea[name="description"]').val());

                        const fileIcon = $('input[name="icon"]');
                        if (fileIcon.val() !== "") {
                            const icon = $('input[name="icon"]')[0].files[0];
                            formData.append('icon', icon);
                        }

                        $.ajax({
                            url,
                            method: "POST",
                            data: formData,
                            contentType: false,
                            processData: false,
                            dataType: 'json',
                            success: function() {
                                // Show success message. For more info check the plugin's official documentation: https://sweetalert2.github.io/
                                Swal.fire({
                                    text: "Form has been successfully submitted!",
                                    icon: "success",
                                    buttonsStyling: false,
                                    confirmButtonText: "Ok, got it!",
                                    customClass: {
                                        confirmButton: "btn btn-primary"
                                    }
                                }).then(function (result) {
                                    if (result.isConfirmed) {
                                        $.PaymentMethod.form.reset(); // Reset form
                                        $.PaymentMethod.modal.hide();
                                        LaravelDataTables["payment-method-table"].ajax.reload();
                                        $('input[name="title"]').val('');
                                    }
                                });
                            },
                            error: function() {
                                Swal.fire({
                                    text: "Sorry, looks like there are some errors detected, please try again.",
                                    icon: "error",
                                    buttonsStyling: false,
                                    confirmButtonText: "Ok, got it!",
                                    customClass: {
                                        confirmButton: "btn btn-primary"
                                    }
                                });
                            },
                            complete: function() {
                                $.PaymentMethod.submitButton.removeAttribute('data-kt-indicator');

                                // Enable button
                                $.PaymentMethod.submitButton.disabled = false;
                            }
                        });

                    });
                }
            });
            this.cancelButton.addEventListener('click', function(e) {
                e.preventDefault();

                Swal.fire({
                    text: "Are you sure you would like to cancel?",
                    icon: "warning",
                    showCancelButton: true,
                    buttonsStyling: false,
                    confirmButtonText: "Yes, cancel it!",
                    cancelButtonText: "No, return",
                    customClass: {
                        confirmButton: "btn btn-primary",
                        cancelButton: "btn btn-active-light"
                    }
                }).then(function (result) {
                    if (result.value) {
                        $.PaymentMethod.form.reset(); // Reset form
                        $.PaymentMethod.modal.hide(); // Hide modal
                    }
                });
            })
        }

        $.PaymentMethod = new PaymentMethod
        $.PaymentMethod.Constructor = PaymentMethod
    }(window.jQuery),

    function($) {
        "use strict"

        $.PaymentMethod.init();
    }(window.jQuery)
});
