$(document).on('click', '.button-ajax', function (e) {
    e.preventDefault();
    var action = $(this).data('action');
    var method = $(this).data('method');
    var csrf = $(this).data('csrf');
    var reload = $(this).data('reload');

    $.ajax({
        url: action,
        method: method,
        data: {
            _token: csrf
        },
        success: function(response) {
            console.log(response);
        },
        error: function(xhr) {
            const response = JSON.parse(xhr.responseText);
            console.log(response);
        },
        complete: function() {
            if (reload) {
                window.location.reload();
            }
        }
    })
});
