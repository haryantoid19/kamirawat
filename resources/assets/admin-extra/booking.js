jQuery(document).ready(function() {

    !function () {
        "use strict"
    },

    function ($) {

        const Booking = function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        }

        Booking.prototype.handleCancelBooking = function () {
            $(document.body).on('click', '#btn-cancel-booking', function(e) {
                e.preventDefault();

                Swal.fire({
                    text: "Are you sure you would like to cancel?",
                    icon: "warning",
                    showCancelButton: true,
                    buttonsStyling: false,
                    confirmButtonText: "Yes, cancel it!",
                    cancelButtonText: "No",
                    customClass: {
                        confirmButton: "btn btn-primary",
                        cancelButton: "btn btn-active-light"
                    }
                }).then(function (result) {
                    if (result.isConfirmed) {
                        $('#form-cancel-booking').submit();
                    }
                });
            })
        }

        Booking.prototype.init = function() {
            $.Booking.handleCancelBooking();
        }

        $.Booking = new Booking
        $.Booking.Constructor = Booking

    }(window.jQuery),

    function($) {
        "use strict"

        $.Booking.init();

    }(window.jQuery)
})
