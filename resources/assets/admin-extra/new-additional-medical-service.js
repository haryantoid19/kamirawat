jQuery(document).ready(function() {
    !function () {
        "use strict"
    },

    function ($) {
        'use strict'

        const NewAdditionalMedicalService = function () {
            // Elements
            const modalEl = document.querySelector('#kr_new_additional_medical_service_modal');

            if (!modalEl) {
                return;
            }

            this.modal = new bootstrap.Modal(modalEl);
            this.form = document.querySelector('#kr_new_additional_medical_service_form');
            this.submitButton = document.getElementById('kr_modal_new_additional_medical_service_submit');
            this.cancelButton = document.getElementById('kr_modal_new_additional_medical_service_cancel');
            this.isEdit = false;
            this.edit_new_additional_medical_url = null;

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        }

        NewAdditionalMedicalService.prototype.init = () => {
            $.NewAdditionalMedicalService.handleForm();
            $.NewAdditionalMedicalService.editForm();
        }

        NewAdditionalMedicalService.prototype.editForm = function() {
            $(document.body).on('click', '[data-edit]', function() {
                const url = $(this).data('edit');
                $.NewAdditionalMedicalService.edit_new_additional_medical_url = url;

                $.NewAdditionalMedicalService.modal.show();
                $.get(url, function({medical_service: {name, price}}) {
                    $('[name="name"]').val(name);
                    $('[name="price"]').val(price);
                    $('input[name="method"]').val('edit');
                });
            });
        }

        NewAdditionalMedicalService.prototype.handleForm = function() {
            document.getElementById('kr_new_additional_medical_service_modal').addEventListener('hidden.bs.modal', function() {
                $('input[name="method"]').val('new');
                $(this).find('input[name="name"]').val('');
                $(this).find('input[name="price"]').val('');
            });

            // Init form validation rules. For more info check the FormValidation plugin's official documentation:https://formvalidation.io/
            const validator = FormValidation.formValidation(
                this.form,
                {
                    fields: {
                        name: {
                            validators: {
                                notEmpty: {
                                    message: 'The name is required'
                                }
                            }
                        },
                        price: {
                            validators: {
                                notEmpty: {
                                    message: 'THe price is required'
                                },
                                numeric: {
                                    message: 'The price must be numeric',
                                },
                                between: {
                                    min: 1,
                                    max: 100000000,
                                    message: 'The price must be between 1 - 1000000000',
                                }
                            }
                        },
                    },
                    plugins: {
                        trigger: new FormValidation.plugins.Trigger(),
                        bootstrap: new FormValidation.plugins.Bootstrap5({
                            rowSelector: '.fv-row',
                            eleInvalidClass: '',
                            eleValidClass: ''
                        })
                    }
                }
            );

            this.submitButton.addEventListener('click', function (e) {
                e.preventDefault();

                // Validate form before submit
                if (validator) {
                    validator.validate().then(function (status) {
                        if (status === 'Valid') {
                            $.NewAdditionalMedicalService.submitButton.setAttribute('data-kt-indicator', 'on');

                            // Disable button to avoid multiple click
                            $.NewAdditionalMedicalService.submitButton.disabled = true;

                            let url = window.new_additional_medical_service_url;
                            const method = $('input[name="method"]').val();
                            if (method === "edit") {
                                url = $.NewAdditionalMedicalService.edit_new_additional_medical_url;
                            }

                            const payload = {
                                name: $('input[name="name"]').val(),
                                price: $('input[name="price"]').val(),
                                _method: method === "edit" ? "PUT" : "POST"
                            };
                            $.post(url, payload, function() {
                                // Show success message. For more info check the plugin's official documentation: https://sweetalert2.github.io/
                                Swal.fire({
                                    text: "Form has been successfully saved!",
                                    icon: "success",
                                    buttonsStyling: false,
                                    confirmButtonText: "Ok, got it!",
                                    customClass: {
                                        confirmButton: "btn btn-primary"
                                    }
                                }).then(function (result) {
                                    if (result.isConfirmed) {
                                        $.NewAdditionalMedicalService.modal.hide();
                                        LaravelDataTables["additional-medical-service-table"].ajax.reload();
                                        $('input[name="title"]').val('');
                                    }
                                });
                            }).fail(function() {
                                Swal.fire({
                                    text: "Sorry, looks like there are some errors detected, please try again.",
                                    icon: "error",
                                    buttonsStyling: false,
                                    confirmButtonText: "Ok, got it!",
                                    customClass: {
                                        confirmButton: "btn btn-primary"
                                    }
                                });
                            }).always(function() {
                                $.NewAdditionalMedicalService.submitButton.removeAttribute('data-kt-indicator');

                                // Enable button
                                $.NewAdditionalMedicalService.submitButton.disabled = false;
                            });
                        } else {
                            // Show error message.
                            Swal.fire({
                                text: "Sorry, looks like there are some errors detected, please try again.",
                                icon: "error",
                                buttonsStyling: false,
                                confirmButtonText: "Ok, got it!",
                                customClass: {
                                    confirmButton: "btn btn-primary"
                                }
                            });
                        }
                    });
                }
            });
            this.cancelButton.addEventListener('click', function(e) {
                e.preventDefault();

                Swal.fire({
                    text: "Are you sure you would like to cancel?",
                    icon: "warning",
                    showCancelButton: true,
                    buttonsStyling: false,
                    confirmButtonText: "Yes, cancel it!",
                    cancelButtonText: "No, return",
                    customClass: {
                        confirmButton: "btn btn-primary",
                        cancelButton: "btn btn-active-light"
                    }
                }).then(function (result) {
                    if (result.value) {
                        $.NewAdditionalMedicalService.form.reset(); // Reset form
                        $.NewAdditionalMedicalService.modal.hide(); // Hide modal
                    } else if (result.dismiss === 'cancel') {
                        Swal.fire({
                            text: "Your form has not been cancelled!.",
                            icon: "error",
                            buttonsStyling: false,
                            confirmButtonText: "Ok, got it!",
                            customClass: {
                                confirmButton: "btn btn-primary",
                            }
                        });
                    }
                });
            })
        }

        $.NewAdditionalMedicalService = new NewAdditionalMedicalService
        $.NewAdditionalMedicalService.Constructor = NewAdditionalMedicalService
    }(window.jQuery),

    function($) {
        "use strict"

        if (window.new_additional_medical_service === true) {
            $.NewAdditionalMedicalService.init();
        }
    }(window.jQuery)
});
