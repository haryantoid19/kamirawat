jQuery(document).ready(function() {
    !function () {
        "use strict"
    },

    function ($) {
        'use strict'

        const NewProfession = function () {
            // Elements
            const modalEl = document.querySelector('#kr_new_profession_modal');

            if (!modalEl) {
                return;
            }

            this.modal = new bootstrap.Modal(modalEl);
            this.form = document.querySelector('#kr_new_profession_form');
            this.submitButton = document.getElementById('kr_modal_new_profession_submit');
            this.cancelButton = document.getElementById('kr_modal_new_profession_cancel');
            this.isEdit = false;
            this.edit_profession_url = null;

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        }

        NewProfession.prototype.init = () => {
            $.NewProfession.handleForm();
            $.NewProfession.editForm();
        }

        NewProfession.prototype.editForm = function() {
            $(document.body).on('click', '[data-edit]', function() {
                const url = $(this).data('edit');
                $.NewProfession.edit_profession_url = url;

                $.NewProfession.modal.show();
                $.get(url, function({profession: {title}}) {
                    $('[name="title"]').val(title);
                    $('input[name="method"]').val('edit');
                });
            });
        }

        NewProfession.prototype.handleForm = function() {
            document.getElementById('kr_new_profession_modal').addEventListener('hidden.bs.modal', function() {
                $('input[name="method"]').val('new');
            });

            // Init form validation rules. For more info check the FormValidation plugin's official documentation:https://formvalidation.io/
            const validator = FormValidation.formValidation(
                this.form,
                {
                    fields: {
                        title: {
                            validators: {
                                notEmpty: {
                                    message: 'Profession title is required'
                                }
                            }
                        },
                    },
                    plugins: {
                        trigger: new FormValidation.plugins.Trigger(),
                        bootstrap: new FormValidation.plugins.Bootstrap5({
                            rowSelector: '.fv-row',
                            eleInvalidClass: '',
                            eleValidClass: ''
                        })
                    }
                }
            );

            this.submitButton.addEventListener('click', function (e) {
                e.preventDefault();

                // Validate form before submit
                if (validator) {
                    validator.validate().then(function (status) {
                        if (status === 'Valid') {
                            $.NewProfession.submitButton.setAttribute('data-kt-indicator', 'on');

                            // Disable button to avoid multiple click
                            $.NewProfession.submitButton.disabled = true;

                            let url = window.new_profession_url;
                            const method = $('input[name="method"]').val();
                            if (method === "edit") {
                                url = $.NewProfession.edit_profession_url;
                            }

                            const payload = {
                                title: $('input[name="title"]').val(),
                                _method: method === "edit" ? "PUT" : "POST"
                            };
                            $.post(url, payload, function() {
                                // Show success message. For more info check the plugin's official documentation: https://sweetalert2.github.io/
                                Swal.fire({
                                    text: "Form has been successfully submitted!",
                                    icon: "success",
                                    buttonsStyling: false,
                                    confirmButtonText: "Ok, got it!",
                                    customClass: {
                                        confirmButton: "btn btn-primary"
                                    }
                                }).then(function (result) {
                                    if (result.isConfirmed) {
                                        $.NewProfession.modal.hide();
                                        LaravelDataTables["profession-table"].ajax.reload();
                                        $('input[name="title"]').val('');
                                    }
                                });
                            }).fail(function() {
                                Swal.fire({
                                    text: "Sorry, looks like there are some errors detected, please try again.",
                                    icon: "error",
                                    buttonsStyling: false,
                                    confirmButtonText: "Ok, got it!",
                                    customClass: {
                                        confirmButton: "btn btn-primary"
                                    }
                                });
                            }).always(function() {
                                $.NewProfession.submitButton.removeAttribute('data-kt-indicator');

                                // Enable button
                                $.NewProfession.submitButton.disabled = false;
                            });
                        } else {
                            // Show error message.
                            Swal.fire({
                                text: "Sorry, looks like there are some errors detected, please try again.",
                                icon: "error",
                                buttonsStyling: false,
                                confirmButtonText: "Ok, got it!",
                                customClass: {
                                    confirmButton: "btn btn-primary"
                                }
                            });
                        }
                    });
                }
            });
            this.cancelButton.addEventListener('click', function(e) {
                e.preventDefault();

                Swal.fire({
                    text: "Are you sure you would like to cancel?",
                    icon: "warning",
                    showCancelButton: true,
                    buttonsStyling: false,
                    confirmButtonText: "Yes, cancel it!",
                    cancelButtonText: "No, return",
                    customClass: {
                        confirmButton: "btn btn-primary",
                        cancelButton: "btn btn-active-light"
                    }
                }).then(function (result) {
                    if (result.value) {
                        $.NewProfession.form.reset(); // Reset form
                        $.NewProfession.modal.hide(); // Hide modal
                    } else if (result.dismiss === 'cancel') {
                        Swal.fire({
                            text: "Your form has not been cancelled!.",
                            icon: "error",
                            buttonsStyling: false,
                            confirmButtonText: "Ok, got it!",
                            customClass: {
                                confirmButton: "btn btn-primary",
                            }
                        });
                    }
                });
            })
        }

        $.NewProfession = new NewProfession
        $.NewProfession.Constructor = NewProfession
    }(window.jQuery),

    function($) {
        "use strict"

        if (window.new_profession === true) {
            $.NewProfession.init();
        }
    }(window.jQuery)
});
