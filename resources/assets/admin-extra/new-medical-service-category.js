jQuery(document).ready(function() {
    !function () {
        "use strict"
    },

    function ($) {
        'use strict'

        const MedicalServiceCategory = function () {
            // Elements
            const modalEl = document.querySelector('#kr_new_medical_service_category_modal');

            if (!modalEl) {
                return;
            }

            this.modal = new bootstrap.Modal(modalEl);
            this.form = document.querySelector('#kr_new_medical_service_category_form');
            this.submitButton = document.getElementById('kr_modal_new_medical_service_category_submit');
            this.cancelButton = document.getElementById('kr_modal_new_medical_service_category_cancel');
            this.isEdit = false;
            this.edit_new_medical_url = null;

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        }

        MedicalServiceCategory.prototype.init = () => {
            $.MedicalServiceCategory.handleForm();
            $.MedicalServiceCategory.editForm();
        }

        MedicalServiceCategory.prototype.editForm = function() {
            $(document.body).on('click', '[data-edit]', function() {
                const url = $(this).data('edit');
                $.MedicalServiceCategory.edit_new_medical_url = url;

                $.MedicalServiceCategory.modal.show();
                $.get(url, function({medical_service_category: {name, type}}) {
                    $('[name="name"]').val(name);
                    $('select[name="type"]').val(type);
                    $('input[name="method"]').val('edit');
                });
            });
        }

        MedicalServiceCategory.prototype.handleForm = function() {
            document.getElementById('kr_new_medical_service_category_modal').addEventListener('hidden.bs.modal', function() {
                $('input[name="method"]').val('new');
            });

            const validator = FormValidation.formValidation(
                this.form,
                {
                    fields: {
                        name: {
                            validators: {
                                notEmpty: {
                                    message: 'Medical service name is required'
                                }
                            }
                        },
                        type: {
                            validators: {
                                notEmpty: {
                                    message: 'Medical service type is required'
                                }
                            }
                        },
                    },
                    plugins: {
                        trigger: new FormValidation.plugins.Trigger(),
                        bootstrap: new FormValidation.plugins.Bootstrap5({
                            rowSelector: '.fv-row',
                            eleInvalidClass: '',
                            eleValidClass: ''
                        })
                    }
                }
            );

            this.submitButton.addEventListener('click', function (e) {
                e.preventDefault();

                // Validate form before submit
                if (validator) {
                    validator.validate().then(function (status) {
                        if (status !== "Valid") {
                            // Show error message.
                            Swal.fire({
                                text: "Sorry, looks like there are some errors detected, please try again.",
                                icon: "error",
                                buttonsStyling: false,
                                confirmButtonText: "Ok, got it!",
                                customClass: {
                                    confirmButton: "btn btn-primary"
                                }
                            });

                            return false;
                        }

                        $.MedicalServiceCategory.submitButton.setAttribute('data-kt-indicator', 'on');

                        // Disable button to avoid multiple click
                        $.MedicalServiceCategory.submitButton.disabled = true;

                        let url = window.new_medical_service_category_url;
                        const method = $('input[name="method"]').val();
                        const formData = new FormData();

                        if (method === "edit") {
                            url = $.MedicalServiceCategory.edit_new_medical_url;
                            formData.append('_method', 'PUT');
                        }

                        formData.append('name', $('input[name="name"]').val());
                        formData.append('type', $('select[name="type"]').val());

                        $.ajax({
                            url,
                            method: "POST",
                            data: formData,
                            contentType: false,
                            processData: false,
                            dataType: 'json',
                            success: function() {
                                // Show success message. For more info check the plugin's official documentation: https://sweetalert2.github.io/
                                Swal.fire({
                                    text: "Form has been successfully submitted!",
                                    icon: "success",
                                    buttonsStyling: false,
                                    confirmButtonText: "Ok, got it!",
                                    customClass: {
                                        confirmButton: "btn btn-primary"
                                    }
                                }).then(function (result) {
                                    if (result.isConfirmed) {
                                        $.MedicalServiceCategory.form.reset(); // Reset form
                                        $.MedicalServiceCategory.modal.hide();
                                        LaravelDataTables["medical-service-category-table"].ajax.reload();
                                        $('input[name="name"]').val('');
                                    }
                                });
                            },
                            error: function() {
                                Swal.fire({
                                    text: "Sorry, looks like there are some errors detected, please try again.",
                                    icon: "error",
                                    buttonsStyling: false,
                                    confirmButtonText: "Ok, got it!",
                                    customClass: {
                                        confirmButton: "btn btn-primary"
                                    }
                                });
                            },
                            complete: function() {
                                $.MedicalServiceCategory.submitButton.removeAttribute('data-kt-indicator');

                                // Enable button
                                $.MedicalServiceCategory.submitButton.disabled = false;
                            }
                        });

                    });
                }
            });
            this.cancelButton.addEventListener('click', function(e) {
                e.preventDefault();

                Swal.fire({
                    text: "Are you sure you would like to cancel?",
                    icon: "warning",
                    showCancelButton: true,
                    buttonsStyling: false,
                    confirmButtonText: "Yes, cancel it!",
                    cancelButtonText: "No, return",
                    customClass: {
                        confirmButton: "btn btn-primary",
                        cancelButton: "btn btn-active-light"
                    }
                }).then(function (result) {
                    if (result.value) {
                        $.MedicalServiceCategory.form.reset(); // Reset form
                        $.MedicalServiceCategory.modal.hide(); // Hide modal
                    }
                });
            })
        }

        $.MedicalServiceCategory = new MedicalServiceCategory
        $.MedicalServiceCategory.Constructor = MedicalServiceCategory
    }(window.jQuery),

    function($) {
        "use strict"

        if (window.new_medical_service_category === true) {
            $.MedicalServiceCategory.init();
        }
    }(window.jQuery)
});
