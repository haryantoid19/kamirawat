jQuery(document).ready(function() {
    !function () {
        "use strict"
    },

    function ($) {
        'use strict'

        const NewMedicalService = function () {
            // Elements
            const modalEl = document.querySelector('#kr_new_medical_service_modal');

            if (!modalEl) {
                return;
            }

            this.modal = new bootstrap.Modal(modalEl);
            this.form = document.querySelector('#kr_new_medical_service_form');
            this.submitButton = document.getElementById('kr_modal_new_medical_service_submit');
            this.cancelButton = document.getElementById('kr_modal_new_medical_service_cancel');
            this.isEdit = false;
            this.edit_new_medical_url = null;

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        }

        NewMedicalService.prototype.init = () => {
            $.NewMedicalService.handleForm();
            $.NewMedicalService.editForm();
        }

        NewMedicalService.prototype.editForm = function() {
            $(document.body).on('click', '[data-edit]', function() {
                const url = $(this).data('edit');
                $.NewMedicalService.edit_new_medical_url = url;

                $.NewMedicalService.modal.show();
                $.get(url, function({medical_service: {name, price, category_id, description, icon_url}}) {
                    $('[name="name"]').val(name);
                    $('[name="price"]').val(price);
                    $('[name="category"]').val(category_id);
                    $('[name="description"]').val(description);
                    $('#kr_new_medical_service_form .image-input-wrapper').css('background-image', `url(${icon_url})`);
                    $('input[name="method"]').val('edit');
                });
            });
        }

        NewMedicalService.prototype.handleForm = function() {
            document.getElementById('kr_new_medical_service_modal').addEventListener('hidden.bs.modal', function() {
                $('input[name="method"]').val('new');
            });

            const validator = FormValidation.formValidation(
                this.form,
                {
                    fields: {
                        name: {
                            validators: {
                                notEmpty: {
                                    message: 'Medical service name is required'
                                }
                            }
                        },
                        price: {
                            validators: {
                                notEmpty: {
                                    message: 'Medical service price is required'
                                }
                            }
                        },
                        category: {
                            validators: {
                                notEmpty: {
                                    message: 'Medical service price is required'
                                }
                            }
                        },
                        description: {
                            validators: {
                                notEmpty: {
                                    message: 'Description service price is required'
                                }
                            }
                        },
                    },
                    plugins: {
                        trigger: new FormValidation.plugins.Trigger(),
                        bootstrap: new FormValidation.plugins.Bootstrap5({
                            rowSelector: '.fv-row',
                            eleInvalidClass: '',
                            eleValidClass: ''
                        })
                    }
                }
            );

            this.submitButton.addEventListener('click', function (e) {
                e.preventDefault();

                // Validate form before submit
                if (validator) {
                    validator.validate().then(function (status) {
                        if (status !== "Valid") {
                            // Show error message.
                            Swal.fire({
                                text: "Sorry, looks like there are some errors detected, please try again.",
                                icon: "error",
                                buttonsStyling: false,
                                confirmButtonText: "Ok, got it!",
                                customClass: {
                                    confirmButton: "btn btn-primary"
                                }
                            });

                            return false;
                        }

                        $.NewMedicalService.submitButton.setAttribute('data-kt-indicator', 'on');

                        // Disable button to avoid multiple click
                        $.NewMedicalService.submitButton.disabled = true;

                        let url = window.new_medical_service_url;
                        const method = $('input[name="method"]').val();
                        const formData = new FormData();

                        if (method === "edit") {
                            url = $.NewMedicalService.edit_new_medical_url;
                            formData.append('_method', 'PUT');
                        }

                        formData.append('name', $('input[name="name"]').val());
                        formData.append('price', $('input[name="price"]').val());
                        formData.append('category', $('select[name="category"]').val());
                        formData.append('description', $('textarea[name="description"]').val());

                        const fileIcon = $('input[name="icon"]');
                        if (fileIcon.val() !== "") {
                            const icon = $('input[name="icon"]')[0].files[0];
                            formData.append('icon', icon);
                        }

                        $.ajax({
                            url,
                            method: "POST",
                            data: formData,
                            contentType: false,
                            processData: false,
                            dataType: 'json',
                            success: function() {
                                // Show success message. For more info check the plugin's official documentation: https://sweetalert2.github.io/
                                Swal.fire({
                                    text: "Form has been successfully submitted!",
                                    icon: "success",
                                    buttonsStyling: false,
                                    confirmButtonText: "Ok, got it!",
                                    customClass: {
                                        confirmButton: "btn btn-primary"
                                    }
                                }).then(function (result) {
                                    if (result.isConfirmed) {
                                        $.NewMedicalService.form.reset(); // Reset form
                                        $.NewMedicalService.modal.hide();
                                        LaravelDataTables["medical-service-table"].ajax.reload();
                                    }
                                });
                            },
                            error: function() {
                                Swal.fire({
                                    text: "Sorry, looks like there are some errors detected, please try again.",
                                    icon: "error",
                                    buttonsStyling: false,
                                    confirmButtonText: "Ok, got it!",
                                    customClass: {
                                        confirmButton: "btn btn-primary"
                                    }
                                });
                            },
                            complete: function() {
                                $.NewMedicalService.submitButton.removeAttribute('data-kt-indicator');

                                // Enable button
                                $.NewMedicalService.submitButton.disabled = false;
                            }
                        });

                    });
                }
            });
            this.cancelButton.addEventListener('click', function(e) {
                e.preventDefault();

                Swal.fire({
                    text: "Are you sure you would like to cancel?",
                    icon: "warning",
                    showCancelButton: true,
                    buttonsStyling: false,
                    confirmButtonText: "Yes, cancel it!",
                    cancelButtonText: "No, return",
                    customClass: {
                        confirmButton: "btn btn-primary",
                        cancelButton: "btn btn-active-light"
                    }
                }).then(function (result) {
                    if (result.value) {
                        $.NewMedicalService.form.reset(); // Reset form
                        $.NewMedicalService.modal.hide(); // Hide modal
                    }
                });
            })
        }

        $.NewMedicalService = new NewMedicalService
        $.NewMedicalService.Constructor = NewMedicalService
    }(window.jQuery),

    function($) {
        "use strict"

        if (window.new_medical_service === true) {
            $.NewMedicalService.init();
        }
    }(window.jQuery)
});
