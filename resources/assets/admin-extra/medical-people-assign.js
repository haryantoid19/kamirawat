jQuery(document).ready(function() {
    !function () {
        "use strict"
    },

    function ($) {
        'use strict'

        const MedicalPeopleAssign = function () {
            if (document.querySelector('#kt_modal_customer_search_handler')) {
                this.element = document.querySelector('#kt_modal_customer_search_handler');
                this.modal = new bootstrap.Modal(document.querySelector('#kt_modal_customer_search'));
                this.suggestionsElement = null;
                this.resultsElement = null;
                this.wrapperElement = null;
                this.emptyElement = null;
                this.searchObject = null;

                if (!this.element) {
                    return;
                }

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
            }
        }

        MedicalPeopleAssign.prototype.process = function (search) {
            const {booking_id, search_medical_people_url} = window;
            const url = `${search_medical_people_url}?booking=${booking_id}&search=${search.getQuery()}`;
            $.get(url, function({html, total}) {
                $('[data-kt-search-element="results"]').html(html);

                // Hide recently viewed
                $.MedicalPeopleAssign.suggestionsElement.classList.add('d-none');

                if (total === 0) {
                    // Hide results
                    $.MedicalPeopleAssign.resultsElement.classList.add('d-none');
                    // Show empty message
                    $.MedicalPeopleAssign.emptyElement.classList.remove('d-none');
                } else {
                    // Show results
                    $.MedicalPeopleAssign.resultsElement.classList.remove('d-none');
                    // Hide empty message
                    $.MedicalPeopleAssign.emptyElement.classList.add('d-none');
                }
            }).fail(function() {
                // Hide results
                $.MedicalPeopleAssign.resultsElement.classList.add('d-none');
                // Show empty message
                $.MedicalPeopleAssign.emptyElement.classList.remove('d-none');
            }).always(function() {
                search.complete();
            });
        }

        MedicalPeopleAssign.prototype.clear = function (search) {
            // Show recently viewed
            $.MedicalPeopleAssign.suggestionsElement.classList.remove('d-none');
            // Hide results
            $.MedicalPeopleAssign.resultsElement.classList.add('d-none');
            // Hide empty message
            $.MedicalPeopleAssign.emptyElement.classList.add('d-none');
        }

        MedicalPeopleAssign.prototype.init = function() {
            this.wrapperElement = this.element.querySelector('[data-kt-search-element="wrapper"]');
            this.suggestionsElement = this.element.querySelector('[data-kt-search-element="suggestions"]');
            this.resultsElement = this.element.querySelector('[data-kt-search-element="results"]');
            this.emptyElement = this.element.querySelector('[data-kt-search-element="empty"]');

            // Initialize search handler
            this.searchObject = new KTSearch(this.element, {
                minLength: 3,
            });

            // Search handler
            this.searchObject.on('kt.search.process', this.process);

            // Clear handler
            this.searchObject.on('kt.search.clear', this.clear);

            // Handle select
            KTUtil.on(this.element, '[data-kt-search-element="medical-people"]', 'click', function() {
                if (confirm('Are you sure ?')) {
                    $(this).find('form').submit();
                }
                // $.MedicalPeopleAssign.modal.hide();
            });

        }

        $.MedicalPeopleAssign = new MedicalPeopleAssign
        $.MedicalPeopleAssign.Constructor = MedicalPeopleAssign
    }(window.jQuery),

    function($) {
        "use strict"

        if (document.querySelector('#kt_modal_customer_search_handler')) {
            $.MedicalPeopleAssign.init();
        }

    }(window.jQuery)
});
