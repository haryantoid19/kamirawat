<?php

use App\Http\Controllers\Account\SettingsController;
use App\Http\Controllers\Auth\AuthenticatedSessionController;
use App\Http\Controllers\Logs\AuditLogsController;
use App\Http\Controllers\Logs\SystemLogsController;
use App\Http\Controllers\PagesController;
use App\Http\Controllers\Theteam\AdditionalMedicalServiceController;
use App\Http\Controllers\Theteam\AssignMedicalPeopleBookingController;
use App\Http\Controllers\Theteam\BookingCancelController;
use App\Http\Controllers\Theteam\BookingController;
use App\Http\Controllers\Theteam\ClinicBookingController;
use App\Http\Controllers\Theteam\InvoiceMarkAsPaidController;
use App\Http\Controllers\Theteam\MedicalPeopleApprovalController;
use App\Http\Controllers\Theteam\MedicalPeopleController;
use App\Http\Controllers\Theteam\MedicalServiceController;
use App\Http\Controllers\Theteam\PatientController;
use App\Http\Controllers\Theteam\PaymentMethodController;
use App\Http\Controllers\Theteam\ProfessionController;
use App\Http\Controllers\Theteam\SearchMedicalPeopleController;
use App\Http\Controllers\Theteam\UsersController;
use App\Http\Controllers\Theteam\UserUpdateEmailController;
use App\Http\Controllers\Theteam\UserUpdatePasswordController;

Route::prefix('theteam')->name('theteam.')->group(function() {
    Route::get('/login', [AuthenticatedSessionController::class, 'create'])->middleware('guest:theteam')->name('login');
    Route::post('/login', [AuthenticatedSessionController::class, 'store'])->middleware('guest:theteam');
    Route::post('/logout', [AuthenticatedSessionController::class, 'destroy'])->middleware('auth:theteam')->name('logout');
});

Route::prefix('theteam')->middleware('auth:theteam')->group(function() {
    Route::get('/', [PagesController::class, 'index'])->name('dashboard');

    $menu = theme()->getMenu();
    array_walk($menu, function ($val) {
        if (isset($val['path'])) {
            Route::get($val['path'], [PagesController::class, 'index']);
        }
    });

    // Account pages
    Route::prefix('account')->group(function () {
        Route::get('settings', [SettingsController::class, 'index'])->name('settings.index');
        Route::put('settings', [SettingsController::class, 'update'])->name('settings.update');
        Route::put('settings/email', [SettingsController::class, 'changeEmail'])->name('settings.changeEmail');
        Route::put('settings/password', [SettingsController::class, 'changePassword'])->name('settings.changePassword');
    });

    // Medical People
    Route::get('medical-people/search', [SearchMedicalPeopleController::class, 'index'])->name('medical_people.search')->middleware('only.ajax');
    Route::get('medical-people/status/{status}', [MedicalPeopleController::class, 'index']);
    Route::put('medical-people/{medicalPeople}/approval', [MedicalPeopleApprovalController::class, 'update'])->name('medical_people.approval');
    Route::get('medical-people', [MedicalPeopleController::class, 'index'])->name('medical-people.index')->middleware('only.ajax');
    Route::get('medical-people/{medicalPeople}', [MedicalPeopleController::class, 'show'])->name('medical-people.show');

    // Bookings
    Route::put('/bookings/invoice/{invoice}/mark-as-paid', [InvoiceMarkAsPaidController::class, 'update'])->name('bookings.invoice.mark-as-paid');
    Route::put('/bookings/{booking}/cancel', [BookingCancelController::class, 'update'])->name('bookings.cancel');
    Route::post('/bookings/{booking}/assign-medical-people/{medicalPeople}', [AssignMedicalPeopleBookingController::class, 'store'])->name('bookings.assign.medical-people');
    Route::get('/bookings/category/{medicalServiceCategory}', [BookingController::class, 'index'])->name('bookings.index');
    Route::get('/bookings/{booking}', [BookingController::class, 'show'])->name('bookings.show');
    Route::put('/bookings/{booking}', [BookingController::class, 'update'])->name('bookings.update');
    Route::delete('/bookings/{booking}', [BookingController::class, 'destroy'])->name('bookings.destroy');

    // Clinic Bookings
    Route::get('/clinic-bookings/category/{medicalServiceCategory}', [ClinicBookingController::class, 'index'])->name('clinic-bookings.index');
    Route::get('/clinic-bookings/{clinicBooking}', [ClinicBookingController::class, 'show'])->name('clinic-bookings.show');
    Route::put('/clinic-bookings/{clinicBooking}', [ClinicBookingController::class, 'update'])->name('clinic-bookings.update');
    // Patient
    Route::resource('patients', PatientController::class)->only(['index', 'show']);

    // Medical Service
    Route::resource('medical-service-categories', \App\Http\Controllers\Theteam\MedicalServiceCategoryController::class)->only(['index', 'store', 'show', 'update', 'destroy']);

    // Medical Service
    Route::resource('medical-services', MedicalServiceController::class)->only(['index', 'store', 'show', 'update', 'destroy']);

    // Additional Medical Service
    Route::resource('additional-medical-services', AdditionalMedicalServiceController::class)->only(['index', 'store', 'show', 'update', 'destroy']);

    // Profession
    Route::resource('professions', ProfessionController::class)->only(['index', 'store', 'show', 'update', 'destroy']);

    // Payment Method
    Route::get('payment-methods', [PaymentMethodController::class, 'index'])->name('payment-methods.index');
    Route::post('payment-methods', [PaymentMethodController::class, 'store'])->name('payment-methods.store');
    Route::get('payment-methods/{paymentMethod}', [PaymentMethodController::class, 'show'])->name('payment-methods.show');
    Route::put('payment-methods/{paymentMethod}', [PaymentMethodController::class, 'update'])->name('payment-methods.update');
    Route::delete('payment-methods/{paymentMethod}', [PaymentMethodController::class, 'destroy'])->name('payment-methods.destroy');

    // Logs pages
    Route::prefix('log')->name('log.')->group(function () {
        Route::resource('system', SystemLogsController::class)->only(['index', 'destroy']);
        Route::resource('audit', AuditLogsController::class)->only(['index', 'destroy']);
    });

    Route::put('users/update-email/{user}', [UserUpdateEmailController::class, 'update'])->name('users.update-email');
    Route::put('users/update-password/{user}', [UserUpdatePasswordController::class, 'update'])->name('users.update-password');
    Route::get('users/view/{user}', [UsersController::class, 'show'])->name('users.show');
    Route::get('users/edit/{user}', [UsersController::class, 'edit'])->name('users.edit');
    Route::resource('users', UsersController::class)->only(['index', 'destroy']);
});
