<?php

use App\Http\Controllers\Auth\Partner\AuthenticatedSessionController as PartnerAuthenticatedSessionController;
use App\Http\Controllers\Auth\Partner\RegisterController;
use App\Http\Controllers\Auth\Partner\RegisterSuccessController;
use App\Http\Controllers\Auth\Patient\AuthenticatedSessionController as PatientAuthenticatedSessionController;
use App\Http\Controllers\Auth\Patient\RegisterController as PatientRegisterController;
use App\Http\Controllers\Auth\Patient\RegisterSuccessController as PatientRegisterSuccessController;
use App\Http\Controllers\Front\ChangeEmailController;
use App\Http\Controllers\Front\Partner\BankAccountController;
use App\Http\Controllers\Front\Partner\BookingPasscodeController;
use App\Http\Controllers\Front\Partner\DashboardController as PartnerDashboardController;
use App\Http\Controllers\Front\Partner\MapOrderController;
use App\Http\Controllers\Front\Partner\OrderCompletionController;
use App\Http\Controllers\Front\Partner\OrderController;
use App\Http\Controllers\Front\Partner\PaymentCheckOrderController;
use App\Http\Controllers\Front\Partner\PopupAdditionalServicesOrderController;
use App\Http\Controllers\Front\Partner\PopupConfirmationAdditionalServiceOrderController;
use App\Http\Controllers\Front\Partner\PopupHandlingOrderController;
use App\Http\Controllers\Front\Partner\PopupMedicalRecordOrderController;
use App\Http\Controllers\Front\Partner\ProfileController as ProfilePartnerController;
use App\Http\Controllers\Front\Partner\UpdateAvatarController;
use App\Http\Controllers\Front\Partner\WaitingVerification;
use App\Http\Controllers\Front\Patient\BookingCancelController;
use App\Http\Controllers\Front\Patient\BookingController;
use App\Http\Controllers\Front\Patient\BookingInvoiceTableController;
use App\Http\Controllers\Front\Patient\BookingSuccessController;
use App\Http\Controllers\Front\Patient\ClinicServiceController;
use App\Http\Controllers\Front\Patient\ClinicServiceSuccessController;
use App\Http\Controllers\Front\Patient\DashboardController;
use App\Http\Controllers\Front\Patient\MedicalRecordController;
use App\Http\Controllers\Front\Patient\ProfileController;
use App\Http\Controllers\Front\Patient\UpdateAvatarController as UpdateAvatarPatientController;
use App\Http\Controllers\Front\PostalCodeController;
use App\Http\Controllers\FrontController;
use App\Http\Controllers\XenditCallbackController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::name('front.')->group(function() {
    Route::get('/', [FrontController::class, 'index'])->name('index');
    Route::get('/syaratdanketentuan', function () {
        return view('front.syaratdanketentuan');
    })->name('syaratdanketentuan');

    Route::get('/faq', function () {
        return view('front.faq');
    })->name('faq');

    Route::get('/tentang-kami', function () {
        return view('front.tentangkami');
    })->name('tentangkami');


    //Layanan Online Page
    Route::get('/home-visit', function () { return view('front.homevisit'); })->name('home-visit');
    Route::get('/home-care', function () { return view('front.homecare'); })->name('home-care');
    Route::get('/home-covid', function () { return view('front.homecovid'); })->name('home-covid');
    Route::get('/vanilla-clinic', function () { return view('front.vanillaclinic'); })->name('vanilla-clinic');


    //Klinik Kamirawat
    Route::get('/konsultasi-dokter', function () { return view('front.konsultasidokter'); })->name('konsultasi-dokter');
    Route::get('/klinik-diabetes', function () { return view('front.klinikdiabetes'); })->name('klinik-diabetes');
    Route::get('/calm-clinic', function () { return view('front.calmclinic'); })->name('calm-clinic');

    Route::get('/promo', function () { return view('front.promo'); })->name('promo');
    Route::get('/blog', function () { return view('front.blog'); })->name('blog');



    Route::post('/postal-code', [PostalCodeController::class, 'index'])->middleware('guest')->name('postal-code');
    Route::post('/change-email', [ChangeEmailController::class, 'store'])->name('change-email');
    Route::get('/change-email/confirm', [ChangeEmailController::class, 'update'])->name('confirm-change-email');

    Route::prefix('patient')->name('patient.')->group(function() {
        Route::get('/login', [PatientAuthenticatedSessionController::class, 'create'])->middleware('guest')->name('login');
        Route::post('/login', [PatientAuthenticatedSessionController::class, 'store'])->middleware('guest');
        Route::post('/logout', [PatientAuthenticatedSessionController::class, 'destroy'])->middleware('auth')->name('logout');
        Route::get('/register', [PatientRegisterController::class, 'create'])->middleware('guest')->name('register');
        Route::post('/register', [PatientRegisterController::class, 'store'])->middleware('guest');
        Route::get('/success', [PatientRegisterSuccessController::class, 'index'])->name('register.success');

        Route::middleware(['auth', 'verified'])->group(function() {
            Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');
            Route::put('/profile/avatar', [UpdateAvatarPatientController::class, 'update'])->name('dashboard.update-avatar');
            Route::put('/profile', [ProfileController::class, 'update'])->name('profile.update');
            Route::put('/profile/email', [ProfileController::class, 'update_email'])->name('profile.update-email');

            Route::get('/bookings', [BookingController::class, 'index'])->name('booking.history');
            Route::put('/booking/cancel/{booking}', [BookingCancelController::class, 'update'])->name('booking.cancel');
            Route::get('/booking/success', [BookingSuccessController::class, 'index'])->name('booking.success');
            Route::get('/booking/{medicalServiceCategory}', [BookingController::class, 'create'])->name('booking.create');
            Route::post('/booking/{medicalServiceCategory}', [BookingController::class, 'store'])->name('booking.store');
            Route::get('/booking/invoice-table/{medicalServiceCategory}', [BookingInvoiceTableController::class, 'index'])->name('booking.invoice-table');

            Route::get('/clinic-service/{medicalServiceCategory}', [ClinicServiceController::class, 'create'])->name('clinic-service.create');
            Route::post('/clinic-service/{medicalServiceCategory}', [ClinicServiceController::class, 'store'])->name('clinic-service.store');
            Route::get('/clinic-service/{medicalServiceCategory}/success', [ClinicServiceSuccessController::class, 'index'])->name('clinic-service.success');

            Route::get('/medical-records', [MedicalRecordController::class, 'index'])->name('medical-record.browse');
            Route::get('/medical-records/show', [MedicalRecordController::class, 'show'])->name('medical-record.show');
        });
    });

    Route::prefix('partner')->name('partner.')->group(function() {
        Route::get('/login', [PartnerAuthenticatedSessionController::class, 'create'])->middleware('guest')->name('login');
        Route::post('/login', [PartnerAuthenticatedSessionController::class, 'store'])->middleware('guest');
        Route::get('/register', [RegisterController::class, 'create'])->name('register');
        Route::post('/register', [RegisterController::class, 'store']);
        Route::get('/success', [RegisterSuccessController::class, 'index'])->middleware('auth')->name('register.success');
        Route::post('/logout', [PartnerAuthenticatedSessionController::class, 'destroy'])->middleware('auth')->name('logout');
        Route::get('/waiting-verification', [WaitingVerification::class, 'index'])->middleware(['auth', 'verified'])->name('waiting-verification');

        Route::middleware(['auth', 'verified', 'medical_people_status_accepted'])->group(function() {
            Route::get('/dashboard', [PartnerDashboardController::class, 'index'])->name('dashboard');
            Route::put('/profile/avatar', [UpdateAvatarController::class, 'update'])->name('dashboard.update-avatar');
            Route::put('/profile', [ProfilePartnerController::class, 'update'])->name('profile.update');
            Route::put('/account-bank', [BankAccountController::class, 'update'])->name('profile.account-bank');

            Route::middleware(['only.ajax'])->group(function() {
                Route::get('/orders/view/{booking}', [OrderController::class, 'show'])->name('orders.show');
                Route::get('/orders/map/{booking}', [MapOrderController::class, 'show'])->name('orders.map');
                Route::get('/orders/payment-check/{invoice}', [PaymentCheckOrderController::class, 'show'])->name('orders.payment-check');
                Route::post('/orders/passcode-check/{booking}', [BookingPasscodeController::class, 'store'])->name('orders.passcode-check');

                Route::get('/orders/popup-handling/{booking}', [PopupHandlingOrderController::class, 'show'])->name('orders.popup-handling');
                Route::get('/orders/popup-medical-record/{booking}', [PopupMedicalRecordOrderController::class, 'show'])->name('orders.popup-medical-record');
                Route::get('/orders/popup-additional-services/{booking}', [PopupAdditionalServicesOrderController::class, 'show'])->name('orders.popup-additional-service');
                Route::post('/orders/popup-confirmation-additional-services/{booking}', [PopupConfirmationAdditionalServiceOrderController::class, 'show'])->name('orders.popup-confirmation-additional-service');
                Route::put('/orders/completion/{booking}', [OrderCompletionController::class, 'update'])->name('orders.completion');
            });

            Route::put('/orders/approval/{booking}', [OrderController::class, 'update'])->name('orders.approval');
            Route::get('/orders/{type}', [OrderController::class, 'index'])->name('orders');
        });
    });
});

Route::post('/xendit/virtual/callback', [XenditCallbackController::class, 'update'])
    ->middleware(['json', 'verify.xendit.callback']);

require __DIR__.'/theteam.php';
require __DIR__.'/auth.php';
