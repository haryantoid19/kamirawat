<?php

namespace Deployer;

require 'recipe/laravel.php';
require 'contrib/npm.php';

set('application', 'Kamirawat');
set('repository', 'git@github.com:aepnat/kamirawat.git');
set('branch', 'master');
set('writable_mode', 'chmod');
set('http_user', 'dev');
set('writable_use_sudo', '');

host('prod')
    ->set('labels', ['stage' => 'prod'])
    ->set('remote_user', 'dev')
    ->set('hostname', '5.181.217.144')
    ->set('port', 22)
    ->set('deploy_path', '/var/www/html/kamirawat')
    ->setForwardAgent(true)
    ->setSshArguments([
        '-o UserKnownHostsFile=/dev/null',
        '-o StrictHostKeyChecking=no',
        '-o PasswordAuthentication=no',
    ]);

host('staging')
    ->set('labels', ['stage' => 'prod'])
    ->set('remote_user', 'dev')
    ->set('hostname', '5.181.217.144')
    ->set('port', 22)
    ->set('deploy_path', '/var/www/html/kamirawat-staging')
    ->setForwardAgent(true)
    ->setSshArguments([
        '-o UserKnownHostsFile=/dev/null',
        '-o StrictHostKeyChecking=no',
        '-o PasswordAuthentication=no',
    ]);

after('deploy:vendors', 'npm:install');
after('deploy:vendors', 'npm:prod');
after('deploy:vendors', 'telescope:publish');

desc('Telescope Publish');
task('telescope:publish', artisan('telescope:publish'));

desc('Run Compiler');
task('npm:prod', function() {
    run("cd {{release_path}} && {{bin/npm}} run prod");
});

after('deploy:failed', 'deploy:unlock');
