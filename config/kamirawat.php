<?php

return [

    'shipping_fee' => env('SHIPPING_FEE', 50000),

    'under_construction_mode' => env("KAMIRAWAT_UNDER_CONSTRUCTION_MODE", true),

    /**
     * Payment method for additional service
     *
     * Silahkan diisi sesuai dengan column code yang ada di table payment_methods
     */
    'additional_service_payment_method' => env('ADDITIONAL_SERVICE_PAYMENT_METHOD', 'BANK_TRANSFER'),

    /**
     * Prefix
     *
     * Ini untuk mengatur prefix untuk generated number di setiap model yang dibutuhkan, ini digunakan untuk model yang
     * memiliki trait CanGenerateIdNumber
     */
    'prefix' => [

        \App\Models\Booking::class => env('PREFIX_BOOKING', 'BO'),

        \App\Models\ClinicBooking::class => env('PREFIX_CLINIC_BOOKING', 'CS'),

        \App\Models\Invoice::class => env('PREFIX_INVOICE', 'IN'),

        \App\Models\Patient::class => env('PREFIX_PATIENT', 'PA'),

        \App\Models\MedicalPeople::class => env('PREFIX_MEDICAL_PEOPLE', 'MP'),
    ],

];
