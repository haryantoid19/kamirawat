<?php

/**
 * Copyright (c) Vincent Klaiber.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 * @see https://github.com/vinkla/laravel-hashids
 */

return [

    /*
    |--------------------------------------------------------------------------
    | Default Connection Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the connections below you wish to use as
    | your default connection for all work. Of course, you may use many
    | connections at once using the manager class.
    |
    */

    'default' => 'main',

    /*
    |--------------------------------------------------------------------------
    | Hashids Connections
    |--------------------------------------------------------------------------
    |
    | Here are each of the connections setup for your application. Example
    | configuration has been included, but you may add as many connections as
    | you would like.
    |
    */

    'connections' => [

        'main' => [
            'salt' => 'M_zNNffj|cgN1i$5BZoWN/hqbS+w9}Bc&,~k-;y<IH+H}O80gSZELc$[w|2V]#M/',
            'length' => '12',
        ],

        'alternative' => [
            'salt' => 'o}--Ti?wKzGZL_|Ku&)hXUxTI`DfHQk0@n+$vsOouZn#2b 76Pm.=N|@u>23O,VD',
            'length' => '12',
        ],

    ],

];
