<?php
return array(
    'theteam' => array(
        'title'       => 'Dashboard',
        'description' => '',
        'view'        => 'theteam',
        'layout'      => array(
            'page-title' => array(
                'description' => true,
                'breadcrumb'  => false,
            ),
        ),
        'assets'      => array(
            'custom' => array(
                'js' => array(),
            ),
        ),

        'login'           => array(
            'title'  => 'Login',
            'assets' => array(
                'custom' => array(
                    'js' => array(
                        'js/custom/authentication/sign-in/general.js',
                    ),
                ),
            ),
            'layout' => array(
                'main' => array(
                    'type' => 'blank', // Set blank layout
                    'body' => array(
                        'class' => theme()->isDarkMode() ? '' : 'bg-body',
                    ),
                ),
            ),
        ),
        'register'        => array(
            'title'  => 'Register',
            'assets' => array(
                'custom' => array(
                    'js' => array(
                        'js/custom/authentication/sign-up/general.js',
                    ),
                ),
            ),
            'layout' => array(
                'main' => array(
                    'type' => 'blank', // Set blank layout
                    'body' => array(
                        'class' => theme()->isDarkMode() ? '' : 'bg-body',
                    ),
                ),
            ),
        ),
        'forgot-password' => array(
            'title'  => 'Forgot Password',
            'assets' => array(
                'custom' => array(
                    'js' => array(
                        'js/custom/authentication/password-reset/password-reset.js',
                    ),
                ),
            ),
            'layout' => array(
                'main' => array(
                    'type' => 'blank', // Set blank layout
                    'body' => array(
                        'class' => theme()->isDarkMode() ? '' : 'bg-body',
                    ),
                ),
            ),
        ),

        'bookings' => [
            '*' => [
                'title' => 'Bookings',
                'assets' => [
                    'custom' => [
                        'css' => [
                            'plugins/custom/datatables/datatables.bundle.css',
                        ],
                        'js' => [
                            'plugins/custom/datatables/datatables.bundle.js',
                        ]
                    ]
                ],
            ],
        ],

        'clinic-bookings' => [
            '*' => [
                'title' => 'Clinic Bookings',
                'assets' => [
                    'custom' => [
                        'css' => [
                            'plugins/custom/datatables/datatables.bundle.css',
                        ],
                        'js' => [
                            'plugins/custom/datatables/datatables.bundle.js',
                        ]
                    ]
                ],
            ]
        ],

        'medical-people' => [
            'title' => 'Medical People',
            '*' => array(
                'title' => 'Show Medical People',

                'edit' => array(
                    'title' => 'Edit Medical People',
                ),
            ),
            'status' => [
                'accepted' => [
                    'title' => 'Medical People',
                    'assets' => [
                        'custom' => [
                            'css' => [
                                'plugins/custom/datatables/datatables.bundle.css',
                            ],
                            'js' => [
                                'plugins/custom/datatables/datatables.bundle.js',
                            ]
                        ]
                    ],
                ],
                'pending' => [
                    'title' => 'Medical People',
                    'assets' => [
                        'custom' => [
                            'css' => [
                                'plugins/custom/datatables/datatables.bundle.css',
                            ],
                            'js' => [
                                'plugins/custom/datatables/datatables.bundle.js',
                            ]
                        ]
                    ],
                ],
                'rejected' => [
                    'title' => 'Medical People',
                    'assets' => [
                        'custom' => [
                            'css' => [
                                'plugins/custom/datatables/datatables.bundle.css',
                            ],
                            'js' => [
                                'plugins/custom/datatables/datatables.bundle.js',
                            ]
                        ]
                    ],
                ],
            ]
        ],

        'patients' => [
            'title' => 'Patients',
            'assets' => [
                'custom' => [
                    'css' => [
                        'plugins/custom/datatables/datatables.bundle.css',
                    ],
                    'js' => [
                        'plugins/custom/datatables/datatables.bundle.js',
                    ]
                ]
            ],
        ],

        'medical-service-categories' => [
            'title' => 'Medical Services Category',
            'create_modal_target' => '#kr_new_medical_service_category_modal',
            'create_tooltip' => 'Create a new medical service category',
            'assets' => [
                'custom' => [
                    'css' => [
                        'plugins/custom/datatables/datatables.bundle.css',
                    ],
                    'js' => [
                        'plugins/custom/datatables/datatables.bundle.js',
                    ]
                ]
            ],
        ],

        'medical-services' => [
            'title' => 'Medical Services',
            'create_modal_target' => '#kr_new_medical_service_modal',
            'create_tooltip' => 'Create a new medical service',
            'assets' => [
                'custom' => [
                    'css' => [
                        'plugins/custom/datatables/datatables.bundle.css',
                    ],
                    'js' => [
                        'plugins/custom/datatables/datatables.bundle.js',
                    ]
                ]
            ],
        ],

        'additional-medical-services' => [
            'title' => 'Additional Medical Services',
            'create_modal_target' => '#kr_new_additional_medical_service_modal',
            'create_tooltip' => 'Create a new additional medical service',
            'assets' => [
                'custom' => [
                    'css' => [
                        'plugins/custom/datatables/datatables.bundle.css',
                    ],
                    'js' => [
                        'plugins/custom/datatables/datatables.bundle.js',
                    ]
                ]
            ],
        ],

        'professions' => [
            'title' => 'Professions',
            'create_modal_target' => '#kr_new_profession_modal',
            'create_tooltip' => 'Create a new profession',
            'assets' => [
                'custom' => [
                    'css' => [
                        'plugins/custom/datatables/datatables.bundle.css',
                    ],
                    'js' => [
                        'plugins/custom/datatables/datatables.bundle.js',
                    ]
                ]
            ],
        ],

        'payment-methods' => [
            'title' => 'Payment Method',
            'create_modal_target' => '#kr_new_payment_method_modal',
            'create_tooltip' => 'Create a new payment method',
            'assets' => [
                'custom' => [
                    'css' => [
                        'plugins/custom/datatables/datatables.bundle.css',
                    ],
                    'js' => [
                        'plugins/custom/datatables/datatables.bundle.js',
                        'js/payment-method.js',
                    ]
                ]
            ],
        ],

        'log' => array(
            'audit'  => array(
                'title'  => 'Audit Log',
                'assets' => array(
                    'custom' => array(
                        'css' => array(
                            'plugins/custom/datatables/datatables.bundle.css',
                        ),
                        'js'  => array(
                            'plugins/custom/datatables/datatables.bundle.js',
                        ),
                    ),
                ),
            ),
            'system' => array(
                'title'  => 'System Log',
                'assets' => array(
                    'custom' => array(
                        'css' => array(
                            'plugins/custom/datatables/datatables.bundle.css',
                        ),
                        'js'  => array(
                            'plugins/custom/datatables/datatables.bundle.js',
                        ),
                    ),
                ),
            ),
        ),

        'account' => array(
            'overview' => array(
                'title'  => 'Account Overview',
                'view'   => 'account/overview/overview',
                'assets' => array(
                    'custom' => array(
                        'js' => array(
                            'js/custom/widgets.js',
                        ),
                    ),
                ),
            ),

            'settings' => array(
                'title'  => 'Account Settings',
                'assets' => array(
                    'custom' => array(
                        'js' => array(
                            'js/custom/account/settings/profile-details.js',
                            'js/custom/account/settings/signin-methods.js',
                            'js/custom/modals/two-factor-authentication.js',
                        ),
                    ),
                ),
            ),
        ),

        'users'  => array(
            'title' => 'User List',
            'create_link' => 'users/create',
            'create_tooltip' => 'Create a new account',
            'assets' => [
                'custom' => [
                    'css' => [
                        'plugins/custom/datatables/datatables.bundle.css',
                    ],
                    'js' => [
                        'plugins/custom/datatables/datatables.bundle.js',
                    ]
                ]
            ],
            'view' => array(
                '*' => [
                    'title' => 'Show User',
                    'assets' => [
                        'custom' => [
                            'js' => [
                                'js/users/update-email.js',
                                'js/users/update-password.js',
                                'js/users/update-role.js',
                            ],
                        ],
                    ],
                ],
            ),
            'edit' => [
                '*' => array(
                    'title' => 'Edit User',
                ),
            ]
        ),

    ),

);
