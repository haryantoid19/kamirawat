<?php

return array(

    // Main menu
    'main'          => [
        //// Dashboard
        [
            'title' => 'Dashboard',
            'path'  => 'theteam',
            'icon'  => theme()->getSvgIcon("demo1/media/icons/duotune/art/art002.svg", "svg-icon-2"),
        ],


        //// Booking
        'booking' => [
            'title' => 'Bookings',
            'icon' => [
                'svg'  => theme()->getSvgIcon("demo1/media/icons/duotune/general/gen056.svg", "svg-icon-2"),
                'font' => '<i class="bi bi-calendar-check fs-2"></i>',
            ],
            'classes' => ['item' => 'menu-accordion'],
            'attributes' => [
                'data-kt-menu-trigger' => 'click',
            ],
            'sub' => [
                'class' => 'menu-sub-accordion menu-active-bg',
                'items' => [
//                    [
//                        'title' => 'Home Visit',
//                        'path'  => 'theteam/bookings',
//                        'bullet' => '<span class="bullet bullet-dot"></span>',
//                    ],
//                    [
//                        'title' => 'Clinic Service',
//                        'path'  => 'theteam/clinic-bookings',
//                        'bullet' => '<span class="bullet bullet-dot"></span>',
//                    ],
                ]
            ]
        ],

        //// Medical People
        [
            'title' => 'Medical People',
            'icon' => [
                'svg'  => theme()->getSvgIcon("demo1/media/icons/duotune/technology/teh002.svg", "svg-icon-2"),
                'font' => '<i class="bi bi-person fs-2"></i>',
            ],
            'classes' => ['item' => 'menu-accordion'],
            'attributes' => [
                'data-kt-menu-trigger' => 'click',
            ],
            'sub' => [
                'class' => 'menu-sub-accordion menu-active-bg',
                'items' => [
                    [
                        'title' => 'Accepted',
                        'path'  => 'theteam/medical-people/status/accepted',
                        'bullet' => '<span class="bullet bullet-dot"></span>',
                    ],
                    [
                        'title' => 'Pending',
                        'path'  => 'theteam/medical-people/status/pending',
                        'bullet' => '<span class="bullet bullet-dot"></span>',
                    ],
                    [
                        'title' => 'Rejected',
                        'path'  => 'theteam/medical-people/status/rejected',
                        'bullet' => '<span class="bullet bullet-dot"></span>',
                    ],
                ]
            ]
        ],


        //// Patients
        [
            'title' => 'Patients',
            'path'  => 'theteam/patients',
            'icon'       => array(
                'svg'  => theme()->getSvgIcon("demo1/media/icons/duotune/communication/com013.svg", "svg-icon-2"),
                'font' => '<i class="bi bi-person fs-2"></i>',
            ),
        ],

        //// Users
        [
            'title' => 'Users',
            'path'  => 'theteam/users',
            'icon'  => theme()->getSvgIcon("demo1/media/icons/duotune/communication/com014.svg", "svg-icon-2"),
        ],

        //// Settings
        array(
            'classes' => array('content' => 'pt-8 pb-2'),
            'content' => '<span class="menu-section text-muted text-uppercase fs-8 ls-1">Settings</span>',
        ),

        [
            'title' => 'Professions',
            'path'  => 'theteam/professions',
            'icon'  => theme()->getSvgIcon("demo1/media/icons/duotune/technology/teh008.svg", "svg-icon-2"),
        ],

        [
            'title' => 'Medical Service Categories',
            'path'  => 'theteam/medical-service-categories',
            'icon'  => theme()->getSvgIcon("demo1/media/icons/duotune/medicine/med004.svg", "svg-icon-2"),
        ],

        [
            'title' => 'Medical Services',
            'path'  => 'theteam/medical-services',
            'icon'  => theme()->getSvgIcon("demo1/media/icons/duotune/medicine/med004.svg", "svg-icon-2"),
        ],

        [
            'title' => 'Additional Medical Services',
            'path'  => 'theteam/additional-medical-services',
            'icon'  => theme()->getSvgIcon("demo1/media/icons/duotune/medicine/med005.svg", "svg-icon-2"),
        ],

        [
            'title' => 'Payment Method',
            'path'  => 'theteam/payment-methods',
            'icon'  => theme()->getSvgIcon("demo1/media/icons/duotune/finance/fin001.svg", "svg-icon-2"),
        ],

        // System
        array(
            'title'      => 'System',
            'icon'       => array(
                'svg'  => theme()->getSvgIcon("demo1/media/icons/duotune/general/gen025.svg", "svg-icon-2"),
                'font' => '<i class="bi bi-layers fs-3"></i>',
            ),
            'classes'    => array('item' => 'menu-accordion'),
            'attributes' => array(
                "data-kt-menu-trigger" => "click",
            ),
            'sub'        => array(
                'class' => 'menu-sub-accordion menu-active-bg',
                'items' => array(
                    array(
                        'title'  => 'Audit Log',
                        'path'   => 'theteam/log/audit',
                        'bullet' => '<span class="bullet bullet-dot"></span>',
                    ),
                    array(
                        'title'  => 'System Log',
                        'path'   => 'theteam/log/system',
                        'bullet' => '<span class="bullet bullet-dot"></span>',
                    ),
                ),
            ),
        ),

        // Separator
        array(
            'content' => '<div class="separator mx-1 my-4"></div>',
        ),

    ],

    // Horizontal menu
    'horizontal'    => [
        // Dashboard
        [
            'title'   => 'Dashboard',
            'path'    => 'index',
            'classes' => ['item' => 'me-lg-1'],
        ],
    ],
);
