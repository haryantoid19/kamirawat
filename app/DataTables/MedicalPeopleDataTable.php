<?php

namespace App\DataTables;

use App\Enums\MedicalPeopleStatus;
use App\Exports\MedicalPeoplesExport;
use App\Models\MedicalPeople;
use Auth;
use Yajra\DataTables\DataTableAbstract;
use Yajra\DataTables\Html\Builder;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class MedicalPeopleDataTable extends DataTable
{
    protected $exportClass = MedicalPeoplesExport::class;

    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return DataTableAbstract
     */
    public function dataTable(mixed $query): DataTableAbstract
    {
        return datatables()
            ->eloquent($query)
            ->addIndexColumn()
            ->editColumn('created_at', function (MedicalPeople $medicalPeople) {
                return $medicalPeople->created_at->format('d M, Y H:i:s');
            })
            ->addColumn('action', function (MedicalPeople $medicalPeople) {
                $show_approve_button = Auth::user()->can('approval', $medicalPeople);
                return view('pages.medical-people._action-menu', compact('medicalPeople', 'show_approve_button'));
            })->addColumn('city', function (MedicalPeople $medicalPeople) {
                return $medicalPeople->postal_code->city;
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param MedicalPeople $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(MedicalPeople $model): \Illuminate\Database\Eloquent\Builder
    {
        /** @var MedicalPeopleStatus $status */
        $status = $this->status;
        return $model->newQuery()->with(['user', 'postal_code', 'postal_code.province'])->where('status', $status->value);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return Builder
     */
    public function html(): Builder
    {
        return $this->builder()
                    ->setTableId('medical-people-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->stateSave(true)
                    ->responsive()
                    ->autoWidth(false)
                    ->parameters([
                        'scrollX' => true,
                        'dom'     => 'Bfrtip',
                        'buttons' => ['excel'],
                    ])
                    ->addTableClass('align-middle table-row-dashed fs-6 gy-5');
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns(): array
    {
        return [
            Column::make('DT_RowIndex')->title('No.')->searchable(false)->orderable(false),
            Column::make('name'),
            Column::make('medical_number'),
            Column::make('gender'),
            Column::make('city'),
            Column::make('created_at'),
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->responsivePriority(-1)
                ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename(): string
    {
        return 'MedicalPeople_' . date('YmdHis');
    }
}
