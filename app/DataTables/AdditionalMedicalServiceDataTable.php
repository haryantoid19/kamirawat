<?php

namespace App\DataTables;

use App\Models\AdditionalMedicalService;
use Yajra\DataTables\DataTableAbstract;
use Yajra\DataTables\Html\Builder;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class AdditionalMedicalServiceDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return DataTableAbstract
     */
    public function dataTable($query): DataTableAbstract
    {
        return datatables()
            ->eloquent($query)
            ->addIndexColumn()
            ->editColumn('price', function (AdditionalMedicalService $medicalService) {
                return "Rp. " . number_format($medicalService->price,0,',','.');
            })
            ->editColumn('created_at', function (AdditionalMedicalService $medicalService) {
                return $medicalService->created_at->format('d M, Y H:i:s');
            })
            ->editColumn('status', function (AdditionalMedicalService $medicalService) {
                return $medicalService->status->toBadge();
            })
            ->addColumn('action', function (AdditionalMedicalService $medicalService) {
                return view('pages.additional-medical-service._action-menu', compact('medicalService'));
            })
            ->rawColumns(['status']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param AdditionalMedicalService $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(AdditionalMedicalService $model): \Illuminate\Database\Eloquent\Builder
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return Builder
     */
    public function html(): Builder
    {
        return $this->builder()
            ->setTableId('additional-medical-service-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->stateSave(true)
            ->orderBy(1)
            ->responsive()
            ->autoWidth(false)
            ->parameters(['scrollX' => true])
            ->addTableClass('align-middle table-row-dashed fs-6 gy-5');
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns(): array
    {
        return [
            Column::make('DT_RowIndex')->title('No.')->searchable(false)->orderable(false),
            Column::make('name'),
            Column::make('price'),
            Column::make('status'),
            Column::make('created_at'),
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->responsivePriority(-1)
                ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename(): string
    {
        return 'MedicalService_' . date('YmdHis');
    }
}
