<?php

namespace App\DataTables;

use App\Models\Profession;
use Yajra\DataTables\DataTableAbstract;
use Yajra\DataTables\Html\Builder;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class ProfessionDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return DataTableAbstract
     */
    public function dataTable(mixed $query): DataTableAbstract
    {
        return datatables()
            ->eloquent($query)
            ->addIndexColumn()
            ->editColumn('created_by', function (Profession $profession) {
                return $profession->creator->name;
            })
            ->editColumn('created_at', function (Profession $profession) {
                return $profession->created_at->format('d M, Y H:i:s');
            })
            ->addColumn('action', function (Profession $profession) {
                return view('pages.profession._action-menu', compact('profession'));
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param Profession $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Profession $model): \Illuminate\Database\Eloquent\Builder
    {
        return $model->newQuery()->orderBy('order')->with(['creator']);
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return Builder
     */
    public function html(): Builder
    {
        return $this->builder()
                    ->setTableId('profession-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->stateSave(true)
                    ->orderBy(1)
                    ->responsive()
                    ->autoWidth(false)
                    ->parameters(['scrollX' => true])
                    ->addTableClass('align-middle table-row-dashed fs-6 gy-5');
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns(): array
    {
        return [
            Column::make('DT_RowIndex')->title('No.')->searchable(false)->orderable(false),
            Column::make('title'),
            Column::make('created_by'),
            Column::make('created_at'),
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->responsivePriority(-1)
                ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename(): string
    {
        return 'Profession_' . date('YmdHis');
    }
}
