<?php

namespace App\DataTables;

use App\Models\PaymentMethod;
use Illuminate\Database\Eloquent\Builder as EloquentBuilder;
use Yajra\DataTables\DataTableAbstract;
use Yajra\DataTables\Html\Builder;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class PaymentMethodDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return DataTableAbstract
     */
    public function dataTable(mixed $query): DataTableAbstract
    {
        return datatables()
            ->eloquent($query)
            ->addIndexColumn()
            ->editColumn('created_by', function (PaymentMethod $payment_method) {
                return $payment_method->creator->name;
            })
            ->editColumn('created_at', function (PaymentMethod $payment_method) {
                return $payment_method->created_at->format('d M, Y H:i:s');
            })
            ->editColumn('status', function (PaymentMethod $payment_method) {
                return $payment_method->status->toBadge();
            })
            ->addColumn('action', function (PaymentMethod $payment_method) {
                return view('pages.payment-method._action-menu', compact('payment_method'));
            })
            ->rawColumns(['status']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param PaymentMethod $model
     * @return EloquentBuilder
     */
    public function query(PaymentMethod $model): EloquentBuilder
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return Builder
     */
    public function html(): Builder
    {
        return $this->builder()
            ->setTableId('payment-method-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->stateSave(true)
            ->orderBy(1)
            ->responsive()
            ->autoWidth(false)
            ->parameters(['scrollX' => true])
            ->addTableClass('align-middle table-row-dashed fs-6 gy-5');
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns(): array
    {
        return [
            Column::make('DT_RowIndex')->title('No.')->searchable(false)->orderable(false),
            Column::make('label'),
            Column::make('gateway'),
            Column::make('status'),
            Column::make('created_by'),
            Column::make('created_at'),
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->responsivePriority(-1)
                ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename(): string
    {
        return 'PaymentMethod_' . date('YmdHis');
    }
}
