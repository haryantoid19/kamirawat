<?php

namespace App\DataTables;

use App\Models\MedicalService;
use Yajra\DataTables\DataTableAbstract;
use Yajra\DataTables\Html\Builder;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;

class MedicalServiceDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return DataTableAbstract
     */
    public function dataTable($query): DataTableAbstract
    {
        return datatables()
            ->eloquent($query)
            ->addIndexColumn()
            ->editColumn('price', function (MedicalService $medicalService) {
                return "Rp. " . number_format($medicalService->price,0,',','.');
            })
            ->addColumn('category', function (MedicalService $medicalService) {
                return $medicalService->category?->name ?: '-';
            })
            ->editColumn('created_at', function (MedicalService $medicalService) {
                return $medicalService->created_at->format('d M, Y H:i:s');
            })
            ->editColumn('status', function (MedicalService $medicalService) {
                return $medicalService->status->toBadge();
            })
            ->addColumn('action', function (MedicalService $medicalService) {
                return view('pages.medical-service._action-menu', compact('medicalService'));
            })
            ->rawColumns(['status']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param MedicalService $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(MedicalService $model): \Illuminate\Database\Eloquent\Builder
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return Builder
     */
    public function html(): Builder
    {
        return $this->builder()
                    ->setTableId('medical-service-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->stateSave(true)
                    ->orderBy(1)
                    ->responsive()
                    ->autoWidth(false)
                    ->parameters(['scrollX' => true])
                    ->addTableClass('align-middle table-row-dashed fs-6 gy-5');
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns(): array
    {
        return [
            Column::make('DT_RowIndex')->title('No.')->searchable(false)->orderable(false),
            Column::make('name'),
            Column::make('price'),
            Column::make('category'),
            Column::make('status'),
            Column::make('created_at'),
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->responsivePriority(-1)
                ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename(): string
    {
        return 'MedicalService_' . date('YmdHis');
    }
}
