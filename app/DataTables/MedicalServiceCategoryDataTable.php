<?php

namespace App\DataTables;

use App\Models\MedicalServiceCategory;
use Yajra\DataTables\DataTableAbstract;
use Yajra\DataTables\Html\Builder;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class MedicalServiceCategoryDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return DataTableAbstract
     */
    public function dataTable($query): DataTableAbstract
    {
        return datatables()
            ->eloquent($query)
            ->addIndexColumn()
            ->editColumn('price', function (MedicalServiceCategory $medicalService) {
                return "Rp. " . number_format($medicalService->price,0,',','.');
            })
            ->editColumn('created_at', function (MedicalServiceCategory $medicalService) {
                return $medicalService->created_at->format('d M, Y H:i:s');
            })
            ->editColumn('status', function (MedicalServiceCategory $medicalService) {
                return $medicalService->status;
            })
            ->editColumn('type', function (MedicalServiceCategory $medicalService) {
                return $medicalService->type->toBadge();
            })
            ->addColumn('action', function (MedicalServiceCategory $category) {
                return view('pages.medical-service-category._action-menu', compact('category'));
            })
            ->rawColumns(['status', 'type']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param MedicalServiceCategory $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(MedicalServiceCategory $model): \Illuminate\Database\Eloquent\Builder
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return Builder
     */
    public function html(): Builder
    {
        return $this->builder()
                    ->setTableId('medical-service-category-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->stateSave(true)
                    ->orderBy(1)
                    ->responsive()
                    ->autoWidth(false)
                    ->parameters(['scrollX' => true])
                    ->addTableClass('align-middle table-row-dashed fs-6 gy-5');
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns(): array
    {
        return [
            Column::make('DT_RowIndex')->title('No.')->searchable(false)->orderable(false),
            Column::make('name'),
            Column::make('type'),
            Column::make('status'),
            Column::make('created_at'),
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->responsivePriority(-1)
                ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename(): string
    {
        return 'MedicalService_' . date('YmdHis');
    }
}
