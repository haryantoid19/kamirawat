<?php

namespace App\DataTables;

use App\Models\Booking;
use App\Models\MedicalServiceCategory;
use Yajra\DataTables\DataTableAbstract;
use Yajra\DataTables\Html\Builder;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class BookingDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return DataTableAbstract
     */
    public function dataTable(mixed $query): DataTableAbstract
    {
        return datatables()
            ->eloquent($query)
            ->addIndexColumn()
            ->editColumn('created_at', function (Booking $booking) {
                return $booking->created_at->format('d M, Y H:i:s');
            })
            ->addColumn('patient_name', function (Booking $booking) {
                return $booking->patient->name;
            })
            ->addColumn('gender', function (Booking $booking) {
                return $booking->patient->gender->value;
            })
            ->addColumn('city', function (Booking $booking) {
                return ucwords(strtolower($booking->patient->postal_code->city));
            })
            ->addColumn('status', function (Booking $booking) {
                return view('pages.bookings._status', compact('booking'));
            })
            ->addColumn('action', function (Booking $booking) {
                return view('pages.bookings._action-menu', compact('booking'));
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param Booking $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Booking $model): \Illuminate\Database\Eloquent\Builder
    {
        /** @var MedicalServiceCategory $category */
        $category = $this->category;
        return $model->newQuery()
            ->whereHas('medical_service', function(\Illuminate\Database\Eloquent\Builder $builder) use($category) {
                $builder->where('category_id', $category->id);
            })
            ->latest('created_at');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return Builder
     */
    public function html(): Builder
    {
        return $this->builder()
                    ->setTableId('booking-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->stateSave(true)
                    ->orderBy(1)
                    ->responsive()
                    ->autoWidth(false)
                    ->parameters(['scrollX' => true])
                    ->addTableClass('align-middle table-row-dashed fs-6 gy-5');
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns(): array
    {
        return [
            Column::make('DT_RowIndex')->title('No.')->searchable(false)->orderable(false),
            Column::make('patient_name'),
            Column::make('booking_number'),
            Column::make('gender'),
            Column::make('city'),
            Column::make('status'),
            Column::make('created_at'),
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->responsivePriority(-1)
                ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename(): string
    {
        return 'Booking_' . date('YmdHis');
    }
}
