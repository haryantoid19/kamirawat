<?php

namespace App\DataTables;

use App\Models\Patient;
use Yajra\DataTables\DataTableAbstract;
use Yajra\DataTables\Html\Builder;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class PatientDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return DataTableAbstract
     */
    public function dataTable(mixed $query): DataTableAbstract
    {
        return datatables()
            ->eloquent($query)
            ->addIndexColumn()
            ->editColumn('created_at', function (Patient $patient) {
                return $patient->created_at->format('d M, Y H:i:s');
            })
            ->addColumn('action', function (Patient $patient) {
                return view('pages.patient._action-menu', compact('patient'));
            })->addColumn('city', function (Patient $patient) {
                return $patient->postal_code->city;
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param Patient $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Patient $model): \Illuminate\Database\Eloquent\Builder
    {
        return $model->newQuery()->with('postal_code');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return Builder
     */
    public function html(): Builder
    {
        return $this->builder()
                    ->setTableId('patient-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->stateSave(true)
                    ->orderBy(1)
                    ->responsive()
                    ->autoWidth(false)
                    ->parameters(['scrollX' => true])
                    ->addTableClass('align-middle table-row-dashed fs-6 gy-5');
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns(): array
    {
        return [
            Column::make('DT_RowIndex')->title('No.')->searchable(false)->orderable(false),
            Column::make('name'),
            Column::make('patient_number'),
            Column::make('gender'),
            Column::make('city'),
            Column::make('created_at'),
            Column::computed('action')
                ->exportable(false)
                ->printable(false)
                ->responsivePriority(-1)
                ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename(): string
    {
        return 'Patient_' . date('YmdHis');
    }
}
