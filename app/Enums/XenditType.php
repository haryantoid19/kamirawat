<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static INVOICE()
 * @method static static VIRTUAL_ACCOUNT()
 */
final class XenditType extends Enum
{
    const INVOICE = "INVOICE";
    const VIRTUAL_ACCOUNT =  "VIRTUAL_ACCOUNT";
}
