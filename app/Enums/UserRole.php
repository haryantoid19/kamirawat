<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static ADMIN()
 * @method static static EDITOR()
 * @method static static MEDICAL_PERSON()
 * @method static static PATIENT()
 */
final class UserRole extends Enum
{
    const ADMIN = "ADMIN";
    const EDITOR = "EDITOR";
    const MEDICAL_PERSON = "MEDICAL_PERSON";
    const PATIENT = "PATIENT";
}
