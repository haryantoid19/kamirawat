<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static KTP()
 * @method static static PASSPORT()
 */
final class IdType extends Enum
{
    const KTP = "KTP";
    const PASSPORT = "PASSPORT";

    public function getLabel(): string
    {
        if (self::is(self::KTP())) {
            return 'No. KTP';
        } else {
            return 'Passport Number';
        }
    }
}
