<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static BANK_TRANSFER()
 * @method static static XENDIT()
 */
final class PaymentMethodGateway extends Enum
{
    const BANK_TRANSFER = "BANK_TRANSFER";
    const XENDIT = "XENDIT";

    public function label(): string
    {
        if (self::is(self::BANK_TRANSFER())) {
            return 'Bank Transfer';
        } else {
            return 'Xendit';
        }
    }
}
