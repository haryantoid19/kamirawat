<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static WAITING_VERIFIED()
 * @method static static PENDING()
 * @method static static ACCEPTED()
 * @method static static REJECTED()
 */
final class MedicalPeopleStatus extends Enum
{
    const WAITING_VERIFIED = 'WAITING_VERIFIED';
    const PENDING = "PENDING";
    const ACCEPTED = "ACCEPTED";
    const REJECTED = "REJECTED";
}
