<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static MAN()
 * @method static static WOMAN()
 */
final class Gender extends Enum
{
    const MAN = "MAN";
    const WOMAN = "WOMAN";

    public function title(): string
    {
        if (self::is(self::MAN())) {
            return 'Bapak';
        }

        return 'Ibu';
    }
}
