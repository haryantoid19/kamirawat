<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static PUBLISH()
 * @method static static UNPUBLISH()
 */
final class MedicalServiceStatus extends Enum
{
    const PUBLISH = "PUBLISH";
    const UNPUBLISH = "UNPUBLISH";

    public function toBadge(): string
    {
        if (self::is(self::PUBLISH())) {
            return '<span class="badge badge-light-success">Publish</span>';
        } else if (self::is(self::UNPUBLISH())) {
            return '<span class="badge badge-light-warning">Draft</span>';
        }

        return '';
    }

}
