<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static PUBLISH()
 * @method static static DRAFT()
 */
final class PaymentMethodStatus extends Enum
{
    const PUBLISH = "PUBLISH";
    const DRAFT = "DRAFT";

    public function toBadge(): string
    {
        if (self::is(self::PUBLISH())) {
            return '<span class="badge badge-light-success">Publish</span>';
        } else if (self::is(self::DRAFT())) {
            return '<span class="badge badge-light-warning">Draft</span>';
        }

        return '';
    }

}
