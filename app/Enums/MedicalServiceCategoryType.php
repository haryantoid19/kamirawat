<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static ONLINE()
 * @method static static OFFLINE()
 */
final class MedicalServiceCategoryType extends Enum
{
    const ONLINE = "ONLINE";
    const OFFLINE = "OFFLINE";

    public function toBadge(): string
    {
        if (self::is(self::ONLINE())) {
            return '<span class="badge badge-success">Online</span>';
        } else {
            return '<span class="badge badge-warning">Offline</span>';
        }
    }

}
