<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static SMA()
 * @method static static SMK()
 * @method static static DIPLOMA()
 * @method static static SARJANA()
 * @method static static MAGISTER()
 * @method static static DOCTOR()
 */
final class LastEducation extends Enum
{
    const SMA = "SMA";
    const SMK = "SMK";
    const DIPLOMA = "DIPLOMA";
    const SARJANA = "SARJANA";
    const MAGISTER = "MAGISTER";
    const DOCTOR = "DOCTOR";
}
