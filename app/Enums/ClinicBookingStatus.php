<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static SUBMITTED()
 */
final class ClinicBookingStatus extends Enum
{
    const SUBMITTED = "SUBMITTED";
    const DONE      = "DONE";

    public function toBadge(): string
    {   
        if(self::is(self::DONE())){
            return '<span class="badge badge-success">Done</span>';
        }else{
            return '<span class="badge badge-warning">Submitted</span>';
        }
        
    }
}
