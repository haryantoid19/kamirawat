<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static WAITING_PAYMENT()
 * @method static static PAID()
 * @method static static ASSIGNED()
 * @method static static ACCEPTED()
 * @method static static COMPLETED()
 * @method static static REJECTED()
 * @method static static CANCELED()
 */
final class BookingStatus extends Enum
{
    const WAITING_PAYMENT = "WAITING_PAYMENT";
    const PAID = "PAID";
    const ASSIGNED = "ASSIGNED";
    const ACCEPTED = "ACCEPTED";
    const COMPLETED = "COMPLETED";
    const REJECTED = "REJECTED";
    const CANCELED = "CANCELED";

    public function toBadge(): string
    {
        if (self::is(self::WAITING_PAYMENT())) {
           return '<span class="badge badge-light-warning">Waiting Payment</span>';
        } else if (self::is(self::PAID())) {
           return '<span class="badge badge-light-success">Paid</span>';
        } else if (self::is(self::ASSIGNED())) {
            return '<span class="badge badge-light-primary">Assigned</span>';
        } else if (self::is(self::ACCEPTED())) {
            return '<span class="badge badge-light-success">Accepted</span>';
        } else if (self::is(self::COMPLETED())) {
            return '<span class="badge badge-success">Completed</span>';
        } else if (self::is(self::CANCELED())) {
            return '<span class="badge badge-light-danger">Canceled</span>';
        } else if (self::is(self::REJECTED())) {
            return '<span class="badge badge-light-danger">Rejected</span>';
        }

        return '';
    }

    public function toTextForPatient(): string
    {
        if (self::is(self::WAITING_PAYMENT())) {
            return 'Menunggu Pembayaran';
        } else if (self::is(self::PAID())) {
            return 'Sedang mencari tenaga medis';
        } else if (self::is(self::ASSIGNED())) {
            return 'Tenaga medis ditugaskan';
        } else if (self::is(self::ACCEPTED())) {
            return 'Tenaga medis melakukan tindakan';
        } else if (self::is(self::COMPLETED())) {
            return 'Selesai';
        } else if (self::is(self::CANCELED())) {
            return 'Dibatalkan';
        } else if (self::is(self::REJECTED())) {
            return 'Ditolak';
        }

        return '';
    }

    public function isPaid(): bool
    {
        if (self::in([self::PAID(), self::ASSIGNED(), self::COMPLETED(), self::REJECTED()])) {
            return true;
        }

        return false;
    }
}
