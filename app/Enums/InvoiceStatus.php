<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static static ACTIVE()
 * @method static static PAID()
 * @method static static EXPIRED()
 */
final class InvoiceStatus extends Enum
{
    const ACTIVE = "ACTIVE";
    const PAID = "PAID";
    const EXPIRED = "EXPIRED";

    public function toBadge(): string
    {
        if (self::is(self::ACTIVE())) {
            return '<span class="badge badge-light-info">Active</span>';
        } else if (self::is(self::PAID())) {
            return '<span class="badge badge-light-success">Paid</span>';
        } else if (self::is(self::EXPIRED())) {
            return '<span class="badge badge-light-danger">Expired</span>';
        }

        return '';
    }

    public function isPaid(): bool
    {
        return self::is(self::PAID());
    }

    public function isActive(): bool
    {
        return self::is(self::ACTIVE());
    }

    public function isExpired(): bool
    {
        return self::is(self::EXPIRED());
    }
}
