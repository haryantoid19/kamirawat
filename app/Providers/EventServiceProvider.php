<?php

namespace App\Providers;

use App\Events\BookingAccepted;
use App\Events\BookingCreated;
use App\Events\BookingPaid;
use App\Listeners\FlagStatusMedicalPeopleAfterEmailVerified;
use App\Listeners\GenerateBookingPasscode;
use App\Listeners\LogSuccessfulLogin;
use App\Listeners\SendNotificationBookingAccepted;
use App\Listeners\SendNotificationNewBookingToAdmin;
use App\Listeners\SendNotificationNewBookingToPatient;
use Illuminate\Auth\Events\Login;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Events\Verified;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        Login::class => [
            LogSuccessfulLogin::class,
        ],
        Verified::class => [
            FlagStatusMedicalPeopleAfterEmailVerified::class,
        ],
        BookingCreated::class => [
            SendNotificationNewBookingToPatient::class,
            SendNotificationNewBookingToAdmin::class,
        ],
        BookingPaid::class => [
            GenerateBookingPasscode::class,
        ],
        BookingAccepted::class => [
            SendNotificationBookingAccepted::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
