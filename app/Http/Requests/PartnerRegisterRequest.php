<?php

namespace App\Http\Requests;

use App\Enums\Gender;
use App\Enums\IdType;
use App\Enums\LastEducation;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Password;

class PartnerRegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $request = $this->request;

        $rule_id_number = ['required', 'unique:App\Models\MedicalPeople'];
        if ($request->get('id_type') === IdType::KTP()->value) {
            $rule_id_number[] = 'string';
            $rule_id_number[] = 'size:16';
        } else {
            $rule_id_number[] = 'size:9';
        }

        return [
            'name' => 'required|max:255',
            'email' => 'required|email|unique:App\Models\User',
            'id_type' => ['required', Rule::in(IdType::getValues())],
            'id_number' => $rule_id_number,
            'phone_number' => 'required|max:16|unique:App\Models\MedicalPeople',
            'province' => 'required|exists:App\Models\Province,province_code',
            'city' => 'required',
            'subdistrict' => 'required',
            'urban' => 'required',
            'rt' => 'string|max:3',
            'rw' => 'string|max:3',
            'profession_id' => 'required',
            'bod' => 'required|date',
            'gender' => ['required', Rule::in(Gender::getValues())],
            'address' => 'required|max:255',
            'nationality' => ['required', Rule::in(['WNA', 'WNI']), function ($attribute, $value, $fail) use($request) {
                if ($request->get('id_type') === IdType::KTP()->value && $value === "WNA") {
                    $fail("WNA hanya bisa menggunakan Tipe ID PASSPORT");
                }
            }],
            'last_education' => ['required', Rule::in(LastEducation::getValues())],
            'password' => ['required', 'confirmed', Password::defaults()],
            'document_ktp' => 'required|file|mimetypes:image/png,image/jpg,image/jpeg,application/pdf|max:2048',
            'document_npwp' => 'required|file|mimetypes:image/png,image/jpg,image/jpeg,application/pdf|max:2048',
            'document_str' => 'required|file|mimetypes:image/png,image/jpg,image/jpeg,application/pdf|max:2048',
            'document_sip' => 'file|mimetypes:image/png,image/jpg,image/jpeg,application/pdf|max:2048',
            'document_certification' => 'required|file|mimetypes:image/png,image/jpg,image/jpeg,application/pdf|max:2048',
            'check-tnc' => 'required|in:1',
        ];
    }

    public function attributes(): array
    {
        return [
            'name' => 'Nama Lengkap',
            'id_type' => 'Tipe ID',
            'id_number' => 'Nomor ID',
            'phone_number' => 'Nomor Handphone',
            'province' => 'Provinsi',
            'city' => 'Kota',
            'subdistrict' => 'Kecamatan',
            'urban' => 'Kelurahan',
            'rt' => 'RT',
            'rw' => 'RW',
            'profession_id' => 'Profesi Anda',
            'bod' => 'Tanggal Lahir',
            'gender' => 'Jenis Kelamin',
            'address' => 'Alamat',
            'nationality' => 'Kewarganegaraan',
            'document_ktp' => 'KTP',
            'document_str' => 'STR',
            'document_sip' => 'SIP',
            'document_certification' => 'Dokumen Pendukung Lainnya',
            'check-tnc' => 'Peraturan pengguna',
        ];
    }
}
