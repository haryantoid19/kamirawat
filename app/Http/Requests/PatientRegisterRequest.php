<?php

namespace App\Http\Requests;

use App\Enums\Gender;
use App\Enums\IdType;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Password;

class PatientRegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        $request = $this;

        $rule_id_number = ['required', 'unique:App\Models\Patient'];
        if ($request->get('id_type') === IdType::KTP()->value) {
            $rule_id_number[] = 'string';
            $rule_id_number[] = 'size:16';
        } else {
            $rule_id_number[] = 'max:40';
        }

        return [
            'name' => 'required|max:255',
            'email' => 'required|email|unique:App\Models\User',
            'id_type' => ['required', Rule::in(IdType::getValues())],
            'id_number' => $rule_id_number,
            'phone_number' => 'required|max:40|unique:App\Models\Patient',
            'province' => 'required|exists:App\Models\Province,province_code',
            'city' => 'required',
            'subdistrict' => 'required',
            'urban' => 'required',
            'rt' => 'string|max:3',
            'rw' => 'string|max:3',
            'bod' => 'required|date',
            'gender' => ['required', Rule::in(Gender::getValues())],
            'address' => 'required|max:255',
            'nationality' => ['required', Rule::in(['WNA', 'WNI']), function ($attribute, $value, $fail) use($request) {
                if ($request->get('id_type') === IdType::KTP()->value && $value === "WNA") {
                    $fail("WNA hanya bisa menggunakan Tipe ID PASSPORT");
                }
            }],
            'password' => ['required', 'confirmed', Password::defaults()],
            'check-tnc' => 'required|in:1',
        ];
    }

    public function attributes(): array
    {
        return [
            'name' => 'Nama Lengkap',
            'id_type' => 'Tipe ID',
            'id_number' => 'Nomor ID',
            'phone_number' => 'Nomor Handphone',
            'province' => 'Provinsi',
            'city' => 'Kota',
            'subdistrict' => 'Kecamatan',
            'urban' => 'Kelurahan',
            'rt' => 'RT',
            'rw' => 'RW',
            'bod' => 'Tanggal Lahir',
            'gender' => 'Jenis Kelamin',
            'address' => 'Alamat',
            'nationality' => 'Kewarganegaraan',
            'check-tnc' => 'Peraturan pengguna',
        ];
    }
}
