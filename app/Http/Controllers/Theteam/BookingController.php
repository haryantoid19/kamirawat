<?php

namespace App\Http\Controllers\Theteam;

use App\DataTables\BookingDataTable;
use App\Http\Controllers\Controller;
use App\Models\Booking;
use App\Models\MedicalServiceCategory;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class BookingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param MedicalServiceCategory $medicalServiceCategory
     * @param BookingDataTable $bookingDataTable
     * @return mixed
     * @throws AuthorizationException
     */
    public function index(MedicalServiceCategory $medicalServiceCategory, BookingDataTable $bookingDataTable): mixed
    {
        $this->authorize('viewAny', Booking::class);

        return $bookingDataTable
            ->with('category', $medicalServiceCategory)
            ->render('pages.bookings.browse');
    }

    /**
     * Display the specified resource.
     *
     * @param Booking $booking
     * @return Factory|View|Application
     * @throws AuthorizationException
     */
    public function show(Booking $booking): Factory|View|Application
    {
        $this->authorize('view', $booking);

        $booking->load(['invoices']);

        return view('pages.bookings.show', compact('booking'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Booking $booking
     */
    public function edit(Booking $booking)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Booking $booking
     */
    public function update(Request $request, Booking $booking)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Booking $booking
     */
    public function destroy(Booking $booking)
    {
        //
    }
}
