<?php

namespace App\Http\Controllers\Theteam;

use App\DataTables\MedicalServiceDataTable;
use App\Enums\MedicalServiceStatus;
use App\Http\Controllers\Controller;
use App\Models\MedicalService;
use App\Models\MedicalServiceCategory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Storage;

class MedicalServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(MedicalServiceDataTable $medicalServiceDataTable)
    {
        $categories = MedicalServiceCategory::get();

        return $medicalServiceDataTable->render('pages.medical-service.browse', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        $request->validate([
            'name' => 'required|max:255',
            'price' => 'required',
            'category' => 'required|exists:App\Models\MedicalServiceCategory,id',
            'description' => 'required|max:200',
            'icon' => 'file|max:1024|mimes:png,jpg,jpeg,svg',
        ]);

        $medicalService = new MedicalService();
        $medicalService->name = $request->get('name');
        $medicalService->price = $request->get('price');
        $medicalService->category_id = $request->get('category');
        $medicalService->status = MedicalServiceStatus::PUBLISH();
        $medicalService->description = $request->get('description');

        if ($request->has('icon')) {
            $medicalService->icon_path = $request->file('icon')->store('medical_services');
        }

        $medicalService->save();

        $medical_service = $medicalService->only('name');

        return response()->json(compact('medical_service'));
    }

    /**
     * Display the specified resource.
     *
     * @param  MedicalService $medicalService
     * @return JsonResponse
     */
    public function show(MedicalService $medicalService): JsonResponse
    {
        $medical_service = $medicalService->only(['name', 'price', 'category_id', 'description', 'icon_url']);

        return response()->json(compact('medical_service'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  MedicalService $medicalService
     * @return JsonResponse
     */
    public function update(Request $request, MedicalService $medicalService): JsonResponse
    {
        $request->validate([
            'name' => 'required|max:255',
            'price' => 'required',
            'category' => 'required|exists:App\Models\MedicalServiceCategory,id',
            'description' => 'required|max:200',
            'icon' => 'file|max:1024|mimes:png,jpg,jpeg,svg',
        ]);

        $medicalService->name = $request->get('name');
        $medicalService->price = $request->get('price');
        $medicalService->category_id = $request->get('category');
        $medicalService->description = $request->get('description');
        $medicalService->price = $request->get('price');

        if ($request->has('icon')) {
            if (Storage::exists($medicalService->icon_path)) {
                Storage::delete($medicalService->icon_path);
            }

            $medicalService->icon_path = $request->file('icon')->store('medical_services');
        }

        $medicalService->save();

        $medical_service = $medicalService->only(['name']);

        return response()->json(compact('medical_service'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  MedicalService $medicalService
     * @return JsonResponse
     */
    public function destroy(MedicalService $medicalService): JsonResponse
    {
        if (Storage::exists($medicalService->icon_path)) {
            Storage::delete($medicalService->icon_path);
        }

        $medicalService->delete();

        return response()->json(['OK']);
    }
}
