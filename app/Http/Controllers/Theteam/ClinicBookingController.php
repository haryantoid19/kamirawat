<?php

namespace App\Http\Controllers\Theteam;

use App\DataTables\ClinicBookingDatatable;
use App\Http\Controllers\Controller;
use App\Models\ClinicBooking;
use App\Enums\ClinicBookingStatus;
use App\Models\MedicalServiceCategory;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;

class ClinicBookingController extends Controller
{
    public function index(MedicalServiceCategory $medicalServiceCategory, ClinicBookingDatatable $clinicBookingDatatable)
    {
        return $clinicBookingDatatable
            ->with('category', $medicalServiceCategory)
            ->render('pages.clinic-bookings.browse');
    }


    /**
     * Display the specified resource.
     *
     * @param ClinicBooking $clinicBooking
     * @return Factory|View|Application
     */
    public function show(ClinicBooking $clinicBooking): Factory|View|Application
    {
        //$this->authorize('view', $booking);

        return view('pages.clinic-bookings.show', compact('clinicBooking'));
    }


    public function update(Request $request, ClinicBooking $clinicBooking)
    {
        //$this->authorize('view', $booking);
        $clinicBooking->status = ClinicBookingStatus::DONE();
        $clinicBooking->save();


        return back()->with([
            'message' => 'Successfully mark as done the booking',
        ]);

    }



}
