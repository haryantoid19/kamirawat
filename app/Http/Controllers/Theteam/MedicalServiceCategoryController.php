<?php

namespace App\Http\Controllers\Theteam;

use App\DataTables\MedicalServiceCategoryDataTable;
use App\Enums\MedicalServiceCategoryType;
use App\Http\Controllers\Controller;
use App\Models\MedicalServiceCategory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class MedicalServiceCategoryController extends Controller
{
    public function index(MedicalServiceCategoryDataTable $medicalServiceCategoryDataTable)
    {
        return $medicalServiceCategoryDataTable->render('pages.medical-service-category.browse');
    }

    public function store(Request $request): JsonResponse
    {
        $request->validate([
            'name' => 'required|max:255',
            'type' => ['required', Rule::in(MedicalServiceCategoryType::getValues())],
        ]);

        $category = MedicalServiceCategory::create([
            'name' => $request->get('name'),
            'type' => $request->get('type'),
        ]);

        return response()->json(['medical_service_category' => $category->only(['name', 'is_active', 'type'])], 201);
    }

    public function show(MedicalServiceCategory $medicalServiceCategory): JsonResponse
    {
        $medical_service_category = $medicalServiceCategory->only(['name', 'is_active', 'type']);

        return response()->json(compact('medical_service_category'));
    }

    public function update(MedicalServiceCategory $medicalServiceCategory, Request $request): JsonResponse
    {
        $request->validate([
            'name' => 'required|max:255',
            'type' => ['required', Rule::in(MedicalServiceCategoryType::getValues())],
        ]);

        $medicalServiceCategory->fill($request->only('name', 'type'));
        $medicalServiceCategory->save();

        $medical_service_category = $medicalServiceCategory->only(['name', 'is_active', 'type']);

        return response()->json(compact('medical_service_category'));
    }

    public function destroy(MedicalServiceCategory $medicalServiceCategory): JsonResponse
    {
        $medicalServiceCategory->delete();

        return response()->json([]);
    }
}
