<?php

namespace App\Http\Controllers\Theteam;

use App\DataTables\AdditionalMedicalServiceDataTable;
use App\Enums\MedicalServiceStatus;
use App\Http\Controllers\Controller;
use App\Models\AdditionalMedicalService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class AdditionalMedicalServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(AdditionalMedicalServiceDataTable $additionalMedicalServiceDataTable)
    {
        return $additionalMedicalServiceDataTable->render('pages.additional-medical-service.browse');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        $request->validate([
            'name' => 'required|max:255',
            'price' => 'required|numeric|between:0,10000000000.00',
        ]);

        $additionalMedicalService = new AdditionalMedicalService();
        $additionalMedicalService->name = $request->get('name');
        $additionalMedicalService->price = $request->get('price');
        $additionalMedicalService->status = MedicalServiceStatus::PUBLISH();
        $additionalMedicalService->save();

        $additional_medical_service = $additionalMedicalService->only('name');

        return response()->json(compact('additional_medical_service'));
    }

    /**
     * Display the specified resource.
     *
     * @param  AdditionalMedicalService $additionalMedical
     * @return JsonResponse
     */
    public function show(AdditionalMedicalService $additionalMedical): JsonResponse
    {
        $medical_service = $additionalMedical->only(['name', 'price']);

        return response()->json(compact('medical_service'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  AdditionalMedicalService $additionalMedicalService
     * @return JsonResponse
     */
    public function update(Request $request, AdditionalMedicalService $additionalMedicalService): JsonResponse
    {
        $request->validate([
            'name' => 'required|max:255',
            'price' => 'required|numeric|between:0,10000000000.00',
        ]);

        $additionalMedicalService->name = $request->get('name');
        $additionalMedicalService->price = $request->get('price');
        $additionalMedicalService->save();

        $additional_medical_service = $additionalMedicalService->only(['name']);

        return response()->json(compact('additional_medical_service'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  AdditionalMedicalService $additionalMedicalService
     * @return JsonResponse
     */
    public function destroy(AdditionalMedicalService $additionalMedicalService): JsonResponse
    {
        $additionalMedicalService->delete();

        return response()->json(['OK']);
    }
}
