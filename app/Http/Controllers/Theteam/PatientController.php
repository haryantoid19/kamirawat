<?php

namespace App\Http\Controllers\Theteam;

use App\DataTables\PatientDataTable;
use App\Http\Controllers\Controller;
use App\Models\Patient;
use Illuminate\Http\Request;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;

class PatientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param PatientDataTable $patientDataTable
     * @return mixed
     */
    public function index(PatientDataTable $patientDataTable): mixed
    {
        return $patientDataTable->render('pages.patient.browse');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param Patient $patient
     * @return View|Factory|Application
     */
    public function show(Patient $patient): View|Factory|Application
    {
        $patient->load(['user', 'postal_code', 'postal_code.province']);

        return view('pages.patient.show', compact('patient'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int  $id
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     */
    public function destroy($id)
    {
        //
    }
}
