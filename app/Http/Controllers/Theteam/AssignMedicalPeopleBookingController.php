<?php

namespace App\Http\Controllers\Theteam;

use App\Enums\BookingStatus;
use App\Http\Controllers\Controller;
use App\Models\Booking;
use App\Models\MedicalPeople;
use Auth;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\RedirectResponse;

class AssignMedicalPeopleBookingController extends Controller
{
    /**
     * @param Booking $booking
     * @param MedicalPeople $medicalPeople
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function store(Booking $booking, MedicalPeople $medicalPeople): RedirectResponse
    {
        $this->authorize('assign bookings to medical people', $booking);

        $booking->medical_people_id = $medicalPeople->id;
        $booking->status = BookingStatus::ASSIGNED();
        $booking->assigned_at = now();
        $booking->assigned_by = Auth::id();
        $booking->save();

        return redirect()->route('bookings.show', $booking->id);
    }
}
