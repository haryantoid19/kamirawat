<?php

namespace App\Http\Controllers\Theteam;

use App\Http\Controllers\Controller;
use App\Models\User;
use Hash;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\Rules\Password;

class UserUpdatePasswordController extends Controller
{
    /**
     * @throws AuthorizationException
     */
    public function update(User $user, Request $request): JsonResponse
    {
        $this->authorize('update', $user);

        $request->validate([
            'new_password' => ['required', 'confirmed', Password::min(8)]
        ], [], [
            'new_password' => 'New Password',
        ]);

        $user->password = Hash::make($request->get('password'));
        $user->save();

        return response()->json([]);
    }
}
