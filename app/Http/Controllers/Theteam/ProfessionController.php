<?php

namespace App\Http\Controllers\Theteam;

use App\DataTables\ProfessionDataTable;
use App\Http\Controllers\Controller;
use App\Models\Profession;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ProfessionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param ProfessionDataTable $professionsDataTable
     * @return mixed
     */
    public function index(ProfessionDataTable $professionsDataTable): mixed
    {
        return $professionsDataTable->render('pages.profession.browse');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        $request->validate([
            'title' => 'required|max:255',
        ]);

        $profession = new Profession();
        $profession->title = $request->get('title');
        $profession->save();

        $profession = $profession->only(['title']);

        return response()->json(compact('profession'));
    }

    /**
     * Display the specified resource.
     *
     * @param Profession $profession
     * @return JsonResponse
     */
    public function show(Profession $profession): JsonResponse
    {
        $profession = $profession->only(['title']);

        return response()->json(compact('profession'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Profession $profession
     * @return JsonResponse
     */
    public function update(Request $request, Profession $profession): JsonResponse
    {
        $request->validate([
            'title' => 'required|max:255',
        ]);

        $profession->title = $request->get('title');
        $profession->save();

        $profession = $profession->only(['title']);

        return response()->json(compact('profession'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Profession $profession
     * @return JsonResponse
     */
    public function destroy(Profession $profession): JsonResponse
    {
        $profession->delete();

        return response()->json(['OK']);
    }
}
