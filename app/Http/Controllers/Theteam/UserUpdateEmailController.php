<?php

namespace App\Http\Controllers\Theteam;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class UserUpdateEmailController extends Controller
{
    /**
     * Update
     *
     * @throws AuthorizationException
     */
    public function update(User $user, Request $request): JsonResponse
    {
        $this->authorize('update', $user);

        $request->validate([
            'email' => ['required', 'email', Rule::unique('users')->ignore($user->id)]
        ]);

        $user->forceFill([
            'email' => $request->get('email'),
        ])->save();

        return response()->json([]);
    }
}
