<?php

namespace App\Http\Controllers\Theteam;

use App\Enums\BookingStatus;
use App\Enums\InvoiceStatus;
use App\Events\BookingPaid;
use App\Http\Controllers\Controller;
use App\Models\Invoice;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\RedirectResponse;

class InvoiceMarkAsPaidController extends Controller
{
    /**
     * @throws AuthorizationException
     */
    public function update(Invoice $invoice): RedirectResponse
    {
        $this->authorize('mark_as_paid', $invoice);

        $invoice->status = InvoiceStatus::PAID();
        $invoice->paid_at = now();
        $invoice->save();

        if ($invoice->is_main) {
            $invoice->booking()->update([
                'status' => BookingStatus::PAID(),
            ]);

            event(new BookingPaid($invoice->booking));
        }

        return back()->with([
            'message' => 'Successfully mark as paid the booking',
        ]);
    }
}
