<?php

namespace App\Http\Controllers\Theteam;

use App\Enums\MedicalPeopleStatus;
use App\Http\Controllers\Controller;
use App\Models\MedicalPeople;
use App\Notifications\ApprovedRegistrationPartner;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class MedicalPeopleApprovalController extends Controller
{
    /**
     * @param MedicalPeople $medicalPeople
     * @param Request $request
     * @return JsonResponse
     * @throws AuthorizationException
     */
    public function update(MedicalPeople $medicalPeople, Request $request): JsonResponse
    {
        $this->authorize('approval', $medicalPeople);

        $request->validate([
            'status' => ['required', Rule::in(MedicalPeopleStatus::ACCEPTED()->value, MedicalPeopleStatus::REJECTED()->value)]
        ]);

        $medicalPeople->load(['user']);

        $status = MedicalPeopleStatus::fromValue($request->get('status'));
        $medicalPeople->status = $status;
        $medicalPeople->save();

        if ($status->is(MedicalPeopleStatus::ACCEPTED())) {
            $medicalPeople->user->notify(new ApprovedRegistrationPartner($medicalPeople));
        }

        return response()->json(['OK']);
    }
}
