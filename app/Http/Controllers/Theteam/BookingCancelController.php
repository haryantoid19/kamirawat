<?php

namespace App\Http\Controllers\Theteam;

use App\Enums\BookingStatus;
use App\Http\Controllers\Controller;
use App\Models\Booking;
use Auth;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\RedirectResponse;

class BookingCancelController extends Controller
{
    /**
     * @param Booking $booking
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function update(Booking $booking): RedirectResponse
    {
        $this->authorize('cancel', $booking);

        $booking->status = BookingStatus::CANCELED();
        $booking->canceled_by = Auth::id();
        $booking->canceled_at = now();
        $booking->save();

        return back()->with([
            'message' => 'Successfully cancel this booking',
        ]);
    }
}
