<?php

namespace App\Http\Controllers\Theteam;

use App\DataTables\MedicalPeopleDataTable;
use App\Enums\MedicalPeopleStatus;
use App\Http\Controllers\Controller;
use App\Models\Bank;
use App\Models\MedicalPeople;
use BenSampo\Enum\Exceptions\InvalidEnumKeyException;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;

class MedicalPeopleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @throws AuthorizationException
     */
    public function index(string $status, MedicalPeopleDataTable $medicalPeopleDataTable)
    {
        $this->authorize('viewAny', MedicalPeople::class);

        try {
            $status = MedicalPeopleStatus::fromKey(strtoupper($status));
        } catch (InvalidEnumKeyException $e) {
            abort(404);
        }

        return $medicalPeopleDataTable
            ->with([
                'status' => $status
            ])->render('pages.medical-people.browse');
    }

    /**
     * Display the specified resource.
     *
     * @param MedicalPeople $medicalPeople
     * @return Application|Factory|View
     * @throws AuthorizationException
     */
    public function show(MedicalPeople $medicalPeople): View|Factory|Application
    {
        $this->authorize('view', $medicalPeople);

        $medicalPeople->load(['user', 'profession', 'postal_code', 'postal_code.province']);

        $documents = $medicalPeople->getMedia('documents');
        $banks = Bank::get();

        return view('pages.medical-people.show', compact('medicalPeople', 'documents', 'banks'));
    }
}
