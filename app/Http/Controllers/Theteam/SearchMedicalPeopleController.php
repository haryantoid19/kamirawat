<?php

namespace App\Http\Controllers\Theteam;

use App\Enums\BookingStatus;
use App\Http\Controllers\Controller;
use App\Models\Booking;
use App\Models\MedicalPeople;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class SearchMedicalPeopleController extends Controller
{
    /**
     * @throws AuthorizationException
     */
    public function index(Request $request): JsonResponse
    {
        $this->authorize('assign bookings to medical people');

        $request->validate([
            'search' => 'required|min:3',
            'booking' => 'required',
        ]);

        $booking = Booking::findOrFail($request->get('booking'));

        $search = $request->get('search');
        $medicalPeopleData = MedicalPeople::whereDoesntHave('bookings', function(Builder $builder) {
            $builder->where('status', BookingStatus::ASSIGNED);
        })->where('name', 'like', "%$search%")
            ->orWhere('medical_number', 'like', "%$search%")
            ->get();

        $total = $medicalPeopleData->count();
        $html = view('pages.medical-people.search-result', compact('medicalPeopleData', 'booking'))->render();

        return response()->json(compact('html', 'total'));
    }
}
