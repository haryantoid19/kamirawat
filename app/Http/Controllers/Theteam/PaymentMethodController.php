<?php

namespace App\Http\Controllers\Theteam;

use App\DataTables\PaymentMethodDataTable;
use App\Enums\PaymentMethodGateway;
use App\Enums\PaymentMethodStatus;
use App\Http\Controllers\Controller;
use App\Models\PaymentMethod;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Storage;

class PaymentMethodController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param PaymentMethodDataTable $dataTable
     * @return mixed
     */
    public function index(PaymentMethodDataTable $dataTable): mixed
    {
        return $dataTable->render('pages.payment-method.browse');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        $request->validate([
            'code' => 'required|max:255',
            'label' => 'required|max:255',
            'gateway' => ['required', Rule::in(PaymentMethodGateway::getValues())],
            'description' => 'max:200',
            'icon' => 'file|max:512|mimes:png,jpg,jpeg,svg',
            'status' => ['required', Rule::in(PaymentMethodStatus::getValues())],
        ]);

        $paymentMethod = new PaymentMethod();
        $paymentMethod->code = $request->get('code');
        $paymentMethod->label = $request->get('label');
        $paymentMethod->description = $request->get('description');
        $paymentMethod->gateway = $request->get('gateway');
        $paymentMethod->status = $request->get('status');

        if ($request->has('icon')) {
            $paymentMethod->icon_path = $request->file('icon')->store('payment_methods');
        }

        $paymentMethod->save();

        $payment_method = $paymentMethod->only('label');

        return response()->json(compact('payment_method'));
    }

    /**
     * Display the specified resource.
     *
     * @param PaymentMethod $paymentMethod
     * @return JsonResponse
     */
    public function show(PaymentMethod $paymentMethod): JsonResponse
    {
        $payment_method = $paymentMethod->only(['label', 'code', 'gateway', 'description', 'status', 'icon_url']);

        return response()->json(compact('payment_method'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param PaymentMethod $paymentMethod
     * @return JsonResponse
     */
    public function update(Request $request, PaymentMethod $paymentMethod): JsonResponse
    {
        $request->validate([
            'code' => 'required|max:255',
            'label' => 'required|max:255',
            'gateway' => ['required', Rule::in(PaymentMethodGateway::getValues())],
            'description' => 'max:200',
            'icon' => 'file|max:512|mimes:png,jpg,jpeg,svg',
            'status' => ['required', Rule::in(PaymentMethodStatus::getValues())],
        ]);

        $paymentMethod->code = $request->get('code');
        $paymentMethod->label = $request->get('label');
        $paymentMethod->description = $request->get('description');
        $paymentMethod->gateway = $request->get('gateway');
        $paymentMethod->status = $request->get('status');

        if ($request->has('icon')) {
            if (Storage::exists($paymentMethod->icon_path)) {
                Storage::delete($paymentMethod->icon_path);
            }

            $paymentMethod->icon_path = $request->file('icon')->store('payment_methods');
        }

        $paymentMethod->save();

        $payment_method = $paymentMethod->only('label');

        return response()->json(compact('payment_method'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param PaymentMethod $paymentMethod
     * @return JsonResponse
     */
    public function destroy(PaymentMethod $paymentMethod): JsonResponse
    {
        $paymentMethod->delete();

        if (Storage::exists($paymentMethod->icon_path)) {
            Storage::delete($paymentMethod->icon_path);
        }

        return response()->json([]);
    }
}
