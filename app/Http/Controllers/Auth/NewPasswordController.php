<?php

namespace App\Http\Controllers\Auth;

use App\Enums\UserRole;
use App\Http\Controllers\Controller;
use Hash;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\Rules\Password as RulesPassword;
use Illuminate\View\View;
use Password;
use Str;

class NewPasswordController extends Controller
{
    /**
     * Display the password reset view.
     *
     * @param Request $request
     * @param $token
     * @return View
     */
    public function create(Request $request, $token): View
    {
        return view('front.auth.reset-password', compact('request', 'token'));
    }

    /**
     * Handle an incoming new password request.
     *
     * @param Request $request
     *
     * @return RedirectResponse
     *
     */
    public function store(Request $request): RedirectResponse
    {
        $request->validate([
            'token'    => 'required',
            'email'    => 'required|email',
            'password' => ['required', 'confirmed', RulesPassword::defaults()],
        ]);

        $route = 'front.index';

        // Here we will attempt to reset the user's password. If it is successful we
        // will update the password on an actual user model and persist it to the
        // database. Otherwise, we will parse the error and return the response.
        $status = Password::reset(
            $request->only('email', 'password', 'password_confirmation', 'token'),
            function ($user) use ($request, &$route) {
                $user->forceFill([
                    'password'       => Hash::make($request->get('password')),
                    'remember_token' => Str::random(60),
                ])->save();

                if ($user->hasRole(UserRole::PATIENT()->value)) {
                    $route = 'front.patient.login';
                } else if ($user->hasRole(UserRole::MEDICAL_PERSON()->value)) {
                    $route = 'front.partner.login';
                }

                event(new PasswordReset($user));
            }
        );

        // If the password was successfully reset, we will redirect the user back to
        // the application's home authenticated view. If there is an error we can
        // redirect them back to where they came from with their error message.
        return $status == Password::PASSWORD_RESET
            ? redirect()->route($route)->with('status', __($status))
            : back()->withInput($request->only('email'))
                ->withErrors(['email' => __($status)]);
    }
}
