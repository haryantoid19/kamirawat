<?php

namespace App\Http\Controllers\Auth;

use App\Enums\UserRole;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class EmailVerificationPromptController extends Controller
{
    /**
     * Display the email verification prompt.
     *
     * @param Request $request
     *
     * @return Application|RedirectResponse|View|Factory
     */
    public function __invoke(Request $request): Application|RedirectResponse|View|Factory
    {
        $view = 'front.auth.verify-email';
        $email = null;
        if (Auth::user()->hasRole(UserRole::MEDICAL_PERSON()->value)) {
            $view = 'front.auth.verify-email-partner';
            $email = Auth::user()->email;
        }

        return $request->user()->hasVerifiedEmail()
            ? redirect()->route(Auth::user()->getDashboardRouteNamePatientOrPartner())
            : view($view, compact('email'));
    }
}
