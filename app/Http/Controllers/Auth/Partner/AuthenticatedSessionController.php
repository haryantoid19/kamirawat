<?php

namespace App\Http\Controllers\Auth\Partner;

use App\Enums\MedicalPeopleStatus;
use App\Enums\UserRole;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Models\User;
use Auth;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class AuthenticatedSessionController extends Controller
{
    /**
     * Display the login view
     *
     * @return Factory|View|Application
     */
    public function create(): Factory|View|Application
    {
        return view('front.partner.login');
    }

    /**
     * Handle an incoming authentication request.
     *
     * @param LoginRequest $request
     * @return RedirectResponse
     * @throws ValidationException
     */
    public function store(LoginRequest $request): RedirectResponse
    {
        $check_email = User::whereEmail($request->get('email'))->exists();
        if (! $check_email) {
            throw ValidationException::withMessages([
                'email' => __('auth.not_registered'),
            ]);
        }

        $request->authenticate();

        $user = Auth::user();
        $user->load('medical_people');
        if ($user->hasRole(UserRole::MEDICAL_PERSON()->value) === false) {
            Auth::logout();

            throw ValidationException::withMessages([
                'email' => __('auth.failed')
            ]);
        }

        $medical_people = $user->medical_people;
        if ($medical_people->status->is(MedicalPeopleStatus::REJECTED())) {
            Auth::logout();

            throw ValidationException::withMessages([
                'email' => __('auth.rejected_status'),
            ]);
        } else if ($medical_people->status->isNot(MedicalPeopleStatus::ACCEPTED())) {
            Auth::logout();

            throw ValidationException::withMessages([
                'email' => __('auth.on_verification'),
            ]);
        }

        $request->session()->regenerate();

        if ($continue = $request->get('continue')) {
            return redirect()->to($continue);
        }

        return redirect()->route('front.partner.dashboard');
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function destroy(Request $request): RedirectResponse
    {
        Auth::logout();

        if ($continue = $request->get('continue')) {
            return redirect()->route($continue);
        }

        return redirect()->route('front.index');
    }
}
