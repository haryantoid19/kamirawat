<?php

namespace App\Http\Controllers\Auth\Partner;

use App\Enums\MedicalPeopleStatus;
use App\Enums\UserRole;
use App\Http\Controllers\Controller;
use App\Http\Requests\PartnerRegisterRequest;
use App\Models\MedicalPeople;
use App\Models\PostalCode;
use App\Models\Profession;
use App\Models\Province;
use App\Models\User;
use Auth;
use DB;
use Hash;
use Illuminate\Auth\Events\Registered;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Throwable;

class RegisterController extends Controller
{
    /**
     * Show register form
     *
     * @return Factory|View|Application
     */
    public function create(): Factory|View|Application
    {
        $provinces = Province::jabodetabek()->get();
        $professions = Profession::all();

        $window_data = [
            'urlGetPostalCode' => route('front.postal-code'),
        ];

        return view('front.partner.register', compact('provinces', 'professions', 'window_data'));
    }

    /**
     * Process registration data
     *
     * @param PartnerRegisterRequest $partnerRegisterRequest
     * @return RedirectResponse
     */
    public function store(PartnerRegisterRequest $partnerRegisterRequest): RedirectResponse
    {
        $postalCode = PostalCode::whereProvinceCode($partnerRegisterRequest->get('province'))
            ->where('urban', $partnerRegisterRequest->get('urban'))
            ->first();

        try {
            DB::transaction(function () use($partnerRegisterRequest, $postalCode) {
                $user = User::create([
                    'email' => $partnerRegisterRequest->get('email'),
                    'password' => Hash::make($partnerRegisterRequest->get('password')),
                    'name' => $partnerRegisterRequest->get('name'),
                ]);

                $user->assignRole(UserRole::MEDICAL_PERSON()->value);

                

                $medicalPeople = new MedicalPeople();
                $medicalPeople->name = $partnerRegisterRequest->get('name');
                $medicalPeople->phone_number = $partnerRegisterRequest->get('phone_number');
                $medicalPeople->user_id = $user->id;
                $medicalPeople->profession_id = $partnerRegisterRequest->get('profession_id');
                $medicalPeople->id_type = $partnerRegisterRequest->get('id_type');
                $medicalPeople->id_number = $partnerRegisterRequest->get('id_number');
                $medicalPeople->postal_code_id = $postalCode->id;
                $medicalPeople->rt = $partnerRegisterRequest->get('rt');
                $medicalPeople->rw = $partnerRegisterRequest->get('rw');
                $medicalPeople->address = $partnerRegisterRequest->get('address');
                $medicalPeople->gender = $partnerRegisterRequest->get('gender');
                $medicalPeople->bod = $partnerRegisterRequest->get('bod');
                $medicalPeople->nationality = $partnerRegisterRequest->get('nationality');
                $medicalPeople->last_education = $partnerRegisterRequest->get('last_education');
                $medicalPeople->status = MedicalPeopleStatus::WAITING_VERIFIED();
                $medicalPeople->save();

                $medicalPeople->addMediaFromRequest('document_ktp')
                    ->withCustomProperties(['name' => 'KTP'])
                    ->toMediaCollection('documents');
                $medicalPeople->addMediaFromRequest('document_npwp')
                    ->withCustomProperties(['name' => 'NPWP'])
                    ->toMediaCollection('documents');
                $medicalPeople->addMediaFromRequest('document_str')
                    ->withCustomProperties(['name' => 'STR'])
                    ->toMediaCollection('documents');
                
                if($partnerRegisterRequest->file('document_sip') != NULL){
                    $medicalPeople->addMediaFromRequest('document_sip')
                    ->withCustomProperties(['name' => 'SIP'])
                    ->toMediaCollection('documents');
                }

                
                
                    $medicalPeople->addMediaFromRequest('document_certification')
                    ->withCustomProperties(['name' => 'Certification'])
                    ->toMediaCollection('documents');

                Auth::login($user);

                // Notify
                event(new Registered($user));
            });
        } catch (Throwable $e) {
            return redirect()->back()
                ->withInput($partnerRegisterRequest->input())
                ->withErrors($e->getMessage());
        }

        $partnerRegisterRequest->session()->flash('registered_email', $partnerRegisterRequest->get('email'));

        return redirect()->route('front.partner.register.success');
    }
}
