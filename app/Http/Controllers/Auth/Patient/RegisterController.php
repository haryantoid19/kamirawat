<?php

namespace App\Http\Controllers\Auth\Patient;

use App\Enums\UserRole;
use App\Http\Controllers\Controller;
use App\Http\Requests\PatientRegisterRequest;
use App\Models\Patient;
use App\Models\PostalCode;
use App\Models\Province;
use App\Models\User;
use DB;
use Hash;
use Illuminate\Auth\Events\Registered;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Throwable;

class RegisterController extends Controller
{
    /**
     * Show register form
     *
     * @return Factory|View|Application
     */
    public function create(): Factory|View|Application
    {
        $provinces = Province::jabodetabek()->get();

        $window_data = [
            'urlGetPostalCode' => route('front.postal-code'),
        ];

        return view('front.patient.register', compact('provinces', 'window_data'));
    }

    /**
     * Process registration data
     *
     * @param PatientRegisterRequest $patientRegisterRequest
     * @return RedirectResponse
     */
    public function store(PatientRegisterRequest $patientRegisterRequest): RedirectResponse
    {
        $postalCode = PostalCode::whereProvinceCode($patientRegisterRequest->get('province'))
            ->where('urban', $patientRegisterRequest->get('urban'))
            ->first();

        try {
            DB::transaction(function () use($patientRegisterRequest, $postalCode) {
                $user = User::create([
                    'email' => $patientRegisterRequest->get('email'),
                    'password' => Hash::make($patientRegisterRequest->get('password')),
                    'name' => $patientRegisterRequest->get('name'),
                ]);

                $user->assignRole(UserRole::PATIENT()->value);

                $patient = new Patient();
                $patient->user_id = $user->id;
                $patient->name = $patientRegisterRequest->get('name');
                $patient->id_type = $patientRegisterRequest->get('id_type');
                $patient->id_number = $patientRegisterRequest->get('id_number');
                $patient->phone_number = $patientRegisterRequest->get('phone_number');
                $patient->postal_code_id = $postalCode->id;
                $patient->rt = $patientRegisterRequest->get('rt');
                $patient->rw = $patientRegisterRequest->get('rw');
                $patient->bod = $patientRegisterRequest->get('bod');
                $patient->gender = $patientRegisterRequest->get('gender');
                $patient->address = $patientRegisterRequest->get('address');
                $patient->nationality = $patientRegisterRequest->get('nationality');
                $patient->is_main = true;
                $patient->save();

                event(new Registered($user));
            });
        } catch (Throwable $e) {
            return redirect()->back()
                ->withInput($patientRegisterRequest->input())
                ->withErrors($e->getMessage());
        }

        $patientRegisterRequest->session()->flash('registered_email', $patientRegisterRequest->get('email'));

        return redirect()->route('front.patient.register.success');
    }
}
