<?php

namespace App\Http\Controllers\Auth\Patient;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class RegisterSuccessController extends Controller
{
    /**
     * @param Request $request
     * @return View|Factory|RedirectResponse|Application
     */
    public function index(Request $request): View|Factory|RedirectResponse|Application
    {
        $email = $request->session()->get('registered_email');

        if (! $email) {
            return redirect()->route('front.index');
        }

        return view('front.patient.success', compact('email'));
    }
}
