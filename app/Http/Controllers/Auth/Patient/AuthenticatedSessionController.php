<?php

namespace App\Http\Controllers\Auth\Patient;

use App\Enums\UserRole;
use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use Auth;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class AuthenticatedSessionController extends Controller
{
    /**
     * Display the login view
     *
     * @return Factory|View|Application
     */
    public function create(): Factory|View|Application
    {
        return view('front.patient.login');
    }

    /**
     * Handle an incoming authentication request.
     *
     * @param LoginRequest $request
     * @return RedirectResponse
     * @throws ValidationException
     */
    public function store(LoginRequest $request): RedirectResponse
    {
        $request->authenticate();

        $user = Auth::user();

        if ($user->hasRole(UserRole::PATIENT()->value) === false) {
            Auth::logout();

            throw ValidationException::withMessages([
                'email' => __('auth.failed')
            ]);
        }

        $request->session()->regenerate();

        if ($continue = $request->get('continue')) {
            return redirect()->to($continue);
        }

        return redirect()->route('front.index');
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function destroy(Request $request): RedirectResponse
    {
        Auth::logout();

        if ($continue = $request->get('continue')) {
            return redirect()->route($continue);
        }

        return redirect()->route('front.index');
    }
}
