<?php

namespace App\Http\Controllers\Account;

use App\Http\Controllers\Controller;
use App\Http\Requests\Account\SettingsEmailRequest;
use App\Http\Requests\Account\SettingsInfoRequest;
use App\Http\Requests\Account\SettingsPasswordRequest;
use Auth;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

class SettingsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index(): View|Factory|Application
    {
        // get the default inner page
        return view('pages.account.settings.settings');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param SettingsInfoRequest $request
     * @return RedirectResponse
     */
    public function update(SettingsInfoRequest $request): RedirectResponse
    {
        $validated = $request->validate([
            'first_name' => 'required|string|max:255',
            'last_name'  => 'required|string|max:255',
        ]);

        Auth::user()->update($validated);

        return redirect()->intended('account/settings');
    }

    /**
     * Function for upload avatar image
     *
     * @param string $folder
     * @param string $key
     * @param string $validation
     *
     * @return false|string|null
     */
    public function upload(string $folder = 'images', string $key = 'avatar', string $validation = 'image|mimes:jpeg,png,jpg,gif,svg|max:2048|sometimes'): bool|string|null
    {
        request()->validate([$key => $validation]);

        $file = null;
        if (request()->hasFile($key)) {
            $file = Storage::disk('public')->putFile($folder, request()->file($key), 'public');
        }

        return $file;
    }

    /**
     * Function to accept request for change email
     *
     * @param SettingsEmailRequest $request
     * @return JsonResponse|RedirectResponse
     */
    public function changeEmail(SettingsEmailRequest $request): JsonResponse|RedirectResponse
    {
        Auth::user()->update(['email' => $request->input('email')]);

        if ($request->expectsJson()) {
            return response()->json($request->all());
        }

        return redirect()->intended('account/settings');
    }

    /**
     * Function to accept request for change password
     *
     * @param SettingsPasswordRequest $request
     * @return JsonResponse|RedirectResponse
     */
    public function changePassword(SettingsPasswordRequest $request): JsonResponse|RedirectResponse
    {
        // prevent change password for demo account
        if ($request->input('current_email') === 'demo@demo.com') {
            return redirect()->intended('account/settings');
        }

        Auth::user()->update(['password' => Hash::make($request->input('password'))]);

        if ($request->expectsJson()) {
            return response()->json($request->all());
        }

        return redirect()->intended('account/settings');
    }
}
