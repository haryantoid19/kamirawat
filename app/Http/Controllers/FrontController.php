<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;

class FrontController extends Controller
{
    public function index(): Factory|View|Application
    {
        if (config('kamirawat.under_construction_mode')) {
            return view('front.index-maintenance');
        } else {
            return view('front.index');
        }
    }
}
