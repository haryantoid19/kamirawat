<?php

namespace App\Http\Controllers\Front\Partner;

use App\Http\Controllers\Controller;
use App\Models\Bank;
use Auth;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;

class DashboardController extends Controller
{
    /**
     * @return Factory|View|Application
     */
    public function index(): Factory|View|Application
    {
        $medical_people = Auth::user()->medical_people;
        $medical_people->load(['bank_account']);
        $documents = $medical_people->getMedia('documents');
        $banks = Bank::get();

        return view('front.partner.dashboard', compact('medical_people','documents', 'banks'));
    }
}
