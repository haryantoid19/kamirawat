<?php

namespace App\Http\Controllers\Front\Partner;

use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class UpdateAvatarController extends Controller
{
    /**
     * Update
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function update(Request $request): RedirectResponse
    {
        $request->validate([
            'file' => 'required|file|max:2048',
        ]);

        $medical_people = Auth::user()->medical_people;
        $medical_people->avatar_path = $request->file('file')->store('partner-avatars');
        $medical_people->save();

        return redirect()->route('front.partner.dashboard');
    }
}
