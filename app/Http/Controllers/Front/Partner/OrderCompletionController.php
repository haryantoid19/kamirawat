<?php

namespace App\Http\Controllers\Front\Partner;

use App\Enums\BookingStatus;
use App\Enums\InvoiceStatus;
use App\Enums\PaymentMethodGateway;
use App\Enums\XenditType;
use App\Http\Controllers\Controller;
use App\Models\Booking;
use App\Models\Invoice;
use App\Models\InvoiceItem;
use App\Models\AdditionalMedicalService;
use App\Models\PaymentMethod;
use Auth;
use DB;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Throwable;
use Xendit\Xendit;

class OrderCompletionController extends Controller
{
    /**
     * Update
     *
     * @param Booking $booking
     * @param Request $request
     * @return JsonResponse
     * @throws Exception
     */
    public function update(Booking $booking, Request $request): JsonResponse
    {
        $request->validate([
            'handling' => 'required|max:200',
            'medical_record' => 'required|max:200',
            'additional_services' => 'required',
        ]);

        $booking->load(['invoice', 'invoice.payment_method']);
        $invoice = $booking->invoice;
        $payment_method = $invoice->payment_method;

        if (! $payment_method) {
            $payment_method_code = config('kamirawat.additional_service_payment_method');
            $payment_method = PaymentMethod::whereCode($payment_method_code)->firstOrFail();
        }

        $booking->status = BookingStatus::COMPLETED();
        $booking->finished_at = now();
        $booking->finished_by = Auth::id();
        $booking->handling = $request->get('handling');
        $booking->medical_record_summary = $request->get('medical_record');
        $booking->save();

        $additional_services = json_decode($request->get('additional_services'));

        $medicalServices = AdditionalMedicalService::published()
            ->whereIn('id', $additional_services)
            ->get();
        $total = $medicalServices->sum('price');

        $payment_link = null;
        $is_reload = true;
        $invoice_id = null;

        if (!empty($additional_services)) {
            try {
                DB::transaction(function () use ($booking, $medicalServices, $total, &$payment_link, &$invoice_id, $payment_method) {
                    $invoice = new Invoice();
                    $invoice->booking_id = $booking->id;
                    $invoice->invoice_number = Invoice::generateIdNumber();
                    $invoice->total = $total;
                    $invoice->payment_method_id = $booking->invoice->payment_method_id;
                    $invoice->status = InvoiceStatus::ACTIVE();
                    $invoice->expired_at = now()->addHour();
                    $invoice->save();
                    $invoice_id = $invoice->id;

                    foreach ($medicalServices as $medicalService) {
                        $invoiceItem = new InvoiceItem();
                        $invoiceItem->invoice_id = $invoice->id;
                        $invoiceItem->name = $medicalService->name;
                        $invoiceItem->price = $medicalService->price;
                        $invoiceItem->itemable()->associate($medicalService);
                        $invoiceItem->save();
                    }

                    if ($payment_method->gateway->is(PaymentMethodGateway::XENDIT())) {
                        Xendit::setApiKey(config('services.xendit.api_key'));
                        $params = [
                            'external_id' => $invoice->invoice_number,
                            'payer_email' => $booking->patient->user->email,
                            'description' => 'Additional Services: ' . $invoice->invoice_number,
                            'amount' => $total,
                            'invoice_duration' => 60 * 60,
                        ];
                        $createInvoice = \Xendit\Invoice::create($params);
                        if (!is_array($createInvoice) || ! array_key_exists('invoice_url', $createInvoice)) {
                            throw new Exception("Failed to create xendit invoice");
                        }

                        $invoice->xendit_id = $createInvoice['id'];
                        $invoice->xendit_invoice_url = $createInvoice['invoice_url'];
                        $invoice->xendit_type = XenditType::INVOICE();
                        $invoice->save();

                        $payment_link = $invoice->xendit_invoice_url;
                    }
                });
            } catch (Throwable $e) {
                return response()->json(['message' => $e->getMessage()], 500);
            }
        }

        if (!empty($additional_services)) {
            $is_reload = false;
        }

        $request->session()->flash('message', 'Berhasil menyelesaikan pesanan');

        $payment_method = $payment_method->only(['gateway', 'description']);
        return response()->json(compact('is_reload', 'payment_link', 'invoice_id', 'payment_method'));
    }

}
