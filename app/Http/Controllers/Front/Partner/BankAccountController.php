<?php

namespace App\Http\Controllers\Front\Partner;

use App\Http\Controllers\Controller;
use App\Models\BankAccount;
use Auth;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class BankAccountController extends Controller
{
    public function update(Request $request): RedirectResponse
    {
        $request->session()->flash('bank_account_in_validation', true);

        $request->validate([
            'name' => 'required|max:255',
            'number' => 'required|max:255',
            'bank' => 'required|exists:App\Models\Bank,id'
        ]);

        $user = Auth::user();
        $medical_people = $user->medical_people;
        $bank_account = $medical_people->bank_account ?: new BankAccount();
        $bank_account->name = $request->get('name');
        $bank_account->number = $request->get('number');
        $bank_account->bank_id = $request->get('bank');
        $medical_people->bank_account()->save($bank_account);

        $request->session()->forget('bank_account_in_validation');

        return redirect()->route('front.partner.dashboard')->with([
            'alert' => 'message',
            'alert-type' => 'success',
            'message' => 'Perubahan nomor rekening sudah disimpan.',
        ]);
    }
}
