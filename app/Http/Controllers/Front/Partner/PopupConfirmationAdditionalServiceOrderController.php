<?php

namespace App\Http\Controllers\Front\Partner;

use App\Http\Controllers\Controller;
use App\Models\Booking;
use App\Models\AdditionalMedicalService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class PopupConfirmationAdditionalServiceOrderController extends Controller
{
    /**
     * @param Booking $booking
     * @param Request $request
     * @return JsonResponse
     */
    public function show(Booking $booking, Request $request): JsonResponse
    {
        $additional_services = AdditionalMedicalService::published()
            ->whereIn('id', $request->get('additional_services'))
            ->get();

        $total = $additional_services->sum('price');

        $html = view('front.partner._order-modal-confirmation-additional-service', compact('booking', 'additional_services', 'total'))->render();

        return response()->json(compact('html'));
    }
}
