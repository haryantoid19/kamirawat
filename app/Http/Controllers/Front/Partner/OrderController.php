<?php

namespace App\Http\Controllers\Front\Partner;

use App\Enums\BookingStatus;
use App\Events\BookingAccepted;
use App\Http\Controllers\Controller;
use App\Models\Booking;
use Auth;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * @param string $type
     * @return View|Factory|RedirectResponse|Application
     */
    public function index(string $type): View|Factory|RedirectResponse|Application
    {
        $types = ['new', 'ongoing', 'complete'];
        if (!in_array($type, $types)) {
            return redirect()->route('front.partner.dashboard');
        }

        $medicalPeople = Auth::user()->medical_people;
        $query = Booking::whereMedicalPeopleId($medicalPeople->id);
        $query->with(['patient', 'patient.user', 'invoices']);
        if ($type === "new") {
            $query->where('status', BookingStatus::ASSIGNED()->value);
            $query->oldest('assigned_at');
        } else if ($type === "ongoing") {
            $query->where('status', BookingStatus::ACCEPTED()->value);
            $query->oldest('assigned_at');
        } else if ($type === "complete") {
            $query->whereIn('status', [
                BookingStatus::COMPLETED()->value,
                BookingStatus::REJECTED()->value,
                BookingStatus::CANCELED()->value,
            ]);
            $query->latest('finished_at');
        }
        $bookings = $query->get();

        return view("front.partner.order-$type", compact('bookings'));
    }

    /**
     * @param Booking $booking
     * @return JsonResponse
     * @throws AuthorizationException
     */
    public function show(Booking $booking): JsonResponse
    {
        $this->authorize('view', $booking);

        $booking->load(['invoices', 'patient']);
        $booking_number = $booking->booking_number;
        $history_bookings = Booking::whereStatus(BookingStatus::COMPLETED())->where('patient_id', $booking->patient_id)->get();
        $html = view('front.partner._order-modal-show', compact('booking', 'history_bookings'))->render();
        return response()->json(compact('html', 'booking_number'));
    }

    /**
     * @param Booking $booking
     * @param Request $request
     * @return RedirectResponse
     */
    public function update(Booking $booking, Request $request): RedirectResponse
    {
        $request->validate([
            'approval' => 'required|in:approve,reject'
        ]);

        if ($request->get('approval') === "reject") {
            $message = 'Berhasil menolak pesanan';

            $booking->status = BookingStatus::REJECTED();
            $booking->rejected_at = now();
            $booking->rejected_by = Auth::id();
            $booking->save();
        } else {
            $message = 'Berhasil menerima pesanan';

            $booking->status = BookingStatus::ACCEPTED();
            $booking->save();

            event(new BookingAccepted($booking));
        }

        return redirect()->back()->with(compact('message'));
    }
}
