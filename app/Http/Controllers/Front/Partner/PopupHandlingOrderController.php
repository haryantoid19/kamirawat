<?php

namespace App\Http\Controllers\Front\Partner;

use App\Http\Controllers\Controller;
use App\Models\Booking;
use Illuminate\Http\JsonResponse;

class PopupHandlingOrderController extends Controller
{
    /**
     * Show
     *
     * @param Booking $booking
     * @return JsonResponse
     */
    public function show(Booking $booking): JsonResponse
    {
        $html = view('front.partner._order-modal-medical-handling', compact('booking'))->render();

        return response()->json(compact('html'));
    }
}
