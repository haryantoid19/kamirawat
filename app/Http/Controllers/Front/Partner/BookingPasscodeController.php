<?php

namespace App\Http\Controllers\Front\Partner;

use App\Http\Controllers\Controller;
use App\Models\Booking;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class BookingPasscodeController extends Controller
{
    /**
     * @throws AuthorizationException
     */
    public function store(Booking $booking, Request $request): JsonResponse
    {
        $this->authorize('input_passcode', $booking);

        $request->validate([
            'passcode' => [
                'required',
                'size:6',
                function($attr, $value, $fail) use($booking) {
                    if ($value !== $booking->passcode) {
                        $fail(__('validation.exists', ['attribute' => $attr]));
                    }
                }
            ],
        ]);

        if ($booking->input_passcode_at === null) {
            $booking->input_passcode_at = now();
            $booking->saveQuietly();
        }

        return response()->json([]);
    }
}
