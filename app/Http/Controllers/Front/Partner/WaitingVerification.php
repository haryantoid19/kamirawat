<?php

namespace App\Http\Controllers\Front\Partner;

use App\Enums\MedicalPeopleStatus;
use App\Enums\UserRole;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class WaitingVerification extends Controller
{
    /**
     * @return Factory|View|Application|RedirectResponse
     */
    public function index(): Factory|View|Application|RedirectResponse
    {
        if (Auth::user()->hasRole(UserRole::MEDICAL_PERSON()->value) === false) {
            abort(403);
        }

        $email = Auth::user()->email;

        return Auth::user()->medical_people->status->is(MedicalPeopleStatus::ACCEPTED())
            ? redirect()->route('front.partner.dashboard')
            : view('front.auth.partner-waiting-verification', compact('email'));
    }
}
