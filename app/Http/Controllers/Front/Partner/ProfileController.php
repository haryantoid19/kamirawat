<?php

namespace App\Http\Controllers\Front\Partner;

use App\Enums\IdType;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class ProfileController extends Controller
{
    public function update(Request $request)
    {
        $user = \Auth::user();
        $user->load('medical_people');
        $medical_people = $user->medical_people;

        $rule_id_number = ['required', Rule::unique('medical_people')->ignore($medical_people->id, 'id')];
        if ($medical_people->id_type->is(IdType::KTP())) {
            $rule_id_number[] = 'string';
            $rule_id_number[] = 'size:16';
        } else {
            $rule_id_number[] = 'max:40';
        }

        $request->session()->flash('profile_in_validation', true);

        $request->validate([
            'name' => 'required|max:255',
            'id_number' => $rule_id_number,
            'address' => 'required|max:255',
            'phone_number' => ['required', 'max:255', Rule::unique('medical_people')->ignore($medical_people->id, 'id')],
        ], [], [
            'name' => 'Nama Pasien',
            'id_number' => 'Nomor ID',
            'address' => 'Alamat',
            'phone_number' => 'Nomor HP',
        ]);

        $medical_people->name = $request->get('name');
        $medical_people->id_number = $request->get('id_number');
        $medical_people->address = $request->get('address');
        $medical_people->phone_number = $request->get('phone_number');
        $medical_people->save();

        $request->session()->forget('profile_in_validation');

        return redirect()->route('front.partner.dashboard')->with([
            'alert' => 'message',
            'alert-type' => 'success',
            'message' => 'Perubahan profil sudah disimpan.',
        ]);
    }
}
