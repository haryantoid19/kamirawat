<?php

namespace App\Http\Controllers\Front\Partner;

use App\Http\Controllers\Controller;
use App\Models\Booking;
use Illuminate\Http\JsonResponse;

class MapOrderController extends Controller
{
    /**
     * @param Booking $booking
     * @return JsonResponse
     */
    public function show(Booking $booking): JsonResponse
    {
        $booking->load(['patient']);
        $booking_number = $booking->booking_number;
        $html = view('front.partner._order-modal-map', compact('booking'))->render();
        return response()->json(compact('html', 'booking_number'));
    }
}
