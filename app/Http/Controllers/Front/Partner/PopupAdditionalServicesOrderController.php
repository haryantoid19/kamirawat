<?php

namespace App\Http\Controllers\Front\Partner;

use App\Http\Controllers\Controller;
use App\Models\Booking;
use App\Models\AdditionalMedicalService;
use Illuminate\Http\JsonResponse;

class PopupAdditionalServicesOrderController extends Controller
{

    /**
     * @param Booking $booking
     * @return JsonResponse
     */
    public function show(Booking $booking): JsonResponse
    {
        $additionalMedicalServices = AdditionalMedicalService::published()->get();
        $html = view('front.partner._order-modal-additional-service', compact('booking', 'additionalMedicalServices'))->render();

        return response()->json(compact('html'));
    }
}
