<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\PostalCode;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class PostalCodeController extends Controller
{
    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $request->validate([
            'method' => 'required|in:city,subdistrict,urban',
            'province_code' => 'required',
        ]);

        if ($request->get('method') === "city") {
            $cities = PostalCode::getCities($request->get('province_code'))->pluck('city');

            return response()->json(compact('cities'));
        }

        if ($request->get('method') === "subdistrict") {
            $payload = PostalCode::whereProvinceCode($request->get('province_code'))
                ->where('city', $request->get('payload'))
                ->first();
            if (! $payload) {
                $message = 'Invalid payload';
                return response()->json(compact('message'), 406);
            }

            $subdistricts = PostalCode::getSubdistricts($payload)->pluck('sub_district');

            return response()->json(compact('subdistricts'));
        }

        if ($request->get('method') === "urban") {
            $payload = PostalCode::whereProvinceCode($request->get('province_code'))
                ->where('sub_district', $request->get('payload'))
                ->first();
            if (! $payload) {
                $message = 'Invalid payload';
                return response()->json(compact('message'), 406);
            }

            $urbans = PostalCode::getUrbanList($payload)->pluck('urban');

            return response()->json(compact('urbans'));
        }

        return response()->json([
            'message' => 'Invalid',
        ], 406);
    }
}
