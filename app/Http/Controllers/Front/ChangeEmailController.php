<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\User;
use Auth;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Str;

class ChangeEmailController extends Controller
{
    public function store(Request $request): RedirectResponse
    {
        $request->session()->flash('email_in_validation', true);

        $user = Auth::user();

        $request->validate([
            'email' => [
                'required',
                'email',
                'max:255',
                Rule::unique('users')->ignore($user->id),
            ],
        ]);

        $new_email = $request->get('email');

        $route = 'front.patient.dashboard';
        if ($user->medical_people) {
            $route = 'front.partner.dashboard';
        }

        if ($new_email === $user->email) {
            return redirect()->route($route);
        }

        $user->new_email = $new_email;
        $user->token_new_email = Str::random(60);
        $user->save();

        $user->sendChangeEmailVerificationNotification();

        $request->session()->forget('email_in_validation');

        return redirect()->route($route);
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function update(Request $request): RedirectResponse
    {
        $user = User::whereTokenNewEmail($request->get('token'))->first();
        if (! $user) {
            abort(403);
        }

        if ($user->isNot(Auth::user())) {
            abort(403);
        }

        $user->email = $user->new_email;
        $user->new_email = null;
        $user->token_new_email = null;
        $user->save();

        return redirect()->route($user->medical_people ? 'front.partner.dashboard' : 'front.patient.dashboard');
    }
}
