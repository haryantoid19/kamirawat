<?php

namespace App\Http\Controllers\Front\Patient;

use App\Http\Controllers\Controller;
use App\Models\MedicalService;
use App\Models\MedicalServiceCategory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class BookingInvoiceTableController extends Controller
{
    public function index(MedicalServiceCategory $medicalServiceCategory, Request $request): JsonResponse
    {
        $medicalService = MedicalService::whereCategoryId($medicalServiceCategory->id)->findOrFail($request->get("service"));
        $shippingFee = config('kamirawat.shipping_fee');
        $total = $medicalService->price + $shippingFee;
        $html = view('front.patient.booking-invoice-table', compact(
            'medicalService',
            'shippingFee',
            'total'
        ))->render();

        return response()->json(compact('html'));
    }
}
