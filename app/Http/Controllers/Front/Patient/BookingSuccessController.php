<?php

namespace App\Http\Controllers\Front\Patient;

use App\Http\Controllers\Controller;
use App\Models\PaymentMethod;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class BookingSuccessController extends Controller
{
    /**
     * Show payment success
     *
     * @param Request $request
     * @return View|Factory|RedirectResponse|Application
     */
    public function index(Request $request): View|Factory|RedirectResponse|Application
    {
        $va_number = $request->session()->get('va_number');
        $payment_method = $request->session()->get('payment_method');

        if (! $payment_method) {
            return redirect()->route('front.index');
        }

        $payment_method = PaymentMethod::published()->find($payment_method);
        if (! $payment_method) {
            return redirect()->route('front.index');
        }

        return view('front.patient.booking-success')
            ->with(compact('va_number', 'payment_method'));
    }
}
