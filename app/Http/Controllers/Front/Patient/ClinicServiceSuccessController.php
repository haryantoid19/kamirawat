<?php

namespace App\Http\Controllers\Front\Patient;

use App\Http\Controllers\Controller;
use App\Models\MedicalServiceCategory;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;

class ClinicServiceSuccessController extends Controller
{
    /**
     * Index
     *
     * @param MedicalServiceCategory $medicalServiceCategory
     * @return View|Factory|Application
     */
    public function index(MedicalServiceCategory $medicalServiceCategory): View|Factory|Application
    {
        return view('front.patient.clinic-service-success', compact('medicalServiceCategory'));
    }
}
