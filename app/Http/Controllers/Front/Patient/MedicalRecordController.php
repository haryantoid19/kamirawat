<?php

namespace App\Http\Controllers\Front\Patient;

use App\Http\Controllers\Controller;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;

class MedicalRecordController extends Controller
{
    /**
     * @return Factory|View|Application
     */
    public function index(): Factory|View|Application
    {
        return view('front.patient.medical-record');
    }

    /**
     * @return Factory|View|Application
     */
    public function show(): Factory|View|Application
    {
        return view('front.patient.medical-record-detail');
    }
}
