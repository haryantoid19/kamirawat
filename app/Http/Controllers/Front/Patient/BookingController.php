<?php

namespace App\Http\Controllers\Front\Patient;

use App\Enums\BookingStatus;
use App\Enums\Gender;
use App\Enums\PaymentMethodGateway;
use App\Enums\PaymentMethodStatus;
use App\Enums\XenditType;
use App\Events\BookingCreated;
use App\Http\Controllers\Controller;
use App\Models\Booking;
use App\Models\Invoice;
use App\Models\InvoiceItem;
use App\Models\MedicalService;
use App\Models\MedicalServiceCategory;
use App\Models\Patient;
use App\Models\PaymentMethod;
use Auth;
use DB;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Log;
use Throwable;
use Xendit\VirtualAccounts;
use Xendit\Xendit;

class BookingController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     */
    public function index(): Factory|View|Application
    {
        $ongoing_bookings = Booking::with(['invoices', 'invoices.payment_method'])->whereHas('patient', function(Builder $builder) {
            $builder->where('user_id', Auth::id());
        })->with(['invoice'])
            ->whereIn('status', [BookingStatus::WAITING_PAYMENT(), BookingStatus::PAID(), BookingStatus::ASSIGNED(), BookingStatus::ACCEPTED()])
            ->latest('created_at')->paginate(5, ['*'], 'ongoing');

        $completed_bookings = Booking::whereHas('patient', function(Builder $builder) {
            $builder->where('user_id', Auth::id());
        })->with(['invoice'])
            ->whereIn('status', [BookingStatus::COMPLETED(), BookingStatus::CANCELED(), BookingStatus::REJECTED()])
            ->latest('finished_at')->paginate(5, ['*'], 'completed');

        return view('front.patient.booking-history', compact('completed_bookings', 'ongoing_bookings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param MedicalServiceCategory $medicalServiceCategory
     * @return Factory|View|Application
     */
    public function create(MedicalServiceCategory $medicalServiceCategory): Factory|View|Application
    {
        $medical_services = MedicalService::whereCategoryId($medicalServiceCategory->id)->published()->get();
        $user = Auth::user();

        $payment_methods = PaymentMethod::published()->get();

        return view('front.patient.booking',
            compact(
                'medical_services',
                'user',
                'payment_methods',
                'medicalServiceCategory'
            )
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param MedicalServiceCategory $medicalServiceCategory
     * @param Request $request
     * @return JsonResponse
     */
    public function store(MedicalServiceCategory $medicalServiceCategory, Request $request): JsonResponse
    {
        $request->validate([
            'name' => 'required|max:255',
            'phone_number' => 'required|max:40',
            'self' => ['required', Rule::in(['true', 'false'])],
            'bod' => 'required|date',
            'gender' => ['required', Rule::in(Gender::getValues())],
            'address' => 'required|max:255',
            'service' => [
                'required',
                Rule::exists('medical_services', 'id')->where('category_id', $medicalServiceCategory->id),
            ],
            'history_of_disease_allergies' => 'max:255',
            'main_symptom' => 'max:255',
            'special_request' => 'max:255',
            'latlong' => 'required',
            'payment_method' => ['required', Rule::exists('payment_methods', 'id')->where('status', PaymentMethodStatus::PUBLISH()->value)],
        ], [], [
            'name' => 'Nama',
            'phone_number' => 'No. Telepon',
            'bod' => 'Tanggal Lahir',
            'gender' => 'Jenis Kelamin',
            'address' => 'Alamat Domisili',
            'service' => 'Layanan',
            'history_of_disease_allergies' => 'Riwayat Penyakit & Alergi',
            'main_symptom' => 'Keluhan Utama',
            'special_request' => 'Permintaan Khusus',
            'latlong' => 'Titik Lokasi',
        ]);

        $user_patient = Auth::user()->patient;

        if ($request->get('self') === "true") {
            $patient = $user_patient;
        } else {
            $patient = new Patient();
            $patient->user_id = Auth::id();
            $patient->postal_code_id = $user_patient->postal_code_id;
            $patient->name = $request->get('name');
            $patient->phone_number = $request->get('phone_number');
            $patient->bod = $request->get('bod');
            $patient->gender = $request->get('gender');
        }

        if (! $patient) {
            $message = 'Invalid data';
            return response()->json(compact('message'), 406);
        }

        $latlong = explode(',', $request->get('latlong'));
        $latitude = $latlong[0];
        $longitude = $latlong[1];

        $patient->address = $request->get('address');
        $patient->latitude = $latitude;
        $patient->longitude = $longitude;

        $medicalService = MedicalService::find($request->get('service'));

        $payment_method = PaymentMethod::published()->find($request->get('payment_method'));

        $booking = null;

        try {
            DB::transaction(function () use ($request, $medicalService, $patient, $payment_method, &$booking) {
                $patient->save();

                $shipping_fee = config('kamirawat.shipping_fee');
                $total = $medicalService->price + $shipping_fee;

                $booking = new Booking();
                $booking->patient_id = $patient->id;
                $booking->total = $total;
                $booking->history_of_disease_allergies = $request->get('history_of_disease_allergies');
                $booking->main_symptom = $request->get('main_symptom');
                $booking->special_request = $request->get('special_request');
                $booking->medical_service_id = $medicalService->id;
                $booking->save();

                $invoice = new Invoice();
                $invoice->payment_method_id = $payment_method->id;
                $invoice->total = $total;
                $invoice->invoice_number = Invoice::generateIdNumber();
                $invoice->expired_at = now()->addHour();
                $invoice->is_main = true;
                $invoice->shipping_fee = $shipping_fee;
                $booking->invoice()->save($invoice);

                $invoiceItems = new InvoiceItem();
                $invoiceItems->invoice_id = $invoice->id;
                $invoiceItems->name = "Medical Service: " . $medicalService->name;
                $invoiceItems->price = $medicalService->price;
                $invoiceItems->itemable()->associate($medicalService);
                $invoice->items()->save($invoiceItems);

                if ($payment_method->gateway->is(PaymentMethodGateway::XENDIT())) {
                    Xendit::setApiKey(config('services.xendit.api_key'));
                    $params = [
                        'external_id' => $invoice->invoice_number,
                        'bank_code' => $request->get('payment_method'),
                        'name' => preg_replace("/[^a-zA-Z\w ]+/", "", $patient->name),
                        'expected_amount' => $total,
                        'is_closed' => true,
                        'expiration_date' => $invoice->expired_at->toIso8601String(),
                    ];

                    $xenditInvoice = VirtualAccounts::create($params);
                    if (!is_array($xenditInvoice) || ! array_key_exists('account_number', $xenditInvoice)) {
                        throw new Exception("Failed to create xendit virtual account");
                    }
                    $invoice->xendit_id = $xenditInvoice['id'];
                    $invoice->xendit_type = XenditType::VIRTUAL_ACCOUNT();
                    $invoice->xendit_va_number = $xenditInvoice['account_number'];
                    $invoice->saveQuietly();

                    $request->session()->flash('va_number', $invoice->xendit_va_number);
                }

                $request->session()->flash('payment_method', $payment_method->id);
            });
        } catch (Throwable $e) {
            Log::error('failed submit booking', ['message' => $e->getMessage()]);
            $message = "Failed to submit your booking.";
            return response()->json(compact('message'), 500);
        }

        if ($booking instanceof Booking) {
            event(new BookingCreated($booking));
        }

        $redirect_url = route('front.patient.booking.success');

        return response()->json(compact('redirect_url'));
    }
}
