<?php

namespace App\Http\Controllers\Front\Patient;

use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class UpdateAvatarController extends Controller
{
    /**
     * Update
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function update(Request $request): RedirectResponse
    {
        $request->validate([
            'file' => 'required|file|max:2048',
        ]);

        $patient = Auth::user()->patient;
        $patient->avatar_path = $request->file('file')->store('partner-avatars');
        $patient->save();

        return redirect()->route('front.patient.dashboard');
    }
}
