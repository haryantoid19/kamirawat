<?php

namespace App\Http\Controllers\Front\Patient;

use App\Enums\IdType;
use App\Http\Controllers\Controller;
use App\Models\User;
use Auth;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Str;

class ProfileController extends Controller
{
    public function update(Request $request): RedirectResponse
    {
        $user = Auth::user();
        $user->load('patients');
        $patient = $user->patient;

        $rule_id_number = ['required', Rule::unique('patients')->ignore($patient->id, 'id')];
        if ($patient->id_type->is(IdType::KTP())) {
            $rule_id_number[] = 'string';
            $rule_id_number[] = 'size:16';
        } else {
            $rule_id_number[] = 'max:40';
        }

        $request->session()->flash('profile_in_validation', true);

        $request->validate([
            'name' => 'required|max:255',
            'id_number' => $rule_id_number,
            'address' => 'required|max:255',
            'phone_number' => ['required', 'max:255', Rule::unique('patients')->ignore($patient->id, 'id')],
        ], [], [
            'name' => 'Nama Pasien',
            'id_number' => 'Nomor ID',
            'address' => 'Alamat',
            'phone_number' => 'Nomor HP',
        ]);

        $patient->name = $request->get('name');
        $patient->id_number = $request->get('id_number');
        $patient->address = $request->get('address');
        $patient->phone_number = $request->get('phone_number');
        $patient->save();

        $request->session()->forget('profile_in_validation');

        return redirect()->route('front.patient.dashboard')->with([
            'alert' => 'message',
            'alert-type' => 'success',
            'message' => 'Perubahan profil sudah disimpan.',
        ]);
    }
}
