<?php

namespace App\Http\Controllers\Front\Patient;

use App\Enums\BookingStatus;
use App\Http\Controllers\Controller;
use App\Models\Booking;
use Auth;
use Illuminate\Http\RedirectResponse;

class BookingCancelController extends Controller
{
    public function update(Booking $booking): RedirectResponse
    {
        $booking->status = BookingStatus::CANCELED();
        $booking->canceled_at = now();
        $booking->canceled_by = Auth::id();
        $booking->save();

        return back()->with([
            'message' => 'Berhasil membatalkan pesanan',
        ]);
    }
}
