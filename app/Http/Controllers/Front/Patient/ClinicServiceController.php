<?php

namespace App\Http\Controllers\Front\Patient;

use App\Enums\Gender;
use App\Http\Controllers\Controller;
use App\Models\ClinicBooking;
use App\Models\MedicalService;
use App\Models\MedicalServiceCategory;
use App\Models\Patient;
use Auth;
use DB;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Log;
use Throwable;

class ClinicServiceController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @param MedicalServiceCategory $medicalServiceCategory
     * @return Factory|View|Application
     */
    public function create(MedicalServiceCategory $medicalServiceCategory): Factory|View|Application
    {
        $medical_services = MedicalService::whereCategoryId($medicalServiceCategory->id)->published()->get();
        $user = Auth::user();

        return view('front.patient.clinic-service',
            compact(
                'medical_services',
                'user',
                'medicalServiceCategory'
            )
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param MedicalServiceCategory $medicalServiceCategory
     * @param Request $request
     * @return JsonResponse
     */
    public function store(MedicalServiceCategory $medicalServiceCategory, Request $request): JsonResponse
    {
        $request->validate([
            'name' => 'required|max:255',
            'phone_number' => 'required|max:40',
            'self' => ['required', Rule::in(['true', 'false'])],
            'bod' => 'required|date',
            'gender' => ['required', Rule::in(Gender::getValues())],
            'address' => 'required|max:255',
            'service' => [
                'required',
                Rule::exists('medical_services', 'id')->where('category_id', $medicalServiceCategory->id),
            ],
            'history_of_disease_allergies' => 'max:255',
            'main_symptom' => 'max:255',
            'special_request' => 'max:255',
        ], [], [
            'name' => 'Nama',
            'phone_number' => 'No. Telepon',
            'bod' => 'Tanggal Lahir',
            'gender' => 'Jenis Kelamin',
            'address' => 'Alamat Domisili',
            'service' => 'Layanan',
            'history_of_disease_allergies' => 'Riwayat Penyakit & Alergi',
            'main_symptom' => 'Keluhan Utama',
            'special_request' => 'Permintaan Khusus',
        ]);

        $user_patient = Auth::user()->patient;

        if ($request->get('self') === "true") {
            $patient = $user_patient;
        } else {
            $patient = new Patient();
            $patient->user_id = Auth::id();
            $patient->name = $request->get('name');
            $patient->phone_number = $request->get('phone_number');
            $patient->bod = $request->get('bod');
            $patient->gender = $request->get('gender');
        }

        if (! $patient) {
            $message = 'Invalid data';
            return response()->json(compact('message'), 406);
        }

        $patient->address = $request->get('address');

        $medicalService = MedicalService::whereCategoryId($medicalServiceCategory->id)->find($request->get('service'));
       
        try {
            DB::transaction(function () use ($request, $medicalService, $patient) {
                $patient->save();

                $booking = new ClinicBooking();
                $booking->patient_id = $patient->id;
                $booking->name = $request->get('name');
                $booking->phone_number = $request->get('phone_number');
                $booking->bod = $request->get('bod');
                $booking->gender = $request->get('gender');
                $booking->address = $request->get('address');
                $booking->history_of_disease_allergies = $request->get('history_of_disease_allergies');
                $booking->main_symptom = $request->get('main_symptom');
                $booking->special_request = $request->get('special_request');
                $booking->medical_service_id = $medicalService->id;
                $booking->save();
            });
        } catch (Throwable $e) {
            Log::error('failed submit booking', ['message' => $e->getMessage()]);
            $message = "Failed to submit your booking.";
            return response()->json(compact('message'), 500);
        }

        $redirect_url = route('front.patient.clinic-service.success', $medicalServiceCategory->id);

        return response()->json(compact('redirect_url'));
    }
}
