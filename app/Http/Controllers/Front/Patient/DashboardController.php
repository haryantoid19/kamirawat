<?php

namespace App\Http\Controllers\Front\Patient;

use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;

class DashboardController extends Controller
{
    /**
     * @return Factory|View|Application
     */
    public function index(): Factory|View|Application
    {
        $user = Auth::user();
        $user->load('patients');
        $patient = $user->patient;

        return view('front.patient.dashboard', compact('patient'));
    }
}
