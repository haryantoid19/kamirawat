<?php

namespace App\Http\Controllers;

use App\Enums\BookingStatus;
use App\Enums\InvoiceStatus;
use App\Events\BookingPaid;
use App\Models\Invoice;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class XenditCallbackController extends Controller
{
    public function update(Request $request): JsonResponse
    {
        $invoice = Invoice::whereInvoiceNumber($request->get('external_id'))->with(['booking'])->first();
        $invoice->paid_at = now();
        $invoice->status = InvoiceStatus::PAID();
        $invoice->save();

        if ($invoice->is_main) {
            $booking = $invoice->booking;
            $booking->status = BookingStatus::PAID();
            $booking->save();

            event(new BookingPaid($booking));
        }

        return response()->json([]);
    }
}
