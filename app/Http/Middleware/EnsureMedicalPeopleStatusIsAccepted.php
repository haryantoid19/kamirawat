<?php

namespace App\Http\Middleware;

use App\Enums\MedicalPeopleStatus;
use App\Enums\UserRole;
use App\Models\MedicalPeople;
use Closure;
use Illuminate\Http\Request;
use Redirect;
use URL;

class EnsureMedicalPeopleStatusIsAccepted
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next): mixed
    {
        if ( ! $request->user() ||
            $request->user()->hasRole(UserRole::MEDICAL_PERSON()->value)
        ) {
            /** @var MedicalPeople $medical_people */
            $medical_people = $request->user()->medical_people()->first();

            if (! $medical_people || $medical_people->status->isNot(MedicalPeopleStatus::ACCEPTED())) {
                return request()->expectsJson()
                    ? abort(403, 'Your status is not accepted')
                    : Redirect::guest(URL::route('front.partner.waiting-verification'));
            }
        }

        return $next($request);
    }
}
