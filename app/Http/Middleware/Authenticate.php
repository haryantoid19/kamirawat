<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Http\Request;

class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  Request  $request
     * @return string|null
     */
    protected function redirectTo($request): ?string
    {
        if (! $request->expectsJson()) {
            // By parameter
            if ($request->get('patient')) {
                return route('front.patient.login');
            } else if ($request->get('partner')) {
                return route('front.partner.login');
            }

            // By Segment
            $segment = $request->segment(1);
            if ($segment === "theteam") {
                return route('theteam.login');
            } else if ($segment === "partner") {
                return route('front.partner.login');
            } else {
                return route('front.patient.login');
            }
        }

        return null;
    }
}
