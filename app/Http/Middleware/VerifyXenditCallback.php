<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class VerifyXenditCallback
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next): mixed
    {
        if (! $request->hasHeader('x-callback-token')) {
            abort(403, 'Unknown token');
        }

        if ($request->header('x-callback-token') !== config('services.xendit.callback_token')) {
            abort(403, 'Invalid token');
        }

        return $next($request);
    }
}
