<?php

namespace App\Http\Middleware;

use App\Enums\UserRole;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @param  string|null  ...$guards
     *
     * @return mixed
     */
    public function handle(Request $request, Closure $next, ...$guards): mixed
    {
        $segment = $request->segment(1);
        if ($segment === "theteam") {
            if (Auth::guard("theteam")->check()) {
                return redirect()->route('dashboard');
            }
        }

        if (Auth::guard('web')->check()) {
            $user = Auth::user();

            if ($user->hasRole(UserRole::PATIENT()->value)) {
                return redirect()->route('front.patient.dashboard');
            } else if ($user->hasRole(UserRole::MEDICAL_PERSON()->value)) {
                return redirect()->route('front.partner.dashboard');
            } else {
                return redirect()->route('front');
            }
        }

        return $next($request);
    }
}
