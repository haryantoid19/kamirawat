<?php

namespace App\Exports;

use Faker\Core\Number;
use Illuminate\Support\Carbon;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithMapping;
use PhpOffice\PhpSpreadsheet\Shared\Date;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Yajra\DataTables\Exports\DataTablesCollectionExport;

class MedicalPeoplesExport extends DataTablesCollectionExport implements WithMapping, WithColumnFormatting, ShouldAutoSize
{
    public function headings(): array
    {
        return [
            'Medical Number',
            'Name',
            'Email',
            'ID Type',
            'ID Number',
            'Phone number',
            'Birth of date',
            'Gender',
            'Province',
            'City',
            'Kecamatan',
            'Kode pos',
            'RT',
            'RW',
            'Address',
            'Nationality',
            'Status'
        ];
    }

    public function map($row): array
    {
//        dd($row);
        return [
            $row['medical_number'],
            $row['name'],
            $row['user']['email'],
            $row['id_type'],
            $row['id_number'],
            $row['phone_number'],
            Date::dateTimeToExcel(Carbon::parse($row['bod'])),
            $row['gender'],
            $row['postal_code']['province']['province_name'],
            $row['postal_code']['city'],
            $row['postal_code']['sub_district'],
            $row['postal_code']['postal_code'],
            $row['rt'],
            $row['rw'],
            $row['address'],
            $row['nationality'],
            $row['status'],
        ];
    }

    public function columnFormats(): array
    {
        return [
            'E' => NumberFormat::FORMAT_NUMBER,
            'F' => NumberFormat::FORMAT_NUMBER,
            'G' => NumberFormat::FORMAT_DATE_DDMMYYYY,
        ];
    }
}
