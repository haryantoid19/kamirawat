<?php

namespace App\Mail;

use App\Enums\MedicalServiceCategoryType;
use App\Models\Booking;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NewBookingToPatient extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public Booking $booking;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Booking $booking)
    {
        $this->booking = $booking;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build(): static
    {
        $booking = $this->booking;
        $booking->load(['medical_service', 'medical_service.category', 'invoice', 'invoice.payment_method']);

        $service_name = $booking->medical_service->name;
        $booking_date = $booking->created_at->format('d F Y H:i:s');
        $booking_total = $booking->total_rupiah;
        $bank_info = $booking->medical_service->category->type->is(MedicalServiceCategoryType::ONLINE())
            ? $booking->invoice?->payment_method?->description
            : null;

        return $this->markdown('emails.new-booking-patient', compact(
            'service_name',
            'booking_date',
            'booking_total',
            'bank_info'
        ));
    }
}
