<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Support\Carbon;

/**
 * App\Models\BankAccount
 *
 * @property int $id
 * @property string $bank_accountable_type
 * @property int $bank_accountable_id
 * @property string $name
 * @property int $number
 * @property int $bank_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Model|Eloquent $bank_accountable
 * @property-read Bank $bank
 * @method static Builder|BankAccount newModelQuery()
 * @method static Builder|BankAccount newQuery()
 * @method static Builder|BankAccount query()
 * @method static Builder|BankAccount whereAccountBankableId($value)
 * @method static Builder|BankAccount whereAccountBankableType($value)
 * @method static Builder|BankAccount whereBankId($value)
 * @method static Builder|BankAccount whereCreatedAt($value)
 * @method static Builder|BankAccount whereId($value)
 * @method static Builder|BankAccount whereName($value)
 * @method static Builder|BankAccount whereNumber($value)
 * @method static Builder|BankAccount whereUpdatedAt($value)
 * @mixin Eloquent
 */
class BankAccount extends Model
{
    protected $casts = [
        'number' => 'integer'
    ];

    /**
     * Bank Accountable
     *
     * @return MorphTo
     */
    public function bank_accountable(): MorphTo
    {
        return $this->morphTo();
    }

    /**
     * Bank
     *
     * @return BelongsTo
     */
    public function bank(): BelongsTo
    {
        return $this->belongsTo(Bank::class);
    }
}
