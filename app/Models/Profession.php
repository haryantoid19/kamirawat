<?php

namespace App\Models;

use App\Traits\Userstamps;
use Database\Factories\ProfessionFactory;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Spatie\EloquentSortable\Sortable;
use Spatie\EloquentSortable\SortableTrait;
use Storage;

/**
 * App\Models\Profession
 *
 * @property int $id
 * @property string $title
 * @property string|null $icon_path
 * @property int|null $order
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read User|null $creator
 * @property-read User|null $editor
 * @property-read string|null $icon_url
 * @method static ProfessionFactory factory(...$parameters)
 * @method static Builder|Profession newModelQuery()
 * @method static Builder|Profession newQuery()
 * @method static Builder|Profession ordered(string $direction = 'asc')
 * @method static Builder|Profession query()
 * @method static Builder|Profession whereCreatedAt($value)
 * @method static Builder|Profession whereCreatedBy($value)
 * @method static Builder|Profession whereIconPath($value)
 * @method static Builder|Profession whereId($value)
 * @method static Builder|Profession whereOrder($value)
 * @method static Builder|Profession whereTitle($value)
 * @method static Builder|Profession whereUpdatedAt($value)
 * @method static Builder|Profession whereUpdatedBy($value)
 * @mixin Eloquent
 */
class Profession extends Model implements Sortable
{
    use HasFactory, Userstamps, SortableTrait;

    public array $sortable = [
        'order_column_name' => 'order',
        'sort_when_creating' => true,
    ];

    public function getIconUrlAttribute(): ?string
    {
        if (Storage::exists($this->icon_path)) {
            return Storage::url($this->icon_path);
        }

        return null;
    }
}
