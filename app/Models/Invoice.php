<?php

namespace App\Models;

use App\Enums\InvoiceStatus;
use App\Enums\XenditType;
use App\Traits\CanGenerateIdNumber;
use Eloquent;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Carbon;

/**
 * App\Models\Invoice
 *
 * @property int $id
 * @property int $booking_id
 * @property string $invoice_number
 * @property float $total
 * @property int $payment_method_id
 * @property InvoiceStatus $status
 * @property string|null $xendit_id
 * @property XenditType|null $xendit_type
 * @property string|null $xendit_invoice_url
 * @property string|null $xendit_va_number
 * @property float|null $shipping_fee
 * @property Carbon|null $paid_at
 * @property bool $is_main
 * @property Carbon|null $expired_at
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Booking $booking
 * @property-read Collection|InvoiceItem[] $items
 * @property-read int|null $items_count
 * @property-read PaymentMethod $payment_method
 * @method static Builder|Invoice newModelQuery()
 * @method static Builder|Invoice newQuery()
 * @method static Builder|Invoice query()
 * @method static Builder|Invoice whereBookingId($value)
 * @method static Builder|Invoice whereCreatedAt($value)
 * @method static Builder|Invoice whereExpiredAt($value)
 * @method static Builder|Invoice whereId($value)
 * @method static Builder|Invoice whereInvoiceNumber($value)
 * @method static Builder|Invoice whereIsMain($value)
 * @method static Builder|Invoice wherePaidAt($value)
 * @method static Builder|Invoice wherePaymentMethodId($value)
 * @method static Builder|Invoice whereShippingFee($value)
 * @method static Builder|Invoice whereStatus($value)
 * @method static Builder|Invoice whereTotal($value)
 * @method static Builder|Invoice whereUpdatedAt($value)
 * @method static Builder|Invoice whereXenditId($value)
 * @method static Builder|Invoice whereXenditInvoiceUrl($value)
 * @method static Builder|Invoice whereXenditType($value)
 * @method static Builder|Invoice whereXenditVaNumber($value)
 * @mixin Eloquent
 */
class Invoice extends Model
{
    use HasFactory, CanGenerateIdNumber;

    protected static string $id_number_column = "invoice_number";

    protected $casts = [
        'total' => 'float',
        'shipping_fee' => 'float',
        'status' => InvoiceStatus::class,
        'xendit_type' => XenditType::class,
        'is_main' => 'boolean',
    ];

    protected $dates = [
        'paid_at',
        'expired_at',
    ];

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::creating(function (Invoice $invoice) {
            if (! $invoice->status) {
                $invoice->status = InvoiceStatus::ACTIVE();
            }
            if (! $invoice->invoice_number) {
                $invoice->invoice_number = Invoice::generateIdNumber();
            }
        });
    }


    /**
     * Booking
     *
     * @return BelongsTo
     */
    public function booking(): BelongsTo
    {
        return $this->belongsTo(Booking::class);
    }

    /**
     * Items
     *
     * @return HasMany
     */
    public function items(): HasMany
    {
        return $this->hasMany(InvoiceItem::class);
    }

    /**
     * @return string
     * @throws Exception
     */
    public static function generateReference(): string
    {
        return self::generateIdNumber();
    }

    /**
     * Payment Method
     *
     * @return BelongsTo
     */
    public function payment_method(): BelongsTo
    {
        return $this->belongsTo(PaymentMethod::class);
    }

}
