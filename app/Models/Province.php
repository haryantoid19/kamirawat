<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Province
 *
 * @property string $province_name
 * @property string $province_name_en
 * @property int $province_code
 * @method static Builder|Province newModelQuery()
 * @method static Builder|Province newQuery()
 * @method static Builder|Province query()
 * @method static Builder|Province whereProvinceCode($value)
 * @method static Builder|Province whereProvinceName($value)
 * @method static Builder|Province whereProvinceNameEn($value)
 * @mixin Eloquent
 * @method static Builder|Province jabodetabek()
 */
class Province extends Model
{
    protected $fillable = [];

    public function scopeJabodetabek(Builder $builder)
    {
        // province for jabodetabek
        // Jakarta, Jawa Barat, Banten

        $codes = [31, 32, 36];

        $builder->whereIn('province_code', $codes);
    }
}
