<?php

namespace App\Models;

use App\Enums\MedicalServiceStatus;
use App\Traits\Userstamps;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;
use Storage;

/**
 * App\Models\AdditionalMedicalService
 *
 * @property int $id
 * @property string $name
 * @property float $price
 * @property MedicalServiceStatus $status
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $deleted_by
 * @property Carbon|null $deleted_at
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read User|null $creator
 * @property-read User|null $editor
 * @method static Builder|AdditionalMedicalService newModelQuery()
 * @method static Builder|AdditionalMedicalService newQuery()
 * @method static \Illuminate\Database\Query\Builder|AdditionalMedicalService onlyTrashed()
 * @method static Builder|AdditionalMedicalService published()
 * @method static Builder|AdditionalMedicalService query()
 * @method static Builder|AdditionalMedicalService whereCreatedAt($value)
 * @method static Builder|AdditionalMedicalService whereCreatedBy($value)
 * @method static Builder|AdditionalMedicalService whereDeletedAt($value)
 * @method static Builder|AdditionalMedicalService whereDeletedBy($value)
 * @method static Builder|AdditionalMedicalService whereId($value)
 * @method static Builder|AdditionalMedicalService whereName($value)
 * @method static Builder|AdditionalMedicalService wherePrice($value)
 * @method static Builder|AdditionalMedicalService whereStatus($value)
 * @method static Builder|AdditionalMedicalService whereUpdatedAt($value)
 * @method static Builder|AdditionalMedicalService whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|AdditionalMedicalService withTrashed()
 * @method static \Illuminate\Database\Query\Builder|AdditionalMedicalService withoutTrashed()
 * @mixin Eloquent
 */
class AdditionalMedicalService extends Model
{
    use HasFactory, SoftDeletes, Userstamps;

    protected $fillable = ['name', 'price'];

    protected $casts = [
        'status' => MedicalServiceStatus::class,
    ];

    /*
     * Scope published
     */
    public function scopePublished(Builder $builder)
    {
        $builder->where('status', MedicalServiceStatus::PUBLISH()->value);
    }

}
