<?php

namespace App\Models;

use App\Enums\MedicalServiceCategoryType;
use App\Traits\Userstamps;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;

/**
 * App\Models\MedicalServiceCategory
 *
 * @property int $id
 * @property string $name
 * @property bool $is_active
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Collection|MedicalService[] $medical_services
 * @property-read int|null $medical_services_count
 * @method static Builder|MedicalServiceCategory newModelQuery()
 * @method static Builder|MedicalServiceCategory newQuery()
 * @method static Builder|MedicalServiceCategory query()
 * @method static Builder|MedicalServiceCategory whereCreatedAt($value)
 * @method static Builder|MedicalServiceCategory whereCreatedBy($value)
 * @method static Builder|MedicalServiceCategory whereId($value)
 * @method static Builder|MedicalServiceCategory whereIsActive($value)
 * @method static Builder|MedicalServiceCategory whereName($value)
 * @method static Builder|MedicalServiceCategory whereUpdatedAt($value)
 * @method static Builder|MedicalServiceCategory whereUpdatedBy($value)
 * @mixin Eloquent
 * @property-read string $status
 * @property Carbon|null $deleted_at
 * @property-read User|null $creator
 * @property-read User|null $editor
 * @method static \Illuminate\Database\Query\Builder|MedicalServiceCategory onlyTrashed()
 * @method static Builder|MedicalServiceCategory whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|MedicalServiceCategory withTrashed()
 * @method static \Illuminate\Database\Query\Builder|MedicalServiceCategory withoutTrashed()
 * @method static Builder|MedicalServiceCategory active()
 * @property MedicalServiceCategoryType $type
 * @method static Builder|MedicalServiceCategory whereType($value)
 */
class MedicalServiceCategory extends Model
{
    use SoftDeletes, Userstamps;

    protected $fillable = ['name', 'is_active', 'type'];

    protected $casts = [
        'is_active' => 'boolean',
        'type' => MedicalServiceCategoryType::class,
    ];

    /**
     * Medical Services
     *
     * @return HasMany
     */
    public function medical_services(): HasMany
    {
        return $this->hasMany(MedicalService::class, 'category_id');
    }

    public function getStatusAttribute(): string
    {
        if ($this->is_active) {
            return '<span class="badge badge-light-success">Active</span>';
        } else {
            return '<span class="badge badge-light-warning">Not Active</span>';
        }
    }

    public function scopeActive(Builder $builder)
    {
        $builder->where('is_active', true);
    }
}
