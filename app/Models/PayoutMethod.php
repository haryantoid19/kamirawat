<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Support\Carbon;

/**
 * App\Models\PayoutMethod
 *
 * @property int $id
 * @property int $bank_id
 * @property string $name
 * @property string $number
 * @property bool|null $is_primary
 * @property string $payout_methodable_type
 * @property int $payout_methodable_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Bank $bank
 * @property-read Model|Eloquent $payout_methodable
 * @method static Builder|PayoutMethod newModelQuery()
 * @method static Builder|PayoutMethod newQuery()
 * @method static Builder|PayoutMethod query()
 * @method static Builder|PayoutMethod whereBankId($value)
 * @method static Builder|PayoutMethod whereCreatedAt($value)
 * @method static Builder|PayoutMethod whereId($value)
 * @method static Builder|PayoutMethod whereIsPrimary($value)
 * @method static Builder|PayoutMethod whereName($value)
 * @method static Builder|PayoutMethod whereNumber($value)
 * @method static Builder|PayoutMethod wherePayoutMethodableId($value)
 * @method static Builder|PayoutMethod wherePayoutMethodableType($value)
 * @method static Builder|PayoutMethod whereUpdatedAt($value)
 * @mixin Eloquent
 */
class PayoutMethod extends Model
{
    protected $fillable = ['name', 'bank_id', 'number', 'is_main'];

    protected $casts = [
        'is_primary' => 'boolean',
    ];

    /**
     * @return BelongsTo
     */
    public function bank(): BelongsTo
    {
        return $this->belongsTo(Bank::class);
    }

    /**
     * @return MorphTo
     */
    public function payout_methodable(): MorphTo
    {
        return $this->morphTo('payoutable');
    }

    public function scopePrimary(Builder $builder)
    {
        $builder->where('is_primary', true);
    }
}
