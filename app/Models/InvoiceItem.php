<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Support\Carbon;

/**
 * App\Models\InvoiceItem
 *
 * @property int $id
 * @property int $invoice_id
 * @property string $name
 * @property float $price
 * @property string $itemable_type
 * @property int $itemable_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Invoice $invoice
 * @property-read Model|Eloquent $itemable
 * @method static Builder|InvoiceItem newModelQuery()
 * @method static Builder|InvoiceItem newQuery()
 * @method static Builder|InvoiceItem query()
 * @method static Builder|InvoiceItem whereCreatedAt($value)
 * @method static Builder|InvoiceItem whereId($value)
 * @method static Builder|InvoiceItem whereInvoiceId($value)
 * @method static Builder|InvoiceItem whereItemableId($value)
 * @method static Builder|InvoiceItem whereItemableType($value)
 * @method static Builder|InvoiceItem whereName($value)
 * @method static Builder|InvoiceItem wherePrice($value)
 * @method static Builder|InvoiceItem whereUpdatedAt($value)
 * @mixin Eloquent
 */
class InvoiceItem extends Model
{
    use HasFactory;

    protected $casts = [
        'price' => 'float',
    ];

    /**
     * Invoice
     *
     * @return BelongsTo
     */
    public function invoice(): BelongsTo
    {
        return $this->belongsTo(Invoice::class);
    }

    /**
     * Itemable
     *
     * @return MorphTo
     */
    public function itemable(): MorphTo
    {
        return $this->morphTo();
    }
}
