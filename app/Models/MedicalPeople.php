<?php

namespace App\Models;

use App\Enums\Gender;
use App\Enums\IdType;
use App\Enums\LastEducation;
use App\Enums\MedicalPeopleStatus;
use App\Traits\CanGenerateIdNumber;
use App\Traits\HasBankAccount;
use Auth;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Collections\MediaCollection;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Storage;

/**
 * App\Models\MedicalPeople
 *
 * @property int $id
 * @property int $user_id
 * @property int $profession_id
 * @property string $medical_number
 * @property string $name
 * @property IdType $id_type
 * @property string $id_number
 * @property string $phone_number
 * @property int $postal_code_id
 * @property string|null $rt
 * @property string|null $rw
 * @property MedicalPeopleStatus $status
 * @property string|null $avatar_path
 * @property int|null $accepted_by
 * @property int|null $rejected_by
 * @property Carbon|null $accepted_at
 * @property Carbon|null $rejected_at
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read PostalCode $postal_code
 * @property-read Profession $profession
 * @property-read User $user
 * @method static Builder|MedicalPeople newModelQuery()
 * @method static Builder|MedicalPeople newQuery()
 * @method static Builder|MedicalPeople query()
 * @method static Builder|MedicalPeople whereAcceptedAt($value)
 * @method static Builder|MedicalPeople whereAcceptedBy($value)
 * @method static Builder|MedicalPeople whereAvatarPath($value)
 * @method static Builder|MedicalPeople whereCreatedAt($value)
 * @method static Builder|MedicalPeople whereId($value)
 * @method static Builder|MedicalPeople whereIdNumber($value)
 * @method static Builder|MedicalPeople whereIdType($value)
 * @method static Builder|MedicalPeople whereMedicalNumber($value)
 * @method static Builder|MedicalPeople whereName($value)
 * @method static Builder|MedicalPeople wherePhoneNumber($value)
 * @method static Builder|MedicalPeople wherePostalCodeId($value)
 * @method static Builder|MedicalPeople whereProfessionId($value)
 * @method static Builder|MedicalPeople whereRejectedAt($value)
 * @method static Builder|MedicalPeople whereRejectedBy($value)
 * @method static Builder|MedicalPeople whereRt($value)
 * @method static Builder|MedicalPeople whereRw($value)
 * @method static Builder|MedicalPeople whereStatus($value)
 * @method static Builder|MedicalPeople whereUpdatedAt($value)
 * @method static Builder|MedicalPeople whereUserId($value)
 * @mixin Eloquent
 * @property Carbon $bod
 * @method static Builder|MedicalPeople whereBod($value)
 * @property Gender $gender
 * @method static Builder|MedicalPeople whereGender($value)
 * @property string|null $nationality
 * @method static Builder|MedicalPeople whereNationality($value)
 * @property int|null $deleted_by
 * @property Carbon|null $deleted_at
 * @method static \Illuminate\Database\Query\Builder|MedicalPeople onlyTrashed()
 * @method static Builder|MedicalPeople whereDeletedAt($value)
 * @method static Builder|MedicalPeople whereDeletedBy($value)
 * @method static \Illuminate\Database\Query\Builder|MedicalPeople withTrashed()
 * @method static \Illuminate\Database\Query\Builder|MedicalPeople withoutTrashed()
 * @property-read string $avatar_url
 * @property string|null $address
 * @property-read MediaCollection|Media[] $media
 * @property-read int|null $media_count
 * @method static Builder|MedicalPeople whereAddress($value)
 * @property-read string $id_type_label
 * @property-read Collection|PayoutMethod[] $payout_method
 * @property-read int|null $payout_method_count
 * @property-read Collection|PayoutMethod[] $payout_methods
 * @property-read int|null $payout_methods_count
 * @property LastEducation|null $last_education
 * @method static Builder|MedicalPeople whereLastEducation($value)
 * @property-read Collection|Booking[] $bookings
 * @property-read int|null $bookings_count
 * @property-read BankAccount|null $bank_account
 */
class MedicalPeople extends Model implements HasMedia
{
    use HasFactory, CanGenerateIdNumber, SoftDeletes, InteractsWithMedia, HasBankAccount;

    protected static string $id_number_column = "medical_number";

    protected $casts = [
        'id_type' => IdType::class,
        'status' => MedicalPeopleStatus::class,
        'accepted_at' => 'datetime',
        'rejected_at' => 'datetime',
        'bod' => 'date',
        'gender' => Gender::class,
        'last_education' => LastEducation::class,
    ];

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::creating(function (MedicalPeople $medicalPeople) {
            if ($medicalPeople->status === null) {
                $medicalPeople->status = MedicalPeopleStatus::PENDING();
            }

            $medicalPeople->medical_number = MedicalPeople::generateIdNumber();
        });

        static::updating(function (MedicalPeople $medicalPeople) {
            if ($medicalPeople->isDirty('status')) {
                if ($medicalPeople->status->is(MedicalPeopleStatus::ACCEPTED())) {
                    $medicalPeople->accepted_at = now();
                    $medicalPeople->accepted_by = Auth::id();
                }

                if ($medicalPeople->status->is(MedicalPeopleStatus::REJECTED())) {
                    $medicalPeople->rejected_at = now();
                    $medicalPeople->rejected_by = Auth::id();
                }
            }
        });
    }

    /**
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return BelongsTo
     */
    public function profession(): BelongsTo
    {
        return $this->belongsTo(Profession::class);
    }

    /**
     * @return BelongsTo
     */
    public function postal_code(): BelongsTo
    {
        return $this->belongsTo(PostalCode::class);
    }

    /**
     * Bookings
     *
     * @return HasMany
     */
    public function bookings(): HasMany
    {
        return $this->hasMany(Booking::class);
    }

    /**
     * Prepare proper error handling for url attribute
     *
     * @return string
     */
    public function getAvatarUrlAttribute(): string
    {
        if ($this->avatar_path && Storage::exists($this->avatar_path)) {
            return Storage::url($this->avatar_path);
        }

        return asset(theme()->getMediaUrlPath().'avatars/blank.png');
    }

    public function getIdTypeLabelAttribute(): string
    {
        return $this->id_type->getLabel();
    }

    /**
     * Payout Methods
     *
     * @return MorphMany
     */
    public function payout_methods(): MorphMany
    {
        return $this->morphMany(PayoutMethod::class, 'payout_methodable');
    }

    /**
     * Payout Method
     *
     * @return MorphMany
     */
    public function payout_method(): MorphMany
    {
        return $this->morphMany(PayoutMethod::class, 'payout_methodable')->where('is_primary', true);
    }

}
