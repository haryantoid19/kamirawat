<?php

namespace App\Models;

use App\Core\Traits\SpatieLogsActivity;
use App\Enums\Gender;
use App\Enums\IdType;
use App\Traits\CanGenerateIdNumber;
use Database\Factories\PatientFactory;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;
use Spatie\Activitylog\Models\Activity;
use Storage;

/**
 * App\Models\Patient
 *
 * @property int $id
 * @property int $user_id
 * @property string $name
 * @property IdType|null $id_type
 * @property string|null $id_number
 * @property Gender $gender
 * @property string|null $nationality
 * @property int|null $postal_code_id
 * @property Carbon $bod
 * @property string $phone_number
 * @property string|null $address
 * @property string|null $latitude
 * @property string|null $longitude
 * @property string|null $avatar_path
 * @property bool $is_main
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Collection|Activity[] $activities
 * @property-read int|null $activities_count
 * @property-read string $avatar_url
 * @property-read PostalCode|null $postal_code
 * @property-read User $user
 * @method static PatientFactory factory(...$parameters)
 * @method static Builder|Patient newModelQuery()
 * @method static Builder|Patient newQuery()
 * @method static Builder|Patient query()
 * @method static Builder|Patient whereAddress($value)
 * @method static Builder|Patient whereAvatarPath($value)
 * @method static Builder|Patient whereBod($value)
 * @method static Builder|Patient whereCreatedAt($value)
 * @method static Builder|Patient whereGender($value)
 * @method static Builder|Patient whereId($value)
 * @method static Builder|Patient whereIdNumber($value)
 * @method static Builder|Patient whereIdType($value)
 * @method static Builder|Patient whereIsMain($value)
 * @method static Builder|Patient whereLatitude($value)
 * @method static Builder|Patient whereLongitude($value)
 * @method static Builder|Patient whereName($value)
 * @method static Builder|Patient whereNationality($value)
 * @method static Builder|Patient wherePhoneNumber($value)
 * @method static Builder|Patient wherePostalCodeId($value)
 * @method static Builder|Patient whereUpdatedAt($value)
 * @method static Builder|Patient whereUserId($value)
 * @mixin Eloquent
 * @property string $patient_number
 * @method static Builder|Patient wherePatientNumber($value)
 * @property string|null $rt
 * @property string|null $rw
 * @method static Builder|Patient whereRt($value)
 * @method static Builder|Patient whereRw($value)
 */
class Patient extends Model
{
    use HasFactory, SpatieLogsActivity, CanGenerateIdNumber;

    protected $guarded = ['id', 'created_at', 'updated_at'];

    protected $casts = [
        'id_type' => IdType::class,
        'gender' => Gender::class,
        'is_main' => 'boolean',
        'bod' => 'date',
    ];

    protected static string $id_number_column = "patient_number";

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::creating(function (Patient $patient) {
            $patient->patient_number = Patient::generateIdNumber();
        });

        static::updated(function (Patient $patient) {
            // Update to users table
            $user = $patient->user;
            $user->name = $patient->name;
            $user->saveQuietly();
        });
    }


    /**
     * User
     *
     * @return BelongsTo
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Postal Code
     *
     * @return BelongsTo
     */
    public function postal_code(): BelongsTo
    {
        return $this->belongsTo(PostalCode::class);
    }

    /**
     * Prepare proper error handling for url attribute
     *
     * @return string
     */
    public function getAvatarUrlAttribute(): string
    {
        if ($this->avatar_path && Storage::exists($this->avatar_path)) {
            return Storage::url($this->avatar_path);
        }

        return asset(theme()->getMediaUrlPath().'avatars/blank.png');
    }

    public function phoneNumber() {
        $firstCharacter = substr($this->phone_number, 0, 3);
        if($firstCharacter != '+62'){
            $number = $this->phone_number;
            $country_code = '+62';
            $new_number = substr_replace($number, $country_code, 0, ($number[0] == '0')); 
        }else{
            $new_number = $this->phone_number;
        }		

        return $new_number;
        // add logic to correctly format number here
        // a more robust ways would be to use a regular expression
    }
}
