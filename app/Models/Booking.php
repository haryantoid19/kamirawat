<?php

namespace App\Models;

use App\Enums\BookingStatus;
use App\Traits\CanGenerateIdNumber;
use App\Traits\Userstamps;
use Eloquent;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Carbon;

/**
 * App\Models\Booking
 *
 * @property int $id
 * @property int $patient_id
 * @property string $booking_number
 * @property float $total
 * @property BookingStatus $status
 * @property int|null $medical_people_id
 * @property int|null $assigned_by
 * @property Carbon|null $assigned_at
 * @property int|null $canceled_by
 * @property Carbon|null $canceled_at
 * @property int|null $rejected_by
 * @property Carbon|null $rejected_at
 * @property int|null $deleted_by
 * @property Carbon|null $deleted_at
 * @property int|null $finished_by
 * @property Carbon|null $finished_at
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read User|null $creator
 * @property-read User|null $editor
 * @property-read MedicalPeople|null $medical_people
 * @property-read Patient $patient
 * @method static \Illuminate\Database\Eloquent\Builder|Booking newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Booking newQuery()
 * @method static Builder|Booking onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|Booking query()
 * @method static \Illuminate\Database\Eloquent\Builder|Booking whereAssignedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Booking whereAssignedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Booking whereBookingNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Booking whereCanceledAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Booking whereCanceledBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Booking whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Booking whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Booking whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Booking whereDeletedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Booking whereFinishedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Booking whereFinishedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Booking whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Booking whereMedicalPeopleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Booking wherePatientId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Booking whereRejectedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Booking whereRejectedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Booking whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Booking whereTotal($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Booking whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Booking whereUpdatedBy($value)
 * @method static Builder|Booking withTrashed()
 * @method static Builder|Booking withoutTrashed()
 * @mixin Eloquent
 * @property string|null $history_of_disease_allergies
 * @property string|null $main_symptom
 * @property string|null $special_request
 * @method static \Illuminate\Database\Eloquent\Builder|Booking whereHistoryOfDiseaseAllergies($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Booking whereMainSymptom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Booking whereSpecialRequest($value)
 * @property-read Collection|Invoice[] $invoices
 * @property-read int|null $invoices_count
 * @property int|null $medical_service_id
 * @method static \Illuminate\Database\Eloquent\Builder|Booking whereMedicalServiceId($value)
 * @property-read MedicalService|null $medical_service
 * @property string|null $handling
 * @property string|null $medical_record_summary
 * @method static \Illuminate\Database\Eloquent\Builder|Booking whereHandling($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Booking whereMedicalRecordSummary($value)
 * @property string|null $passcode
 * @property-read Invoice|null $invoice
 * @method static \Illuminate\Database\Eloquent\Builder|Booking wherePasscode($value)
 * @property Carbon|null $input_passcode_at
 * @method static \Illuminate\Database\Eloquent\Builder|Booking whereInputPasscodeAt($value)
 * @property-read string $total_rupiah
 */
class Booking extends Model
{
    use HasFactory, Userstamps, CanGenerateIdNumber, SoftDeletes;

    protected static string $id_number_column = "booking_number";

    protected $casts = [
        'status' => BookingStatus::class,
        'assigned_at' => 'datetime',
        'accepted_at' => 'datetime',
        'finished_at' => 'datetime',
        'rejected_at' => 'datetime',
        'canceled_at' => 'datetime',
        'input_passcode_at' => 'datetime',
    ];

    protected $guarded = ['id', 'created_by', 'updated_by', 'created_at', 'updated_at'];

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::creating(function (Booking $booking) {
            if (! $booking->status) {
                $booking->status = BookingStatus::WAITING_PAYMENT();
            }

            $booking->booking_number = Booking::generateIdNumber();
        });
    }

    /**
     * Patient
     *
     * @return BelongsTo
     */
    public function patient(): BelongsTo
    {
        return $this->belongsTo(Patient::class);
    }

    /**
     * Medical People
     *
     * @return BelongsTo
     */
    public function medical_people(): BelongsTo
    {
        return $this->belongsTo(MedicalPeople::class);
    }

    /**
     * Invoices
     *
     * @return HasMany
     */
    public function invoices(): HasMany
    {
        return $this->hasMany(Invoice::class);
    }

    /**
     * Invoice
     *
     * @return HasOne
     */
    public function invoice(): HasOne
    {
        return $this->hasOne(Invoice::class)->where('is_main', 1);
    }

    /**
     * Medical Service
     *
     * @return BelongsTo
     */
    public function medical_service(): BelongsTo
    {
        return $this->belongsTo(MedicalService::class);
    }

    /**
     * Total Rupiah
     *
     * @return string
     */
    public function getTotalRupiahAttribute(): string
    {
        return "Rp. " . number_format($this->total,0,',','.');
    }
}
