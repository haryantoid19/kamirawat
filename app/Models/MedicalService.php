<?php

namespace App\Models;

use App\Enums\MedicalServiceStatus;
use App\Traits\Userstamps;
use Eloquent;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Query\Builder;
use Illuminate\Support\Carbon;
use Storage;

/**
 * App\Models\MedicalService
 *
 * @property int $id
 * @property string $name
 * @property float $price
 * @property MedicalServiceStatus $status
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $deleted_by
 * @property Carbon|null $deleted_at
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read User|null $creator
 * @property-read User|null $editor
 * @method static \Illuminate\Database\Eloquent\Builder|MedicalService newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|MedicalService newQuery()
 * @method static Builder|MedicalService onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|MedicalService query()
 * @method static \Illuminate\Database\Eloquent\Builder|MedicalService whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MedicalService whereCreatedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MedicalService whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MedicalService whereDeletedBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MedicalService whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MedicalService whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MedicalService wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MedicalService whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MedicalService whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MedicalService whereUpdatedBy($value)
 * @method static Builder|MedicalService withTrashed()
 * @method static Builder|MedicalService withoutTrashed()
 * @mixin Eloquent
 * @property string $description
 * @method static \Illuminate\Database\Eloquent\Builder|MedicalService whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MedicalService published()
 * @property string|null $icon_path
 * @property-read string|null $icon_url
 * @method static \Illuminate\Database\Eloquent\Builder|MedicalService whereLogoPath($value)
 * @property int|null $category_id
 * @property-read MedicalServiceCategory|null $category
 * @method static \Illuminate\Database\Eloquent\Builder|MedicalService whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|MedicalService whereIconPath($value)
 */
class MedicalService extends Model
{
    use HasFactory, SoftDeletes, Userstamps;

    protected $fillable = ['name', 'description', 'icon_path', 'price'];

    protected $casts = [
        'status' => MedicalServiceStatus::class,
    ];

    protected static function booted()
    {
        static::creating(function(MedicalService $medicalService) {
            if (! $medicalService->icon_path) {
                $default_path = 'medical_services/default-icon.svg';
                if (Storage::exists($default_path)) {
                    $medicalService->icon_path = $default_path;
                }
            }
        });
    }

    /**
     * Category
     *
     * @return BelongsTo
     */
    public function category(): BelongsTo
    {
        return $this->belongsTo(MedicalServiceCategory::class, 'category_id');
    }

    /*
     * Scope published
     */
    public function scopePublished(\Illuminate\Database\Eloquent\Builder $builder)
    {
        $builder->where('status', MedicalServiceStatus::PUBLISH()->value);
    }

    /**
     * Icon URL
     *
     * @return string|null
     */
    public function getIconUrlAttribute(): ?string
    {
        if (Storage::exists($this->icon_path)) {
            return Storage::url($this->icon_path);
        }

        return null;
    }
}
