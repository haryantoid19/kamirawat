<?php

namespace App\Models;

use App\Enums\PaymentMethodGateway;
use App\Enums\PaymentMethodStatus;
use App\Traits\Userstamps;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Spatie\EloquentSortable\Sortable;
use Spatie\EloquentSortable\SortableTrait;
use Storage;

/**
 * App\Models\PaymentMethod
 *
 * @property int $id
 * @property string $code
 * @property string $label
 * @property string $description
 * @property string $icon_path
 * @property PaymentMethodGateway $gateway
 * @property PaymentMethodStatus $status
 * @property int|null $order
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read User|null $creator
 * @property-read User|null $editor
 * @property-read string|null $icon_url
 * @method static Builder|PaymentMethod newModelQuery()
 * @method static Builder|PaymentMethod newQuery()
 * @method static Builder|PaymentMethod ordered(string $direction = 'asc')
 * @method static Builder|PaymentMethod published()
 * @method static Builder|PaymentMethod query()
 * @method static Builder|PaymentMethod whereCode($value)
 * @method static Builder|PaymentMethod whereCreatedAt($value)
 * @method static Builder|PaymentMethod whereCreatedBy($value)
 * @method static Builder|PaymentMethod whereDescription($value)
 * @method static Builder|PaymentMethod whereGateway($value)
 * @method static Builder|PaymentMethod whereId($value)
 * @method static Builder|PaymentMethod whereIconPath($value)
 * @method static Builder|PaymentMethod whereOrder($value)
 * @method static Builder|PaymentMethod whereStatus($value)
 * @method static Builder|PaymentMethod whereLabel($value)
 * @method static Builder|PaymentMethod whereUpdatedAt($value)
 * @method static Builder|PaymentMethod whereUpdatedBy($value)
 * @mixin Eloquent
 */
class PaymentMethod extends Model implements Sortable
{
    use Userstamps, SortableTrait;

    protected $casts = [
        'status' => PaymentMethodStatus::class,
        'gateway' => PaymentMethodGateway::class,
    ];

    public array $sortable = [
        'order_column_name' => 'order',
        'sort_when_creating' => true,
    ];

    /**
     * @return string|null
     */
    public function getIconUrlAttribute(): ?string
    {
        $icon_path = $this->icon_path;
        if (! $icon_path) {
            $icon_path = 'bank-transfer-icon.jpeg';
        }

        if (Storage::exists($icon_path)) {
            return Storage::url($icon_path);
        }

        return null;
    }

    /**
     * @param Builder $builder
     */
    public function scopePublished(Builder $builder)
    {
        $builder->where('status', PaymentMethodStatus::PUBLISH()->value);
    }
}
