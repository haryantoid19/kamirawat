<?php

namespace App\Models;

use App\Core\Traits\SpatieLogsActivity;
use App\Enums\UserRole;
use App\Notifications\ChangeEmailVerification;
use App\Notifications\VerifyEmail;
use App\Notifications\VerifyEmailPatient;
use Bavix\Wallet\Interfaces\Wallet;
use Bavix\Wallet\Models\Transaction;
use Bavix\Wallet\Models\Transfer;
use Bavix\Wallet\Traits\HasWallet;
use Database\Factories\UserFactory;
use Eloquent;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Notifications\DatabaseNotificationCollection;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Carbon;
use Spatie\Activitylog\Models\Activity;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Traits\HasRoles;

/**
 * App\Models\User
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property Carbon|null $email_verified_at
 * @property string $password
 * @property string|null $remember_token
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Collection|Activity[] $activities
 * @property-read int|null $activities_count
 * @property-read string $avatar_url
 * @property-read DatabaseNotificationCollection|DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read Collection|Permission[] $permissions
 * @property-read int|null $permissions_count
 * @property-read Collection|Role[] $roles
 * @property-read int|null $roles_count
 * @method static UserFactory factory(...$parameters)
 * @method static Builder|User newModelQuery()
 * @method static Builder|User newQuery()
 * @method static Builder|User permission($permissions)
 * @method static Builder|User query()
 * @method static Builder|User whereCreatedAt($value)
 * @method static Builder|User whereEmail($value)
 * @method static Builder|User whereEmailVerifiedAt($value)
 * @method static Builder|User whereFirstName($value)
 * @method static Builder|User whereId($value)
 * @method static Builder|User whereLastName($value)
 * @method static Builder|User wherePassword($value)
 * @method static Builder|User whereRememberToken($value)
 * @method static Builder|User whereUpdatedAt($value)
 * @mixin Eloquent
 * @method static Builder|User role($roles, $guard = null)
 * @method static Builder|User whereName($value)
 * @property-read Collection|Patient[] $patients
 * @property-read int|null $patients_count
 * @property-read MedicalPeople|null $medical_people
 * @property-read Patient|null $patient
 * @property Carbon|null $last_login_at
 * @property string|null $last_ip_address
 * @property-read int|float|string $balance
 * @property-read mixed $first_role
 * @property-read Collection|Transaction[] $transactions
 * @property-read int|null $transactions_count
 * @property-read Collection|Transfer[] $transfers
 * @property-read int|null $transfers_count
 * @property-read \Bavix\Wallet\Models\Wallet $wallet
 * @method static Builder|User whereLastIpAddress($value)
 * @method static Builder|User whereLastLoginAt($value)
 * @property-read int $balance_int
 * @method static Builder|User whereOnChangingEmail($value)
 * @property string|null $new_email
 * @property string|null $token_new_email
 * @method static Builder|User whereNewEmail($value)
 * @method static Builder|User whereTokenNewEmail($value)
 */
class User extends Authenticatable implements MustVerifyEmail, Wallet
{
    use HasFactory, Notifiable;
    use SpatieLogsActivity;
    use HasRoles;
    use HasWallet;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'last_login_at' => 'datetime',
    ];

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::updated(function (User $user) {
            // Update to patients
            if ($user->hasRole(UserRole::PATIENT()->value) && $user->patient) {
                $patient = $user->patient;
                $patient->name = $user->name;
                $patient->save();
            }

            // Update to medical people
            if ($user->hasRole(UserRole::MEDICAL_PERSON()->value) && $user->medical_people) {
                $medical_people = $user->medical_people;
                $medical_people->name = $user->name;
                $medical_people->saveQuietly();
            }
        });
    }

    /**
     * Patients
     *
     * @return HasMany
     */
    public function patients(): HasMany
    {
        return $this->hasMany(Patient::class);
    }

    /**
     * @return Patient|null
     */
    public function getPatientAttribute(): ?Patient
    {
        return $this->patients->where('is_main', '=', 1)->first();
    }

    /**
     * Medical People
     *
     * @return HasOne
     */
    public function medical_people(): HasOne
    {
        return $this->hasOne(MedicalPeople::class);
    }

    /**
     * Prepare proper error handling for url attribute
     *
     * @return string
     */
    public function getAvatarUrlAttribute(): string
    {
        return asset(theme()->getMediaUrlPath().'avatars/blank.png');
    }

    /**
     * @return string
     */
    public function getDashboardRouteNamePatientOrPartner(): string
    {
        if ($this->hasRole(UserRole::PATIENT()->value)) {
            return 'front.patient.dashboard';
        } else if ($this->hasRole(UserRole::MEDICAL_PERSON()->value)) {
            return 'front.partner.dashboard';
        }

        return 'front.index';
    }

    public function getFirstRoleAttribute()
    {
        return $this->roles->first();
    }

    public function sendEmailVerificationNotification()
    {   
        if ($this->hasRole(UserRole::PATIENT()->value)) {
            $this->notify(new VerifyEmailPatient());
        } else if ($this->hasRole(UserRole::MEDICAL_PERSON()->value)) {
            $this->notify(new VerifyEmail());
        }
       
    }

    public function isPatient(): bool
    {
        return $this->patient !== null;
    }

    public function isPartner(): bool
    {
        return $this->medical_people !== null;
    }

    public function sendChangeEmailVerificationNotification()
    {
        $this->notify(new ChangeEmailVerification());
    }
}
