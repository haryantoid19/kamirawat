<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Collection;

/**
 * App\Models\PostalCode
 *
 * @property int $id
 * @property string $urban
 * @property string $sub_district
 * @property string $city
 * @property int $province_code
 * @property string $postal_code
 * @property-read Province $province
 * @method static Builder|PostalCode newModelQuery()
 * @method static Builder|PostalCode newQuery()
 * @method static Builder|PostalCode query()
 * @method static Builder|PostalCode whereCity($value)
 * @method static Builder|PostalCode whereId($value)
 * @method static Builder|PostalCode wherePostalCode($value)
 * @method static Builder|PostalCode whereProvinceCode($value)
 * @method static Builder|PostalCode whereSubDistrict($value)
 * @method static Builder|PostalCode whereUrban($value)
 * @mixin Eloquent
 */
class PostalCode extends Model
{
    protected $fillable = [];

    /**
     * Province
     *
     * @return BelongsTo
     */
    public function province(): BelongsTo
    {
        return $this->belongsTo(Province::class, 'province_code', 'province_code');
    }

    /**
     * Get Cities
     *
     * @param int $provinceCode
     * @return Collection
     */
    public static function getCities(int $provinceCode): Collection
    {
        // only jabodetabek
        $allowed_cities = [
            'JAKARTA UTARA',
            'JAKARTA BARAT',
            'JAKARTA TIMUR',
            'JAKARTA SELATAN',
            'JAKARTA PUSAT',
            'KEPULAUAN SERIBU',

            'BOGOR',
            'DEPOK',
            'BEKASI',

            'TANGERANG',
            'TANGERANG SELATAN'
        ];

        return self::select('city')
            ->groupBy('city')
            ->where('province_code', $provinceCode)
            ->whereIn('city', $allowed_cities)
            ->get();
    }

    /**
     * Get Subdistrict
     *
     * @param PostalCode $postalCode
     * @return Collection
     */
    public static function getSubdistricts(PostalCode $postalCode): Collection
    {
        return self::select('sub_district')
            ->groupBy('sub_district')
            ->where('city', $postalCode->city)
            ->where('province_code', $postalCode->province_code)
            ->get();
    }

    /**
     * Get Urban List
     *
     * @param PostalCode $postalCode
     * @return Collection
     */
    public static function getUrbanList(PostalCode $postalCode): Collection
    {
        return self::select('urban')
            ->groupBy('urban')
            ->where('sub_district', $postalCode->sub_district)
            ->where('city', $postalCode->city)
            ->where('province_code', $postalCode->province_code)
            ->get();
    }
}
