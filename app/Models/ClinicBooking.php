<?php

namespace App\Models;

use App\Enums\BookingStatus;
use App\Enums\ClinicBookingStatus;
use App\Enums\Gender;
use App\Traits\CanGenerateIdNumber;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;

/**
 * App\Models\ClinicBooking
 *
 * @property int $id
 * @property int $patient_id
 * @property int $medical_service_id
 * @property string $number
 * @property ClinicBookingStatus $status
 * @property string $name
 * @property string $phone_number
 * @property string $bod
 * @property Gender $gender
 * @property string $history_of_disease_allergies
 * @property string $main_symptom
 * @property string $special_request
 * @property string $address
 * @property bool $is_self
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read MedicalService $medical_service
 * @property-read Patient $patient
 * @method static Builder|ClinicBooking newModelQuery()
 * @method static Builder|ClinicBooking newQuery()
 * @method static Builder|ClinicBooking query()
 * @method static Builder|ClinicBooking whereAddress($value)
 * @method static Builder|ClinicBooking whereBod($value)
 * @method static Builder|ClinicBooking whereCreatedAt($value)
 * @method static Builder|ClinicBooking whereGender($value)
 * @method static Builder|ClinicBooking whereHistoryOfDiseaseAllergies($value)
 * @method static Builder|ClinicBooking whereId($value)
 * @method static Builder|ClinicBooking whereIsSelf($value)
 * @method static Builder|ClinicBooking whereMainSymptom($value)
 * @method static Builder|ClinicBooking whereMedicalServiceId($value)
 * @method static Builder|ClinicBooking whereName($value)
 * @method static Builder|ClinicBooking whereNumber($value)
 * @method static Builder|ClinicBooking wherePatientId($value)
 * @method static Builder|ClinicBooking wherePhoneNumber($value)
 * @method static Builder|ClinicBooking whereSpecialRequest($value)
 * @method static Builder|ClinicBooking whereStatus($value)
 * @method static Builder|ClinicBooking whereUpdatedAt($value)
 * @mixin Eloquent
 */
class ClinicBooking extends Model
{
    use HasFactory, CanGenerateIdNumber;

    protected static string $id_number_column = "number";

    protected $casts = [
        'status' => ClinicBookingStatus::class,
        'gender' => Gender::class,
        'dob' => 'date',
        'is_self' => 'boolean',
    ];

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {
        static::creating(function (ClinicBooking $clinicBooking) {
            if (! $clinicBooking->status) {
                $clinicBooking->status = ClinicBookingStatus::SUBMITTED();
            }

            $clinicBooking->number = ClinicBooking::generateIdNumber();
        });
    }

    /**
     * Patient
     *
     * @return BelongsTo
     */
    public function patient(): BelongsTo
    {
        return $this->belongsTo(Patient::class);
    }

    /**
     * Medical Service
     *
     * @return BelongsTo
     */
    public function medical_service(): BelongsTo
    {
        return $this->belongsTo(MedicalService::class);
    }

}
