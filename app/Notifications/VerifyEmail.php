<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Lang;

class VerifyEmail extends \Illuminate\Auth\Notifications\VerifyEmail implements ShouldQueue
{
    use Queueable;

    /**
     * Get verify email notification mail message for the given URL.
     *
     * @param  string  $url
     * @return MailMessage
     */
    protected function buildMailMessage($url): MailMessage
    {
        return (new MailMessage)
            ->subject(Lang::get('Terima Kasih Telah Mendaftar Di Kamirawat'))
            ->line('Terima kasih telah melakukan pendaftaran sebagai Mitra Tenaga Medis di www.kamirawat.com. Seluruh tim **Kamirawat** tidak sabar menunggu untuk segera berkolaborasi bersama Anda.')
            ->line('Silakan klik tombol di bawah untuk memverifikasi alamat email Anda.')
            ->action(Lang::get('Verify Email Address'), $url)
            ->line('Apabila Anda sudah melakukan verifikasi email, kami akan melakukan verifikasi berkas terlebih dahulu. Apabila berkas Anda sudah memenuhi syarat sebagai Mitra Tenaga Medis **Kamirawat**, maka kami akan segera menghubungi Anda kembali untuk memberitahukan bahwa akun Anda sudah siap digunakan.');
    }
}
