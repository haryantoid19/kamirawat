<?php

namespace App\Notifications;

use App\Models\MedicalPeople;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ApprovedRegistrationPartner extends Notification implements ShouldQueue
{
    use Queueable;

    public MedicalPeople $medicalPeople;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(MedicalPeople $medicalPeople)
    {
        $this->medicalPeople = $medicalPeople;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via(mixed $notifiable): array
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return MailMessage
     */
    public function toMail(mixed $notifiable): MailMessage
    {
        $name = $this->medicalPeople->gender->title() . ' ' . $this->medicalPeople->name;

        return (new MailMessage)
            ->greeting("Halo $name,")
            ->subject('Selamat! Anda Telah Memenuhi Syarat Sebagai Mitra Tenaga Medis Kamirawat')
            ->line('Kami sangat senang Anda bisa bergabung bersama Keluarga Kamirawat. Tahap Selanjutnya akan kami berikan undangan interview oleh tim kamirawat. Sesi interview tersebut untuk membicarakan Perjanjian Kontrak Kerjasama sebagai Mitra dan Skema Tarif yang dapat Kamirawat tawarkan.')
            ->line('Seluruh tim **KamiRawat** tidak sabar menunggu untuk segera berkolaborasi bersama Anda.')
            ->line('Mimpi Kamirawat adalah dapat memberikan akses layanan kesehatan yang setara dan berkualitas dari Sabang sampai Merauke. Mari berkembang bersama, memberikan akses layanan kesehatan yang paripurna untuk masa depan Indonesia yang lebih sehat dan bahagia.')
            ->line('Nantikan kabar dari kami selanjutnya terkait pendaftaran Anda sebagai Tenaga Medis di Kamirawat, atau hubungi Custumer Service Kamirawat di 081909201120 untuk informasi lebih lanjut.');
    }
}
