<?php

namespace App\Notifications;

use App\Models\Booking;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class BookingAcceptedNotification extends Notification implements ShouldQueue
{
    use Queueable;

    public Booking $booking;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Booking $booking)
    {
        $this->booking = $booking;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @return array
     */
    public function via(): array
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  User  $user
     * @return MailMessage
     */
    public function toMail(User $user): MailMessage
    {
        $booking = $this->booking;

        return (new MailMessage)
            ->subject('Pesanan Diterima Tenaga Medis - ' . $booking->booking_number)
            ->line('Pesanan anda dengan nomor booking ' . $booking->booking_number . ' sudah terima oleh tenaga medis. Anda bisa melihat status pesanan di akun anda. Klik link dibawah ini untuk melihat pesanan:')
            ->action('Lihat pesanan', route('front.patient.booking.history'))
            ->line('Hubungi kami apabila anda memiliki pertanyaan!');
    }
}
