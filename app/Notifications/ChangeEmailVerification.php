<?php

namespace App\Notifications;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ChangeEmailVerification extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @return array
     */
    public function via(): array
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  User  $user
     * @return MailMessage
     */
    public function toMail(User $user): MailMessage
    {
        $url = route('front.confirm-change-email', ['token' => $user->token_new_email]);

        return (new MailMessage)
                    ->line('Anda melakukan pemintaan perubahaan email, silahkan klik link dibawah ini untuk mengkonfirmasi perubahan email.')
                    ->action('Confirm New Email', $url)
                    ->line('Abaikan apabila anda tidak melakukannya!');
    }
}
