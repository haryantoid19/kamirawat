<?php

namespace App\Notifications;

use App\Models\Booking;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Notification;

class NewBookingToPatient extends Notification implements ShouldQueue
{
    use Queueable;

    public Booking $booking;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Booking $booking)
    {
        $this->booking = $booking;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @return array
     */
    public function via(): array
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param User $user
     * @return \App\Mail\NewBookingToPatient
     */
    public function toMail(User $user): \App\Mail\NewBookingToPatient
    {
        $booking = $this->booking;
        $booking->load(['medical_service', 'medical_service.category']);
        $subject = sprintf('Pesanan Baru - %s #%s', $booking->medical_service->category->name, $booking->booking_number);

        return (new \App\Mail\NewBookingToPatient($booking))
            ->to($user->email)
            ->subject($subject);
    }
}
