<?php

namespace App\Listeners;

use App\Events\BookingAccepted;
use App\Notifications\BookingAcceptedNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendNotificationBookingAccepted implements ShouldQueue
{
    use InteractsWithQueue;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param BookingAccepted $bookingAccepted
     * @return void
     */
    public function handle(BookingAccepted $bookingAccepted)
    {
        $booking = $bookingAccepted->booking;
        $booking->load(['patient', 'patient.user']);
        $booking->patient->user->notify(new BookingAcceptedNotification($booking));
    }
}
