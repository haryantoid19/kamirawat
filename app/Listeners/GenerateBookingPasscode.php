<?php

namespace App\Listeners;

use App\Events\BookingPaid;

class GenerateBookingPasscode
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BookingPaid  $event
     * @return void
     */
    public function handle(BookingPaid $event)
    {
        $booking = $event->booking;
        $booking->passcode = mt_rand(100000, 999999);
        $booking->saveQuietly();
    }
}
