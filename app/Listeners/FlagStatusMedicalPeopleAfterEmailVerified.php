<?php

namespace App\Listeners;

use App\Enums\MedicalPeopleStatus;
use App\Enums\UserRole;
use App\Models\User;
use Illuminate\Auth\Events\Verified;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class FlagStatusMedicalPeopleAfterEmailVerified implements ShouldQueue
{
    use InteractsWithQueue;

    /**
     * Handle the event.
     *
     * @param  Verified  $event
     * @return void
     */
    public function handle(Verified $event)
    {
        /** @var User $user */
        $user = $event->user;

        if ($user->hasRole(UserRole::MEDICAL_PERSON()->value)) {
            $medical_people = $user->medical_people;
            $medical_people->status = MedicalPeopleStatus::PENDING();
            $medical_people->save();
        }
    }
}
