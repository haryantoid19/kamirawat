<?php

namespace App\Listeners;

use App\Models\User;
use Illuminate\Auth\Events\Login;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Http\Request;

class LogSuccessfulLogin implements ShouldQueue
{

    public ?string $last_ip_address;

    public function __construct(Request $request)
    {
        $this->last_ip_address = $request->ip();
    }

    /**
     * Handle the event.
     *
     * @param  Login  $event
     * @return void
     */
    public function handle(Login $event)
    {
        /** @var User $user */
        $user = $event->user;

        $user->last_login_at = now();
        $user->last_ip_address = $this->last_ip_address;
        $user->save();
    }
}
