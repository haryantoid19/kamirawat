<?php

namespace App\Listeners;

use App\Events\BookingCreated;
use App\Notifications\NewBookingToPatient;

class SendNotificationNewBookingToPatient
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param BookingCreated $bookingCreated
     * @return void
     */
    public function handle(BookingCreated $bookingCreated)
    {
        $booking = $bookingCreated->booking;
        $booking->load(['patient', 'patient.user']);
        $booking->patient->user->notify(new NewBookingToPatient($booking));
    }
}
