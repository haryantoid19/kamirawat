<?php

namespace App\Listeners;

use App\Enums\UserRole;
use App\Events\BookingCreated;
use App\Models\User;
use App\Notifications\NewBookingToAdmin;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class SendNotificationNewBookingToAdmin implements ShouldQueue
{
    use InteractsWithQueue;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param BookingCreated $bookingCreated
     * @return void
     */
    public function handle(BookingCreated $bookingCreated)
    {
        $booking = $bookingCreated->booking;
        $admins = User::role([UserRole::ADMIN()->value], 'theteam')->get();
        foreach ($admins as $admin) {
            $admin->notify(new NewBookingToAdmin($booking));
        }

    }
}
