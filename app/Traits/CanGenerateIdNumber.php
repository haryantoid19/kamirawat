<?php

namespace App\Traits;

use Exception;

trait CanGenerateIdNumber
{
    /**
     * @throws Exception
     */
    public static function generateIdNumber(): string
    {
        $model_class = static::class;
        $prefix = config('kamirawat.prefix');

        if (! array_key_exists($model_class, $prefix)) {
            throw new Exception("Prefix unknown. Please set prefix to model " . static::class . ".");
        }

        if (self::$id_number_column === "") {
            throw new Exception("Invalid column name. Please set prefix to model " . static::class . ".");
        }

        $maxSequence = 4;
        $idNumber = null;
        $attempt = 5;
        $now = now();
        $prefix = $prefix[$model_class] . $now->format('y') . $now->format('m');
        while($idNumber === null && $attempt > 0) {
            $latest = self::latest('created_at')->first();
            $last = 1;
            if ($latest) {
                $parse = explode("-", $latest->{self::$id_number_column});
                $last = (int) $parse[1] + 1;
            }

            $last = str_pad($last, $maxSequence, 0, STR_PAD_LEFT);
            $candidate = implode('-', [$prefix, $last]);
            $exists = self::where(self::$id_number_column, $candidate)->exists();
            if (! $exists) {
                $idNumber = $candidate;
            }

            $attempt--;
        }

        if (! $idNumber) {
            throw new Exception("ID number cannot generated for model " . static::class . ". Please try again!");
        }

        return $idNumber;
    }
}
