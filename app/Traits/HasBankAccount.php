<?php

namespace App\Traits;

use App\Models\BankAccount;
use Illuminate\Database\Eloquent\Relations\MorphOne;

trait HasBankAccount
{
    /**
     * Account Bank
     *
     * @return MorphOne
     */
    public function bank_account(): MorphOne
    {
        return $this->morphOne(BankAccount::class, 'bank_accountable');
    }
}
