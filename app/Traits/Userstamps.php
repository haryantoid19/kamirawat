<?php

namespace App\Traits;

use App\Scopes\UserstampsScope;
use Auth;
use Barryvdh\LaravelIdeHelper\Eloquent;
use Illuminate\Database\Eloquent\Concerns\HasEvents;
use Illuminate\Database\Eloquent\Concerns\HasGlobalScopes;
use Illuminate\Database\Eloquent\Concerns\HasRelationships;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Trait Userstamps
 *
 * @mixin Eloquent
 * @mixin HasRelationships
 * @mixin HasGlobalScopes
 * @mixin HasEvents
 */
trait Userstamps
{
    /**
     * Whether we're currently maintain userstamps.
     *
     * @param bool
     */
    protected bool $userstamping = true;

    /**
     * Boot the userstamps trait for a model.
     *
     * @return void
     */
    public static function bootUserstamps()
    {
        static::addGlobalScope(new UserstampsScope);

        static::registerListeners();
    }

    /**
     * Register events we need to listen for.
     *
     * @return void
     */
    public static function registerListeners()
    {
        static::creating(function($model) {
            if (! $model->isUserstamping()) {
                return;
            }

            if (is_null($model->{$model->getCreatedByColumn()})) {
                $model->{$model->getCreatedByColumn()} = Auth::id();
            }

            if (is_null($model->{$model->getUpdatedByColumn()}) && ! is_null($model->getUpdatedByColumn())) {
                $model->{$model->getUpdatedByColumn()} = Auth::id();
            }
        });

        static::updating(function($model) {
            if (! $model->isUserstamping() || is_null(Auth::id())) {
                return;
            }
            $model->{$model->getUpdatedByColumn()} = Auth::id();
        });
    }

    /**
     * Get the user that created the model.
     */
    public function creator(): BelongsTo
    {
        return $this->belongsTo($this->getUserClass(), $this->getCreatedByColumn());
    }

    /**
     * Get the user that edited the model.
     */
    public function editor(): BelongsTo
    {
        return $this->belongsTo($this->getUserClass(), $this->getUpdatedByColumn());
    }

    /**
     * Get the name of the "created by" column.
     *
     * @return string
     */
    public function getCreatedByColumn(): string
    {
        return defined('static::CREATED_BY') && ! is_null(static::CREATED_BY) ? static::CREATED_BY : 'created_by';
    }

    /**
     * Get the name of the "updated by" column.
     *
     * @return string
     */
    public function getUpdatedByColumn(): string
    {
        return defined('static::UPDATED_BY') && ! is_null(static::UPDATED_BY) ? static::UPDATED_BY : 'updated_by';
    }

    /**
     * Check if we're maintain Userstamps on the model.
     *
     * @return bool
     */
    public function isUserstamping(): bool
    {
        return $this->userstamping;
    }

    /**
     * Stop maintaining Userstamps on the model.
     *
     * @return void
     */
    public function stopUserstamping()
    {
        $this->userstamping = false;
    }

    /**
     * Start maintaining Userstamps on the model.
     *
     * @return void
     */
    public function startUserstamping()
    {
        $this->userstamping = true;
    }

    /**
     * Get the class being used to provide a User.
     *
     * @return string
     */
    protected function getUserClass(): string
    {
        return 'App\\Models\\User';
    }
}
