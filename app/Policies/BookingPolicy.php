<?php

namespace App\Policies;

use App\Enums\BookingStatus;
use App\Models\Booking;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class BookingPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param User $user
     * @return bool
     */
    public function viewAny(User $user): bool
    {
        return $user->hasPermissionTo('browse bookings');
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param User $user
     * @param Booking $booking
     * @return bool
     */
    public function view(User $user, Booking $booking): bool
    {
        // For patient
        if ($user->isPatient()) {
            return $booking->patient->is($user->patient);
        }

        // For Medical People
        if ($user->isPartner()) {
            return $booking->input_passcode_at !== NULL && $booking->medical_people->is($user->medical_people);
        }

        // For Admin
        return $user->hasPermissionTo('view bookings');
    }

    /**
     * Determine whether the user can input passcode the model.
     *
     * @param User $user
     * @param Booking $booking
     * @return bool
     */
    public function input_passcode(User $user, Booking $booking): bool
    {
        return $user->isPartner() && $booking->medical_people->is($user->medical_people);
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param User $user
     * @param Booking $booking
     * @return bool
     */
    public function update(User $user, Booking $booking): bool
    {
        return $user->hasPermissionTo('update bookings');
    }

    /**
     * @param User $user
     * @param Booking $booking
     * @return bool
     */
    public function cancel(User $user, Booking $booking): bool
    {
        return $user->hasPermissionTo('cancel bookings') && $booking->status->is(BookingStatus::WAITING_PAYMENT());
    }

    /**
     * @param User $user
     * @param Booking $booking
     * @return bool
     */
    public function assign_medical_people(User $user, Booking $booking): bool
    {
        return $user->hasPermissionTo('assign bookings to medical people') && $booking->status->in([BookingStatus::PAID(), BookingStatus::ASSIGNED()]);
    }

    /**
     * @param User $user
     * @param Booking $booking
     * @return bool
     */
    public function mark_as_paid(User $user, Booking $booking): bool
    {
        return $user->hasPermissionTo('mark as paid bookings') && $booking->status->is(BookingStatus::WAITING_PAYMENT());
    }
}
