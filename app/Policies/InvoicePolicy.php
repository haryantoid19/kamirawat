<?php

namespace App\Policies;

use App\Enums\InvoiceStatus;
use App\Models\Invoice;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class InvoicePolicy
{
    use HandlesAuthorization;

    /**
     * @param User $user
     * @param Invoice $invoice
     * @return bool
     */
    public function mark_as_paid(User $user, Invoice $invoice): bool
    {
        return $user->hasPermissionTo('mark as paid invoices') && $invoice->status->is(InvoiceStatus::ACTIVE());
    }
}
