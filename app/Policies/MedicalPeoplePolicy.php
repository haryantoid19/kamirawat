<?php

namespace App\Policies;

use App\Enums\MedicalPeopleStatus;
use App\Models\MedicalPeople;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class MedicalPeoplePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view any models.
     *
     * @param User $user
     * @return bool
     */
    public function viewAny(User $user): bool
    {
        return $user->hasPermissionTo('browse medical people');
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param User $user
     * @param MedicalPeople $medicalPeople
     * @return bool
     */
    public function view(User $user, MedicalPeople $medicalPeople): bool
    {
        return $user->hasPermissionTo('view medical people') || $user->id === $medicalPeople->user_id;
    }

    /**
     * Determine whether the user can create models.
     *
     * @param User $user
     * @param MedicalPeople $medicalPeople
     * @return bool
     */
    public function approval(User $user, MedicalPeople $medicalPeople): bool
    {
        return $user->hasPermissionTo('approval medical people') && $medicalPeople->status->is(MedicalPeopleStatus::PENDING());
    }
}
