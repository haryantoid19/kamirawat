<?php

namespace Database\Seeders;

use App\Models\Bank;
use Illuminate\Database\Seeder;

class BanksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $banks = [
            0 => [
                'name' => 'BANK BRI',
                'code' => '002',
            ],
            1 => [
                'name' => 'BANK EKSPOR INDONESIA',
                'code' => '003',
            ],
            2 => [
                'name' => 'BANK MANDIRI',
                'code' => '008',
            ],
            3 => [
                'name' => 'BANK BNI',
                'code' => '009',
            ],
            4 => [
                'name' => 'BANK BNI SYARIAH',
                'code' => '427',
            ],
            5 => [
                'name' => 'BANK DANAMON',
                'code' => '011',
            ],
            6 => [
                'name' => 'PERMATA BANK',
                'code' => '013',
            ],
            7 => [
                'name' => 'BANK BCA',
                'code' => '014',
            ],
            8 => [
                'name' => 'BANK BII',
                'code' => '016',
            ],
            9 => [
                'name' => 'BANK PANIN',
                'code' => '019',
            ],
            10 => [
                'name' => 'BANK ARTA NIAGA KENCANA',
                'code' => '020',
            ],
            11 => [
                'name' => 'BANK NIAGA',
                'code' => '022',
            ],
            12 => [
                'name' => 'BANK BUANA IND',
                'code' => '023',
            ],
            13 => [
                'name' => 'BANK LIPPO',
                'code' => '026',
            ],
            14 => [
                'name' => 'BANK NISP',
                'code' => '028',
            ],
            15 => [
                'name' => 'AMERICAN EXPRESS BANK LTD',
                'code' => '030',
            ],
            16 => [
                'name' => 'CITIBANK N.A.',
                'code' => '031',
            ],
            17 => [
                'name' => 'JP. MORGAN CHASE BANK, N.A.',
                'code' => '032',
            ],
            18 => [
                'name' => 'BANK OF AMERICA, N.A',
                'code' => '033',
            ],
            19 => [
                'name' => 'ING INDONESIA BANK',
                'code' => '034',
            ],
            20 => [
                'name' => 'BANK MULTICOR TBK.',
                'code' => '036',
            ],
            21 => [
                'name' => 'BANK ARTHA GRAHA',
                'code' => '037',
            ],
            22 => [
                'name' => 'BANK CREDIT AGRICOLE INDOSUEZ',
                'code' => '039',
            ],
            23 => [
                'name' => 'THE BANGKOK BANK COMP. LTD',
                'code' => '040',
            ],
            24 => [
                'name' => 'THE HONGKONG & SHANGHAI B.C.',
                'code' => '041',
            ],
            25 => [
                'name' => 'THE BANK OF TOKYO MITSUBISHI UFJ LTD',
                'code' => '042',
            ],
            26 => [
                'name' => 'BANK SUMITOMO MITSUI INDONESIA',
                'code' => '045',
            ],
            27 => [
                'name' => 'BANK DBS INDONESIA',
                'code' => '046',
            ],
            28 => [
                'name' => 'BANK RESONA PERDANIA',
                'code' => '047',
            ],
            29 => [
                'name' => 'BANK MIZUHO INDONESIA',
                'code' => '048',
            ],
            30 => [
                'name' => 'STANDARD CHARTERED BANK',
                'code' => '050',
            ],
            31 => [
                'name' => 'BANK ABN AMRO',
                'code' => '052',
            ],
            32 => [
                'name' => 'BANK KEPPEL TATLEE BUANA',
                'code' => '053',
            ],
            33 => [
                'name' => 'BANK CAPITAL INDONESIA, TBK.',
                'code' => '054',
            ],
            34 => [
                'name' => 'BANK BNP PARIBAS INDONESIA',
                'code' => '057',
            ],
            35 => [
                'name' => 'BANK UOB INDONESIA',
                'code' => '058',
            ],
            36 => [
                'name' => 'KOREA EXCHANGE BANK DANAMON',
                'code' => '059',
            ],
            37 => [
                'name' => 'RABOBANK INTERNASIONAL INDONESIA',
                'code' => '060',
            ],
            38 => [
                'name' => 'ANZ PANIN BANK',
                'code' => '061',
            ],
            39 => [
                'name' => 'DEUTSCHE BANK AG.',
                'code' => '067',
            ],
            40 => [
                'name' => 'BANK WOORI INDONESIA',
                'code' => '068',
            ],
            41 => [
                'name' => 'BANK OF CHINA LIMITED',
                'code' => '069',
            ],
            42 => [
                'name' => 'BANK BUMI ARTA',
                'code' => '076',
            ],
            43 => [
                'name' => 'BANK EKONOMI',
                'code' => '087',
            ],
            44 => [
                'name' => 'BANK ANTARDAERAH',
                'code' => '088',
            ],
            45 => [
                'name' => 'BANK HAGA',
                'code' => '089',
            ],
            46 => [
                'name' => 'BANK IFI',
                'code' => '093',
            ],
            47 => [
                'name' => 'BANK CENTURY, TBK.',
                'code' => '095',
            ],
            48 => [
                'name' => 'BANK MAYAPADA',
                'code' => '097',
            ],
            49 => [
                'name' => 'BANK JABAR',
                'code' => '110',
            ],
            50 => [
                'name' => 'BANK DKI',
                'code' => '111',
            ],
            51 => [
                'name' => 'BPD DIY',
                'code' => '112',
            ],
            52 => [
                'name' => 'BANK JATENG',
                'code' => '113',
            ],
            53 => [
                'name' => 'BANK JATIM',
                'code' => '114',
            ],
            54 => [
                'name' => 'BPD JAMBI',
                'code' => '115',
            ],
            55 => [
                'name' => 'BPD ACEH',
                'code' => '116',
            ],
            56 => [
                'name' => 'BANK SUMUT',
                'code' => '117',
            ],
            57 => [
                'name' => 'BANK NAGARI',
                'code' => '118',
            ],
            58 => [
                'name' => 'BANK RIAU',
                'code' => '119',
            ],
            59 => [
                'name' => 'BANK SUMSEL',
                'code' => '120',
            ],
            60 => [
                'name' => 'BANK LAMPUNG',
                'code' => '121',
            ],
            61 => [
                'name' => 'BPD KALSEL',
                'code' => '122',
            ],
            62 => [
                'name' => 'BPD KALIMANTAN BARAT',
                'code' => '123',
            ],
            63 => [
                'name' => 'BPD KALTIM',
                'code' => '124',
            ],
            64 => [
                'name' => 'BPD KALTENG',
                'code' => '125',
            ],
            65 => [
                'name' => 'BPD SULSEL',
                'code' => '126',
            ],
            66 => [
                'name' => 'BANK SULUT',
                'code' => '127',
            ],
            67 => [
                'name' => 'BPD NTB',
                'code' => '128',
            ],
            68 => [
                'name' => 'BPD BALI',
                'code' => '129',
            ],
            69 => [
                'name' => 'BANK NTT',
                'code' => '130',
            ],
            70 => [
                'name' => 'BANK MALUKU',
                'code' => '131',
            ],
            71 => [
                'name' => 'BPD PAPUA',
                'code' => '132',
            ],
            72 => [
                'name' => 'BANK BENGKULU',
                'code' => '133',
            ],
            73 => [
                'name' => 'BPD SULAWESI TENGAH',
                'code' => '134',
            ],
            74 => [
                'name' => 'BANK SULTRA',
                'code' => '135',
            ],
            75 => [
                'name' => 'BANK NUSANTARA PARAHYANGAN',
                'code' => '145',
            ],
            76 => [
                'name' => 'BANK SWADESI',
                'code' => '146',
            ],
            77 => [
                'name' => 'BANK MUAMALAT',
                'code' => '147',
            ],
            78 => [
                'name' => 'BANK MESTIKA',
                'code' => '151',
            ],
            79 => [
                'name' => 'BANK METRO EXPRESS',
                'code' => '152',
            ],
            80 => [
                'name' => 'BANK SHINTA INDONESIA',
                'code' => '153',
            ],
            81 => [
                'name' => 'BANK MASPION',
                'code' => '157',
            ],
            82 => [
                'name' => 'BANK HAGAKITA',
                'code' => '159',
            ],
            83 => [
                'name' => 'BANK GANESHA',
                'code' => '161',
            ],
            84 => [
                'name' => 'BANK WINDU KENTJANA',
                'code' => '162',
            ],
            85 => [
                'name' => 'HALIM INDONESIA BANK',
                'code' => '164',
            ],
            86 => [
                'name' => 'BANK HARMONI INTERNATIONAL',
                'code' => '166',
            ],
            87 => [
                'name' => 'BANK KESAWAN',
                'code' => '167',
            ],
            88 => [
                'name' => 'BANK TABUNGAN NEGARA (PERSERO)',
                'code' => '200',
            ],
            89 => [
                'name' => 'BANK HIMPUNAN SAUDARA 1906, TBK .',
                'code' => '212',
            ],
            90 => [
                'name' => 'BANK TABUNGAN PENSIUNAN NASIONAL',
                'code' => '213',
            ],
            91 => [
                'name' => 'BANK SWAGUNA',
                'code' => '405',
            ],
            92 => [
                'name' => 'BANK JASA ARTA',
                'code' => '422',
            ],
            93 => [
                'name' => 'BANK MEGA',
                'code' => '426',
            ],
            94 => [
                'name' => 'BANK JASA JAKARTA',
                'code' => '427',
            ],
            95 => [
                'name' => 'BANK BUKOPIN',
                'code' => '441',
            ],
            96 => [
                'name' => 'BANK SYARIAH MANDIRI',
                'code' => '451',
            ],
            97 => [
                'name' => 'BANK BISNIS INTERNASIONAL',
                'code' => '459',
            ],
            98 => [
                'name' => 'BANK SRI PARTHA',
                'code' => '466',
            ],
            99 => [
                'name' => 'BANK JASA JAKARTA',
                'code' => '472',
            ],
            100 => [
                'name' => 'BANK BINTANG MANUNGGAL',
                'code' => '484',
            ],
            101 => [
                'name' => 'BANK BUMIPUTERA',
                'code' => '485',
            ],
            102 => [
                'name' => 'BANK YUDHA BHAKTI',
                'code' => '490',
            ],
            103 => [
                'name' => 'BANK MITRANIAGA',
                'code' => '491',
            ],
            104 => [
                'name' => 'BANK AGRO NIAGA',
                'code' => '494',
            ],
            105 => [
                'name' => 'BANK INDOMONEX',
                'code' => '498',
            ],
            106 => [
                'name' => 'BANK ROYAL INDONESIA',
                'code' => '501',
            ],
            107 => [
                'name' => 'BANK ALFINDO',
                'code' => '503',
            ],
            108 => [
                'name' => 'BANK SYARIAH MEGA',
                'code' => '506',
            ],
            109 => [
                'name' => 'BANK INA PERDANA',
                'code' => '513',
            ],
            110 => [
                'name' => 'BANK HARFA',
                'code' => '517',
            ],
            111 => [
                'name' => 'PRIMA MASTER BANK',
                'code' => '520',
            ],
            112 => [
                'name' => 'BANK PERSYARIKATAN INDONESIA',
                'code' => '521',
            ],
            113 => [
                'name' => 'BANK AKITA',
                'code' => '525',
            ],
            114 => [
                'name' => 'LIMAN INTERNATIONAL BANK',
                'code' => '526',
            ],
            115 => [
                'name' => 'ANGLOMAS INTERNASIONAL BANK',
                'code' => '531',
            ],
            116 => [
                'name' => 'BANK DIPO INTERNATIONAL',
                'code' => '523',
            ],
            117 => [
                'name' => 'BANK KESEJAHTERAAN EKONOMI',
                'code' => '535',
            ],
            118 => [
                'name' => 'BANK UIB',
                'code' => '536',
            ],
            119 => [
                'name' => 'BANK ARTOS IND',
                'code' => '542',
            ],
            120 => [
                'name' => 'BANK PURBA DANARTA',
                'code' => '547',
            ],
            121 => [
                'name' => 'BANK MULTI ARTA SENTOSA',
                'code' => '548',
            ],
            122 => [
                'name' => 'BANK MAYORA',
                'code' => '553',
            ],
            123 => [
                'name' => 'BANK INDEX SELINDO',
                'code' => '555',
            ],
            124 => [
                'name' => 'BANK VICTORIA INTERNATIONAL',
                'code' => '566',
            ],
            125 => [
                'name' => 'BANK EKSEKUTIF',
                'code' => '558',
            ],
            126 => [
                'name' => 'CENTRATAMA NASIONAL BANK',
                'code' => '559',
            ],
            127 => [
                'name' => 'BANK FAMA INTERNASIONAL',
                'code' => '562',
            ],
            128 => [
                'name' => 'BANK SINAR HARAPAN BALI',
                'code' => '564',
            ],
            129 => [
                'name' => 'BANK HARDA',
                'code' => '567',
            ],
            130 => [
                'name' => 'BANK FINCONESIA',
                'code' => '945',
            ],
            131 => [
                'name' => 'BANK MERINCORP',
                'code' => '946',
            ],
            132 => [
                'name' => 'BANK MAYBANK INDOCORP',
                'code' => '947',
            ],
            133 => [
                'name' => 'BANK OCBC – INDONESIA',
                'code' => '948',
            ],
            134 => [
                'name' => 'BANK CHINA TRUST INDONESIA',
                'code' => '949',
            ],
            135 => [
                'name' => 'BANK COMMONWEALTH',
                'code' => '950',
            ],
            136 => [
                'name' => 'BANK BJB SYARIAH',
                'code' => '425',
            ],
            137 => [
                'name' => 'BPR KS (KARYAJATNIKA SEDAYA)',
                'code' => '688',
            ],
            138 => [
                'name' => 'INDOSAT DOMPETKU',
                'code' => '789',
            ],
            139 => [
                'name' => 'TELKOMSEL TCASH',
                'code' => '911',
            ],
            140 => [
                'name' => 'LINKAJA',
                'code' => '911',
            ],
        ];

        foreach ($banks as $bank) {
            Bank::updateOrCreate([
                'code' => $bank['code']
            ], $bank);
        }
    }
}
