<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RolesAndPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        $permission_data = [
            'theteam' => [
                'browse users',
                'view users',
                'edit users',
                'delete users',
                'restore users',
                'force delete users',

                'browse bookings',
                'view bookings',
                'cancel bookings',
                'update bookings',
                'assign bookings to medical people',

                'mark as paid invoices',

                'browse medical people',
                'view medical people',
                'approval medical people',
            ],
        ];

        foreach ($permission_data as $guard => $permissions) {
            foreach ($permissions as $permission) {
                Permission::firstOrCreate(['name' => $permission, 'guard_name' => $guard]);
            }
        }

        $privileges = [
            'ADMIN' => [
                'theteam' => [
                    'browse users',
                    'view users',
                    'edit users',
                    'delete users',
                    'restore users',
                    'force delete users',

                    'browse bookings',
                    'view bookings',
                    'cancel bookings',
                    'update bookings',
                    'assign bookings to medical people',

                    'mark as paid invoices',

                    'browse medical people',
                    'view medical people',
                    'approval medical people',
                ],
            ],
        ];

        foreach ($privileges as $role_name => $guards) {
            foreach ($guards as $guard_name => $permissions) {
                $role = Role::firstOrCreate(['name' => $role_name, 'guard_name' => $guard_name]);
                foreach ($permissions as $permission) {
                    $role->givePermissionTo($permission);
                }
            }
        }
    }
}
