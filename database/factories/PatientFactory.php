<?php

namespace Database\Factories;

use App\Enums\Gender;
use App\Enums\IdType;
use App\Models\Patient;
use App\Models\PostalCode;
use App\Models\User;
use Exception;
use Hash;
use Illuminate\Database\Eloquent\Factories\Factory;

class PatientFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Patient::class;

    /**
     * Define the model's default state.
     *
     * @return array
     * @throws Exception
     */
    public function definition(): array
    {
        $name = $this->faker->name();
        $email = $this->faker->email();

        $user = User::create([
            'name' => $name,
            'email' => $email,
            'password' => Hash::make('password'),
        ]);
        $user->assignRole('patient');

        return [
            'user_id' => $user->id,
            'name' => $name,
            'patient_number' => Patient::generateIdNumber(),
            'id_type' => IdType::getRandomValue(),
            'id_number' => $this->faker->randomDigit(),
            'gender' => Gender::getRandomValue(),
            'nationality' => $this->faker->randomElement(array_values(config('countries'))),
            'postal_code_id' => PostalCode::inRandomOrder()->first(),
            'bod' => $this->faker->date(),
            'phone_number' => $this->faker->phoneNumber(),
            'address' => $this->faker->address(),
            'latitude' => $this->faker->latitude(),
            'longitude' => $this->faker->longitude(),
        ];
    }
}
