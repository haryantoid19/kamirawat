<?php

namespace Database\Factories;

use App\Enums\Gender;
use App\Enums\IdType;
use App\Models\PatientUser;
use App\Models\PostalCode;
use Illuminate\Database\Eloquent\Factories\Factory;

class PatientUserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = PatientUser::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'name' => $this->faker->name(),
            'email' => $this->faker->email(),
            'password' => \Hash::make('password'),
            'id_type' => IdType::getRandomValue(),
            'id_number' => $this->faker->randomDigit(),
            'gender' => Gender::getRandomValue(),
            'nationality' => $this->faker->randomElement(array_keys(config('countries'))),
            'postal_code_id' => PostalCode::inRandomOrder()->first(),
            'bod' => $this->faker->date,
            'phone_number' => $this->faker->phoneNumber,
            'address' => $this->faker->address,
        ];
    }
}
