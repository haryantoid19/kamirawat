<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('booking_id');
            $table->string('invoice_number');
            $table->float('total', 12);
            $table->string('payment_method');
            $table->string('status', 40);
            $table->string('xendit_id')->nullable();
            $table->string('xendit_type', 40)->nullable();
            $table->text('xendit_invoice_url')->nullable();
            $table->string('xendit_va_number')->nullable();
            $table->float('shipping_fee', 12)->nullable();
            $table->boolean('is_main')->default(0);
            $table->timestamp('paid_at')->nullable();
            $table->timestamp('expired_at')->nullable();
            $table->timestamps();
        });

        Schema::table('invoices', function (Blueprint $table) {
            $table->foreign('booking_id')->references('id')->on('bookings')->restrictOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
