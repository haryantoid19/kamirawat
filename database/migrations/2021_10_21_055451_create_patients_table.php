<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePatientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patients', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->string('name');
            $table->string('patient_number')->unique();
            $table->string('id_type', 40)->nullable();
            $table->string('id_number', 40)->nullable();
            $table->string('gender', 10);
            $table->string('nationality')->nullable();
            $table->integer('postal_code_id')->nullable();
            $table->string('rt', 5)->nullable();
            $table->string('rw', 5)->nullable();
            $table->date('bod');
            $table->string('phone_number', 40);
            $table->string('address')->nullable();
            $table->string('latitude')->nullable();
            $table->String('longitude')->nullable();
            $table->text('avatar_path')->nullable();
            $table->boolean('is_main')->default(0);
            $table->timestamps();
        });

        Schema::table('patients', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')->restrictOnDelete();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patients');
    }
}
