<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClinicBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clinic_bookings', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('patient_id');
            $table->unsignedBigInteger('medical_service_id');
            $table->string('number');
            $table->string('status');
            $table->string('name');
            $table->string('phone_number');
            $table->date('bod');
            $table->string('gender');
            $table->text('history_of_disease_allergies')->nullable();
            $table->text('main_symptom')->nullable();
            $table->text('special_request')->nullable();
            $table->text('address');
            $table->boolean('is_self')->default(0);
            $table->timestamps();
        });

        Schema::table('clinic_bookings', function (Blueprint $table) {
            $table->foreign('patient_id')->references('id')->on('patients')->restrictOnDelete();
            $table->foreign('medical_service_id')->references('id')->on('medical_services')->restrictOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clinic_bookings');
    }
}
