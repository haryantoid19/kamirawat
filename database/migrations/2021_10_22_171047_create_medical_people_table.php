<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMedicalPeopleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medical_people', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('profession_id');
            $table->string('medical_number');
            $table->string('name');
            $table->string('id_type', 40);
            $table->string('id_number', 40);
            $table->string('phone_number', 40);
            $table->bigInteger('postal_code_id');
            $table->string('rt', 5)->nullable();
            $table->string('rw', 5)->nullable();
            $table->string('address')->nullable();
            $table->string('status', 40);
            $table->date('bod');
            $table->string('gender', 40);
            $table->string('nationality')->nullable();
            $table->text('avatar_path')->nullable();
            $table->unsignedBigInteger('accepted_by')->nullable();
            $table->unsignedBigInteger('rejected_by')->nullable();
            $table->unsignedBigInteger('deleted_by')->nullable();
            $table->timestamp('accepted_at')->nullable();
            $table->timestamp('rejected_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
        });

        Schema::table('medical_people', function (Blueprint $table) {
            $table->foreign('accepted_by')->references('id')->on('users')->nullOnDelete();
            $table->foreign('rejected_by')->references('id')->on('users')->nullOnDelete();
            $table->foreign('deleted_by')->references('id')->on('users')->nullOnDelete();
            $table->foreign('user_id')->references('id')->on('users')->restrictOnDelete();
            $table->foreign('profession_id')->references('id')->on('professions')->restrictOnDelete();
            $table->foreign('postal_code_id')->references('id')->on('postal_codes')->restrictOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medical_people');
    }
}
