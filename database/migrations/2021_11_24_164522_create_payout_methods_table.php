<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePayoutMethodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payout_methods', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('bank_id');
            $table->string('name');
            $table->string('number');
            $table->boolean('is_primary')->nullable()->default(0);
            $table->morphs('payout_methodable');
            $table->timestamps();
        });

        Schema::table('payout_methods', function (Blueprint $table) {
            $table->foreign('bank_id')->references('id')->on('banks')->restrictOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payout_methods');
    }
}
